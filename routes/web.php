<?php

// Sample: just change later
// Route::get('/', 'CatalogController@index');

// Sample Grouping Structure
// Route::group(['namespace' => 'Company', 'prefix' => 'c', 'middleware' => ['auth']], function() {
// 	Route::get('{id}/link/{employee_id?}', 'SampleController@getLink');
// 	Route::get('{id}/image/{employee_id?}', 'SampleController@getImage');
// 	Route::get('{id}/install', 'SampleController@getInstall');
// 	Route::get('{id}/instructions', 'SampleController@getInstructions');
// });


// Route::group(['middleware' => 'setcache'], function () {
// });
	




	// Redirects
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	// Supplier evaluation
	Route::get('/supplier-evaluation.html',function(Illuminate\Http\Request $rqst){

		$param_ar =  explode('?', $rqst->fullUrl());
		if(count($param_ar)==2) {
			$param = "?".$param_ar[1];
		} else {
			$param = "";
		}

		return redirect('https://manage.almani.ae/supplier-evaluation.html'.$param);
	});


	Route::get('/supplier-evaluation',function(Illuminate\Http\Request $rqst){

		$param_ar =  explode('?', $rqst->fullUrl());
		if(count($param_ar)==2) {
			$param = "?".$param_ar[1];
		} else {
			$param = "";
		}

		return redirect('https://manage.almani.ae/supplier-evaluation.html'.$param);
	});


	// feedback form
	Route::get('/feedback-form.html',function(Illuminate\Http\Request $rqst){

		$param_ar =  explode('?', $rqst->fullUrl());
		if(count($param_ar)==2) {
			$param = "?".$param_ar[1];
		} else {
			$param = "";
		}

		return redirect('https://manage.almani.ae/feedback-form.html'.$param);
	});


	Route::get('/catalog/{category_alias}/products.html',function($category_alias){
		return redirect(null,301)->route('catalog_product_page',['category_alias'=>$category_alias]);
	});
	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	// Redirects


	// HTML Menus
	////////////////////////////////////////////////////////////////////////////////////
	// for generation of static menu, fix the performance issue

	Route::get('/htmlmenu/create', 'MenuController@createHTMLMenus');

	Route::get('/htmlmenu/show/{menu}', 'MenuController@showHTMLMenu');


	Route::get('/htmlmenu/showsub/allmenu', function(){
		
		$agent = new Jenssegers\Agent\Agent;
		$isMobile = $agent->isMobile();
		$isTablet = $agent->isTablet();

        return view('sections.static-templates')->with('isMobile',$isMobile)->with('isTablet',$isTablet);

	})->name('showallhtmlsubmenu');

	Route::get('/htmlmenu/showsub/{submenu}', 'MenuController@showHTMLSubMenu')->name('showhtmlsubmenu');
	Route::get('/htmlmenu/showsub/mobile/{submenu}', 'MenuController@showMobileHTMLSubMenu')->name('showmobilehtmlsubmenu');

	Route::get('/htmlmenu/mobile/create', 'MenuController@createHTMLMenusforMobile');
	Route::get('/htmlmenu/mobile/{menu}', 'MenuController@showHTMLMenuforMobile');

	////////////////////////////////////////////////////////////////////////////////////
	// HTML Menus



	Route::get('/', 'HomeController@index')->name('home_page');
	Route::get('/gallery', 'HomeController@gallery')->name('gallery_page');

	Route::get('/blog/sitemap', 'BlogController@sitemap')->name('blog_sitemap_page');
	Route::get('/blog/sitemap.xml', 'BlogController@sitemap')->name('blog_sitemap_page');
	Route::get('/blog/{slug}', 'BlogController@blogPostPage')->name('blog_post_page');
	Route::get('/blog', 'BlogController@blogMainPage')->name('blog_page');



	// Get a Quote
	Route::get('/get-a-quote', 'GetQuoteController@index')->name('getaquote_page');
	Route::get('/get-a-quote/viewemail', 'GetQuoteController@viewEmail')->name('viewquote');
	Route::post('/get-a-quote/sendquote', 'GetQuoteController@sendQuote')->name('sendquote');

	// Contact us
	Route::get('/contact-us', 'ContactUsController@index')->name('contactus_page');
	Route::post('/contact-us/sendcontactdetails', 'ContactUsController@sendContactDetails')->name('sendcontactdetails');

	Route::post('/contact-us/sendhomedetails', 'ContactUsController@sendHomeDetails')->name('sendhomedetails');

	// Enquiry Product
	Route::post('/enquiry/message', 'EnquiryController@sendMessage')->name('sendmessage');
	Route::post('/enquiry/limitedoffer', 'EnquiryController@sendMessageLimitedOffer')->name('sendmessagelimitedoffer');
	
	Route::post('/enquiry/send', 'EnquiryController@sendEnquiry')->name('sendenquiry');
	Route::post('/enquiry/list/send', 'EnquiryController@sendListEnquiry')->name('sendlistenquiry');
	Route::post('/enquiry/category/send', 'EnquiryController@sendCategoryEnquiry')->name('sendcategoryenquiry');
	Route::post('/enquiry/requestdatasheet', 'EnquiryController@requestDatasheet')->name('requestdatasheet');
	Route::post('/enquiry/requesttechnicalfiles', 'EnquiryController@requestTechnicalFiles')->name('requesttechnicalfiles');
	Route::post('/enquiry/sendb2bportalenquiry', 'EnquiryController@sendB2BPortalEnquiry')->name('sendb2bportalenquiry');
	Route::post('/enquiry/senddownloads', 'EnquiryController@requestDownloads')->name('requestdownloads');




	Route::post('/enquiry/shareviaemail', 'EnquiryController@shareViaEmail')->name('shareviaemail');

	// Route::get('/qualitycontrol', 'HomeController@qualitycontrol')->name('qualitycontrol_page');

	// Route::get('/whatwedo', 'HomeController@whatwedo')->name('whatwedo_page');



	Route::get('/list', 'ListController@index')->name('list_page');



	// Search
	Route::get('/search', 'SearchController@index')->name('search_page');
	Route::post('/search', 'SearchController@searchNav')->name('search_nav_page');
	Route::post('/search/submit', 'SearchController@search')->name('submitsearch');



	// No Cache
	Route::get('/sitemapx','HomeController@sitemap')->name('sitemap_page');
	Route::get('/sitemap','HomeController@sitemap')->name('sitemap_page');
	Route::get('/sitemap.xml','HomeController@sitemap')->name('sitemap_page');




// Datasheet PDF
// for viewing test
//////////////////////////////////////////////////////
Route::get('/datasheet/view/{part}/{product_code}',function($part, $product_code){

	$product = App\Product\Product::where("product_code","=",$product_code)->first();
	$brand = "default";

	///////////////////////////////////////////
	$haystack_camel = ["cd","cc","ac",];
	$haystack_almani = ["ad","aa","ca",];
	$needle = "";
	if($product->camel==1)
	$needle = "c";
	else
	$needle = "a";
	if($brand=="default")
	$needle = $needle."d";
	elseif($brand=="camel")
	$needle = $needle."c";
	elseif($brand=="almani")
	$needle = $needle."a";

	$data['needle'] = $needle;
	$data['haystack_almani'] = $haystack_almani;
	$data['haystack_camel'] = $haystack_camel;
	///////////////////////////////////////////

	if($part == "body")
	$template = 'pdf.datasheets.template1';
	elseif($part == "cover")
	$template = 'pdf.datasheets.covers.template1';
	elseif($part == "footer")
	$template = 'pdf.datasheets.footer';
	elseif($part == "header")
	$template = 'pdf.datasheets.header';


	return view($template)
					->with('product',$product)
			        ->with('needle',$needle)
			        ->with('haystack_camel',$haystack_camel)
			        ->with('haystack_almani',$haystack_almani)
					->with('project_name','Elektra Lite Elektra Lite ildings and Warehousesildings and Warehouses sad  asdasd')
					->with('project_location','Sharjah residential buildings and Warehouses in Dubai, United Arab Emirates');
});




// Route::get('/datasheet/view/{section}/{product_id}',function($section,$product_id){
// 	$product = App\Product\Product::find($product_id);
// 	return view('pdf.datasheets.'.$section)->with('product',$product);
// });

//////////////////////////////////////////////////////
// for viewing test


Route::post('/datasheet/create/{product_code}', 'DatasheetController@createDatasheet')->name('create_datasheet');
// Uncomment if localhost
// Route::get('/datasheet/create/{product_code}', 'DatasheetController@createDatasheet')->name('create_datasheet');

// comment this on production
// Route::get('/datasheet/create/{product_id}', 'DatasheetController@createDatasheet')->name('create_datasheet');
// Route::get('/datasheet/create/code/{product_code}', 'DatasheetController@createDatasheetCode')->name('create_datasheet_code');	




Route::get('/sandbox', 'SandboxController@sandbox');
Route::get('/sandbox2', 'SandboxController@sandbox2');
Route::get('/sandbox3', 'SandboxController@sandbox3');
Route::get('/sandbox4', 'SandboxController@sandbox4');
Route::get('/sandbox5', 'SandboxController@sandbox5');

// Route::get('/setappareastoproducts', 'SandboxController@setAppAreasToProducts');

Route::post('/sandbox', 'SandboxController@sandbox')->name('sandbox');

Route::get('/login/{user}', 'HomeController@loginLEDDesigners')->name('loginLEDDesigners');

Route::get('/message-from-the-ceo', 'HomeController@message')->name('message_page');
Route::get('/message-from-the-ceo/{anchor}', 'HomeController@message')->name('message_page_goto');

Route::get('/homeowners', 'HomeController@homeowners')->name('homeowners');
Route::get('/homeowners{anchor}', 'HomeController@homeowners')->name('homeowners_goto');
Route::get('/promotions', 'HomeController@promotions')->name('promotions');
Route::get('/promotions/{anchor}', 'HomeController@promotions')->name('promotions_goto');


// Downloads pages
////////////////////////////////////////////////////////////////////////
Route::get('downloads/camelled-catalog', function () {
	return view('catalog-download', ['pages' => 'camel-catalog', 'name' => 'CamelLED Catalog', 'type' => 'camel-catalog' ]);
})->name('downloadcatalog_camelled_page');

Route::get('downloads/camel-catalog', function () {
	return view('catalog-download', ['pages' => 'camel-catalog', 'name' => 'CamelLED Catalog', 'type' => 'camel-catalog' ]);
})->name('downloadcatalog_camel_page');

Route::get('downloads/almani-catalog', function () {
	return view('catalog-download', ['pages' => 'catalog', 'name' => 'Almani Catalog', 'type' => 'almani-catalog' ]);
})->name('downloadcatalog_page');

Route::get('downloads/company-profile', function () {
	return view('catalog-download', ['pages' => 'profile', 'name' => 'Company Profile', 'type' => 'company-download']);
})->name('downloadprofile_page');

Route::get('downloads/company-prequalification', function () {
	return view('catalog-download', ['pages' => 'pre_qualifications', 'name' => 'Company Pre-Qualification', 'type' => 'pq']);
})->name('downloadprequalification_page');

/* -----------------------------------------------------------------------------------------------------------------------*/

Route::get('/flyers', 'HomeController@flyers')->name('flyers_page');
Route::get('/flyers{anchor}', 'HomeController@flyers')->name('flyers_page_goto');

Route::get('downloads/b2b', function() {
	return view('catalog-download', ['pages' => 'b2b', 'name' => 'Business to Business', 'type' => 'b2b' ]);
})->name('downloadb2b');

Route::get('downloads/led-to-own', function() {
	return view('catalog-download', ['pages' => 'ledtoown', 'name' => 'Led to Own', 'type' => 'ledtowon']);
})->name('downloadledtoown');

Route::get('downloads/approved-supplier', function() {
	return view('catalog-download', ['pages' => 'approved-supplier', 'name' => 'Approved Supplier', 'type' => 'approved-supplier']);
})->name('downloadapprovedsupplier');

Route::get('downloads/price-match-promise', function() {
	return view('catalog-download', ['pages' => 'price-match-promise', 'name' => 'Price Match Promise', 'type' => 'price-match-promise' ]);
})->name('downloadpricematchpromise');



////////////////////////////////////////////////////////////////////////
// Downloads pages

Route::get('/downloads', 'HomeController@downloads')->name('download_page');
Route::get('/downloads/{anchor}', 'HomeController@downloads')->name('download_page_goto');

	
// anchors
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Route::get('/custom-projects', 'HomeController@customProjects')->name('customprojects_page');
Route::get('/custom-projects/{anchor}', 'HomeController@customProjects')->name('customprojects_page_goto');

Route::get('/approved-supplier', 'HomeController@approvedSupplier')->name('approvedsupplier_page');
Route::get('/approved-supplier/{anchor}', 'HomeController@approvedSupplier')->name('approvedsupplier_page_goto');

Route::get('/catalog/brands', 'HomeController@catalogBrands')->name('catalog_brand_page');
Route::get('/catalog/brands/{anchor}', 'HomeController@catalogBrands')->name('catalog_brand_page_goto');

Route::get('/careers','HomeController@careers')->name('careers_page');
Route::get('/careers/{anchor}','HomeController@careers')->name('careers_page_goto');

Route::get('/b2b-portal', 'HomeController@b2bPortal')->name('b2bportal_page');
Route::get('/b2b-portal/{anchor}', 'HomeController@b2bPortal')->name('b2bportal_page_goto');

Route::get('/roi', 'HomeController@roi')->name('roi_page');
Route::get('/roi/{anchor}', 'HomeController@roi')->name('roi_page_goto');

Route::get('/projects','HomeController@projects')->name('projects_page');
Route::get('/projects/{anchor}','HomeController@projects')->name('projects_page_goto');

Route::get('/led-to-own','HomeController@ledToOwn')->name('ledtoown_page');
Route::get('/led-to-own/{anchor}','HomeController@ledToOwn')->name('ledtoown_page_goto');

Route::get('/lighting-design','HomeController@lightingDesign')->name('lightingdesign_page');
Route::get('/lighting-design/{anchor}','HomeController@lightingDesign')->name('lightingdesign_page_goto');

Route::get('/what-we-do', 'HomeController@whatwedo')->name('whatwedo_page');
Route::get('/what-we-do/{anchor}', 'HomeController@whatwedo')->name('whatwedo_page_goto');

Route::get('/why-led', 'HomeController@whyled')->name('whyled_page');
Route::get('/why-led/{anchor}', 'HomeController@whyled')->name('whyled_page_goto');

Route::get('/about-us', 'HomeController@aboutus')->name('aboutus_page');
Route::get('/about-us/{anchor}', 'HomeController@aboutus')->name('aboutus_page_goto');

Route::get('/price-match-promise','HomeController@priceMatchPromise')->name('pricematchpromise_page');
Route::get('/price-match-promise/{anchor}','HomeController@priceMatchPromise')->name('pricematchpromise_page_goto');

Route::get('/quality-assurance','HomeController@qualityAssurance')->name('qualityassurance_page');
Route::get('/quality-assurance/{anchor}','HomeController@qualityAssurance')->name('qualityassurance_page_goto');

Route::get('/quality-control', 'HomeController@qualitycontrol')->name('qualitycontrol_page');
Route::get('/quality-control/{anchor}', 'HomeController@qualitycontrol')->name('qualitycontrol_page_goto');

Route::get('/service-promise','HomeController@servicePromise')->name('servicepromise_page');
Route::get('/service-promise/{anchor}', 'HomeController@servicePromise')->name('servicepromise_page_goto');

Route::get('/en/terms', function(){	
	return redirect()->route('terms_page');
})->name('terms_en_page');
Route::get('/terms', 'HomeController@terms')->name('terms_page');
Route::get('/terms/{anchor}', 'HomeController@terms')->name('terms_page_goto');

Route::get('/privacy-policy', 'HomeController@privacypolicy')->name('privacypolicy_page');
Route::get('/privacy-policy/{anchor}', 'HomeController@privacypolicy')->name('privacypolicy_page_goto');

Route::get('/sectors-we-serve','HomeController@sectorsWeServe')->name('sectorsweserve_page');
Route::get('/sectors-we-serve/{anchor}','HomeController@sectorsWeServe')->name('sectorsweserve_page_goto');

Route::get('/welcome-to-almani', 'HomeController@welcomeToAlmani')->name('welcome_to_almani');
Route::get('/welcome-to-almani/{anchor}', 'HomeController@welcomeToAlmani')->name('welcome_to_almani_page_goto');


Route::get('/client/login', 'ClientLoginController@client')->name('clientlogin');

// Route::post('/client/loginAction', 'Auth\LoginController@clientLogin')->name('clientloginprocess');

Route::post('/client/loginAction', 'ClientLoginController@clientAction')->name('clientloginprocess');

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// anchors





// Product Page
Route::get('/catalog/{category_alias}/{product_code}.html', function($category_alias, $product_code){
	 return redirect(null,301)->route('product_page',['category_alias'=>$category_alias, 'product_code' => $product_code]);
})->name('product_page_redirect');


Route::get('/catalog/{category_alias}/{product_code}/model/{model_code}.html', function($category_alias, $product_code, $model_code) {
	return redirect(null,301)->route('productmodel_page',['category_alias'=>$category_alias, 'product_code' => $product_code, 'model_code' => $model_code]);
})->name('productmodel_page_redirect');


Route::get('/catalog/{category_alias}/{product_code}/model/{model_code}', 'CatalogController@productModel')->name('productmodel_page');
Route::get('/catalog/{category_alias}/{product_code}', 'CatalogController@product')->name('product_page');

// Product Page


// Catalog Page

Route::get('/catalog/{category_alias}.html', function($category_alias) {
	return redirect(null,301)->route('catalog_product_page',['category_alias'=> $category_alias]);
})->name('catalog_product_page_redirect');

Route::get('/catalog.html', function() {
	return redirect(null,301)->route('catalog_landing_page');
})->name('catalog_landing_page_redirect');

Route::get('/catalog/{category_alias}', 'CatalogController@catalogProduct')->name('catalog_product_page');
Route::get('/catalog', 'CatalogController@index')->name('catalog_landing_page');
// Route::get('/catalog', 'HomeController@catalogBrands')->name('catalog_brand_page');

// for sorting asc and desc
Route::post('/catalog/sorting', 'CatalogController@productSort')->name('product_sorting');
// end of sorting asc and desc

//  Catalog Page


// Application Areas
Route::get('/application-area', 'CatalogController@applicationAreaLanding')->name('application_area_landing_page');
Route::get('/application-area/{app_slug}', 'CatalogController@applicationArea')->name('application_area_page');





Route::get('/emailclientsandsuppliers','EnquiryController@emailCLientsAndSuppliers');
Route::get('/viewemailclientsandsuppliers','EnquiryController@viewEmailCLientsAndSuppliers');







// Mobile version
Route::get('/m', 'MobileController@index')->name('mobile_landing');































































// Htaccess test
//////////////////////////////////////////////////////
Route::get('/htaccesstest',function(){
	return view('htaccesstest');
});

