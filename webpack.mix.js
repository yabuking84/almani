let mix = require('laravel-mix');

var path_public_assets = "public/assets";
var path_resources_assets = "resources/assets";
var npm_gen = "public/assets/js/npm-generated";

mix
    .sass(path_resources_assets+'/sass/pages/main-layout.scss', path_public_assets+'/css/master-main-layout.css')
    .sass(path_resources_assets+'/sass/pages/home.scss', path_public_assets+'/css/master-home.css')
    .sass(path_resources_assets+'/sass/pages/catalog-product.scss', path_public_assets+'/css/master-catalog-product.css')
    .sass(path_resources_assets+'/sass/pages/product.scss', path_public_assets+'/css/master-product.css')
    .sass(path_resources_assets+'/sass/pages/projects.scss', path_public_assets+'/css/master-projects.css')
    .sass(path_resources_assets+'/sass/pages/quality-assurance.scss', path_public_assets+'/css/master-quality-assurance.css')
    .sass(path_resources_assets+'/sass/pages/getaquote.scss', path_public_assets+'/css/master-getaquote.css')
    .sass(path_resources_assets+'/sass/pages/contactus.scss', path_public_assets+'/css/master-contactus.css')
    .sass(path_resources_assets+'/sass/pages/homeowners.scss', path_public_assets+'/css/master-homeowners.css')
    .sass(path_resources_assets+'/sass/pages/promotions.scss', path_public_assets+'/css/master-promotions.css')
    .sass(path_resources_assets+'/sass/pages/search.scss', path_public_assets+'/css/master-search.css')
    .sass(path_resources_assets+'/sass/pages/service-promise.scss', path_public_assets+'/css/master-service-promise.css')
    .sass(path_resources_assets+'/sass/pages/aboutus.scss', path_public_assets+'/css/master-aboutus.css')
    .sass(path_resources_assets+'/sass/pages/terms.scss', path_public_assets+'/css/master-terms.css')
    .sass(path_resources_assets+'/sass/pages/privacy-policy.scss', path_public_assets+'/css/master-privacy-policy.css')
    .sass(path_resources_assets+'/sass/pages/careers.scss', path_public_assets+'/css/master-careers.css')
    .sass(path_resources_assets+'/sass/pages/sectorsweserve.scss', path_public_assets+'/css/master-sectorsweserve.css')
    .sass(path_resources_assets+'/sass/pages/qualitycontrol.scss', path_public_assets+'/css/master-qualitycontrol.css')
    .sass(path_resources_assets+'/sass/pages/whatwedo.scss', path_public_assets+'/css/master-whatwedo.css')
    .sass(path_resources_assets+'/sass/pages/pricematchpromise.scss', path_public_assets+'/css/master-pricematchpromise.css')
    .sass(path_resources_assets+'/sass/pages/lightingdesign.scss', path_public_assets+'/css/master-lightingdesign.css')
    .sass(path_resources_assets+'/sass/pages/ledtoown.scss', path_public_assets+'/css/master-ledtoown.css')
    .sass(path_resources_assets+'/sass/pages/roi.scss', path_public_assets+'/css/master-roi.css')
    .sass(path_resources_assets+'/sass/pages/whyled.scss', path_public_assets+'/css/master-whyled.css')
    .sass(path_resources_assets+'/sass/pages/catalog-brands.scss', path_public_assets+'/css/master-catalog-brands.css')
    .sass(path_resources_assets+'/sass/pages/catalog-download.scss', path_public_assets+'/css/master-catalog-download.css')
    .sass(path_resources_assets+'/sass/pages/b2bportal.scss', path_public_assets+'/css/master-b2bportal.css')
    .sass(path_resources_assets+'/sass/pages/custom-projects.scss', path_public_assets+'/css/master-custom-projects.css')
    .sass(path_resources_assets+'/sass/pages/approved-supplier.scss', path_public_assets+'/css/master-approved-supplier.css')
    .sass(path_resources_assets+'/sass/pages/blog.scss', path_public_assets+'/css/master-blog.css')
    .sass(path_resources_assets+'/sass/pages/message-from-ceo.scss', path_public_assets+'/css/master-message-from-ceo.css')
    .sass(path_resources_assets+'/sass/pages/catalog-landing.scss', path_public_assets+'/css/master-catalog-landing.css')
    .sass(path_resources_assets+'/sass/pages/welcome-to-almani.scss', path_public_assets+'/css/master-welcome-to-almani.css')
    .sass(path_resources_assets+'/sass/pages/application-area-landing.scss', path_public_assets+'/css/master-application-area-landing.css')
    .sass(path_resources_assets+'/sass/pages/downloads.scss', path_public_assets+'/css/master-downloads.css')
    .sass(path_resources_assets+'/sass/pages/clientlogin.scss', path_public_assets+'/css/master-clientlogin.css')


    // mobile 
    .sass(path_resources_assets+'/sass/mobile/main-layout.scss', path_public_assets+'/css/master-main-layout-mobile.css')
    .sass(path_resources_assets+'/sass/mobile/home.scss', path_public_assets+'/css/master-home-mobile.css')

;




// JS Group Scripts 1
mix.scripts([   
    path_resources_assets+'/vendors/jquery/3.2.1/jquery.min.js',
    path_resources_assets+'/vendors/bootstrap/js/bootstrap.bundle.min.js',

], npm_gen+'/js-group-scripts_1.js');


// JS Group Scripts 2
mix.scripts([
    path_resources_assets+'/vendors/easing/easing.min.js',
    
    // Menu
    path_resources_assets+'/vendors/slideout/js/slideout.min.js',

    path_resources_assets+'/vendors/superfish/hoverIntent.js',
    path_resources_assets+'/vendors/superfish/superfish.min.js',
    path_resources_assets+'/vendors/sticky/sticky.js',


], npm_gen+'/js-group-scripts_2.js');


// Mobile Home page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/vendors/owlcarousel/owl.carousel.min.js',
    path_resources_assets+'/vendors/swiper/js/swiper.min.js',

    path_resources_assets+'/js/mobile-layout.js',

    path_resources_assets+'/js/mobile/home.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-home-mobile.js');



    // path_resources_assets+'/vendors/wow/wow.min.js',
    // path_resources_assets+'/vendors/owlcarousel/owl.carousel.min.js',
    // path_resources_assets+'/vendors/magnific-popup/magnific-popup.min.js',


// Home page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/vendors/owlcarousel/owl.carousel.min.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/home.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-home.js');


// Catalog Product page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/catalog-product.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-catalog-product.js');




// Catalog Brand page

mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',
    
    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/vendors/owlcarousel/owl.carousel.min.js',

    path_resources_assets+'/js/pages/catalog-brands.js',
    path_resources_assets+'/js/preloader.js',

], path_public_assets+'/js/master-catalog-brands.js')


// Catalog page

mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',
    
    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/vendors/owlcarousel/owl.carousel.min.js',

    path_resources_assets+'/js/pages/catalog-landing.js',
    path_resources_assets+'/js/preloader.js',

], path_public_assets+'/js/master-catalog-landing.js')



// Catalog Download page

mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',
    
    path_resources_assets+'/js/main-layout.js',


    path_resources_assets+'/js/pages/catalog-download.js',
    path_resources_assets+'/js/preloader.js',

], path_public_assets+'/js/master-catalog-download.js')


// Projects page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/projects.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-projects.js');



// Custom Projects page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/custom-projects.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-custom-projects.js');


// Quality Assurance page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/quality-assurance.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-quality-assurance.js');

// Service Promise page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/service-promise.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-service-promise.js');


// Get a Quote page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/getaquote.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-getaquote.js');


// Contact Us page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/contactus.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-contactus.js');


// Home Owners page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/homeowners.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-homeowners.js');

mix.scripts([
    path_resources_assets+'/js/pages/promotions.js',
], path_public_assets+'/js/master-promotions.js');




// Product page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/vendors/owlcarousel/owl.carousel.min.js',
    path_resources_assets+'/vendors/magnific-popup/magnific-popup.min.js',    

    // path_resources_assets+'/vendors/easyzoom/js/easyzoom.js',
    path_resources_assets+'/vendors/easyzoom/src/easyzoom.js',

    // path_resources_assets+'/vendors/imagezoom/js/image_zoom.min.js',
    // path_resources_assets+'/vendors/elevatezoom/js/jquery.elevateZoom-3.0.8.min.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/product.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-product.js');






// Search page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/vendors/select2/js/select2.min.js',
    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/search.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-search.js');





// About Us page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/vendors/owlcarousel/owl.carousel.min.js',
    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/aboutus.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-aboutus.js');



// What We Do page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/vendors/owlcarousel/owl.carousel.min.js',
    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/whatwedo.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-whatwedo.js');




// Sectors We Serve page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/sectorsweserve.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-sectorsweserve.js');



// Price Match Promise
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/pricematchpromise.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-pricematchpromise.js');



// Approved Supplier
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/approved-supplier.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-approved-supplier.js');





// terms page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/terms.js',    
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-terms.js');




// Privacy Policy page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/privacy-policy.js',    
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-privacy-policy.js');


// Careers page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/careers.js',    
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-careers.js');


// Careers page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/message-from-ceo.js',    
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-message-from-ceo.js');






// Quality Control page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/vendors/owlcarousel/owl.carousel.min.js',
    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/qualitycontrol.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-qualitycontrol.js');



// Lighting Design page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/lightingdesign.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-lightingdesign.js');



// LED to Own Promise
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/ledtoown.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-ledtoown.js');


// B2B Portal Promise
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/b2bportal.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-b2bportal.js');


// ROI page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',
    path_resources_assets+'/vendors/flot/js/jquery.flot.min.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/roi.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-roi.js');


// Why LED page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/whyled.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-whyled.js');

// Why LED page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',

    path_resources_assets+'/js/pages/blog.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-blog.js');


// Welcome to Almani
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',
  
    path_resources_assets+'/js/pages/welcome-to-almani.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-welcome-to-almani.js');


// Application Area Landing Page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',
  
    path_resources_assets+'/js/pages/application-area-landing.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-application-area-landing.js');

// Downloads Area Landing Page
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',
  
    path_resources_assets+'/js/pages/downloads.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-downloads.js');


// CLient Login JS
mix.scripts([
    npm_gen+'/js-group-scripts_1.js',
    npm_gen+'/js-group-scripts_2.js',

    path_resources_assets+'/js/main-layout.js',
  
    path_resources_assets+'/js/pages/clientlogin.js',
    path_resources_assets+'/js/preloader.js',
], path_public_assets+'/js/master-clientlogin.js');





mix.sourceMaps(); // Enable sourcemaps
mix.version(); // Enable versioning.


mix.options({
  // extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
  processCssUrls: false, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
  // purifyCss: false, // Remove unused CSS selectors.
  // uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
  // postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
});