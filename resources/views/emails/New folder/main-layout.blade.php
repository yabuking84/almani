<!DOCTYPE html>
<html>
<head>
  <title>
	    @section('title')
	    	CamelLED Email
	    @show('title')  	
  </title>

</head>
<body style="background-color: #e2eace; 
			font-family: Montserrat, Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif;
			padding: 20px;">

<div style="background-color: #2b582b;
		    text-align: center;
		    padding: 35px 10px 10px 10px;
		    max-width: 900px;
			width:70%;
		    color: #fff;
		    margin-left: auto;
		    margin-right: auto;
			background-image: url('https://almani.ae/assets/images/dessert3.png');
			background-position-x: 110%;
			background-position-y: bottom;
			background-repeat: no-repeat;
			background-size: 50%;
		    ">

	<!-- <img src="{{App\Util::asset('assets/images/home/camel-white.png')}}" /> -->
	<a href="#"><img src="https://almani.ae/assets/images/camel-white.png" alt="CamelLED" /></a>
	<p style="margin-top:20px; margin-bottom:0px;">Exceptional German Quality Control</p>
	<p style="margin-top:5px; margin-bottom:0;">Every Step of the way</p>
	<div style="display: block;text-align: right;padding-right: 20px;margin-top: 20px;font-size: 12px;">
		By 
		<a style="color: #fff; text-decoration: none; " href="https://almani.ae">
			<img style="height: 23px;vertical-align: middle;margin-left: 20px;" 
				src="https://almani.ae/assets/img/whitelogo.png" 
				alt="Almani Lighting">
		</a>
	</div>
	
</div>





<!-- ////////////////////////////// -->



<div style="background-color: #fff;
		    text-align: center;
		    padding: 20px 10px;
		    max-width: 900px;
			width:70%;
		    color: #555;
		    margin-left: auto;
		    margin-right: auto;">
<p style="margin: 0;
	    text-align: left;
	    font-size: 15px;
	    padding: 5px 15px;">

	    @yield('title1')
</p>
</div>



<!-- ////////////////////////////// -->




<div style="background-color: #fff;
		    text-align: center;
		    padding: 10px 10px;
		    max-width: 900px;
			width:70%;
		    color: #555;
		    margin-left: auto;
		    margin-right: auto;">

<span style="padding:5px 10%;
			display: block;
			font-size:13px;
			font-weight:normal;">

@yield('title2')



@section('table')
<table style="font-size:13px;
			font-weight:normal;
			line-height:20px;">
	<tbody>

		@yield('tbody')

	</tbody>
</table>
@show



</span>	
</div>






<!-- ////////////////////////////// -->



<div style="background-color: #fff;
		    text-align: center;
		    padding: 20px 10px;
		    max-width: 900px;
			width:70%;
		    color: #555;
		    margin-left: auto;
		    margin-right: auto;">
<p style="margin: 0;
	    text-align: left;
	    font-size: 14px;
        font-weight: bold;
	    padding: 5px 15px;
	    color: #2b582b;">

Kind regards,<br>
<br>
CamelLED L.L.C.<br>
info@almani.ae<br>
+971 4-887-3265<br>
camelled.almani.ae<br>
<br>
Visit Us<br>
CamelLED L.L.C.<br>
Showroom 10, Dubai Investment Park 1,<br>
Opposite Green Community East, Beside Park N' Shop, Dubai, UAE<br>
</p>
</div>



<!-- ////////////////////////////// -->


<div style="background-color: #2b582b;
		    text-align: center;
		    padding:20px 10px 0px 10px;
		    max-width: 900px;
			width:70%;
		    color: #fff;
		    margin-left: auto;
		    margin-right: auto;">

<table style="width: 100%;font-size: 14px; color: #fff;">
	<tr>
		<td style="width: 40%;
				    text-align: left;
				    padding-left: 20px;">
			<a href="#"><img src="https://almani.ae/assets/img/FB-f-Logo__white_29.png" alt="Facebook" /></a>
		</td>
		<td>
			<span style="color: #61ff61;
					    display: inline-block;
					    padding-top: 1px;
					    padding-right: 5px;
					    vertical-align: middle;">
				<img src="https://almani.ae/assets/img/phone.png" alt="Tel:"/>
			</span>
			<span>+971 4 514 8687</span>
		</td>
		<td>
			<span style="color: #61ff61;
					    display: inline-block;
					    padding-top: 1px;
					    padding-right: 5px;
					    vertical-align: middle;">
				<img src="https://almani.ae/assets/img/email.png" alt="Email:"/>
			</span>
			<span>info@almani.ae</span>
		</td>
	</tr>
</table>

</div>
<!-- ////////////////////////////// -->


<div style="background-color: #2b582b;
		    text-align: center;
		    padding: 20px 10px;
		    max-width: 900px;
			width:70%;
		    color: #fff;
		    margin-left: auto;
		    margin-right: auto;">

<p style="margin: 0;
	    text-align: left;
	    font-size: 13px;
	    padding: 5px 15px;
	    line-height: 20px;">
Disclaimer:<br>
<span style="font-style: italic;font-size: 12px;">
This email is intended for the exclusive use by the person(s) mentioned as recipient(s). 
This email and its attachments, if any, contain confidential information and/or may contain information protected by 
intellectual property rights or others rights. This email does not constitute any commitment from CamelLED L.L.C. or 
its subsidiaries except when expressly agreed in a written agreement between the intended recipient and 
CamelLED L.L.C. or its subsidiaries. If you receive this email by mistake, 
please notify the sender and delete this email immediately from your system and destroy all copies of it. 
You may not, directly or indirectly, use, disclose, distribute, 
print or copy this email or any part of it if you are not the intended recipient.
</span>
</p>

</div>



<!-- ////////////////////////////// -->





</body>
</html>