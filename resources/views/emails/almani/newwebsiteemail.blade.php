@extends('emails.almani.main-layout')


@section('title1')
@endsection


@section('title2')
@endsection



@section('tbody')
	<tr>
		<td style="text-align:left;vertical-align:top;">
			<span style="display: block;margin: -30px 0 0;font-weight: bold;">
				To our valued customers & suppliers:
			</span>
			<p style="text-align: left;">
				It's here! We are happy to announce the launch of our newly redesigned website! Our new website provides a clear message of who we are, what we stand for and where our value lies in designing, supplying and installing the finest quality of LED lights in the market.
				<br><br>

				You may also download our catalogue using the link below:
				<br><br>
				Almani Lighting : <a href="{{route('downloadcatalog_page')}}">Download Link</a>								
				
				<br><br>
				We’re really proud of the new website and feel it will create the experience you’re looking for when you pay us a visit.

				<br><br>
				Check out the new website here: <a href="https://almani.ae">www.almani.ae</a>
			</p>

		</td>
	</tr>
@endsection
