@extends('emails.almani.main-layout')


@section('title1')
<?=$title1?>
@endsection


@section('title2')
<?=$title2?><br><br>
@endsection


@section('tbody')


	<tr>
		<td style="text-align:left;vertical-align:top; width:25%;"><strong style="margin-right: 15px;">Name:</strong></td>
		<td style="text-align:left;vertical-align:top;">{{$name_to}}</td>
	</tr>
	<tr>
		<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;">Email:</strong></td>
		<td style="text-align:left;vertical-align:top;">{{$email_to}}</td>
	</tr>
	<tr>
		<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;">Contact No.:</strong></td>
		<td style="text-align:left;vertical-align:top;">{{$tel}}</td>
	</tr>
	<tr>
		<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;">Message:</strong></td>
		<td style="text-align:left;vertical-align:top;"><?=nl2br($other_message)?></td>
	</tr>



@endsection