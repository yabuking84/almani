@extends('emails.almani.main-layout')




@section('title1')

	Thank you {{strtoupper($name_from)}} for contacting Almani Lighting. We will reply to you as soon as possible.<br><br>
	A copy of your Qoute is added below for your record.	

@endsection




@section('tbody')


	<tr>
		<td style="text-align:left;vertical-align:top; width:25%;"><strong style="margin-right: 15px;">Name:</strong></td>
		<td style="text-align:left;vertical-align:top;">{{$name_from}}</td>
	</tr>
	<tr>
		<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;">Email:</strong></td>
		<td style="text-align:left;vertical-align:top;">{{$email_from}}</td>
	</tr>
	<tr>
		<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;">Contact No.:</strong></td>
		<td style="text-align:left;vertical-align:top;">{{$tel}}</td>
	</tr>
	<tr>
		<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;">Message:</strong></td>
		<td style="text-align:left;vertical-align:top;"><?=nl2br($other_message)?></td>
	</tr>
	<tr>
		<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;"></strong></td>
		<td style="text-align:left;vertical-align:top;"></td>
	</tr>
	<tr>
		<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;">Project Type:</strong></td>
		<td style="text-align:left;vertical-align:top;">{{($project_type=='Others')?$project_type_others:$project_type}}</td>
	</tr>
	<tr>
		<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;">Construction Status:</strong></td>
		<td style="text-align:left;vertical-align:top;">{{($construction_status=='Others')?$construction_status_others:$construction_status}}</td>
	</tr>
	<tr>
		<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;">Project Budget:</strong></td>
		<td style="text-align:left;vertical-align:top;">{{($project_budget!='')?$project_budget_currency.' '.$project_budget:'---'}}</td>
	</tr>
	<tr>
		<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;">Location:</strong></td>
		<td style="text-align:left;vertical-align:top;">{{$location}}</td>
	</tr>
	<tr>
		<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;">Files:</strong></td>
		<td style="text-align:left;vertical-align:top;">
			@if(count($files))
				@foreach($files as $file)
					<a href="{{$file['url']}}">{{$file['filename']}}</a><br>
				@endforeach
			@else
				---
			@endif
		</td>
	</tr>

@endsection