@extends('emails.almani.main-layout')


@section('title1')
{{$title1}}	
@endsection

@section('tbody')

	<tr>
		<td style="text-align:left;vertical-align:top; width:25%;"><strong style="margin-right: 15px;">Name:</strong></td>
		<td style="text-align:left;vertical-align:top;">{{$name_to}}</td>
	</tr>
	<tr>
		<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;">Email:</strong></td>
		<td style="text-align:left;vertical-align:top;">{{$email_to}}</td>
	</tr>
	<tr>
			<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;">Phone:</strong></td>
			<td style="text-align:left;vertical-align:top;">{{$phone}}</td>
		</tr>
	<tr>
		<td style="text-align:left;vertical-align:top;"><strong style="margin-right: 15px;">File:</strong></td>
		<td style="text-align:left;vertical-align:top;"><a href="<?=trim($url)?>">Click here to download - <?=$catalog_name?></a></td>
    </tr>


@endsection