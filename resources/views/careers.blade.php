@extends('main-layout', ['careers_menu' => 'active', 'company_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-careers.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-careers.js')}}"></script>
@endsection



@section('title')
Careers{{App\Meta::webpageTitle()}}
@endsection



<style type="text/css">
    
    .hide {
        display: none;
    }

</style>

@section('main-content')

@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif

<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Careers </a>
    </h1>
</section>


<section class="section">
    <div class="container text_container">
        <div class="text">
            
            <h3>We are Almani</h3>
            
            <p>Our mission is to supply and install the finest quality LED lightings. We deliver maximum energy- and cost efficiency to commercial and industrial clients throughout the GCC region and neighboring countries. We will earn our place as a global leader in LED lighting design and system, trusted to deliver the highest quality and value every time.</p>

            <h3>
                Our Core Values:
            </h3>
            <div>
                <ul>
                    <li>Excellence, in all that we sell and all that we do</li>   
                    <li>Reliability, if we say it, we do it</li>   
                    <li>Integrity, building our business by earning trust</li>   
                    <li>Positivity, listen, understand, deliver, and improve</li>   
                    <li>Value, absolute quality at a fair price</li>   
                    <li>Environmental, respecting protecting our planet</li>   
                    <li>Partnership, individual strengths build ever-stronger teams</li>   
                </ul>
            </div>
        </div>
        <div class="text img" style="background-image: url({{App\Util::asset('assets/images/careers/careers_img.jpg')}});">
            <div class="backdrop"></div>
        </div>

    </div>
        
        <div class="document">
            <div style="text-align: center;">
                <h2>Available Positions:</h2>
            </div>
            <div class="available_positions">
                <h3 data-target=".sales_manager">Sales Executive</h3>
                <h3 data-target=".led_lighting_designer">LED Lighting Designer</h3>
                {{-- <h3 data-target=".bsn_executive">Business Development / Sales Executive</h3> --}}
                {{-- <h3 data-target=".it_developer">Front End Developer</h3> --}}
                {{-- <h3 data-target=".ios_android_developer">Android / IOS App Developer</h3> --}}
            </div>
        </div>

    <div class="container">
        <div class="document">
            

            <!-- /////////////////////////////////////////// -->
            <div class="tab sales_manager">
                <p><span style="font-weight: 400;">APPLICANTS WHO ARE NOT ARABIC SPEAKER AND WITHOUT CV ATTACHMENTS WILL NOT BE CONSIDERED!!! - UAE Driving License is a must!</span></p>
                <p><span style="font-weight: 400;">Up for a challenge?</span></p>
                <p><span style="font-weight: 400;">If you want to be a part of a young, dynamic company with a unique team-culture, then you are just the person we are looking for. Apply for the job today! </span><strong>Along with your CV, we require an explanation of how you can benefit and add value to Almani Lighting. Only when those details are given, we will consider your application.</strong></p>
                <p><span style="font-weight: 400;">Job Type: Full-time</span></p>
                <p><span style="font-weight: 400;">Salary: AED6,000 /month</span></p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p><strong>Job Description</strong></p>
                <p><span style="font-weight: 400;">We are looking for an experienced Sales Executive (m/f) to oversee the relationship of the company with its most important clients. You will be building, maintaining and optimizing all sales channels and channel partners in the UAE. The goal is to contribute to sustaining and growing our business to achieve long-term success.</span></p>
                <p>&nbsp;</p>
                <p><strong>YOUR RESPONSIBILITIES</strong></p>
                <ul>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Holding responsibility for an annual revenue target of close to AED 4 million</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Acquiring new clients and managing existing customer relations</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Setting up strategic sales and marketing relationships in order to position the company as the prime player for LED Lighting</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Development of revenue growth plans and coordination of company resources to ensure efficient and growing sales results</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Close cooperation with customer care and marketing to ensure a world-class sales process</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Achieving and surpassing weekly and monthly targets and goals</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Conducting daily coaching and weekly feedback sessions with all team members</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Implementing new processes and tools to drive sales performance through innovation</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Leading by example through calling clients and closing deals on your own</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Cooperating closely with the senior leadership to improve our sales operations &amp; processes</span></li>
                </ul>
                <p>&nbsp;</p>
                <p><strong>YOUR PROFILE</strong></p>
                <ul>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have an outstanding degree from a good university</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have at least 3 years of experience in sales from the LED lighting industry</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have excellent English written and verbal communication skills. Arabic speaker would be an extra advantage</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have an existing Construction/ Lighting network and must be presented with hard facts.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have an in-depth knowledge about the technical aspects and terminologies&nbsp;</span><strong>of LED Lights (You will be tested!)</strong></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You are experienced working in highly successful and talented teams</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You are goal oriented with a track record of over-achievement (consistently beating targets, Rep of the Year, etc.)</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have a passion for sales and you are always setting the right note</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have excellent English and Arabic written and verbal communication skills&nbsp;</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have outstanding analytical abilities and a high comfort to make data-driven decisions</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You are able to achieve great results independently without much guidance</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You are enthusiastic, competitive, self-motivated and hands-on with a strong work ethic</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You enjoy working in a fast-paced, target-driven and team-oriented environment</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You can deal in a friendly manner both with coworkers and clients</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You possess outstanding web and computer skills: MS Office</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have an excellent knowledge of the UAE, Dubai in particular</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have a driving license</span></li>
                </ul>
                <p>&nbsp;</p>
                <p><strong>WHAT WE OFFER</strong></p>
                <ul>
                <li style="font-weight: 400;"><span style="font-weight: 400;">A fast-paced working environment in a rapidly developing German company located in Dubai</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Challenges which let you grow day by day: Continuous development of your skills</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Varied, responsible tasks and projects with virtually unlimited career opportunities</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">A short decision-making process and great conceptual freedom</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Amazing office in Dubai Investment Park with perfect connections</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Excellent LED lighting products with great sales tools to support your sales efforts</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">An attractive remuneration (up to 10% profit share)</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Fixed salary based on your skill set and experience</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Flexible working hours and state of the art IT systems with worldwide access</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Employment Visa</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Company car, laptop, and cellphone</span></li>
                </ul>
                <p><span style="font-weight: 400;">(PLEASE ATTACH YOUR CV WHEN YOU APPLY)</span></p>
                <p><span style="font-weight: 400;">Email your CV at </span><a href="mailto:info@almani.ae"><span style="font-weight: 400;">info@almani.ae</span></a></p>
            </div>
            <!-- /////////////////////////////////////////// -->



            <!-- /////////////////////////////////////////// -->
            <div class="tab led_lighting_designer">
                <p><span style="font-weight: 400;">Up for a challenge?</span></p>
                <p><span style="font-weight: 400;">If you want to be a part of a young, dynamic company with a unique team-culture, then you are just the person we are looking for. Apply for the job today! </span><strong>Along with your CV, we require an explanation of how you can benefit and add value to Almani Lighting. Only when those details are given, we will consider your application.</strong></p>
                <p><span style="font-weight: 400;">Job Type: Full-time</span></p>
                <p><span style="font-weight: 400;">Salary: AED4,000 /month</span></p>
                <p>&nbsp;</p>
                <p><strong>ABOUT THE JOB &amp; YOUR TASKS</strong></p>
                <p><span style="font-weight: 400;">As a LED LIGHTING DESIGNER (m/f) you will be responsible to identify yourself with our core values. You will be working on LED lighting projects from the early stages of design through to documentation and commissioning. In this role, you will be facing the client and need to take ownership of projects while assisting in creating appropriate led lighting design solutions through to completion.</span></p>
                <p><span style="font-weight: 400;">Your responsibilities include but are not limited to:</span></p>
                <ul>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Interacting with clients &amp; Architects to know the requirements</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Choosing the right luminaire with proper specification</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Simulate lighting projects</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Designing led lighting projects on AutoCAD, 3D Max, Photoshop and Dialux software</span></li>
                </ul>
                <p><span style="font-weight: 400;"><br /></span><strong>YOUR PROFILE</strong></p>
                <p><span style="font-weight: 400;">Someone looking to learn and contribute as a team player will be an ideal candidate. A degree in electrical/architecture/interior design or similar courses will be required. Although not essential, work experience in any design filed would be preferred, along with a strong interest towards LED lighting design. You will be proficient in AutoCAD, 3D Max, Photoshop, Dialux and MS Office. You will be involved in all aspects of LED lighting design for a project from concept design to detailed design development to tender and execution. An understanding in preparing concept presentations/visualizations would be essential. You will be self- motivated, enthusiastic and able to thrive under pressure.</span></p>
                <ul>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Strong knowledge about LED</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Relevant experience with a lighting design consultancy</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Perform CAD design, interpreting markups and generating accurate drawings</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Perform drawing revisions, track changes and archive versions throughout the submission process</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Produce documentation based on input from multiple sources, including directly with customers for integration into specifications and/or installation materials</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Interface with team for design review before drawings are finalized</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Create transfer files for distribution as required</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Preparing &amp; presenting best LED technical data to client</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Able to convey engineering design concepts through technical drawings utilizing clear, concise drafting methods</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Able to read, understand and interpret lighting and controls specifications</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Demonstrable experience with wiring diagrams, electrical engineering or lighting layouts</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Proficient in all standards pertaining to layouts / floorplans / electrical and system layouts</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have minimum of 3 years experience in LED lighting</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You are experienced working in highly successful and talented teams</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have experience in start-ups, high-growth companies and/or a clearly articulated passion for entrepreneurial environments</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You are goal oriented with a track record of overachievement (consistently beating targets, Rep of the Year, etc.)</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have an impactful personality, which is naturally driving anyone to perform better</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have excellent English written and verbal communication skills. Arabic speaker would be an extra advantage.</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have a hands-on-mentality and a high perception</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have outstanding analytical abilities and a high comfort to make data-driven decisions</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You are able to achieve great results independently without much guidance</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You are enthusiastic, competitive, self-motivated and hands-on with a strong work ethic</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You enjoy working in a fast-paced, target-driven and team-oriented environment</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You can deal in a friendly manner both with coworkers and clients</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Car and driver license are an advantage</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">You have an in-depth knowledge about the technical aspects and terminologies&nbsp;</span><strong>of LED Lights (You will be tested!)</strong></li>
                </ul>
                <p>&nbsp;</p>
                <p><strong>WHAT WE OFFER</strong></p>
                <ul>
                <li style="font-weight: 400;"><span style="font-weight: 400;">An attractive remuneration based on your experience and skills plus a 10% profit share for each deal you close</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">A fast-paced working environment in a rapidly developing German company located in Dubai</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Flat hierarchies with space for your own ideas in a dynamic and motivating start-up</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Challenges which let you grow day by day: Continuous development of your skills</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Varied, responsible tasks and projects with virtually unlimited career opportunities</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">A short decision-making process and great conceptual freedom</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Amazing office in Dubai Investment Park with perfect connections</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Excellent LED lighting products with great sales tools to support your efforts</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Flexible working hours and state of the art IT systems with worldwide access</span></li>
                <li style="font-weight: 400;"><span style="font-weight: 400;">Single status in Visa</span></li>
                </ul>
                <p><span style="font-weight: 400;">Email your CV at </span><a href="mailto:info@almani.ae"><span style="font-weight: 400;">info@almani.ae</span></a><span style="font-weight: 400;">&nbsp;</span></p>
            </div>
            <!-- /////////////////////////////////////////// -->





            <!-- /////////////////////////////////////////// -->
            <div class="tab it_developer">
                <h3>Job details</h3>
                <div>
                    <ul>
                        <li>Front-end web development only</li>
                        <li>Basic knowledge of PHP and MySQL</li>
                        <li>At least 2 years minimum experience required</li>
                        <li>Photoshop and Illustrator knowledge</li>
                        <li>Modern standards, clean codes and responsive</li>
                        <li>Fast turnaround time</li>
                    </ul>
                </div>

                <h3>What we offer</h3>
                <div>
                    <ul>
                        <li>A fast-paced working environment in a rapidly developing German company located in Dubai</li>
                        <li>Flat hierarchies with space for your own ideas in a dynamic and motivating start-up</li>
                        <li>Challenges which let you grow day by day: Continuous development of your skills</li>
                        <li>Varied, responsible tasks and projects with virtually unlimited career opportunities</li>
                        <li>A short decision-making process and great conceptual freedom</li>
                        <li>Amazing office in Dubai Investment Park with perfect connections</li>
                        <li>Excellent LED lighting products which offers true value to our clients</li>
                        <li>An attractive remuneration</li>
                        <li>Fixed salary based on your skillset and experience</li>
                        <li>Flexible working hours and state of the art IT systems with worldwide access</li>
                        <li>Single status Visa</li>
                    </ul>
                </div>


                <h3>Up for a challenge?</h3>
                <div>
                    <p>
                        If you want to be a part of a young, dynamic company with a unique team-culture, then you are just the person we are looking for. Apply for the job today!
                        <br><br>
                        Email us your CV at <a class="link" href="mailto:careers@almani.ae" style="border-bottom: 1px solid;">careers@almani.ae</a>.
                    </p>
                </div>
            </div>
            <!-- /////////////////////////////////////////// -->





            <!-- /////////////////////////////////////////// -->
            <div class="tab ios_android_developer">
                <h3>Job details</h3>
                <div>
                    <p>We’re seeking an iOS and Android mobile developer.</p>
                </div>

                <h3>Responsibilities will include:</h3>
                <div>
                    <ul>
                        <li>Hands-on mobile application maintenance and development with the various mobile operating systems (iOS, Android)</li>
                        <li>Deliver mobile app prototypes and finished apps</li>
                        <li>Integrate apps with a web-based application platform</li>
                        <li>Participate in the development of mobile solution architectures.</li>
                    </ul>
                </div>



                <h3>Required skills:</h3>
                <div>
                    <ul>
                        <li>At least 2 years of experience in Mobile Application Development</li>
                        <li>Experience with one or more device platforms including iOS and Android</li>
                        <li>Strong knowledge / experience with OOP</li>
                        <li>Experience deploying apps to Apple Store and Google Play Store</li>
                        <li>Excellent communication (written and oral)</li>
                        <li>Experience with GIT</li>
                    </ul>
                </div>


                <h3>Optional skills:</h3>
                <div>
                    <ul>
                        <li>Experience with AJAX/accessing Restful APIs is preferred</li>
                        <li>Knowledge with PHP, Javascript and MySQL</li>
                        <li>Prior working experience in UX and UI designs</li>
                        <li>Understanding of server-side technologies</li>
                    </ul>
                </div>

                <h3>What we offer</h3>
                <div>
                    <ul>
                        <li>A fast-paced working environment in a rapidly developing German company located in Dubai</li>
                        <li>Flat hierarchies with space for your own ideas in a dynamic and motivating start-up</li>
                        <li>Challenges which let you grow day by day: Continuous development of your skills</li>
                        <li>Varied, responsible tasks and projects with virtually unlimited career opportunities</li>
                        <li>A short decision-making process and great conceptual freedom</li>
                        <li>Amazing office in Dubai Investment Park with perfect connections</li>
                        <li>Excellent LED lighting products which offers true value to our clients</li>
                        <li>An attractive remuneration</li>
                        <li>Fixed salary based on your skillset and experience</li>
                        <li>Flexible working hours and state of the art IT systems with worldwide access</li>
                        <li>Single status Visa</li>
                    </ul>
                </div>



                <h3>Up for a challenge?</h3>
                <div>
                    <p>
                        If you want to be a part of a young, dynamic company with a unique team-culture, then you are just the person we are looking for. Apply for the job today!
                        <br><br>
                        Email us your CV at <a class="link" href="mailto:careers@almani.ae" style="border-bottom: 1px solid;">careers@almani.ae</a>.
                    </p>
                </div>
            </div>
            <!-- /////////////////////////////////////////// -->





            <!-- /////////////////////////////////////////// -->
            <div class="tab bsn_executive">
                    <h3>About the job & your tasks</h3>
                    <div>
                        <p>As a Business Development and Sales you will be responsible for researching and pursuing new business leads for the growth of the business and should be able to provide ideas to attract new clients and keep the senior management updated about marketplace and competitor activity.
                        <br><br>
                        Your responsibilities include: </p>
                        <p> <i>Business Development</i> </p>
                        <ul>
                                <li>Build contacts with potential clients to create new business opportunities</li>
                                <li>Contacting existing clients to inform them about new developments in the company's products</li>
                                <li>Researching the needs of other companies and learning who makes decisions about purchasing</li>
                                <li>Finding out how to be a registered vendor in major and minor developers and consultants</li>
                                <li>Conducting presentations on behalf of the Almani Lighting to introduce the company and the products that we can offer</li>
                                <li>Follow company guidelines and procedures for acquisition of customers, submission of tenders etc.</li>
                                <li>Arrange meetings for senior management with prospective clients</li>
                        </ul>
                        <p> <i>Sales</i> </p>
                        <ul>
                                <li>Holding responsibility for an annual revenue target of close to AED 5 million</li>
                                <li>Acquiring new clients and managing existing customer relations</li>
                                <li>Setting up strategic sales and marketing relationships in order to position the company as the prime player for LED Lighting</li>
                                <li>Achieving and surpassing weekly and monthly targets and goals</li>
                                <li>Conducting daily coaching and weekly feedback sessions with all team members</li>
                                <li>Implementing new processes and tools to drive sales performance through innovation</li>
                                <li>Leading by example through calling clients and closing deals on your own</li>
                              </ul>
                    </div>
    
                    <h3>Your profile</h3>
                    <div>
                        <ul>
                        <li>You have an outstanding degree from a good university</li>
                        <li>You have at least <strong>3 years of experience in sales from the LED lighting industry</strong></li>
                        <li>You have an in-depth knowledge about the technical aspects and terminologies <strong>of LED Lights (You will be tested!)</strong></li>
                        <li>You are experienced working in highly successful and talented teams</li>
                        <li>You have experience in start-ups, high-growth companies and/or a clearly articulated passion for entrepreneurial environments</li>
                        <li>You are goal oriented with a track record of over-achievement (consistently beating targets, Rep of the Year, etc.)</li>
                        <li>You have a passion for sales and you are always setting the right note</li>
                        <li>You have an influencial personality, which is naturally driving anyone to perform better</li>
                        <li>You have excellent English written and verbal communication skills (additional languages are an advantage: Arabic, Hindi)</li>
                        <li>You have outstanding analytical abilities and a high comfort to make data-driven decisions</li>
                        <li>You are able to achieve great results independently without much guidance</li>
                        <li>You are enthusiastic, competitive, self-motivated and hands-on with a strong work ethic</li>
                        <li>You enjoy working in a fast-paced, target-driven and team-oriented environment</li>
                        <li>You can deal in a friendly manner both with coworkers and clients</li>
                        <li>You possess outstanding web and computer skills: MS Office</li>
                        <li>You have an excellent knowledge of the UAE, Dubai in particular</li>
                        <li>You have a driving license</li>
                        </ul>
                    </div>
    
                    <h3>What we offer</h3>
                    <div>
                            <ul>
                                    <li>A fast-paced working environment in a rapidly developing German company located in Dubai</li>
                                    <li>Flat hierarchies with space for your own ideas in a dynamic and motivating start-up</li>
                                    <li>Challenges which let you grow day by day: Continuous development of your skills</li>
                                    <li>Varied, responsible tasks and projects with virtually unlimited career opportunities</li>
                                    <li>A short decision-making process and great conceptual freedom</li>
                                    <li>Amazing office in Dubai Investment Park with perfect connections</li>
                                    <li>Excellent LED lighting products with great sales tools to support your sales efforts</li>
                                    <li>An attractive remuneration (up to 10% profit share)</li>
                                    <li>Fixed salary based on your skill set and experience</li>
                                    <li>Flexible working hours and state of the art IT systems with worldwide access</li>
                                    <li>Employment Visa</li>
                                    <li>Company car, laptop, and cellphone</li>
                                  </ul>
                    </div>
    
                    <h3>Up for a challenge?</h3>
                    <div>
                        <p>
                            If you want to be a part of a young, dynamic company with a unique team-culture, then you are just the person we are looking for. Apply for the job today!
                            <br><br>
                            Email us your CV at <a class="link" href="mailto:careers@almani.ae" style="border-bottom: 1px solid;">careers@almani.ae</a>.
                        </p>
                        <p><em>If  you got selected by our HR department, we will send you some led lighting  informational material along with a short questionnaire. This has to be answered  maximum 3 days later through our website. If you got 90% of the questions  right, we will be happy to invite you for an interview.</em></p>
                    </div>
                </div>
            <!-- /////////////////////////////////////////// -->
    




        </div>
    </div>



</section>
@endsection




