@extends('main-layout', ['products_menu' => 'active'])

@section('title')
@if($category->category_page_title)
{{$category->category_page_title}}{{App\Meta::webpageTitle()}}
@else
{{$category->category_name}}{{App\Meta::webpageTitle()}}
@endif
@endsection

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-catalog-product.css')}}" rel="stylesheet">
@endsection
@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-catalog-product.js')}}"></script>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
@endsection

@section('js-head-lib')
@parent
<script>
    var category_name = "{{$category->category_name}}";
    var $glb_url = "{{route('catalog_product_page',['category_alias'=>strtolower($category->category_alias)])}}";
</script>
@endsection


@section('meta')
<!-- META -->
<!-- <meta name="google-site-verification" content="" /> -->
<meta name="description" content="<?= $meta['description'] ?>" />
<meta name="author" content="Almani Lighting LLC">
<meta name="keywords" content="<?= $meta['keywords'] ?>" />

<link rel="canonical" href="{{ URL::current() }}" />

<meta property="og:title" content="<?= ucwords($meta['title']) ?>">
<meta property="og:type" content="article">
<meta property="og:url" content="{{route('catalog_product_page', [
                              'category_alias' => strtolower($category->category_alias),
                        ])}}">

<meta property="og:image" content="<?= $category->image() ?>">
<meta property="og:description" content="<?= $meta['description'] ?>" />
<!-- /META -->
@endsection



@section('breadcrumb')
@parent
<div class="container breadcrumb_container">
<nav>
<ol class="breadcrumb">

    @if($category->parentCategory)
        <li class="breadcrumb-item">
            <a href="{{route('catalog_landing_page')}}">
                CATEGORIES
            </a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{route('catalog_product_page',['category_alias'=>$category->parentCategory->category_alias])}}">
                {{$category->parentCategory->category_name}}
            </a>
        </li>
        <li class="breadcrumb-item active">
            {{$category->category_name}}
        </li>
    @else
        <li class="breadcrumb-item">
            <a href="{{route('catalog_landing_page')}}">
                CATEGORIES
            </a>
        </li>
        <li class="breadcrumb-item active">
            {{$category->category_name}}
        </li>    
    @endif

</ol>
</nav>
</div>
@endsection





@section('main-content')

@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif


<section class="section section_title" style="">

    <h1 class="title main_title" style="">
        {{$category->category_name}}
    </h1>

    <?php $has_products = (count($category_products))?'':'hide_group'; ?>

    <div class="category_desc_sm {{$has_products}}" style="">
        <p>
            <?=$category->category_other2_single_sentence?>
        </p>
    </div>

    @if($category->category_other2_desc)
    <p class="category_desc" style="display: none;">
        <?=$category->category_other2_desc?>
    </p>

    <div style="width: 100%;text-align: center;">
        <button class="btn-almani btn-sm readmore {{$has_products}}">
            <span class="readmore_text">read more</span>
            <span class="close_text hide_group">close</span>
        </button>
    </div>
    @endif

    @if($category->subCategories->count())
    <nav class="sub_categories">
        <h4>Sub-categories:</h4>
        <ol class="breadcrumb">
            @foreach($category->subCategories as $sub_category)
            <li class="breadcrumb-item">
                <a href="{{route('catalog_product_page',['category_alias'=>$sub_category->category_alias])}}">
                    {{$sub_category->category_name}}
                </a>
            </li>
            @endforeach
        </ol>
    </nav>
    @endif


</section>


<?php if(count($category_products)) { ?>
<section class="section section_products">

    {{-- search modal --}}
    <div class="container has_search_tab">
        <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        <form action="{{route('search_nav_page')}}" id="form-search" method="POST">
            {{ csrf_field() }}
            <div class="search_tab">

                <div class="close_search_tab_div">
                    <a href="javascript:void(0);" class="btn btn-almani close_search_tab">X</a>
                </div>

                <div class="form-group col-12" style="text-align: center; justify-content: center;">
                    <h5 style="margin-bottom: 10px;">Search</h5>
                </div>

                <div class="search_fld_grps">

                    <div class="search_fld_grp">

                        <input type="hidden" value="1" name="frcatalog" class="frcatalog">
                        <input type="hidden" value="almani" name="brand_name" id="brand_name" class="brand_name">


                        <div class="form-row">
                            <label class="col-12">Brand name</label>
                            <div class="form-group col" style="width: 170px;">
                                <div class="brand_options options">
                                    <a href="javascript:void(0)" class="option btn-sm btn-option btn-almani_brand">Almani
                                        Lighting</a>
                                    <input type="hidden" name="almani" class="almani">
                                    <a href="javascript:void(0)" class="option btn-sm btn-option btn-camel_brand">CamelLED</a>
                                    <input type="hidden" name="camel" class="camel">
                                </div>
                            </div>
                        </div>


                        <div class="form-row">
                            <label class="col-12">Category name</label>
                            <input type="text" class="form-control category_name" value="{{$category->category_name}}"
                                name="category_name" placeholder="Category">
                        </div>

                        <div class="form-row">
                            <label class="col-12">Product name</label>
                            <input type="text" class="form-control product_name" name="product_name" placeholder="Product">
                        </div>

                        <div class="form-row">
                            <label class="col-12">Model Code</label>
                            <input type="text" class="form-control model_code" name="model_code" placeholder="Model Code">
                        </div>

                        <div class="form-row">
                            <label class="col-12">Dim Options</label>
                            <div class="form-group col">
                                <div class="dim_options options">
                                    <a href="javascript:void(0);" class="option btn-sm btn-option btn-dimm">DIMM</a>
                                    <input type="hidden" name="dimm" class="dimm">
                                    <a href="javascript:void(0);" class="option btn-sm btn-option btn-triac">TRIAC</a>
                                    <input type="hidden" name="triac" class="triac">
                                    <a href="javascript:void(0);" class="option btn-sm btn-option btn-v0_10">0-10v</a>
                                    <input type="hidden" name="v0_10" class="v0_10">
                                    <a href="javascript:void(0);" class="option btn-sm btn-option btn-dali">DALI</a>
                                    <input type="hidden" name="dali" class="dali">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <label class="col-12">Emergency</label>
                            <div class="form-group col fld_md">
                                <div class="options">
                                    <a href="javascript:void(0)" class="option btn-sm btn-option btn-emergency">Enabled</a>
                                    <input type="hidden" name="emergency" class="emergency">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <label class="col-12">WIFI</label>
                            <div class="form-group col fld_md">
                                <div class="options">
                                    <a class="option btn-sm btn-option btn-wifi" href="javascript:void(0)">Enabled</a>
                                    <input type="hidden" name="wifi" class="wifi">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="search_fld_grp">

                        <div class="form-row">
                            <label class="col-12">CCT</label>
                            <div class="form-group col fld_md">
                                <input type="text" class="form-control cct_min" name="cct_min" placeholder="min">
                            </div>
                            <div class="form-group col fld_md">
                                <input type="text" class="form-control cct_max" name="cct_max" placeholder="max">
                            </div>
                        </div>

                        <div class="form-row">
                            <label class="col-12">Lumen</label>
                            <div class="form-group col fld_md">
                                <input type="text" class="form-control lumen_min" name="lumen_min" placeholder="min">
                            </div>
                            <div class="form-group col fld_md">
                                <input type="text" class="form-control lumen_max" name="lumen_max" placeholder="max">
                            </div>
                        </div>

                        <div class="form-row">
                            <label class="col-12">Power</label>
                            <div class="form-group col fld_md">
                                <input type="text" class="form-control power_min" name="power_min" placeholder="min">
                            </div>
                            <div class="form-group col fld_md">
                                <input type="text" class="form-control power_max" name="power_max" placeholder="max">
                            </div>
                        </div>

                        <div class="form-row">
                            <label class="col-12">IP Rating</label>
                            <div class="form-group col fld_sm">
                                <input type="text" class="form-control ip_rating_min" name="ip_rating_min" placeholder="min">
                            </div>
                            <div class="form-group col fld_sm">
                                <input type="text" class="form-control ip_rating_max" name="ip_rating_max" placeholder="max">
                            </div>
                        </div>

                        <div class="form-row price_row">
                            <label class="col-12">Price</label>
                            <div class="form-group col fld_bg">
                                <input type="text" class="form-control price_min" name="price_min" placeholder="min">
                            </div>
                            <div class="form-group col fld_bg">
                                <input type="text" class="form-control price_max" name="price_max" placeholder="max">
                            </div>
                             @if((session('login_led_designer') || session('login_sales')))
                            <div class="form-group col">
                                <select name="currency" class="currency form-control fld_sm">
                                    <option>AED</option>
                                    <option>USD</option>
                                    <option>EUR</option>
                                </select>
                            </div>
                            @endif
                        </div>

                    </div>

                </div>

                <div class="form-row col-12" style="justify-content:flex-end; padding-right: 5px;">
                    <!-- <button class="btn-almani clear_search">clear</button> -->
                    <button class="btn-almani submit_search">search<i class="fas fa-circle-notch fa-spin"></i></button>
                </div>

            </div>
        </form>
        <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->

    </div>
    {{-- end search modal --}}

    <div class="container">
        <div class="form-row">
            <div class="form-group col">
                <div class="sort_options options">

                    <span class="sort-options frst-phase" id="frst-phase" style="display: block;">
                        {{-- price --}}
                        <!-- <button class="option btn-sm btn-option p-price sorting-price" data-category="{{ $category_alias }}" data-name="Price" data-url="{{ route('product_sorting') }}" id="sorting-price" data-type="model_price" data-icon="fas fa-money-bill-alt" data-sort="desc"> <i
                                class="fas fa-money-bill-alt"></i>
                            Price <i class="fa fa-sort-down" aria-hidden="true"></i>
                        </button> -->
                        {{-- power --}}
                        <button class="option btn-sm btn-option p-power sorting-power" data-category="{{ $category_alias }}" id="sorting-power"  data-name="Power" data-icon="fas fa-bolt" data-type="model_power" data-url="{{ route('product_sorting') }}" data-sort="desc"> <i class="fas fa-bolt"></i>
                            Power <i class="fa fa-sort-down" aria-hidden="true"></i>
                        </button>
                        {{-- lumen --}}
                        <button class="option btn-sm btn-option p-lumens sorting-lumen" data-category="{{ $category_alias }}" data-icon="fas fa-sun" data-name="Lumen" data-type="model_lumen" id="sorting-lumen" data-url="{{ route('product_sorting') }}" data-sort="desc"> <i class="fas fa-sun"
                                aria-hidden="true"></i>
                            Lumens <i class="fa fa-sort-down" aria-hidden="true"></i>
                        </button>
                        {{-- <button class="option btn-sm btn-option wattage" data-type="wattage"> <i class="fas fa-sun"></i>
                            Wattage</button> --}}
                    </span>

                      <div class="sort-options secnd-phase" id="secnd-phase" style="display: none;">
                       <!--  <button class="option btn-sm btn-option price  selected" data-type="price" data-sort="desc"> <i
                                class="fas fa-money-bill-alt"></i>
                            Price <i class="fa fa-sort-down" aria-hidden="true"></i>
                        </button> -->
                        <button class="option btn-sm btn-option power" data-type="power" data-sort="desc"> <i class="fas fa-bolt"></i>
                            Power <i class="fa fa-sort-down" aria-hidden="true"></i>
                        </button>
                        <button class="option btn-sm btn-option lumens" data-type="lumens" data-sort="desc"> <i class="fas fa-sun"
                                aria-hidden="true"></i>
                            Lumens <i class="fa fa-sort-down" aria-hidden="true"></i>
                        </button>
                        {{-- <button class="option btn-sm btn-option wattage" data-type="wattage"> <i class="fas fa-sun"></i>
                            Wattage</button> --}}
                    </div>
                </div>

                <div class="search-div">
                    <div class="form-c form-inline">
                        <div class="select-container">
                            <div class="select">
                                <select id="itm_pg" class="itm_pg">
                                    <option value="15" selected="selected">Show 15</option>
                                    <option value="30">Show 30</option>
                                    <option value="80">Show 80</option>
                                    <option value="160">Show 160</option>
                                    <option value="500">Show 500</option>
                                </select>
                            </div>
                        </div>
                        @if((session('login_led_designer') || session('login_sales')))
                        <div class="select-container currency_container">
                            <div class="select">
                                <select id="currency" class="select_currency">
                                    <option selected="selected">AED</option>
                                    <option><i class="fas fa-dollar-sign"></i> USD</option>
                                    <option><i class="fas fa-euro-sign"></i> EUR</option>
                                </select>
                            </div>
                        </div>
                        @endif
                        <div class="search-btn-container d-none">
                            <button style="display:block;float:right;" class="btn-sm btn-almani search"><i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container product-container" >
        
        <div class="product-container" id="product-container">
            
        <div class="row grid justify-content-center product-holder">

            <?php foreach($category_products as $prod) {
                    // foreach($category->products->sortBy('minPrice_eager') as $prod) {

                        // Image
                        //////////////////////////////////////
                        // $products_img = $prod->image('l');
                        $products_img = $prod->image('m');
                        //////////////////////////////////////

                        // Product Code
                        //////////////////////////////////////
                        $prod_code = '<span class="prod_code">'.$prod->product_code.'</span>';
                        //////////////////////////////////////

                        // Product IP
                        //////////////////////////////////////
                        $prod_ip = ($prod->product_ip)?'<span class="prod_ip">IP'.$prod->product_ip.'</span>':'';
                        //////////////////////////////////////

                        // Product Name
                        //////////////////////////////////////
                        $prod_name = "<span class='prod_name'>".ucwords(str_replace('[camel]', '', strtolower($prod->product_name)))."</span>";
                        //////////////////////////////////////

                        // if no LED is present
                        //////////////////////////////////////
                        $alt = $prod_name.' '.$prod->product_code.' '.$prod->category->category_name;
                        if (strpos($alt, 'LED') == false) {
                            $alt = $alt.' LED';
                        }
                        $alt = strip_tags($alt);
                        //////////////////////////////////////

                ?>

            <!-- <div class="item-container col-sm-3"> -->
            <div class="item-container col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                
                <div class="prod_block">
                    <a class="item" href="{{route('product_page', [
                                                    'category_alias' => strtolower($category->category_alias),
                                                    'product_code' => $prod->product_code,
                                                ])}}">

                        @if(App\Util::checkIfNew($prod->product_date_added))
                        <img class="new_icon" src="{{App\Util::asset('assets/images/icons/new.png')}}">
                        @endif


                        <div class="more_details hide_group">
                            <div class="specs_legend">
                                <div class="specs_legend_lifespan">
                                    <img alt="Lifespan" src="{{App\Util::asset('assets/images/icons/unit_icons/time.png')}}">
                                    <label>Lifespan</label>
                                </div>

                                <div>
                                    <img alt="Power" src="{{App\Util::asset('assets/images/icons/unit_icons/power.png')}}">
                                    <label>Power</label>
                                </div>

                                <div>
                                    <img alt="Kelvin" src="{{App\Util::asset('assets/images/icons/unit_icons/kelvin.png')}}">
                                    <label>Kelvin</label>
                                </div>

                                <div>
                                    <img alt="Lumens" src="{{App\Util::asset('assets/images/icons/unit_icons/lumens.png')}}">
                                    <label>Delivered Lumens</label>
                                </div>

                            </div>

                            <div class="specs">
                                <span class="mini-specs" title="Lifespan" class="lifespan" data-lifespan="{{$prod->lifespan()}}">
                                    <img alt="time" src="{{App\Util::asset('assets/images/icons/unit_icons/time.png')}}">
                                    {{$prod->lifespan()}}
                                </span>
                                <span class="mini-specs" title="Power" class="power" data-power="{{App\Util::changeDash((trim($prod->power())=='-')?'N/A':$prod->power())}}">
                                    <img alt="power" src="{{App\Util::asset('assets/images/icons/unit_icons/power.png')}}">
                                    {{App\Util::changeDash((trim($prod->power())=='-')?'N/A':$prod->power())}}
                                </span>
                                <span class="mini-specs" title="Kelvin" class="wattage" data-kelvin="{{App\Util::changeDash((trim($prod->kelvin())=='-')?'N/A':$prod->kelvin())}}">
                                    <img alt="kelvin" src="{{App\Util::asset('assets/images/icons/unit_icons/kelvin.png')}}">
                                    {{App\Util::changeDash((trim($prod->kelvin())=='-')?'N/A':$prod->kelvin())}}
                                </span>
                                <span class="mini-specs" title="Lumens" data-lumens="{{App\Util::changeDash(($prod->product_lumen=='9999999')?'N/A':$prod->product_lumen)}}">
                                    <img alt="lumens" src="{{App\Util::asset('assets/images/icons/unit_icons/lumens.png')}}">
                                    {{App\Util::changeDash(($prod->product_lumen=='9999999')?'N/A':$prod->product_lumen)}}
                                </span>
                                <div class="view_product" title="View Product">
                                    <button class="btn btn-sm btn-almani goto_product">View</button>
                                </div>
                            </div>
                        </div>


                        <div class="power_container d-none">
                            {{App\Util::changeDash((trim($prod->power())=='-')?'N/A':$prod->power())}} </div>
                        <div class="lumen_container d-none">
                            {{App\Util::changeDash(($prod->product_lumen=='9999999')?'N/A':$prod->product_lumen)}}
                        </div>
                        <div class="img_container col-sm-12">
                            <img src="{{App\Util::asset($products_img)}}" alt="{{$alt}}">
                        </div>

                        <div class="prod_title">
                            <?=$prod_ip?>
                            <?=$prod_name?>
                            <?=$prod_code?>
                            <div class="prod_group">
                                @if((session('login_led_designer') || session('login_sales')))
                                @if($prod->minPrice()=="AVAILABLE UPON REQUEST") 
                                    AVAILABLE UPON REQUEST 
                                @elseif (session('login_sales'))
                                   <div class="price_container" data-value="<?=$prod->minPrice()*2?>" data-price_value_uae="<?=$prod->minPrice()*2?>">
                                    <?=$prod->minPrice()*2?> AED
                                </div>
                                @else
                              <div class="price_container" data-value="<?=$prod->minPrice()?>" data-price_value_uae="<?=$prod->minPrice()?>">
                                    <?=$prod->minPrice()?> AED
                                </div>
                                @endif
                                @else
                                <br>
                                @endif
                            </div>
                        </div>
                    </a>


                    @if(session('login_led_designer'))
                        <h1 style="font-weight: 500;
                                        font-size: 10px; 
                                        margin-bottom: 5px;
                                        display: flex;
                                        align-items: center;
                                        justify-content: space-between;
                                        ">
                            <?php if($prod->supplier)
                                    echo $prod->supplier->supplier_company;
                                else 
                                    echo "NO SUPPLIER";
                                ?>
                            <span style="">
                                <?=$prod->epOptions();?>
                            </span>
                        </h1>


                        {-- MOQ --}
                        {{-- xxxxxxxxxxxxxxxxxxxxxxxx --}}
                        <h1 style="font-weight: bold;
                                    color: #000;
                                    font-size: 13px; 
                                    margin-bottom: 5px;
                                    display: flex;
                                    align-items: center;
                                    justify-content: flex-start;
                                    position: absolute;
                                    width: 100%;
                                    top: 20px;">
                                        &nbsp;
                        @if($prod->product_consider_qty)
                            <span style="">
                                MOQ: <?=$prod->product_min_qty?>
                            </span>
                        @endif

                        </h1>
                        {{-- xxxxxxxxxxxxxxxxxxxxxxxx --}}
                        {{-- MOQ --}}




                    @endif

                </div>
            </div>

            <?php } ?>
            <!-- foreach($category->getProductsAndSubProductsNonClosure() as $prod) -->
        </div>
     
        </div>
       
        <div class="pagination_nav"> {{$products_links}} </div>

    </div>

</section>

<?php } else {?>

<section class="section section_products no_products">
    <div class="container">
        <p>
            Almani Lighting currently doesn't have any products available under
            <strong>{{$category->category_name}}</strong>.
            You can check our other brand, CamelLED.
            Please click below to view:
        </p>

        <div class="camel_link">
            <a href="https://camel.almani.ae/catalog/{{$category->category_alias}}" target="_blank">
                <img src="{{App\Util::asset('assets/images/logo/camel.png')}}" alt="Camel LED" title="Visit Camel LED!"
                    class="camel_logo">
            </a>
        </div>

        <p>
            For more information, please call us at <a href="tel:+97148873265"><strong>+971 4-887-3265</strong></a>.
            or <a class="btn-almani btn-sm send_message" data-toggle="modal" data-target="#contact_us_modal">send us a
                message</a> to help you.
        </p>
    </div>
</section>

<?php } ?>




@endsection


@section('hint')
<div class="custom-social-proof catalog-product-hint">
    <div class="custom-notification">
        <div class="custom-notification-container">
            <div class="custom-notification-content-wrapper">
                <p class="custom-notification-content">
                    <small>Hello! Visit and Explore <a href="https://camel.almani.ae/catalog/{{$category->category_alias}}" target="_blank">Camel LED</a>. </small>
                </p>
            </div>
            <div class="custom-notification-image-wrapper">
                <a href="https://camel.almani.ae/catalog/{{$category->category_alias}}" target="_blank"> <img src="{{App\Util::asset('assets/images/logo/camel-logo-colored-sm.png')}}">
                </a>
            </div>
        </div>
        <div class="custom-close"></div>
    </div>
</div>
@endsection