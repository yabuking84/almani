@extends('main-layout', [    
    'products_menu' => 'active'
])
 
    @section('css-lib')
     <link href="{{App\Util::asset('assets/css/master-catalog-landing.css')}}" rel="stylesheet">  
    @endsection

    @section('javascript-lib')
    <script src="{{App\Util::asset('assets/js/master-catalog-landing.js')}}"></script>
    @endsection

@section('title')
    Catalog{{App\Meta::webpageTitle()}}
@endsection







@section('main-content')

	<section class="section section_title" style="">
			<h1 class="title main_title" style="">
				Catalog
			</h1>
			<div class="category_desc_sm" style="">
				<p> 	We are pleased to present our distinctive array of products which were carefully made and designed with high-quality, durability, and reliability. 
						We are happy to serve our clients and help them achieve unparalleled results at the most reasonable and economical prices.  </p>
			</div>
			<p class="category_desc" style="display: none;">
				<span class="category_desc_title">
						Catch a glimpse of LED lights that can change <span class="block"> how you experience light! </span>
				</span>
				<br/>
				Because we continuously update our catalogue to include the latest LED designs and innovations on the market, we encourage you to make regular visits to these pages to discover our exciting new products!
				As always, if you’re looking for something special and bespoke, our expert lighting designers are at your service to create the precise LED products and systems you need.
			</p>
			<button class="btn-almani btn-sm readmore">
				<span class="readmore_text">read more</span>
				<span class="close_text hide_group">close</span>
			</button>
		</section>
		

	<div class="section-categories">
		<div class="row categories">
			@foreach ($categories as $cat)
				@if($cat->products->count())
				@php
					$image = $cat->image();
				@endphp
					<div class="col-12 col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3">
						<div class="category">
							<a href="{{route('catalog_product_page', ['category_alias' => strtolower($cat->category_alias)])}}">
								<div class="category_image">
									{{-- https://almani.ae/assets/images/products/led-ceiling-lights-arnstein-al-cl-1254-6744.jpg --}}
										<img src="{{ App\Util::asset($image) }}" alt="<?=str_replace('LIGHTS','LIGHTING',strtoupper($cat->category_name))?>">
								</div>
								<div class="box has-text-centered">
										<h3 class="category_name">						
											<?=str_replace("LED", "LED<br>", $cat->category_name)?>
										</h3>
								</div>
							</a> 
						</div>
					</div>
				@endif
			@endforeach
		</div>
	</div>

@endsection


