
        <div class="row grid justify-content-center product-holder">

            <?php foreach($category_products as $prod) {
                    // foreach($category->products->sortBy('minPrice_eager') as $prod) {

                        // Image
                        //////////////////////////////////////
                        // $products_img = $prod->image('l');
                        $products_img = $prod->image('m');
                        //////////////////////////////////////

                        // Product Code
                        //////////////////////////////////////
                        $prod_code = '<span class="prod_code">'.$prod->product_code.'</span>';
                        //////////////////////////////////////

                        // Product IP
                        //////////////////////////////////////
                        $prod_ip = ($prod->product_ip)?'<span class="prod_ip">IP'.$prod->product_ip.'</span>':'';
                        //////////////////////////////////////

                        // Product Name
                        //////////////////////////////////////
                        $prod_name = "<span class='prod_name'>".ucwords(str_replace('[camel]', '', strtolower($prod->product_name)))."</span>";
                        //////////////////////////////////////

                        // if no LED is present
                        //////////////////////////////////////
                        $alt = $prod_name.' '.$prod->product_code.' '.$prod->category->category_name;
                        if (strpos($alt, 'LED') == false) {
                            $alt = $alt.' LED';
                        }
                        $alt = strip_tags($alt);
                        //////////////////////////////////////

                ?>

            <!-- <div class="item-container col-sm-3"> -->
            <div class="item-container col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">

                @php
                    $category_alias = '';
                    if(isset($category->category_alias)) {
                        $category_alias = $category->category_alias;
                    } else if (isset($prod->category->category_alias)) {
                        $category_alias = $prod->category->category_alias;
                    }
                @endphp
                <div class="prod_block">
                    <a class="item" href="{{route('product_page', [
                                                    'category_alias' => strtolower($category_alias),
                                                    'product_code' => $prod->product_code,
                                                ])}}">

                        @if(App\Util::checkIfNew($prod->product_date_added))
                        <img class="new_icon" src="{{App\Util::asset('assets/images/icons/new.png')}}">
                        @endif


                        <div class="more_details hide_group">
                            <div class="specs_legend">
                                <div class="specs_legend_lifespan">
                                    <img alt="Lifespan" src="{{App\Util::asset('assets/images/icons/unit_icons/time.png')}}">
                                    <label>Lifespan</label>
                                </div>

                                <div>
                                    <img alt="Power" src="{{App\Util::asset('assets/images/icons/unit_icons/power.png')}}">
                                    <label>Power</label>
                                </div>

                                <div>
                                    <img alt="Kelvin" src="{{App\Util::asset('assets/images/icons/unit_icons/kelvin.png')}}">
                                    <label>Kelvin</label>
                                </div>

                                <div>
                                    <img alt="Lumens" src="{{App\Util::asset('assets/images/icons/unit_icons/lumens.png')}}">
                                    <label>Lumens</label>
                                </div>

                            </div>

                            <div class="specs">
                                <span class="mini-specs" title="Lifespan" class="lifespan" data-lifespan="{{$prod->lifespan()}}">
                                    <img alt="time" src="{{App\Util::asset('assets/images/icons/unit_icons/time.png')}}">
                                    {{$prod->lifespan()}}
                                </span>
                                <span class="mini-specs" title="Power" class="power" data-power="{{App\Util::changeDash((trim($prod->power())=='-')?'N/A':$prod->power())}}">
                                    <img alt="power" src="{{App\Util::asset('assets/images/icons/unit_icons/power.png')}}">
                                    {{App\Util::changeDash((trim($prod->power())=='-')?'N/A':$prod->power())}}
                                </span>
                                <span class="mini-specs" title="Kelvin" class="wattage" data-kelvin="{{App\Util::changeDash((trim($prod->kelvin())=='-')?'N/A':$prod->kelvin())}}">
                                    <img alt="kelvin" src="{{App\Util::asset('assets/images/icons/unit_icons/kelvin.png')}}">
                                    {{App\Util::changeDash((trim($prod->kelvin())=='-')?'N/A':$prod->kelvin())}}
                                </span>
                                <span class="mini-specs" title="Lumens" data-lumens="{{App\Util::changeDash(($prod->product_lumen=='9999999')?'N/A':$prod->product_lumen)}}">
                                    <img alt="lumens" src="{{App\Util::asset('assets/images/icons/unit_icons/lumens.png')}}">
                                    {{App\Util::changeDash(($prod->product_lumen=='9999999')?'N/A':$prod->product_lumen)}}
                                </span>
                                <div class="view_product" title="View Product">
                                    <button class="btn btn-sm btn-almani goto_product">View</button>
                                </div>
                            </div>
                        </div>


                        <div class="power_container d-none">
                            {{App\Util::changeDash((trim($prod->power())=='-')?'N/A':$prod->power())}} </div>
                        <div class="lumen_container d-none">
                            {{App\Util::changeDash(($prod->product_lumen=='9999999')?'N/A':$prod->product_lumen)}}
                        </div>
                        <div class="img_container col-sm-12">
                            <img src="{{App\Util::asset($products_img)}}" alt="{{$alt}}">
                        </div>

                        <div class="prod_title">
                            <?=$prod_ip?>
                            <?=$prod_name?>
                            <?=$prod_code?>
                            <div class="prod_group">
                                @if($prod->minPrice()=="AVAILABLE UPON REQUEST")
                                AVAILABLE UPON REQUEST
                                @else
                                <div class="price_container" data-value="<?=$prod->minPrice()?>" data-price_value_uae="<?=$prod->minPrice()?>">
                                    <?=$prod->minPrice()?> AED
                                </div>
                                @endif
                            </div>
                        </div>
                    </a>


                    @if($isOffice && session('login_led_designer'))
                    <h1 style="font-weight: 500;
                                    font-size: 10px; 
                                    margin-bottom: 5px;
                                    display: flex;
                                    align-items: center;
                                    justify-content: space-between;
                                    ">
                        <?php if($prod->supplier)
                                echo $prod->supplier->supplier_company;
                            else 
                                echo "NO SUPPLIER";
                            ?>
                        <span style="">
                            <?=$prod->epOptions();?>
                        </span>
                    </h1>
                    @endif

                </div>
            </div>

            <?php } ?>
            <!-- foreach($category->getProductsAndSubProductsNonClosure() as $prod) -->
        </div>
        
 
 <script type="text/javascript">
    
    var $grid = $('.grid');

    var $grid = $('.grid').isotope({
        itemSelector: '.item-container',
        // layoutMode: 'fitRows',
        getSortData: {
        // set sortable value
            price: '.price_container parseFloat',
            power: '.power_container parseFloat',
            lumen: '.lumen_container parseFloat'
        },
        // declare sortable values
            // sortBy: [ 'original-order' ]
        sortBy: [ 'power' , 'lumen', 'price' ]
    });

</script> 