@extends('main-layout', ['blog_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-blog.css')}}" rel="stylesheet">  

@endsection

@section('javascript-lib')
    <script src="{{App\Util::asset('assets/js/master-blog.js')}}"></script>
@endsection

@section('title')
Blog{{App\Meta::webpageTitle()}}
@endsection

@section('main-content')

{{-- <div class="section_blog_container" style="background-image: url({{App\Util::asset('assets/images/blog/blog.jpg')}});"> --}}
<div class="section_blog_container">
<div class="backdrop"></div>
    
<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Blog</a>
    </h1>
</section>
@if($posts->count())

<section id="section_blog" class="section" >
    <div class="posts">

        @foreach($posts as $post)
        <article class="post">
            <header>
                <h1 class="post_title">
                    <?php
                        $route = route('blog_post_page',['slug'=>$post->b_slug]);
                    ?>

                    <a href="{{$route}}">
                        {{$post->b_title}}
                    </a>
                </h1>
                <p class="created_at">Posted {{date("M j, Y g:ia", strtotime($post->created_at))}}</p>                
                <p class="posted_by">&nbsp;by {{$post->b_author}}</p> 
            </header>
            <?php
                $matches = "";
                preg_match_all('/<img>(.*?)<\/img>/s', $post->b_content, $matches);
                $img = (count($matches[1])>=1)? $matches[1][0]:'';                
                $p_content = preg_replace('/<img>(.*?)<\/img>/s', '<img src="'.asset('assets/images/blog/posts/'.$img).'">', $post->b_content);
            ?>
            <p class="post_content"><?=nl2br($p_content)?></p>            
        </article>
        @endforeach

    </div>
</section>
@else

    <section id="image-holder" class="image-holder">
        <div class="backdrop"></div>
            <div class="container">
                <div class="warning-content">
                    <div class="svg-container">
                         <div class="container">
                                <div class="msg-container">
                                    <h1>Coming<span class="soon"> Soon </span> </h1>
                                    <p>
                                        Forgive us for the Inconvenience. <br />
                                        We are currently updating our <span class="bold"> Blog. </span>
                                        <span class="boldup"> In the mean time please check out other things in our Site. </span> 
                                    </p>
                                    <p class="bold">
                                        We're Excited to Launch Soon!
                                    </p>
                                </div>
                         </div>
                    </div>
                </div>
            </div>
    </section>

@endif



    
</div>
@endsection




