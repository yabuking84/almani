<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">

	<url> 
		<loc>https://almani.ae/blog</loc>
		<changefreq>monthly</changefreq>
		<priority>0.5</priority>
	</url>




	@foreach($posts as $post)
	<url> 
	    <loc>{{route('blog_post_page',['slug'=>$post->b_slug])}}</loc>
	    <changefreq>monthly</changefreq>
	    <priority>0.7</priority>
  	</url>
  	@endforeach



</urlset>
  