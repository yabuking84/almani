@extends('main-layout', ['legal_menu' => 'active','terms_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-terms.css')}}" rel="stylesheet">
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-terms.js')}}"></script>
@endsection



@section('title')
Terms & Conditions{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')
<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Terms & Conditions </a>
    </h1>
</section>


<section class="section">


    <div class="container">


        <div class="document">

            <p>Ordering Products from Almani Lighting L.L.C. constitutes acceptance of the terms set forth herein, as
                such terms may have been updated through the date of such order. Any different, or additional terms in
                any purchase order, blanket instructions or other writing from purchaser shall be deemed a material
                alteration hereof and are hereby expressly objected to and rejected and shall be of no force or effect.</p>
            <p>Commencement of performance or shipment shall not be construed as acceptance of any of purchaser's terms
                and conditions which are different from or in addition to those contained in the Agreement. Course of
                performance or usage of trade shall not be applied to modify these Terms and Conditions.</p>

            <h3 id="orderandconfirmation"><a href="{{route('terms_page_goto',['orderandconfirmation'])}}">Order and
                    Confirmation</a> <i class="fa fa-caret-down caret_down hide_group"></i><i class="fa fa-caret-right caret_right"></i></h3>
            <ul>
                <li>All orders must be placed in writing and delivered directly to Almani Lighting L.L.C. from the
                    entity that will be liable for the payment of the order. Verbal orders or orders through a third
                    party will not be accepted.</li>
                <li>No order is final as to Almani Lighting L.L.C. until accepted by Almani Lighting L.L.C. by written
                    acknowledgement. All orders that are accepted by Almani Lighting L.L.C. are subject to these Terms
                    and Conditions. After acceptance, requests to change orders must be submitted in writing to Almani
                    Lighting L.L.C. and are subject to 25% cancellation fees of total order value.</li>
                <li>Acceptance of any order is subject to availability of product and the ability of Almani Lighting
                    L.L.C. to deliver.</li>
                <li>Additions to orders are allowed provided the original order has not yet been released to
                    manufacturing.</li>
                <li>If the order confirmation differs from the order, Almani Lighting L.L.C. shall only be bound if it
                    has agreed to such deviation in writing. Acceptance of goods or services or payment does not
                    constitute such agreement.</li>
            </ul>

            <h3 id="cancellations"><a href="{{route('terms_page_goto',['cancellations'])}}">Cancellations</a> <i class="fa fa-caret-down caret_down hide_group"></i><i
                    class="fa fa-caret-right caret_right"></i></h3>
            <ul>
                <li>Stock Product - Buyer may cancel orders with written Stock Product notice to Almani Lighting L.L.C.
                    subject to the following conditions and with Almani Lighting L.L.C. written consent. If an order
                    for stocked product is stopped after picking prior to shipment buyer shall pay any costs associated
                    with this order.</li>
                <li>Non-Stock Product - Cancellation of non-stock product may be made only if no work has been
                    performed and no material purchased. If work has been performed any costs incurred will be charged
                    to the customer, which may include a cancellation charge up to the price of the product.</li>
            </ul>

            <h3 id="delay"><a href="{{route('terms_page_goto',['delay'])}}">Delay</a> <i class="fa fa-caret-down caret_down hide_group"></i><i
                    class="fa fa-caret-right caret_right"></i></h3>
            <ul>
                <li>Almani Lighting L.L.C. will use reasonable efforts to meet shipment or delivery dates specified by
                    Almani Lighting L.L.C., but such dates are estimates only.</li>
                <li>Almani Lighting L.L.C. will not be liable for any delay or non-delivery in shipping for any reason
                    but not limited to delay or non-delivery caused directly or indirectly by Acts of God, fire, flood,
                    strike or lockout or other labor dispute, accident, civil commotion, riot, war, governmental
                    regulation or order, whether or not it later proves to be invalid, or from any other cause or
                    beyond Almani Lighting L.L.C. control.</li>
            </ul>

            <h3 id="prices"><a href="{{route('terms_page_goto',['prices'])}}">Prices</a> <i class="fa fa-caret-down caret_down hide_group"></i><i
                    class="fa fa-caret-right caret_right"></i></h3>
            <ul>
                <li>All prices represent those in effect at the time of quotation and are subject to change without
                    notice.</li>
                <li>All quotations on special products or modifications to catalog items are binding only if confirmed
                    in writing by Almani Lighting L.L.C. for the period shown on the quotation.</li>
                <li>Pricing will be firm and assured for a period of thirty days from date of quotation from Almani
                    Lighting L.L.C.</li>
                <li>Any increase or reduction in the price as a result of changes affecting the execution of the works
                    must be notified to Almani Lighting L.L.C. immediately and is subject to Almani Lighting L.L.C.
                    written approval prior to the shipment of the goods or the provision of the service.</li>
            </ul>

            <h3 id="salesmaterialsandspecifications"><a href="{{route('terms_page_goto',['salesmaterialsandspecifications'])}}">Sales
                    Materials and Specifications</a> <i class="fa fa-caret-down caret_down hide_group"></i><i class="fa fa-caret-right caret_right"></i></h3>
            <ul>
                <li>Any catalog, specification or price sheet or other similar documentation prepared by Almani
                    Lighting L.L.C. is strictly for the convenience of the user and shall not be deemed as an offer to
                    sell.<strong></strong></li>
                <li>Almani Lighting L.L.C. believes such documentation is complete and accurate at time of printing,
                    but does not warrant that such documentation is error free.<strong></strong></li>
                <li>Products will be shipped in accordance with the standard styles and sizes as described in Almani
                    Lighting L.L.C. catalogs or, for special or made-to-order Products, in accordance with Almani
                    Lighting L.L.C. drawings and specifications sheets. <strong></strong></li>
                <li>In the event of a conflict between a customer’s written order and a Almani Lighting L.L.C. drawing
                    or specification sheet marked “approved” or the like, the Almani Lighting L.L.C. drawing or
                    specification sheet shall prevail. Almani Lighting L.L.C. reserves the right to change details of
                    design, materials and finish at any time without written notice.<strong></strong></li>
            </ul>

            <h3 id="termsofpayment"><a href="{{route('terms_page_goto',['termsofpayment'])}}">Terms of Payment</a> <i
                    class="fa fa-caret-down caret_down hide_group"></i><i class="fa fa-caret-right caret_right"></i></h3>
            <ul>
                <li>The terms of payment are subject to review of purchaser's credit by Almani Lighting L.L.C. Almani
                    Lighting L.L.C. shall have the right, at any time and from time to time, to require cash payments
                    in advance or a letter of credit or other assurance of payment satisfactory to Almani Lighting
                    L.L.C. as a condition to acceptance of any order or shipment of any Product.<strong></strong></li>
                <li>Unless otherwise agreed to by Almani Lighting L.L.C., payment shall be by check to be drawn on
                    purchaser's corporate account, by wire transfer to Almani Lighting L.L.C. account at a commercial
                    bank designated by Almani Lighting L.L.C., or by Almani Lighting L.L.C. draw upon a letter of
                    credit satisfactory in form and substance to Almani Lighting L.L.C.<strong></strong></li>
                <li>Unless otherwise agreed separately, the payment term is 50% advance upon order confirmation and 50%
                    balance before delivery.</li>
                <li>Almani Lighting L.L.C. shall not be bound to make any payment or otherwise fulfill the obligations
                    under the accepted order, as applicable, in the event of a conflict with national or international
                    foreign trade law regulations, embargos or other sanctions.</li>
            </ul>

            <h3 id="taxesandgovernmentalcharges"><a href="{{route('terms_page_goto',['taxesandgovernmentalcharges'])}}">Taxes
                    and Governmental Charges</a> <i class="fa fa-caret-down caret_down hide_group"></i><i class="fa fa-caret-right caret_right"></i></h3>
            <ul>
                <li>Prices do not include any taxes or other governmental fees, charges or assessments, including,
                    without limitation, value-added, sales, use or privileges taxes, required governmental withholdings
                    or excise or similar taxes levied by any government, now or hereafter enacted.<strong></strong></li>
                <li>In Almani Lighting L.L.C. discretion, any such taxes, charges or withholdings may be added to the
                    price for any Products or may be billed separately. purchaser will pay all such taxes and charges,
                    on or before their due dates.<strong></strong></li>
                <li>In the event Almani Lighting L.L.C. is required at any time to pay any such tax or charge,
                    purchaser will reimburse Manufacturer promptly on demand.<strong></strong></li>
            </ul>

            <h3 id="packagingandhandling"><a href="{{route('terms_page_goto',['packagingandhandling'])}}">Packaging and
                    Handling</a> <i class="fa fa-caret-down caret_down hide_group"></i><i class="fa fa-caret-right caret_right"></i></h3>
            <ul>
                <li>Products will be packaged as Almani Lighting L.L.C. deems appropriate for protection of goods
                    against damage due to normal handling. <strong></strong></li>
                <li>Unless specifically agreed to in writing, Almani Lighting L.L.C. will not be responsible for the
                    payment of any penalties or special handling charges relating to Almani Lighting L.L.C. failure to
                    comply with a customer’s special requirement for order processing, handling, packaging, shipping or
                    invoicing.<strong></strong></li>
            </ul>

            <h3 id="shipmentdeliveryandtitle"><a href="{{route('terms_page_goto',['shipmentdeliveryandtitle'])}}">Shipment,
                    Delivery and Title</a> <i class="fa fa-caret-down caret_down hide_group"></i><i class="fa fa-caret-right caret_right"></i></h3>
            <ul>
                <li>Almani Lighting L.L.C. will select the carrier and routing and ship Products freight prepaid and
                    added to the price of the Products. Almani Lighting L.L.C. may, in its discretion, choose to make
                    partial shipments and shall bill each shipment as it is made, but on terms applicable to the
                    complete order.<strong></strong></li>
                <li>Meeting the delivery date requires that the goods were received at the reception point by the
                    agreed delivery date.</li>
                <li>Delivery dates of all shipments are estimated and are not guaranteed. The shipment date mentioned
                    on Almani Lighting L.L.C. quote or order acknowledgement, if any, is Almani Lighting L.L.C.
                    approximation of a shipment date, and is not a fixed or guaranteed shipment date.<strong></strong></li>
                <li>Almani Lighting L.L.C. assumes no liability in connection with any delay in delivery.</li>
                <li>Deliveries exceeding the contracted quantity or advance deliveries require Almani Lighting L.L.C.
                    prior approval. Any additional costs incurred shall be borne by the contractor.</li>
                <!-- <li>Any claims for shortages,  losses, or damages sustained in transit shall be made by purchaser with the  carrier and must be documented on delivery receipt. Upon request, Almani  Lighting L.L.C. will provide evidence of delivery of Products to the carrier,  but reserves the right to charge a reasonable fee for all proof of delivery requests.<strong></strong></li> -->
                <li>Any claims for shortages, losses, damages or other discrepancies sustained in transit shall be made
                    by purchaser with the carrier and must be documented on delivery note. Otherwise, the delivery was
                    accepted by the purchaser and no further claims will be accepted. Upon request, Almani Lighting
                    L.L.C. will provide evidence of delivery of Products to the carrier, but reserves the right to
                    charge a reasonable fee for all proof of delivery requests.<strong></strong></li>
            </ul>

            <h3 id="warrantyterms"><a href="{{route('terms_page_goto',['warrantyterms'])}}"><i>Warranty Terms</i></a>
                <i class="fa fa-caret-down caret_down hide_group"></i><i class="fa fa-caret-right caret_right"></i></h3>

            <ul>
                <li>
                    Almani Lighting L.L.C./GmbH guarantees that Almani products are free from manufacturing and
                    material defects, provided a product is used in compliance with its intended use. The warranty
                    period for material defects and workmanship is 5 years from the invoice date.
                </li>
                <br>
                <p><b>The warranty shall only be valid provided:</b></p>
                <ul>
                    <li>The product is properly handled, installed and maintained by qualified technical staff in
                        compliance with the instructions provided along with the product.</li>
                    <li>Temperature and voltage limit values are not exceeded than the rated values and the product is
                        not subjected to mechanical load which do not comply with its intended use.</li>
                    <li>The product is used in compliance with the technical specifications and user’s instructions.</li>
                    <li>For luminaires installed outdoor:

                        <ul>
                            <br />
                            <li>Maximum ambient temperature should not exceed 35°C.</li>
                            <li>Products must be switched ON/OFF every day.</li>    
                            <li>Products must be switched ON no earlier than 30 minutes after sunset.</li>
                            <li>Products with IP rating of IP66 (or below) should not be installed in ground (recessed
                                installation). Furthermore, such products should not be installed in waterlogged areas.</li>
                            <br />
                        </ul>

                    </li>
                    <li>The customer has paid full amount in accordance with the payment terms of the relevant sales
                        contract.</li>
                </ul>
                <br>
                <li>Warranty does not apply to damage or failure to perform arising as a result of any Acts of God
                    and/or other Force Majeure events (including electric shocks, lightning) or from any abuse, misuse,
                    abnormal use or use in violation of any applicable standard, code or instructions for use including
                    without limitation those contained in the latest safety, industry and/ or electrical standards for
                    the relevant region(s). In the event and to the extent not defined in the Terms and Conditions,
                    Force Majeure means and includes any circumstances or occurrences beyond the reasonable control of
                    Almani Lighting - whether or not foreseeable at the time of the agreement - as a result of which
                    Almani Lighting cannot reasonably be required to execute its obligations including force majeure or
                    non-performance by suppliers or subcontractors of Almani Lighting.</li>
                <li>Almani Lighting reserves the right to make the final decision on the validity of any warrantee
                    claim. If requested by Almani Lighting, the non-conforming or defective Products shall become
                    Almani’ property as soon as they have been replaced. </li>
                <li>The warranty period is based on a burning behavior of max. 4,200 hrs/year and standard working
                    conditions. Lifetime specifications, e.g. 50.000 hours, are averaged estimates only and no
                    conclusion for the overall item lifetime can be guaranteed unless all influential factors like e.g.
                    environmental temperature are considered as well. Warranty shall be void in the event any repairs
                    or alterations, not duly authorized by Almani Lighting in writing, are made to the Product by any
                    person.</li>
                <li>For luminaires installed within 5km from the sea side, corrosion of luminaires (painted areas) is
                    included in the warranty only if luminaires are treated with MSP (Marine Salt Painting). For any
                    warranty claim, adequate records of operating history are kept and available for inspection by
                    Almani Lighting. An Almani Lighting representative will have access to the defective Products. If
                    the Products or other parts become suspect, the representative shall have the right to invite other
                    manufacturers’ representatives to evaluate the lighting systems. Labor costs for (de)-installation
                    of the Products are not covered under any Almani Lighting warranty.</li>
                <br />
                <p><b>The warranty does not cover:</b></p>

                <ul>
                    <li>Defects caused by power disturbances (surges).</li>
                    <li>Product defects caused by generator supply.</li>
                    <li>Defect caused by improper installation.</li>
                    <li>Damaged or defaced product.</li>
                    <li>Product subjected to neglect, accidental breakage or misuse, including but not limited to,
                        failing to operate product in accordance with Almani Lighting specifications.</li>
                    <li>Paintwork defects caused by chemical substances, fertilizers, water containing corrosive agents
                        and stray electrical currents.</li>
                    <li>Any parts subject to wear and tear, such as batteries, mechanical parts and water-proof
                        connectors.</li>
                </ul>
                <br>
                <li>Almani Lighting L.L.C. reserves the right to examine all failed products and in its sole discretion to determine whether any Products are covered under the terms of this Limited Warranty.</li>
                <li>Almani Lighting shall be free to decide at its sole discretion whether to repair or replace the product with the same or a similar product – subject to any technological progress that has occurred as from the release of the original product.</li>
                <li>The warranty shall not cover any extra costs resulting from any work required to repair the defect (e.g. costs incurred to assemble/disassemble the product or to transport the defective/repaired/new product as well as expenses incurred for disposal, allowances, travel, lifting devices and scaffolding). Said costs shall be charged to the customer.</li>
            </ul>

            {{-- hide this 10-9-18 // reason replace new content at the top --}}
            {{-- HIDE --}}

            <ul class="hide_group">
                <li>
                    Warranty does not apply to damage or failure to perform arising as a result of any Acts of God
                    and/or other Force Majeure events or from any abuse, misuse, abnormal use or use in violation of
                    any applicable standard, code or instructions for use including without limitation those contained
                    in the latest safety, industry and/ or electrical standards for the relevant region(s).In the
                    event and to the extent not defined in the Terms and Conditions, Force Majeure means and includes
                    any circumstances or occurrences beyond the reasonable control of Almani Lighting - whether or not
                    foreseeable at the time of the agreement - as a result of which Almani Lighting cannot reasonably
                    be required to execute its obligations including force majeure or non-performance by suppliers or
                    subcontractors of Almani Lighting. Warranty shall be void in the event any repairs or alterations,
                    not duly authorized by Almani Lighting in writing, are made to the Product by any person. The
                    manufacturing date of the product has to be clearly readable on the product label. Almani Lighting
                    reserves the right to make the final decision on the validity of any warrantee claim. If requested
                    by Almani Lighting, the non-conforming or defective Products shall become Almani’ property as soon
                    as they have been replaced.
                    <br>
                    <br> The warranty period for material defects and workmanship is 5 years from original purchase
                    from Almani Lighting L.L.C./GmbH or an authorized reseller. The warranty period is based on a
                    burning behaviour of max. 4,200 hrs/year and standard working conditions. Lifetime specifications,
                    e.g. 50.000 hours, are averaged estimates only and no conclusion for the overall item lifetime can
                    be guaranteed unless all influential factors like e.g. environmental temperature are taken into
                    account as well. For outdoor luminaires, the warranty is valid with a maximum ambient temperature
                    of +35°C. The warranty period is no longer valid when/if the driver settings of the product have
                    been changed by non-Almani Lighting authorized staff/partners and/or making use of non-Almani
                    Lighting authorized tools. The warranty is only applicable when the product is properly handled,
                    installed and maintained according to our instructions written in the mounting instructions of the
                    product, and taking into account the specific tolerances on flux and system power, as mentioned in
                    the product documentation available on our websites. With regards to the output of the light source
                    (defined, agreed and programmed by the time of purchase), a claim is only approved valid when the
                    useful life of the luminaire population is below LxxB10 during the warranty period of the
                    luminaire, where ‘xx’ shall be the value stated in the most recent version of the product datasheet
                    at the time of purchase, or as advised separately in writing directly by Almani Lighting GmbH.
                    <br>
                    <br> For luminaires installed within 5km from the sea side, corrosion of luminaires (painted areas)
                    is included in the warranty only if luminaires are treated with MSP (Marine Salt Painting). For any
                    warranty claim, adequate records of operating history are kept and available for inspection by
                    Almani Lighting. An Almani Lighting representative will have access to the defective Products. If
                    the Products or other parts become suspect, the representative shall have the right to invite other
                    manufacturers’ representatives to evaluate the lighting systems. Labour costs for (de)-installation
                    of the Products are not covered under any Almani Lighting warranty.

                </li>
                <li>The warranty will be void in the event the product is:
                    <ul>
                        <li>Improperly installed</li>
                        <li>Operated on generator</li>
                        <li>Damaged</li>
                        <li>Defaced</li>
                        <li>Subjected to neglect, accidental breakage or misuse, including but not limited to, failing
                            to operate Product in accordance with Almani Lighting L.L.C. specifications.</li>
                    </ul>
                </li>
                <li>Almani Lighting L.L.C. reserves the right to examine all failed products and in its sole discretion
                    to determine whether any Products are covered under the terms of this Limited Warranty.</li>
                <li>Defects which result in a refusal of acceptance as well as all defects determined upon transfer of
                    risks or during the warranty period have to be remedied by the contractor at its own costs or
                    contractor has to provide a replacement delivery without defects, at Almani Lighting L.L.C. choice.</li>
                <li>If the contractor does not remedy the defects or does not provide replacement delivery during an
                    adequate period of time determined by Almani Lighting L.L.C., Almani Lighting L.L.C. is entitled
                    to:
                    <ul>
                        <li>Partially or fully withdraw from the contract.</li>
                        <li>Request a price deduction. </li>
                        <li>Remedy the defect or effect replacement delivery itself, or have it remedied, and/or </li>
                        <li>Claim damages for a breach of contractual obligations.</li>
                    </ul>
                </li>
            </ul>
            {{-- HIDE --}}


            <h3>
                10-Year No Quibble Warranty (Projects)
                <i class="fa fa-caret-down caret_down hide_group"></i><i class="fa fa-caret-right caret_right"></i>
            </h3>

            <ul>
                {{-- <li> Almani Lighting LLC is offering a full ten-year warranty on all items in case our initial offer is accepted within 30-Days! Otherwise, standard warranty terms apply.</li> --}}
                <li>Almani Lighting LLC is offering a full ten-year warranty for selective projects on all items in case our initial offer is accepted within 30-Days! Otherwise, standard warranty terms apply.</li>
            </ul>


            <h3 id="returnpolicy"><a href="{{route('terms_page_goto',['returnpolicy'])}}"><i>Return Policy</i></a> <i
                    class="fa fa-caret-down caret_down hide_group"></i><i class="fa fa-caret-right caret_right"></i></h3>
            <ul>
                <li>Almani Lighting L.L.C. will replace or repair defective products within 30 working days upon our
                    approved inspection of the returned products.</li>
                <li>Almani Lighting L.L.C. will not be responsible for any costs or expenses caused by following
                    conditions:
                    <ul>
                        <li>Product that has been modified by the user.</li>
                        <li>Product was not installed or maintained according to manual or specification accompanied.</li>
                        <li>Product was subject to unusual physical or electrical stress, misuse, or negligence.</li>
                        <li>Other damages caused after receipt of product by customer.</li>
                    </ul>
                </li>
            </ul>

            <h3 id="returnprocedure"><a href="{{route('terms_page_goto',['returnprocedure'])}}"><i>Return Procedure</i></a>
                <i class="fa fa-caret-down caret_down hide_group"></i><i class="fa fa-caret-right caret_right"></i></h3>
            <ul>
                <li>To obtain approval of product return to Almani Lighting L.L.C., the customer needs to follow Almani
                    Lighting L.L.C. return procedures:
                    <ul>
                        <li>Detailed information has to be emailed to Almani Lighting L.L.C. It should include the
                            “customer name and address”, “Invoice number”, “Defective products quantity and items”,
                            “Test report of defective products or proof photos”.</li>
                        <li>Returned product will be inspected upon arrival at Almani Lighting L.L.C.</li>
                        <li>Almani Lighting L.L.C. requests 5 pieces of defective products for a detailed analysis.</li>
                    </ul>
                </li>
                <li>Almani Lighting L.L.C. does not pay compensation for the defected products. Almani Lighting L.L.C.
                    will only replace the defected products.</li>
                <li>Returned products will be inspected upon arrival at Almani Lighting L.L.C. Once a product is
                    determined to be defective, Almani Lighting L.L.C. will replace or repair the product within 30
                    working days.</li>
                <li>Any return without authorization of Almani Lighting L.L.C. will not be accepted.</li>
            </ul>

            <h3 id="returnedgoods"><a href="{{route('terms_page_goto',['returnedgoods'])}}"><i>Returned Goods</i></a>
                <i class="fa fa-caret-down caret_down hide_group"></i><i class="fa fa-caret-right caret_right"></i></h3>
            <ul>
                <li>Merchandise is not returnable without the written authorization from Almani Lighting L.L.C. Almani
                    Lighting L.L.C. is not obligated to consider requests to accept returned merchandise except for
                    Almani Lighting L.L.C. shipped in error by Almani Lighting L.L.C.<strong></strong></li>
                <li>If the return of goods is made necessary through any fault of Almani Lighting L.L.C. and permission
                    is granted for its return, Almani Lighting L.L.C. will give full credit including all
                    transportation charges if returned per transportation routing instructions of Almani Lighting
                    L.L.C.<strong></strong></li>
            </ul>

            <h3 id="generalterms"><a href="{{route('terms_page_goto',['generalterms'])}}"><i>General Terms</i></a> <i
                    class="fa fa-caret-down caret_down hide_group"></i><i class="fa fa-caret-right caret_right"></i></h3>
            <ul>
                <li><strong>Bankruptcy</strong> : If purchaser becomes insolvent, is unable to pay its debts when due,
                    files for bankruptcy, is the subject of involuntary bankruptcy, has a receiver appointed, or has
                    its assets assigned, Almani Lighting L.L.C. may cancel any unfulfilled obligations, or suspend
                    performance; however, purchaser’s financial obligations to Almani Lighting L.L.C. shall remain in
                    effect.<strong></strong></li>
                <li><strong>Severability</strong> : If any provision of these Terms and Conditions are deemed to be
                    illegal, unenforceable, or invalid, in whole or in part, the validity and enforceability of the
                    remaining provisions shall not be affected or impaired, and shall continue in full force and
                    effect.<strong></strong></li>
                <li><strong>Performance</strong> : The failure of purchaser or of Almani Lighting L.L.C. at any time to
                    require the performance of any obligation will not affect the right to require such performance at
                    any time thereafter. Course of dealing, course of performance, course of conduct, prior dealings,
                    usage of trade, community standards, industry standards, and customary standards and customary
                    practice or interpretation in matters involving the sale, delivery, installation, use, or service
                    of similar or dissimilar products or services shall not serve as references in interpreting these
                    Terms and Conditions.<strong></strong></li>
            </ul>

            <h3 id="confidentiality"><a href="{{route('terms_page_goto',['confidentiality'])}}"><i>Confidentiality</i></a>
                <i class="fa fa-caret-down caret_down hide_group"></i><i class="fa fa-caret-right caret_right"></i></h3>
            <ul>
                <li>Buyer agrees that any and all confidential information furnished by Almani Lighting L.L.C. in
                    connection with the sale of items will remain confidential. The Buyer agrees not to disclose any
                    such information to any other person, or use such information for any purposes other than
                    performance hereunder.</li>
            </ul>

        </div>
        <!-- careers_tab -->

    </div>


    </div>


</section>
@endsection