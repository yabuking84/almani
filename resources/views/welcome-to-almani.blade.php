@extends('main-layout', ['company_menu' => 'active' ,'welcometoalmani_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-welcome-to-almani.css')}}" rel="stylesheet">  

@endsection

@section('javascript-lib')
    <script src="{{App\Util::asset('assets/js/master-welcome-to-almani.js')}}"></script>
@endsection

@section('title')
        Welcome to Almani{{App\Meta::webpageTitle()}}
@endsection
        
@section('main-content')

@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif

<div class="section">
  <section class="section section_title" >
                    <h1 class="title main_title" >
                        <a class="button is-primary">Welcome to Almani </a>
                    </h1>
            </section>
        <section class="section_container"    style="background-image: url({{App\Util::asset('assets/images/welcometoalmani/welcometoalmani.jpeg')}});">
                <div class="backdrop"></div>
                <div class="container">
                        <div class="section_message">

                                <div class="section_text m-pb m-pt">
                                        <div class="image-container">
                                            <div class="item">
                                                 <div class="img-shadow-cont">
                                                     <img class="img-shadow" src="{{App\Util::asset('assets/images/welcometoalmani/johannes-eidens-aachen-germany-almani-lighting3.png')}}" alt="Johannes Eidens Aachen Germany Almani Lighting"/>                                                     
                                                 </div>
                                            </div>
                                            <div class="item">
                                                <p class="lbl-ceo align-right"> Johannes N. Eidens (M.A.) <span class="role">  CEO and Founder ALMANI  </span> </p>
                                            </div>
                                        </div>
                                </div>
                                <div class="section_text">
                                        <div class="image-container">
                                                <div class="item">
                                                 <div class="img-shadow-cont">
                                                     <img class="img-shadow" src="{{App\Util::asset('assets/images/welcometoalmani/khalid-almutawa-partner-and-chairman-asas-holding-group.png')}}" alt="Khalid Almutawa Partner and Chairman ASAS Holding Group"/>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                        <p class="lbl-ceo align-right"> Khalid Almutawa <span class="role"> Partner and Chairman <a href="https://asasholding.ae/" target="_blank">ASAS Holding Group </a> </span> </p>
                                                </div>
                                        </div>
                                </div>

                                <div class="section_text welcome-message">
                                        <!-- <h1>Welcome to Almani Lighting!</h1> -->
                                        <p>
                                                By choosing to study our website, we conclude that you are a discerning LED procurer of LED for whom quality matters. Your choice is therefore very wise…
                                        </p>
                                        <p>
                                                Almani Lighting is committed to quality in all that we do. Indeed, it is the reason we established our business in 2015. Appalled at the poor standard of LED products and service across the GCC region, we launched Almani Lighting to raise the bar and offer consistent LED excellence. 
                                        </p>
                                        <p>
                                                We are proud to design, manufacture and install superb yet reasonably priced LED units and systems which deliver exceptional lighting, energy and cost savings, and longevity. That commitment to quality extends also to the way we do business: we build excellent, trusting relationships with clients who know they can rely on us for the right advice, support and services, every time.
                                        </p>
                                        <p>
                                                Excelling in large and complex LED challenges, we are delighted to invest our time, knowledge and care in your project from concept to completion and beyond. You can put your trust in us with 100% confidence, for we are at your service now and long into the future. 
                                        </p>

                                        <p>  Please browse our catalogue, explore our website and of course, call us at any time. We aspire to be your trusted LED partner, at your service whenever you need us.</p>
                             
                                </div>
                                  

                        </div>
                </div>
            </section>
</div>

          
@endsection




