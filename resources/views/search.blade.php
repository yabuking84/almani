@extends('main-layout', ['search_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-search.css')}}" rel="stylesheet">
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-search.js')}}"></script>
@endsection

@section('js-head-lib')
@parent
<script>

    var frcatalog, category_name, product_name, model_code, dimm, triac, v0_10, dali, emergency, wifi, cct_min, cct_max, lumen_min, lumen_max, power_min, power_max, ip_rating_min, ip_rating_min, price_min, price_max, currency;
    
    @if(isset($search_fld) && $search_fld) 
    var search_fld = '{{$search_fld}}';
    @else
    var search_fld = '';
    @endif

    @if (isset($frcatalog) && $frcatalog === '1')
         frcatalog = '{{$frcatalog}}';
         almani = '{{ $datacatalog['almani'] != null ? $datacatalog['almani'] : ''}}'
         camel = '{{ $datacatalog['camel'] != null ? $datacatalog['camel'] : ''}}'
         category_name = '{{ $datacatalog['category_name'] != null ? $datacatalog['category_name'] : '' }}'
         product_name = '{{ $datacatalog['product_name'] != null ? $datacatalog['product_name'] : '' }}'
         model_code = '{{ $datacatalog['model_code'] != null ? $datacatalog['model_code'] : '' }}'
         dimm = '{{ $datacatalog['dimm'] != null ? $datacatalog['dimm'] : '' }}'
         triac = '{{ $datacatalog['triac'] != null ? $datacatalog['triac'] : '' }}'
         v0_10 = '{{ $datacatalog['v0_10'] != null ? $datacatalog['v0_10'] : '' }}'
         dali = '{{ $datacatalog['dali'] != null ? $datacatalog['dali'] : '' }}'
         emergency = '{{ $datacatalog['emergency'] != null ? $datacatalog['emergency'] : '' }}'
         wifi = '{{ $datacatalog['wifi'] != null ? $datacatalog['wifi'] : '' }}'
         cct_min = '{{ $datacatalog['cct_min'] != null ? $datacatalog['cct_min'] : '' }}'
         cct_max = '{{ $datacatalog['cct_max'] != null ? $datacatalog['cct_max'] : '' }}'
         lumen_min = '{{ $datacatalog['lumen_min'] != null ? $datacatalog['lumen_min'] : '' }}'
         lumen_max = '{{ $datacatalog['lumen_max'] != null ? $datacatalog['lumen_max'] : '' }}'
         power_min = '{{ $datacatalog['power_min'] != null ? $datacatalog['power_min'] : '' }}'
         power_max = '{{ $datacatalog['power_max'] != null ? $datacatalog['power_max'] : '' }}'
         ip_rating_min = '{{ $datacatalog['ip_rating_min'] != null ? $datacatalog['ip_rating_min'] : '' }}'
         ip_rating_min = '{{ $datacatalog['ip_rating_max'] != null ? $datacatalog['ip_rating_max'] : '' }}'
         price_min = '{{ $datacatalog['price_min'] != null ? $datacatalog['price_min'] : '' }}'
         price_max = '{{ $datacatalog['price_max'] != null ? $datacatalog['price_max'] : '' }}'
         currency = '{{ $datacatalog['currency'] != null ? $datacatalog['currency'] : '' }}'
    @endif
    
</script>

@endsection


@section('title')
Search{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')


<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="is-primary"> FIND YOUR BEST ALMANI LED LIGHT</a>
    </h1>


    <h5 style="text-align: center;margin-bottom: 0;">
        <a class="btn btn-almani open_search_tab"><i class="fas fa-search"></i></a>
    </h5>
</section>


@isset($record)



@endisset



<section class="section">


    <div class="container has_search_tab" id="has_search_tab">

        <div class="search_tab show_tab">
            <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            <input type="hidden" class="csrf_token" value="{{csrf_token()}}" />
            <input type="hidden" class="href" value="{{route('submitsearch')}}" />

            <div class="close_search_tab_div">

                <button class="btn btn-almani close_search_tab">X</button>
            </div>

            <div class="form-group col-12" style="text-align: center; justify-content: center;">
                <h5 style="margin-bottom: 10px;">Search</h5>
            </div>
            <div class="w-100"></div>
            <!-- <div class="w-100"></div> -->

            <div class="search_fld_grps">
                <div class="search_fld_grp">

                    <div class="form-row">
                        <label class="col-12">Brand name</label>
                        <div class="form-group col" style="width: 170px;">
                            <div class="brand_options options">
                                <a href="javascript:void(0);" class="option btn-sm btn-option almani_brand selected">Almani Lighting</a>
                                <!-- <a href="javascript:void(0);" class="option btn-sm btn-option camel_brand">CamelLED</a> -->
                          <!--       <a href="javascript:void(0);" class="option btn-sm btn-option all_brand">All</a> -->
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12">Category name</label>
                        {{-- <input type="text" class="form-control category_name" placeholder="Category">
                         --}}
                         <div class="col-12">
                            <select class="category_id" multiple="multiple" placeholder="Category" style="width: 270px;">
                                <option></option>
                               @foreach($categories as $category)
                                   <option value="{{$category->category_id}}">{{$category->category_name}}</option>
                               @endforeach
                             </select>
                         </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12">Application Areas</label>
                        {{-- <input type="text" class="form-control model_code" placeholder="Model Code"> --}}
                        <div class="col-12">
                            <select class="application_id" multiple="multiple" placeholder="Application Areas" style="width: 270px;">
                                <option></option>
                               @foreach($app_areas as $app_area)
                                    <option value="{{$app_area->app_title}}">{{ ucwords($app_area->app_title) }}</option>     
                               @endforeach
                             </select>
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <label class="col-12">Product name</label>
                        {{-- <input type="text" class="form-control product_name" placeholder="Product"> --}}
                        <div class="col-12">
                            <select class="product_id" placeholder="Product" style="width: 270px;">
                                <option></option>
                               @foreach($products as $product)
                                    @if (!empty($product->product_name) || $product->name != "")
                                    <option value="{{strtolower($product->product_name)}}">{{ucwords(str_replace('[camel]', '', strtolower($product->product_name)))}}</option>     
                                    @endif
                               @endforeach
                             </select>
                        </div>
                    </div>
 
                   
                    <div class="form-row">
                        <label class="col-12">Model Code</label>
                        <input type="text" class="form-control model_code" placeholder="Model Code">
                    </div>

                   

                    <div class="form-row">
                        <label class="col-12">Dim Options</label>
                        <div class="form-group col" style="width: 170px;">
                            <div class="dim_options options">
                                <button class="option btn-sm btn-option dimm">DIMM</button>
                                <button class="option btn-sm btn-option triac">TRIAC</button>
                                <button class="option btn-sm btn-option v0_10">0-10v</button>
                                <button class="option btn-sm btn-option DALI">DALI</button>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col">Emergency</label>
                        <div class="form-group col fld_md">
                            <div class="options">
                                <button class="option btn-sm btn-option emergency">Enabled</button>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col">WIFI</label>
                        <div class="form-group col fld_md">
                            <div class="options">
                                <button class="option btn-sm btn-option wifi">Enabled</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="search_fld_grp">



                    <div class="form-row">
                        <label class="col-12">CCT</label>
                        <div class="form-group col fld_md">
                            <input type="text" class="form-control cct_min" placeholder="min">
                        </div>
                        <div class="form-group col fld_md">
                            <input type="text" class="form-control cct_max" placeholder="max">
                        </div>
                    </div>


                    <div class="form-row">
                        <label class="col-12">Lumen</label>
                        <div class="form-group col fld_md">
                            <input type="text" class="form-control lumen_min" placeholder="min">
                        </div>
                        <div class="form-group col fld_md">
                            <input type="text" class="form-control lumen_max" placeholder="max">
                        </div>
                    </div>


                    <div class="form-row">
                        <label class="col-12">Power</label>
                        <div class="form-group col fld_md">
                            <input type="text" class="form-control power_min" placeholder="min">
                        </div>
                        <div class="form-group col fld_md">
                            <input type="text" class="form-control power_max" placeholder="max">
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="col-12">IP Rating</label>
                        <div class="form-group col fld_sm">
                            <input type="text" class="form-control ip_rating_min" placeholder="min">
                        </div>
                        <div class="form-group col fld_sm">
                            <input type="text" class="form-control ip_rating_max" placeholder="max">
                        </div>
                    </div>


                    <div class="form-row price_row">
                        <label class="col-12">Price</label>
                        <div class="form-group col fld_bg">
                            <input type="text" class="form-control price_min" placeholder="min">
                        </div>
                        <div class="form-group col fld_bg">
                            <input type="text" class="form-control price_max" placeholder="max">
                        </div>
                        <div class="form-group col">
                            <select class="currency form-control fld_sm">
                                <option>AED</option>
                                <option>USD</option>
                                <option>EUR</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="w-100"></div>
            <div class="form-row col-12" style="justify-content:flex-end; padding-right: 5px;">
                <!-- <button class="btn-almani clear_search">clear</button> -->
                <button class="btn-almani submit_search">search<i class="fas fa-circle-notch fa-spin"></i></button>
            </div>

            <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        </div>





        <div class="products row" id="products">
            <!-- ///////////////////////////////////////////////////// -->


            <!-- ///////////////////////////////////////////////////// -->
        </div>

        <div class="pagination_nav">

        </div>


    </div>

</section>
@endsection





@section('html-templates')
<div class="product" id="product">
    {{-- <div class="col-sm-3"> --}}
        <div class="item-container col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
            <div class="prod_block">
                <!-- <img class="image-brand-identifier" src="https://almani.ae/assets/images/logo/logo-colored-xm.png?v=1.28" alt=""> -->
                <a class="item" href="">
                    <div class="img_container col-sm-12">
                        <img src="" alt="" />
                    </div>

                    <div class="more_details hide_group">
                        <div class="specs_legend">
                            <div class="specs_legend_lifespan">
                                <img alt="Lifespan" src="{{App\Util::asset('assets/images/icons/unit_icons/time.png')}}">
                                <label>Lifespan</label>
                            </div>

                            <div>
                                <img alt="Power" src="{{App\Util::asset('assets/images/icons/unit_icons/power.png')}}">
                                <label>Power</label>
                            </div>

                            <div>
                                <img alt="Kelvin" src="{{App\Util::asset('assets/images/icons/unit_icons/kelvin.png')}}">
                                <label>Kelvin</label>
                            </div>

                            <div>
                                <img alt="Lumens" src="{{App\Util::asset('assets/images/icons/unit_icons/lumens.png')}}">
                                <label>Lumens</label>
                            </div>

                        </div>

                        <div class="specs">
                            <span class="mini-specs" title="Lifespan" class="lifespan" data-lifespan="">
                                <img alt="time" src="{{App\Util::asset('assets/images/icons/unit_icons/time.png')}}">
                                {{-- --}}
                                <label for="" id="lifespan">12</label>
                            </span>
                            <span class="mini-specs" title="Power" class="power" data-power="">
                                <img alt="power" src="{{App\Util::asset('assets/images/icons/unit_icons/power.png')}}">
                                {{-- --}}
                                <label for="" id="power">12</label>

                            </span>
                            <span class="mini-specs" title="Kelvin" class="wattage" data-kelvin="">
                                <img alt="kelvin" src="{{App\Util::asset('assets/images/icons/unit_icons/kelvin.png')}}">
                                {{-- --}}
                                <label for="" id="wattage">12</label>

                            </span>
                            <span class="mini-specs" title="Lumens" data-lumens="">
                                <img alt="lumens" src="{{App\Util::asset('assets/images/icons/unit_icons/lumens.png')}}">
                                {{-- --}}
                                <label for="" id="lumens">12</label>

                            </span>
                            <div class="view_product" title="View Product">
                                <button class="btn btn-sm btn-almani goto_product">View</button>

                            </div>
                        </div>

                    </div>

                    <div class="prod_title">
                        <span class="prod_ip"></span>
                        <span class="prod_name"></span>
                        <span class="prod_code"></span>
                        @if((session('login_led_designer') || session('login_sales')))
                        <div class="prod_group prod_price_grp">
                            <div class="price_container" data-price_value_uae="">

                            </div>
                        </div>
                        @endif

                        <span class="prod_active"></span>

                    </div>
                </a>
            </div>
        </div>
    </div>


    <!-- <div class="product">
    <div class="col-sm-3">
        <div class="prod_block">
            <a class="item" href="https://almani.ddns.net:2019/almaninew/public/catalog/led-downlights/AL-DO-0990">
                <div class="img_container col-sm-12">
                    <img src="https://almani.ae/assets/images/products/m/led-downlights-dahn-2-al-do-0990-5496.jpg" alt="<span class='prod_name'>Dahn 2</span> AL-DO-0990 LED DOWNLIGHTS">
                </div>

                <div class="prod_title">
                    <span class="prod_ip">IP20</span> 
                    <span class="prod_name">Dahn 2</span> 
                    <span class="prod_code">AL-DO-0990</span>
                    <div class="prod_group prod_price_grp">
                        <div class="price_container" data-price_value_uae="238.91">
                            238.91 AED
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div> 
</div> -->

    @endsection