@extends('main-layout', [
    'projects_menu' => 'active'
])


@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-projects.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-projects.js')}}"></script>
@endsection


@section('title')
Projects{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')

@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif

<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Projects</a>
    </h1>
</section>






<section class="section">
<div class="container projects_container">

<div class="row items">
@foreach($projects as $project)

    <div class="col column">
    <div class="item see_project" 
            data-toggle="modal" 
            data-target="#project_modal"
            data-image="<?=$project->imagePath()?>"
            data-title="<?=$project->p_title?>"
            data-location="<?=$project->p_location?>"
            data-description="<?=$project->p_description?>"
            data-project_value="<?=$project->p_project_value?>">
        <div class="desc_img" style="background-image: url(<?=$project->imagePath()?>);">
            <div class="desc_filter"></div>
            <div class="desc_text">

                <div class="project_details">
                    <h1><?=$project->p_title?></h1>
                    <h2><?=$project->p_location?></h2>
                </div>

            </div>

        </div>
    </div>
    </div>

@endforeach
</div>

<div class="pagination_nav"> {{$projects_link}} </div>

</div>
</section>
@endsection



 




@section('modal')
<div class="modal fade" id="project_modal" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="" 
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                
                <h1 class="title"></h1>

                <div class="desc">
                    <div class="desc_img">
                        <img src="" />
                    </div>
                    <div class="desc_text">

                        <span class="location">
                            <h3>Location</h3>
                            <p></p>
                        </span>

                        <span class="description">
                            <h3>Description</h3>
                            <p></p>
                        </span>

                        <span class="project_value">
                            <h3>Project Value</h3>
                            <p></p>
                        </span>

                    </div>
                </div>   
                             
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">Close</button>
            </div> -->
        </div>
    </div>
</div>
@endsection







