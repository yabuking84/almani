@extends('main-layout', ['products_menu' => 'active'])

@section('title')
Application Areas {{App\Meta::webpageTitle()}}
@endsection

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-application-area-landing.css')}}" rel="stylesheet">
@endsection
@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-application-area-landing.js')}}"></script>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
@endsection

@section('js-head-lib')
@parent
@endsection

@section('meta')
@endsection



@section('main-content')

@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif

<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        Application Areas
    </h1>
</section>

<div class="col-12">
    <div class="container">
        <div class="app-container">
            
            @foreach ($app_areas as $app_area)
                @php
                     $app_products = $app_area->getProducts();
                @endphp

                @if ($app_products->count())
                    <div class="app-item">
                            <a href="{{route('application_area_page', ['app_areas' => $app_area['app_slug'] ] ) }}">
                                <div class="item">
                                    <div class="img-container">
                                        <img src="{{App\Util::asset('assets/images/datasheets/application_areas/'. $app_area['app_image'].'.'.$app_area['app_extension'] )}}"
                                            alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="name-container">
                                        <p>{{$app_area['app_title']}}</p>
                                    </div>
                                </div>
                            </a>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>
@endsection