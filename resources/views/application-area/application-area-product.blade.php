@extends('main-layout', ['products_menu' => 'active'])

@section('title')
{{$app_area->app_title}}{{App\Meta::webpageTitle()}}
@endsection

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-catalog-product.css')}}" rel="stylesheet">
@endsection
@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-catalog-product.js')}}"></script>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>

<script type="text/javascript">
        var dataType;
    var sortType;

    var $grid = $('.grid');

    var $grid = $('.grid').isotope({
     itemSelector: '.item-container',
     // layoutMode: 'fitRows',
     getSortData: {
     // set sortable value
         price: '.price_container parseFloat',
         power: '.power_container parseFloat',
         lumen: '.lumen_container parseFloat'
     },
     // declare sortable values
         // sortBy: [ 'original-order' ]
     sortBy: [ 'price', 'power' , 'lumen' ]
    });
    </script>

   


@endsection

@section('js-head-lib')
@parent
<script>
    var category_name = "{{$app_area->app_title}}";
    var $glb_url = "{{route('application_area_page',['app_slug'=>$app_area->app_slug])}}";
</script>
@endsection



@section('meta')
<!-- META -->
<!-- <meta name="google-site-verification" content="" /> -->
<meta name="description" content="<?= $meta['description'] ?>" />
<meta name="author" content="Almani Lighting LLC">
<meta name="keywords" content="<?= $meta['keywords'] ?>" />
<meta property="og:title" content="<?= ucwords($meta['title']) ?>">
<meta property="og:type" content="article">
<meta property="og:url" content="{{route('application_area_page', [
                              'app_slug' => strtolower($app_area->app_slug),
                        ])}}">
<meta property="og:image" content="<?= $meta['image_url'] ?>">
<meta property="og:description" content="<?= $meta['description'] ?>" />
<!-- /META -->
@endsection


@section('breadcrumb')
@parent
<div class="container breadcrumb_container">
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{route('application_area_landing_page')}}">
                    APPLICATION AREAS
                </a>
            </li>
            <li class="breadcrumb-item active">
                {{$app_area->app_title}}
            </li>
        </ol>
    </nav>
</div>
@endsection


@section('main-content')

<section class="section section_title" style="">

    <h1 class="title main_title" style="">
        {{$app_area->app_title}}
    </h1>

</section>


<?php if($prod_count) { ?>
<section class="section section_products">

    {{-- search modal --}}
    <div class="container has_search_tab">
        <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
        <form action="{{route('search_nav_page')}}" id="form-search" method="POST">
            {{ csrf_field() }}
            <div class="search_tab">

                <div class="close_search_tab_div">
                    <a href="javascript:void(0);" class="btn btn-almani close_search_tab">X</a>
                </div>

                <div class="form-group col-12" style="text-align: center; justify-content: center;">
                    <h5 style="margin-bottom: 10px;">Search</h5>
                </div>

                <div class="search_fld_grps">

                    <div class="search_fld_grp">

                        <input type="hidden" value="1" name="frcatalog" class="frcatalog">
                        <input type="hidden" value="almani" name="brand_name" id="brand_name" class="brand_name">

                        <div class="form-row">
                            <label class="col-12">Brand name</label>
                            <div class="form-group col" style="width: 170px;">
                                <div class="brand_options options">

                                    <a href="javascript:void(0)" class="option btn-sm btn-option btn-almani_brand">Almani
                                        Lighting</a>
                                    <input type="hidden" name="almani" class="almani">
                                    <a href="javascript:void(0)" class="option btn-sm btn-option btn-camel_brand">CamelLED</a>
                                    <input type="hidden" name="camel" class="camel">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <label class="col-12">Category name</label>
                            <input type="text" class="form-control category_name" value="" name="category_name"
                                placeholder="Category">
                        </div>

                        <div class="form-row">
                            <label class="col-12">Product name</label>
                            <input type="text" class="form-control product_name" name="product_name" placeholder="Product">
                        </div>

                        <div class="form-row">
                            <label class="col-12">Model Code</label>
                            <input type="text" class="form-control model_code" name="model_code" placeholder="Model Code">
                        </div>

                        <div class="form-row">
                            <label class="col-12">Dim Options</label>
                            <div class="form-group col">
                                <div class="dim_options options">
                                    <a href="javascript:void(0);" class="option btn-sm btn-option btn-dimm">DIMM</a>
                                    <input type="hidden" name="dimm" class="dimm">
                                    <a href="javascript:void(0);" class="option btn-sm btn-option btn-triac">TRIAC</a>
                                    <input type="hidden" name="triac" class="triac">
                                    <a href="javascript:void(0);" class="option btn-sm btn-option btn-v0_10">0-10v</a>
                                    <input type="hidden" name="v0_10" class="v0_10">
                                    <a href="javascript:void(0);" class="option btn-sm btn-option btn-dali">DALI</a>
                                    <input type="hidden" name="dali" class="dali">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <label class="col-12">Emergency</label>
                            <div class="form-group col fld_md">
                                <div class="options">
                                    <a href="javascript:void(0)" class="option btn-sm btn-option btn-emergency">Enabled</a>
                                    <input type="hidden" name="emergency" class="emergency">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <label class="col-12">WIFI</label>
                            <div class="form-group col fld_md">
                                <div class="options">
                                    <a class="option btn-sm btn-option btn-wifi" href="javascript:void(0)">Enabled</a>
                                    <input type="hidden" name="wifi" class="wifi">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="search_fld_grp">

                        <div class="form-row">
                            <label class="col-12">CCT</label>
                            <div class="form-group col fld_md">
                                <input type="text" class="form-control cct_min" name="cct_min" placeholder="min">
                            </div>
                            <div class="form-group col fld_md">
                                <input type="text" class="form-control cct_max" name="cct_max" placeholder="max">
                            </div>
                        </div>

                        <div class="form-row">
                            <label class="col-12">Lumen</label>
                            <div class="form-group col fld_md">
                                <input type="text" class="form-control lumen_min" name="lumen_min" placeholder="min">
                            </div>
                            <div class="form-group col fld_md">
                                <input type="text" class="form-control lumen_max" name="lumen_max" placeholder="max">
                            </div>
                        </div>

                        <div class="form-row">
                            <label class="col-12">Power</label>
                            <div class="form-group col fld_md">
                                <input type="text" class="form-control power_min" name="power_min" placeholder="min">
                            </div>
                            <div class="form-group col fld_md">
                                <input type="text" class="form-control power_max" name="power_max" placeholder="max">
                            </div>
                        </div>

                        <div class="form-row">
                            <label class="col-12">IP Rating</label>
                            <div class="form-group col fld_sm">
                                <input type="text" class="form-control ip_rating_min" name="ip_rating_min" placeholder="min">
                            </div>
                            <div class="form-group col fld_sm">
                                <input type="text" class="form-control ip_rating_max" name="ip_rating_max" placeholder="max">
                            </div>
                        </div>

                        <div class="form-row price_row">
                            <label class="col-12">Price</label>
                            <div class="form-group col fld_bg">
                                <input type="text" class="form-control price_min" name="price_min" placeholder="min">
                            </div>
                            <div class="form-group col fld_bg">
                                <input type="text" class="form-control price_max" name="price_max" placeholder="max">
                            </div>
                            <div class="form-group col">
                                <select name="currency" class="currency form-control fld_sm">
                                    <option>AED</option>
                                    <option>USD</option>
                                    <option>EUR</option>
                                </select>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="form-row col-12" style="justify-content:flex-end; padding-right: 5px;">
                    <!-- <button class="btn-almani clear_search">clear</button> -->
                    <button class="btn-almani submit_search">search<i class="fas fa-circle-notch fa-spin"></i></button>
                </div>

            </div>
        </form>
        <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->

    </div>
    {{-- end search modal --}}

    <div class="container">
        <div class="form-row">
            <div class="form-group col">
                <div class="sort_options options">
                    <div class="sort-options">
                        <button class="option btn-sm btn-option price  selected" data-type="price" data-sort="desc"> <i
                                class="fas fa-money-bill-alt"></i>
                            Price <i class="fa fa-sort-down" aria-hidden="true"></i>
                        </button>
                        <button class="option btn-sm btn-option power" data-type="power" data-sort="desc"> <i class="fas fa-bolt"></i>
                            Power <i class="fa fa-sort-down" aria-hidden="true"></i>
                        </button>
                        <button class="option btn-sm btn-option lumens" data-type="lumens" data-sort="desc"> <i class="fas fa-sun"
                                aria-hidden="true"></i>
                            Lumens <i class="fa fa-sort-down" aria-hidden="true"></i>
                        </button>
                        {{-- <button class="option btn-sm btn-option wattage" data-type="wattage"> <i class="fas fa-sun"></i>
                            Wattage</button> --}}
                    </div>
                </div>

                <div class="search-div">
                    <div class="form-c form-inline">
                        <div class="select-container">
                            <div class="select">
                                <select id="itm_pg" class="itm_pg">
                                    <option value="15" selected="selected">Show 15</option>
                                    <option value="30">Show 30</option>
                                    <option value="80">Show 80</option>
                                    <option value="160">Show 160</option>
                                    <option value="500">Show 500</option>
                                </select>
                            </div>
                        </div>
                        <div class="select-container currency_container">
                            <div class="select">
                                <select id="currency" class="select_currency">
                                    <option selected="selected">AED</option>
                                    <option><i class="fas fa-dollar-sign"></i> USD</option>
                                    <option><i class="fas fa-euro-sign"></i> EUR</option>
                                </select>
                            </div>
                        </div>
                        <div class="search-btn-container d-none">
                            <button style="display:block;float:right;" class="btn-sm btn-almani search"><i class="fas fa-search"></i>
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="row grid justify-content-center product_items">

            <?php 
                    foreach($products as $prod) {
                    // foreach($category->products->sortBy('minPrice_eager') as $prod) {

                        // Image
                        //////////////////////////////////////
                        // $products_img = $prod->image('l');
                        $products_img = $prod->image('m');
                        //////////////////////////////////////

                        // Product Code
                        //////////////////////////////////////
                        $prod_code = '<span class="prod_code">'.$prod->product_code.'</span>';
                        //////////////////////////////////////

                        // Product IP
                        //////////////////////////////////////
                        $prod_ip = ($prod->product_ip)?'<span class="prod_ip">IP'.$prod->product_ip.'</span>':'';
                        //////////////////////////////////////

                        // Product Name
                        //////////////////////////////////////
                        $prod_name = "<span class='prod_name'>".ucwords(str_replace('[camel]', '', strtolower($prod->product_name)))."</span>";
                        //////////////////////////////////////

                        // if no LED is present
                        //////////////////////////////////////
                        $alt = $prod_name.' '.$prod->product_code.' '.$prod->category->category_name;
                        if (strpos($alt, 'LED') == false) {
                            $alt = $alt.' LED';
                        }
                        $alt = strip_tags($alt);
                        //////////////////////////////////////                
                ?>



            <!-- <div class="item-container col-sm-3"> -->
            <div class="item-container col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                <div class="prod_block">
                    <a class="item" href="{{route('product_page', [
                                                    'category_alias' => strtolower($prod->category->category_alias),
                                                    'product_code' => $prod->product_code,
                                                ])}}">

                        @if(App\Util::checkIfNew($prod->product_date_added))
                        <img class="new_icon" src="{{App\Util::asset('assets/images/icons/new.png')}}">
                        @endif


                        <div class="more_details hide_group">
                            <div class="specs_legend">
                                <div class="specs_legend_lifespan">
                                    <img alt="Lifespan" src="{{App\Util::asset('assets/images/icons/unit_icons/time.png')}}">
                                    <label>Lifespan</label>
                                </div>

                                <div>
                                    <img alt="Power" src="{{App\Util::asset('assets/images/icons/unit_icons/power.png')}}">
                                    <label>Power</label>
                                </div>

                                <div>
                                    <img alt="Kelvin" src="{{App\Util::asset('assets/images/icons/unit_icons/kelvin.png')}}">
                                    <label>Kelvin</label>
                                </div>

                                <div>
                                    <img alt="Lumens" src="{{App\Util::asset('assets/images/icons/unit_icons/lumens.png')}}">
                                    <label>Lumens</label>
                                </div>

                            </div>

                            <div class="specs">
                                <span class="mini-specs" title="Lifespan" class="lifespan" data-lifespan="{{$prod->lifespan()}}">
                                    <img alt="time" src="{{App\Util::asset('assets/images/icons/unit_icons/time.png')}}">
                                    {{$prod->lifespan()}}
                                </span>
                                <span class="mini-specs" title="Power" class="power" data-power="{{App\Util::changeDash((trim($prod->power())=='-')?'N/A':$prod->power())}}">
                                    <img alt="power" src="{{App\Util::asset('assets/images/icons/unit_icons/power.png')}}">
                                    {{App\Util::changeDash((trim($prod->power())=='-')?'N/A':$prod->power())}}
                                </span>
                                <span class="mini-specs" title="Kelvin" class="wattage" data-kelvin="{{App\Util::changeDash((trim($prod->kelvin())=='-')?'N/A':$prod->kelvin())}}">
                                    <img alt="kelvin" src="{{App\Util::asset('assets/images/icons/unit_icons/kelvin.png')}}">
                                    {{App\Util::changeDash((trim($prod->kelvin())=='-')?'N/A':$prod->kelvin())}}
                                </span>
                                <span class="mini-specs" title="Lumens" data-lumens="{{App\Util::changeDash(($prod->product_lumen=='9999999')?'N/A':$prod->product_lumen)}}">
                                    <img alt="lumens" src="{{App\Util::asset('assets/images/icons/unit_icons/lumens.png')}}">
                                    {{App\Util::changeDash(($prod->product_lumen=='9999999')?'N/A':$prod->product_lumen)}}
                                </span>
                                <div class="view_product" title="View Product">
                                    <button class="btn btn-sm btn-almani goto_product">View</button>
                                </div>
                            </div>

                        </div>
                        <div class="power_container d-none">
                            {{App\Util::changeDash((trim($prod->power())=='-')?'N/A':$prod->power())}} </div>
                        <div class="lumen_container d-none">
                            {{App\Util::changeDash(($prod->product_lumen=='9999999')?'N/A':$prod->product_lumen)}}
                        </div>
                        <div class="img_container col-sm-12">
                            <img src="{{App\Util::asset($products_img)}}" alt="{{$alt}}">
                        </div>

                        <div class="prod_title">
                            <?=$prod_ip?>
                            <?=$prod_name?>
                            <?=$prod_code?>
                            <div class="prod_group">
                               @if($isOffice && (session('login_led_designer') || session('login_sales')))
                                @if($prod->minPrice()=="AVAILABLE UPON REQUEST")
                                AVAILABLE UPON REQUEST
                                @else
                                <div class="price_container" data-value="<?=$prod->minPrice()?>" data-price_value_uae="<?=$prod->minPrice()?>">
                                    <?=$prod->minPrice()?> AED
                                </div>
                                @endif
                                @else
                                <br>
                                @endif
                            </div>
                        </div>
                    </a>
                </div>
            </div>


            <?php } ?>
            <!-- foreach($category->getProductsAndSubProductsNonClosure() as $prod) -->

        </div>

        <div class="pagination_nav"> {{$products_links}} </div>

    </div>

</section>

<?php } else {?>

<section class="section section_products no_products">
    <div class="container">
        <p>
            Almani Lighting currently doesn't have any products available under
            <strong>{{$app_area->app_title}}</strong>.
            You can check our other brand, CamelLED.
            Please click below to view:
        </p>

        <div class="camel_link">
            <a href="https://camel.almani.ae/application-area/{{$app_area->app_slug}}" target="_blank">
                <img src="{{App\Util::asset('assets/images/logo/camel.png')}}" alt="Camel LED" title="Visit Camel LED!"
                    class="camel_logo">
            </a>
        </div>

        <p>
            For more information, please call us at <a href="tel:+97148873265"><strong>+971 4-887-3265</strong></a>.
            or <a class="btn-almani btn-sm send_message" data-toggle="modal" data-target="#contact_us_modal">send us a
                message</a> to help you.
        </p>
    </div>
</section>

<?php } ?>



@endsection