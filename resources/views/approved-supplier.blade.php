@extends('main-layout', ['approvedsupplier_menu' => 'active', 'company_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-approved-supplier.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-approved-supplier.js')}}"></script>
@endsection



@section('title')
Approved Supplier{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')

@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif


<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Approved Supplier</a>
    </h1>
</section>


<section class="section">

    <div class="container">
        <div class="intro_text">
            <h1 class="title">            
                If you are a construction client or consultant, then this information could save you a FORTUNE!
            </h1>
            <p class="title_p">
                These are the facts – plain and simple..
            </p>

        </div>
    </div>


    <div class="container" id="plainandsimple">
        <div class="img_text_boxes">

            <?php
                $items = [];
                $items[] = ['image'=>'manufactures.jpg','text'=>'Almani Lighting GmbH manufactures, supplies and installs only the highest quality LED units on the market – equal to or better than the famous brands.'];
                $items[] = ['image'=>'prices.jpg','text'=>'Almani Lighting LED prices are much, much lower than the well-known brands.'];
                $items[] = ['image'=>'customercare.jpg','text'=>'Almani service and customer care are the finest on the market: just ask our clients!'];
                $items[] = ['image'=>'approved.jpg','text'=>'You need to make sure that Almani Lighting is on your \'Approved Suppliers\' list!'];
            ?>
            @foreach($items as $item)
            <div class="img_text_box">
                <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/approvedsupplier/'.$item['image'])}});">
                </div>
                <div class="text_box">
                    <p><?=$item['text']?></p>
                </div>
            </div>
            @endforeach

        </div>


    </div>




    <div class="whyitpays" style="background-image: url({{App\Util::asset('assets/images/approvedsupplier/approvedsupplier.jpg')}});">
        <div class="backdrop"></div>
        <h1 class="title">
            WHY IT PAYS TO CHOOSE ALMANI AS AN APPROVED SUPPLIER
        </h1>        
        <p class="title_p" style="">    
            No matter how far into the construction planning process you might be, adding Almani Lighting to your Approved Suppliers list is the wisest move you can make. 
            <br><br>
            You see, most clients and consultants assume that the only way to buy quality LED is to specify the biggest brands. But with Almani Lighting’s absolute commitment to high German standards and guaranteed quality at lowest possible prices, there’s no longer any need to pay a high price for the finest LED products. 
        </p>
    </div>



    <div class="container">
        <div class="overview_poor" >
            <div class="backdrop"></div>
            <div class="overview">
                <h1 class="title">
                    At Almani, we value large LED projects of all kinds, making certain that your project benefits from:
                </h1>
                <ul>
                    <li>Products that match or exceed your big-brand specification</li>
                    <li>Major cost savings</li>
                    <li>Exceptional value</li>
                    <li>One single supplier able to fulfill ALL your LED needs</li>
                    <li>Full compliance</li>
                    <li>Excellent guarantees</li>
                </ul>
            </div>
            <div class="poor_quality">
                <div class="poor_quality_img" style="background-image: url({{App\Util::asset('assets/images/approvedsupplier/valuelargeledprojects.jpg')}});">
                    
                </div>
            </div>
        </div>
    </div>




    <div class="bar_text">
        <p class="title_p" style="">
            Want to know more? <br>
            <a class="prompt_callback" data-toggle="modal" data-target="#contact_us_modal">CLICK HERE</a> for a prompt callback.
        </p>
        <h3 class="title"></h3>        
    </div>



        <div class="container" id="downloadables">    
             <div class="download-container">
            <div class="download-item">
                    <div class="img-container">
                        <a href="{{ asset('assets/pdfs/approved-supplier.pdf') }}" target="_blank">
                            <img class="animate flipInY animated" src="{{ App\Util::asset('assets/images/covers/approvedsupplier-cover.png') }}" alt="Download Price Match Promise" class="download-img-center"
                                class="almani-catgalog">
                            <span class="dl-button"><button class="btn-almani">Download Flyer</button> </span>
                        </a>
                    </div>
            </div>
    </div>
</div>





</section>
@endsection




