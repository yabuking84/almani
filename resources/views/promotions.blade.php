@extends('main-layout', ['promotions_menu'=> 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-promotions.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-promotions.js')}}"></script>
@endsection

@section('title')
    Promotions{{App\Meta::webpageTitle()}}
@endsection

@section ('main-content')

<div style="background-image: url({{App\Util::asset('/assets/images/homeowners/promo2.jpg')}}); background-size: cover; background-position: center; background-repeat: no-repeat; height: 100vh;" class="d-flex justify-content-center align-items-center">
	<div class="container d-flex justify-content-center">
		<div class="size">
			<div class="text-center pb-3">
				<img src="{{App\Util::asset('/assets/images/logo/logo-white.png')}}" class="img-size">
				<h5 class="mt-3 text-white">Almani Lighting - the UAE finest LED Choice </h5>
			</div>
			<h3 class="text-center pb-3 text-white">
				<b>
					Please select which of the following fits you best?
				</b>
			</h3>
			<div>
				<div class="card-deck">
					<div class="card-size" style="">
						<div class="card card-body h-100 d-flex justify-content-center align-items-center" style="background-color: black; box-shadow: 12px 9px 20px 0px #232323; border: 1px solid white;">
							<div class="text-white text-center">
								<h4 class="mb-1">
									<b>
										<div class="link-text">
											I am a Property Owner
										</div>
										<div class="link-icon">
											<i class="fas fa-arrow-right"></i> 
										</div>
									</b>
								</h4>
								<p class="m-0">Villa, Tower, Apartment, Hotel etc. </p>
							</div>
						</div>
					</div>
					<div class="card-size" style="">
						<div class="card card-body h-100 d-flex justify-content-center align-items-center" style="background-color: black; box-shadow: 12px 9px 20px 0px #232323; border: 1px solid white;">
							<div class="text-white text-center">
								<h4 class="mb-1">
									<b>
										<div class="link-text">
											I am a Contractor
										</div>
										<div class="link-icon">
											<i class="fas fa-arrow-right"></i> 
										</div>
									</b>
								</h4>
								<p class="m-0">
									MEP Contractor, Developer etc.
								</p>
							</div>
						</div>
					</div>
					<div class="card-size second-row" style="">
						<div class="card card-body h-100 d-flex justify-content-center align-items-center" style="background-color: black; box-shadow: 12px 9px 20px 0px #232323; border: 1px solid white;">
							<div class="text-white text-center">
								<h4 class="mb-1">
									<b>
										<div class="link-text">
											I am a Consultant
										</div>
										<div class="link-icon">
											<i class="fas fa-arrow-right"></i> 
										</div>
									</b>
								</h4>
								<p class="m-0">
									Project Consultant, Interior Designer etc.
								</p>
							</div>
						</div>
					</div>
					<div class="card-size second-row" style="">
						<div class="card card-body h-100 d-flex justify-content-center align-items-center" style="background-color: black; box-shadow: 12px 9px 20px 0px #232323; border: 1px solid white;">
							<div class="text-white text-center">
								<h4 class="mb-1">
									<b>
										<div class="link-text">
											Other
										</div>
										<div class="link-icon">
											<i class="fas fa-arrow-right"></i> 
										</div>
									</b>
								</h4>
								<p class="m-0">
									Supplier, Manufacturer, Job Seeker etc.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection