@extends('main-layout', ['products_menu' => 'active', 'roi_menu' => 'active' ])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-roi.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-roi.js')}}"></script>
@endsection


@section('js-head-lib')
@parent
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuqUdjg_q8ZGWgtAgUo82w4IYj3WGGY6k" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js" async="" defer="" type="text/javascript"></script>
@endsection


@section('title')
Return of Investment{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')


@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif


<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Return of Investment</a>
    </h1>
</section>


<section class="section">
<div class="container white_bar">

    <div class="intro_text">
        <h3 class="title">Return On Investment Graph: Almani LED vs. Old Lighting</h3>        
    </div>

    <div id="ReturnGraph" style=""></div>
</div>



<div class="roi_section" style="background-image: url({{App\Util::asset('assets/images/roi/roi.jpg')}});">
    <div class="backdrop"></div> 
    
    <div class="container">

        
        <h3 class="roi_title">LED Lighting Savings Calculator</h3>
        <p class="roi_text">
            Use the calculator below to calculate your return on investment (ROI) payback period for installing Almani Lighting LED lights. The general rule of thumb is that it is worth replacing any traditional light to LED that is used for more than 2 hours a day. Please note, if labor/replacement costs would be considered as well - not included in our calculator – you will reach your breakeven point even faster!
        </p>        
        

        <div class="roi_fields">

            <form name="LEDCalc" id="LEDCalc" class="row">
              <div class="roi_field_tables">
                <div class="roi_field_table">
                  <table class="table">
                    <tbody>
                        <tr>
                            <th style="width: 30%;">Input values</th>
                            <th>LED Light</th>
                            <th>Light being replaced</th>
                        </tr>
                        <tr>
                            <td>Number of lights</td>
                            <td><input class="input" type="Text" name="NumLights" size="4" placeholder="0" onkeyup="CalculateReturn()"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Average hours of use per day</td>
                            <td><input class="input" type="Text" name="HoursUse" size="4" placeholder="0" onkeyup="CalculateReturn()"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Power cost, AED per kW/h</td>
                            <td><input class="input" type="Text" name="PwrCost" size="4" placeholder="0.29" onkeyup="CalculateReturn()"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Light wattage (W)</td>
                            <td><input class="input" type="Text" name="LEDWatts" size="4" placeholder="0" onkeyup="CalculateReturn()"></td>
                            <td><input class="input" type="Text" name="OLDWatts" size="4" placeholder="0" onkeyup="CalculateReturn()"></td>
                        </tr>
                        <tr>
                            <td>Purchase cost per LED fixture &amp; bulb (AED)</td>
                            <td><input class="input" type="Text" name="LEDBulbCost" size="4" placeholder="0" onkeyup="CalculateReturn()"></td>
                            <td><input class="input" type="Text" name="OLDBulbCost" size="4" placeholder="0" onkeyup="CalculateReturn()"></td>
                        </tr>
                        <tr>
                            <td>Average LED &amp; Bulb lifetime in months</td>
                            <td><input class="input" type="Text" name="LEDLifetime" size="4" placeholder="0" onkeyup="CalculateReturn()"></td>
                            <td><input class="input" type="Text" name="OLDLifetime" size="4" placeholder="0" onkeyup="CalculateReturn()"></td>
                        </tr>


                      </tbody>
                  </table>
                </div>
                <div class="roi_field_table">
                  <table class="table" id="output-values">
                        <tbody>
                            <tr>
                                <th class="is-hidden-touch" style="width: 20%;">Output values</th>
                                <th>LED Light</th>
                                <th>Light being replaced</th>
                                <th>Saving</th>
                            </tr>
                            <tr class="bold">
                                <td class="is-hidden-desktop has-text-centered" style="" colspan="4">
                                    <span>Power consumption per month (kW/h)</span>
                                </td>
                            </tr>
                            <tr class="sub_row">
                                <td class="is-hidden-touch">Power consumption per month (kW/h)</td>
                                <td><input class="input" type="Text" name="LEDPwrConsumption" disabled=""></td>
                                <td><input class="input" type="Text" name="OLDPwrConsumption" disabled=""></td>
                                <td><input class="input" type="Text" name="DifPwrConsumption" disabled=""></td>
                            </tr>
                            <tr class="bold">
                                <td class="is-hidden-desktop has-text-centered" style="" colspan="4">
                                    <span>CO2 saved per month (kg)</span>
                                </td>
                            </tr>
                            <tr class="sub_row">
                                <td class="is-hidden-touch">CO2 saved per month (kg)</td>
                                <td><input class="input" type="Text" name="LEDCO2" disabled=""></td>
                                <td><input class="input" type="Text" name="OLDCO2" disabled=""></td>
                                <td><input class="input" type="Text" name="DifCO2" disabled=""></td>
                            </tr>
                            <tr class="bold">
                                <td class="is-hidden-desktop has-text-centered" style="" colspan="4">
                                    <span>Running cost per month in AED</span>
                                </td>
                            </tr>
                            <tr class="sub_row">
                                <td class="is-hidden-touch">Running cost per month in AED</td>
                                <td><input class="input" type="Text" name="LEDRunningCost" disabled=""></td>
                                <td><input class="input" type="Text" name="OLDRunningCost" disabled=""></td>
                                <td><input class="input" type="Text" name="DifRunningCost" disabled=""></td>
                            </tr>
                            <tr class="bold">
                                <td class="is-hidden-desktop has-text-centered" style="" colspan="4">
                                    <span>Total cost over 6 months in AED</span>
                                </td>
                            </tr>
                            <tr class="sub_row">
                                <td class="is-hidden-touch">Total cost over 6 months in AED</td>
                                <td><input class="input" type="Text" name="LEDTotalCost1" disabled=""></td>
                                <td><input class="input" type="Text" name="OLDTotalCost1" disabled=""></td>
                                <td><input class="input" type="Text" name="DifTotalCost1" disabled=""></td>
                            </tr>
                            <tr class="bold">
                                <td class="is-hidden-desktop has-text-centered" style="" colspan="4">
                                    <span>Total cost over 1 year in AED</span>
                                </td>
                            </tr>
                            <tr class="sub_row">
                                <td class="is-hidden-touch">Total cost over 1 year in AED</td>
                                <td><input class="input" type="Text" name="LEDTotalCost3" disabled=""></td>
                                <td><input class="input" type="Text" name="OLDTotalCost3" disabled=""></td>
                                <td><input class="input" type="Text" name="DifTotalCost3" disabled=""></td>
                            </tr>
                            <tr class="bold">
                                <td class="is-hidden-desktop has-text-centered" style="" colspan="4">
                                    <span>Total cost over 3 years in AED</span>
                                </td>
                            </tr>
                            <tr class="sub_row">
                                <td class="is-hidden-touch">Total cost over 3 years in AED</td>
                                <td><input class="input" type="Text" name="LEDTotalCost5" disabled=""></td>
                                <td><input class="input" type="Text" name="OLDTotalCost5" disabled=""></td>
                                <td><input class="input" type="Text" name="DifTotalCost5" disabled=""></td>
                            </tr>
                            <tr class="bold">
                                <td class="is-hidden-desktop has-text-centered" style="" colspan="4">
                                    <span>Total cost over 5 years in AED</span>
                                </td>
                            </tr>
                            <tr class="sub_row">
                                <td class="is-hidden-touch">Total cost over 5 years in AED</td>
                                <td><input class="input" type="Text" name="LEDTotalCost10" disabled=""></td>
                                <td><input class="input" type="Text" name="OLDTotalCost10" disabled=""></td>
                                <td><input class="input" type="Text" name="DifTotalCost10" disabled=""></td>
                            </tr>
                        </tbody>
                  </table>
                </div>
              </div>
            </form>
        </div> <!-- roi_fields -->

        <p class="roi_text">
            Based on your input, this calculator provides an example of potential savings. 
            Almani Lighting does not warrant or make any representations regarding the use, validity, 
            or accuracy of the results of the tool. Actual savings may vary. 
            It will provide a close approximation of energy savings, maintenance savings, 
            and ROI of a lighting retrofit project. Actual results may vary due to individual parameters, usage and factors. 
            Please use this calculator at your own discretion. The payback period (breakeven point) 
            is when the green line crosses the red line. If the green line is always below the red line, 
            then the payback period is < 12 months. A conservative estimate of compounding electricity price 
            increases of 5% per year is included in the calculation. 
            You should enter in the price you are currently paying (AED per kW/h).
        </p>        
    </div>
</div>


<div class="container">
        <form class="form_quote" id="form_quote" method="post" action="{{route('sendcontactdetails')}}" enctype="multipart/form-data" style="justify-content: center;">
        {{ csrf_field() }}
            <div class="form-col">
                <div class="form-group" style="text-align: center;">
                    <label for="full_name" style="font-weight: 500; border-bottom: 1px solid black; font-size: 17px;">
                        Contact Us
                    </label>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-user"></i></span>            
                        </div>
                        <input name="full_name" class="form-control" type="text" placeholder="Full Name" >
                    </div>        
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                        </div>
                        <input name="email" class="form-control email" type="email" placeholder="Email" required>
                    </div>        
                </div>        
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-phone"></i></span>
                        </div>
                        <input name="tel" class="form-control" type="tel" placeholder="Phone / Mobile" >
                    </div>        
                </div>
                <div class="form-group">     
                    <textarea name="message" class="form-control textarea message" placeholder="Message.."></textarea>
                </div>        
                <div class="submit_div" style="">
                    <button id="btn-almani" type="submit" class="btn-almani">Submit</button>
                </div>
            </div>
            <input type="hidden" value="" name="recaptcha_response" id="recaptchaResponse">
        </form>
</div>


</section>
@endsection




