@extends('main-layout', ['homeowners_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-homeowners.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-homeowners.js')}}"></script>
@endsection



@section('js-head-lib')
@parent
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuqUdjg_q8ZGWgtAgUo82w4IYj3WGGY6k" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js" async="" defer="" type="text/javascript"></script>
@endsection


@section('title')
    Home Owners{{App\Meta::webpageTitle()}}
@endsection


@section('main-content')

@if (session('error') !== null)
<div class="container alert_container" style="margin-top: 0px; 
    margin-bottom: -66px;">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center; position: relative; top: 10px;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif


<section>
	<div class="d-flex align-items-center justify-content-center">
		<div class="w-100">
			<div class="jumbotron" style=" background-image: url({{App\Util::asset('/assets/images/homeowners/home1.jpg')}}); background-size: cover; ">
  				<div class="container h-100">
   			 		<div class="row h-100">
   			 			<div class="col-12 col-md-12 d-flex align-items-center text-center">
   			 				<div class="text-white w-100">
			  				 	<h2>Almani Lighting - the UAE's finest LED choice</h2>
			   			 		<p class="lead">Experience innovations on how you acquire LED lights for your project. <br> Our experts offer the best solutions for all your lighting needs. Consult with us today and we will help you save a fortune for all your projects.</p>
   			 					<a href="#contact" class="btn btn-light"> Get in Touch Now!</a>
   			 				</div>
   			 			</div>
   	<!-- 		 			<div class="col-12 col-md-6 d-flex align-items-center justify-content-center">
		   			 		<div class="card">
								<div class="card-body">
									<div class="text-center">
										<h3>
											<b class="text-center">Get the best consultation now!</b>
										</h3>
									</div>
									<form class="form_quote pt-3" id="form_quote" method="post" action="{{route('sendhomedetails')}}" enctype="multipart/form-data" style="justify-content: center;">
										{{ csrf_field() }}
		    							<input type="hidden" class="form_url" id="form_url" value="" name="">
		    							<div class="form-col">
		        							<div class="form-group">
		            							<div class="input-group">
		                							<div class="input-group-prepend">
		                    							<span class="input-group-text"><i class="fa fa-user"></i></span>           
		                							</div>
		                							<input name="full_name" class="form-control" type="text" placeholder="Full Name" >
		            							</div>        
		        							</div>
		        							<div class="form-group">
		        					    		<div class="input-group">
		        					        		<div class="input-group-prepend">
		        					            	<span class="input-group-text"><i class="fa fa-envelope"></i></span>
		        					        		</div>
		        					        		<input name="email" class="form-control email" type="email" placeholder="Email" required>
		        					    		</div>        
		        							</div>        
		        							<div class="form-group">
		        					    		<div class="input-group">
		        					        		<div class="input-group-prepend">
		        					            		<span class="input-group-text"><i class="fa fa-phone"></i></span>
		        					        		</div>
		        					        		<input name="tel" class="form-control" type="tel" placeholder="Phone / Mobile" >
		        					    		</div>        
		        							</div>
		        							<div class="form-group">     
		        					    		<textarea name="message" class="form-control textarea message" placeholder="Message.."></textarea>
		        							</div>   
		           							<input type="hidden" value="" name="recaptcha_response" id="recaptchaResponse">
		         							<div style="text-align: center;font-size: 12px">
			    								This site is protected by reCAPTCHA and the Google
									    		<a href="https://policies.google.com/privacy">Privacy Policy</a> and
									    		<a href="https://policies.google.com/terms">Terms of Service</a> apply.
			    							</div>
		    							</div>
		    							<div class="submit_div" style="">
		    					    		<button type="submit" class="btn-almani sendenquiry w-100 mt-3" disabled="disabled">Submit</button>
		    							</div>
									</form>
								</div>
							</div>
   			 			</div> -->
   			 		</div>
  				</div>
			</div>
			<!-- <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel"> -->
	<!-- 			<ol class="carousel-indicators">
		    		<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		    		<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		    		<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		  		</ol>
		  		<div class="carousel-inner">
		   			<div class="carousel-item active">
		   				<div class="w-100" style="height: 90vh">
		     				<img src="{{App\Util::asset('/assets/images/homeowners/home1.jpg')}}" class="d-block w-100"	alt="Home Owner Image 1">
		   				</div>
		   			</div>
		    		<div class="carousel-item">
		    			<div class="w-100" style="height: 90vh">
		      				<img src="{{App\Util::asset('/assets/images/homeowners/home2.jpg')}}" class="d-block w-100"	alt="Home Owner Image 2" style="position: relative; top: -100px;">
		    			</div>
		    		</div>
		    		<div class="carousel-item">
		    			<div class="w-100" style="height: 90vh;">
		      				<img src="{{App\Util::asset('/assets/images/homeowners/home3.jpg')}}" class="d-block w-100"	alt="Home Owner Image 3" style="position: relative; top: -100px;">
		    			</div>
		    		</div>
		  		</div> -->
		<!--   		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		    		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    		<span class="sr-only">Previous</span>
		  		</a>
		  		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		    		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		    		<span class="sr-only">Next</span>
		  		</a> -->
			<!-- </div> -->
			<div class="container-fluid d-flex justify-content-end align-items-center" style="position: absolute;">
					
			</div>
		</div>
	</div>
	<div class="mb-5">
		<div class="section section_title" style="">
    		<h1 class="title main_title" style="">
        		<a class="button is-primary">About Us</a>
    		</h1>
		</div>
		<div class="container">
    		<div class="img_text_boxes">
        		<div class="img_text_box">
            		<div class="img_box" id="mission" style="background-image: url({{App\Util::asset('assets/images/homeowners/about1.jpg')}});">
                		<!-- <img src="https://localhost/almaninew/public/assets/images/quality-assurance/quality.jpg?v=1.0"> -->
           		 	</div>
            		<div class="text_box">
                		<p class="mt-3">Almani Lighting designs, supplies and installs finest quality LED lighting, delivering maximum energy- and cost-efficiency to commercial and industrial clients throughout the GCC region and neighbouring countries.</p>
            		</div>
        		</div>
        		<div class="img_text_box" id="vision">
            		<div class="img_box" style="background-image: url({{App\Util::asset('assets/images/homeowners/about2.jpg')}});">
                		<!-- <img src="https://localhost/almaninew/public/assets/images/quality-assurance/trust.jpg?v=1.0"> -->
            		</div>
            		<div class="text_box">
                		<p class="mt-3">
                   	 		We will earn our place as a global leader in LED lighting design and systems, trusted to deliver the highest quality and value every time.
                		</p>
            		</div>
        		</div>
        		<div class="img_text_box" id="values">
            		<div class="img_box" style="background-image: url({{App\Util::asset('assets/images/homeowners/about3.jpg')}}); background-position: top;">
                		<!-- <img src="https://localhost/almaninew/public/assets/images/quality-assurance/award.jpg?v=1.0"> -->
            		</div>
            		<div class="text_box">
                		<p class="mt-3">
                    		The values which inspire and guide all that we do at Almani Lighting are heavily influenced by our German heritage.
                		</p>
            		</div>
        		</div>
    		</div>
		</div>
	</div>
	<div class="">
		<div class="benefits" id="almanibenefits">

		<?php
	    	$benefits = [];
	    	$benefits[] = ['img'=>'5yrs.png','text'=>'Almani excellence - a five-year warranty on every product'];
	    	$benefits[] = ['img'=>'quality.png','text'=>'Exceptional German Quality Control'];
	    	$benefits[] = ['img'=>'nodefects.png','text'=>'Zero tolerance for defects'];
	    	// $benefits[] = ['img'=>'check.png','text'=>'Raising the bar on quality, reliability and value'];
	    	$benefits[] = ['img'=>'idea.png','text'=>'LED from Almani: brighter, greener, and maximum value'];
	    	$benefits[] = ['img'=>'world.png','text'=>'World-beating products, crafted to the highest standards'];
	    	// $benefits[] = ['img'=>'like.png','text'=>'Your ideal LED lighting partner: experienced, insightful, involved'];
	    	$benefits[] = ['img'=>'gears.png','text'=>'We give you the edge in LED technologies'];
	    	$benefits[] = ['img'=>'edit.png','text'=>'Comprehensive LED lighting services, tailored for you'];
	    	$benefits[] = ['img'=>'work.png','text'=>'Standard or bespoke: we’ll work with you, your way'];
		?>

	    	<div class="section section_title" style="    padding-top: 10px;
    padding-bottom: 10px;">
    		<h1 class="title main_title" style="color: white;">
        		<a class="button is-primary">Why Choose Us</a>
    		</h1>
		</div>     
	    	<div class="benefits_items">
	        	@foreach($benefits as $benefit)
	        	<div class="benefits_item">
	            	<img src="{{App\Util::asset('assets/images/aboutus/benefits/'.$benefit['img'])}}">
	            	<p>
	                	{{$benefit['text']}}
	            	</p>
	        	</div>
	        	@endforeach
	    	</div>
	    	<div class="text-center mt-2">
	    		<a href="#contact" class="btn btn-light"> Get in Touch Now!</a>
	    	</div>
		</div>
	</div>
	<div class="mb-5 pb-3">
		<div class="section section_title" style="">
    		<h1 class="title main_title" style="">
        		<a class="button is-primary">Our Best Projects</a>
    		</h1>
		</div>

		<?php
	    	$projects = [];

	    	$projects[] = ['id' =>'project_one', 'title'=>'Sherena Residence', 'img'=> 'sherena-residences.jpg' ,'location' => 'Al Barari, Dubai', 'desc'=> 'Almani Lighting provided all interior and parking lights, facade and landscape lights for this trio of residential apartment towers.','value'=>'US $70.0 million'];

	    	$projects[] = ['id' =>'project_two', 'title'=> 'HH Sheikh Khalid Al Qassimi Palace', 'img'=> 'royal-palace-of-hh-sheikh-khalid.jpg' ,'location' => 'Al Noof 4, Sharjah', 'desc'=> 'This project involves the installation of all external and internal LED lighting systems.','value'=>'US $ 25.0 million'];

	    	$projects[] = ['id' =>'project_three', 'title' => 'Eclipse Towers', 'img'=> 'PRJAE16116345.jpg' ,'location' => 'Al Reem Island, Abu Dhabi', 'desc'=> 'The project involves construction of 2 commercial buildings comprising a ground floor and 4 common podium levels. Tower A will have 18 additional floors and a pent house and tower B will have 19 additional floors and a pent house.','value'=>'US $ 40.0 million'];

	    	$projects[] = ['id' =>'project_four', 'title'=>'The Opus', 'img'=> 'PRJAE0722635.jpg' ,'location' => 'Business Bay, Dubai', 'desc'=> 'The project involves the construction of the cube-shaped Opus Tower also known as the Floating Tower spread over an area of 23,226 square meters. <br><br>

The Opus Tower will be divided into three separate towers each connected by a common podium. The towers will consist of 19 floors, six basement levels, three podium levels and 2,000 parking berths.
<br><br>
The project will also include the ME by Melia hotel and serviced apartments within the complex, the hotel will consist of 93 rooms and suites, across 19 floors, including the ultra-luxurious Suite ME, as well as 98 serviced apartments and 15 restaurants.
<br><br>
The uppermost floor, which is located above the 50-meter bridge connection between the two towers includes facilities like a tranquility zone, a beach deck with reflective pool, a shaded roof terrace, media zone and a gymnasium. There will be floors dedicated to office space and four floors dedicated to retail, food and beverage outlets. There will also be a five-story basement. The retail design inspired by natural light and space, reflexive fitting patterns and will be applied in the glass facade to reduce the solar gains inside the building.','value'=>'US $ 680.0 million'];
		?>

		<div class="container">	
			<div class="card-deck">
			@foreach($projects as $project)
				<div class="card" style="height: 500px;">
	  				<img src="{{App\Util::asset('assets/images/ongoing-projects/projects/'.$project['img'])}}" class="card-img-top" alt="..." height="73%">
	  				<div class="card-body">
	    				<div class="card-text">
	    					<b>
								{{$project['title']}}
							</b><br>
							<i class="fas fa-map-marker-alt"></i> &nbsp; {{$project['location']}}
							<div class="mt-2 mb-2">
								<button class="btn btn-dark"  data-toggle="modal" data-target="#{{$project['id']}}">Know More</button>
							</div>
	    				</div>
	  				</div>
				</div>
			@endforeach
			@section('modal')
				@foreach($projects as $project)
					<div class="modal fade" id="{{$project['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  						<div class="modal-dialog modal-dialog-centered" role="document">
    						<div class="modal-content" style="">
      							<div class="modal-header">
        							<h5 class="modal-title" id="exampleModalCenterTitle">Project Details</h5>
        							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          								<span aria-hidden="true">&times;</span>
        							</button>
      							</div>
      							<div class="modal-body h-100">
       								<div class="row h-100">
       									<div class="col-12 col-md-12 h-100">
       										<img src="{{App\Util::asset('assets/images/ongoing-projects/projects/'.$project['img'])}}" class="card-img-top h-100">
       									</div>
       									<div class="col-12 col-md-12">
       										<div class="mt-3">
       											<h5 class="m-0">
       												<b>{{$project['title']}}</b>
       											</h5><br>
       											<i class="fas fa-map-marker-alt"></i> &nbsp; {{$project['location']}}<br>
       											<i class="fas fa-dollar-sign"></i> &nbsp;
       											{{$project['value']}}
       											<hr>
       											<h5 class="m-0">
       												<b>
       													Description
       												</b>
       											</h5>
       											<p>
       												<br>
       												{!! $project['desc'] !!}
       											</p>
       										</div>
       									</div>
       								</div>
      							</div>
    						</div>
  						</div>
					</div>
					@endforeach
			@endsection
			</div>
			<div class="mt-5 text-center">
				<a href="{{route('projects_page')}}" class="btn-almani btn btn-lg" target="_blank">View all Projects</a>
			</div>
		</div>
	</div>
	<div style="background-color: black;" class="pb-5" id="contact">
		<div class="container">
				
			<div class="section section_title" style="">
    			<h1 class="title main_title" style="">
        			<a class="button is-primary text-white">Contact Us</a>
    			</h1>
			</div>
			<div class="d-flex justify-content-center">
			<div class="card-size">
				<div class="card card-body">
					<p>
						<b>
							Get in touch to get the best light consultation.
						</b>
					</p>
					<form class="form_quote" id="form_quote" method="post" action="{{route('sendhomedetails')}}" enctype="multipart/form-data" style="justify-content: center;">
					{{ csrf_field() }}
		    			<input type="hidden" class="form_url" id="form_url" value="" name="">
		    			<div class="form-col">
		        			<div class="form-group">
		            			<div class="input-group">
		                			<div class="input-group-prepend">
		                    			<span class="input-group-text"><i class="fa fa-user"></i></span>
		                			</div>
		                			<input name="full_name" class="form-control" type="text" placeholder="Full Name" >
		            			</div>        
		        			</div>
		        			<div class="form-group">
		        				<div class="input-group">
		        					<div class="input-group-prepend">
		        						<span class="input-group-text"><i class="fa fa-envelope"></i></span>
		        					</div>
		        					<input name="email" class="form-control email" type="email" placeholder="Email" required>
		        				</div>        
		        			</div>        
		        			<div class="form-group">
		        				<div class="input-group">
		        					<div class="input-group-prepend">
		        					    <span class="input-group-text"><i class="fa fa-phone"></i></span>
		        					</div>
		        					<input name="tel" class="form-control" type="tel" placeholder="Phone / Mobile" >
		        				</div>        
		        			</div>
		        			<div class="form-group">     
		        				<textarea name="message" class="form-control textarea message" placeholder="Message.."></textarea>
		        			</div>   
		           			<input type="hidden" value="" name="recaptcha_response" id="recaptchaResponse2">
		         		<!-- 	<div style="text-align: center;font-size: 12px;">
			    			This site is protected by reCAPTCHA and the Google
								<a href="https://policies.google.com/privacy">Privacy Policy</a> and
								<a href="https://policies.google.com/terms">Terms of Service</a> apply.
			    			</div> -->
		    			</div>
		    			<div class="submit_div" style="">
		    				<button type="submit" class="btn-almani sendenquiry2 w-100 mt-3" disabled="disabled">Submit</button>
		    			</div>
					</form>
				</div>
			</div>
			</div>
		</div>
	</div>
</section>
@endsection