@extends('main-layout', [
    'aboutus_menu' => 'active',
    'company_menu' => 'active',
])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-aboutus.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-aboutus.js')}}"></script>
@endsection



@section('title')
    About Us{{App\Meta::webpageTitle()}}
@endsection

@section('main-content')

@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif


<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">About Us</a>
    </h1>
</section>

<section class="section">
<div class="container">
    <div class="img_text_boxes">
        <div class="img_text_box">
            <div class="img_box" id="mission" style="background-image: url({{App\Util::asset('assets/images/aboutus/mission.jpg')}});">
                <!-- <img src="{{App\Util::asset('assets/images/quality-assurance/quality.jpg')}}"> -->
            </div>
            <div class="text_box">
                <h1>Our Mission</h1>
                <p>Almani Lighting designs, supplies and installs finest quality LED lighting, delivering maximum energy- and cost-efficiency to commercial and industrial clients throughout the GCC region and neighbouring countries.</p>
            </div>
        </div>
        <div class="img_text_box" id="vision">
            <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/aboutus/vision.jpg')}});">
                <!-- <img src="{{App\Util::asset('assets/images/quality-assurance/trust.jpg')}}"> -->
            </div>
            <div class="text_box">
                <h1>Our Vision</h1>
                <p>
                    We will earn our place as a global leader in LED lighting design and systems, trusted to deliver the highest quality and value every time.
                </p>
            </div>
        </div>
        <div class="img_text_box" id="values">
            <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/aboutus/values.jpg')}});">
                <!-- <img src="{{App\Util::asset('assets/images/quality-assurance/award.jpg')}}"> -->
            </div>
            <div class="text_box">
                <h1>Our Values</h1>
                <p>
                    The values which inspire and guide all that we do at Almani Lighting are heavily influenced by our German heritage.
                </p>
            </div>
        </div>
    </div>


</div>












<div class="container">
        <div class="video_container" style="" id="whychooseus">
            <div class="video_intro_img" 
                    data-toggle="modal" 
                    data-target="#video_modal"
                    data-video_url="https://www.youtube.com/embed/7-7jbUNA4PQ?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                <img src="{{App\Util::asset('assets/images/icons/youtube_play_icon.png')}}" alt="play">
                <p>
                   Why Choose us?
                </p>
            </div>

        </div>    
</div>









<div class="benefits" id="almanibenefits">

<?php
    $benefits = [];
    $benefits[] = ['img'=>'5yrs.png','text'=>'Almani excellence - a five-year warranty on every product'];
    $benefits[] = ['img'=>'quality.png','text'=>'Exceptional German Quality Control'];
    $benefits[] = ['img'=>'nodefects.png','text'=>'Zero tolerance for defects'];
    $benefits[] = ['img'=>'check.png','text'=>'Raising the bar on quality, reliability and value'];
    $benefits[] = ['img'=>'idea.png','text'=>'LED from Almani: brighter, greener, and maximum value'];
    $benefits[] = ['img'=>'world.png','text'=>'World-beating products, crafted to the highest standards'];
    $benefits[] = ['img'=>'like.png','text'=>'Your ideal LED lighting partner: experienced, insightful, involved'];
    $benefits[] = ['img'=>'gears.png','text'=>'We give you the edge in LED technologies'];
    $benefits[] = ['img'=>'edit.png','text'=>'Comprehensive LED lighting services, tailored for you'];
    $benefits[] = ['img'=>'work.png','text'=>'Standard or bespoke: we’ll work with you, your way'];
?>

    <h1 class="title">
        Almani Benefits
    </h1>        
    <div class="benefits_items">
        @foreach($benefits as $benefit)
        <div class="benefits_item">
            <img src="{{App\Util::asset('assets/images/aboutus/benefits/'.$benefit['img'])}}">
            <p>
                {{$benefit['text']}}
            </p>
        </div>
        @endforeach
    </div>
</div>


<div class="almaniidealpartner" id="almaniidealpartner">
    <div class="backdrop"></div>
    <h1 class="title">
        ALMANI LIGHTING <span> YOUR IDEAL LED PARTNER </span> 
    </h1>        
    <p class="title_p">
            Firmly rooted in our German principles of absolute quality and value, we aim to be and deliver the best the world has to oﬀer in LED solutions. Part of the respected <a style="border-bottom: 1px solid gainsboro;" href="https://asasholding.ae/" target="_blank">ASAS Holding Group</a> , we serve organizations in many sectors across and beyond the GCC region. By minimizing the supply chain and delivering consistent and excellent value, we develop enduring relationships with clients who trust us to do an exceptional job, time after time.
    </p>
</div>



<div class="container" id="whoweare" style="background-image: url({{App\Util::asset('assets/images/aboutus/whoweare.jpg')}});">
    <div class="backdrop"></div>

    <h1 class="title main_title" style="">
        <a class="button is-primary">Who we are</a>
    </h1>

    <?php 
        $items_d1 = [];
        $items_d1[] = ['title'=>'Excellence','text'=>'in all that we sell and all that we do'];
        $items_d1[] = ['title'=>'Reliability','text'=>'if we say it, we do it'];
        $items_d1[] = ['title'=>'Integrity','text'=>'building our business by earning trust'];
        $items_d1[] = ['title'=>'Positivity','text'=>'listen, understand, deliver, improve'];

        $items_d2 = [];
        $items_d2[] = ['title'=>'Value','text'=>'absolute quality at a fair price'];
        $items_d2[] = ['title'=>'Environmental care','text'=>'respecting and protecting our planet'];
        $items_d2[] = ['title'=>'Partnership','text'=>'individual strengths build ever-stronger teams'];
        $items_d2[] = ['title'=>'Dedication','text'=>'100% commitment to every client'];
    ?>
    <div class="div_container d1">
        @foreach($items_d1 as $item)
        <div>      
            <h3 class="title"><?=$item['title']?></h3>
            <p><?=$item['text']?></p>
        </div>
        @endforeach
    </div>
    <div class="div_container d2">
        @foreach($items_d2 as $item)
        <div>      
            <h3 class="title"><?=$item['title']?></h3>
            <p><?=$item['text']?></p>
        </div>
        @endforeach
    </div>
</div>














<section class="section section_title testimonials" id="testimonials">
<?php
    $testimonials = array();

    $testimonials[] = [ 'desc' => "I am very glad we chose Almani Lighting for the Sherena Residence. Not only were the LED lights of the highest quality, they also added to the elegance of our luxurious apartments.  Furthermore, the Almani team's outstanding LED knowledge and insights helped us greatly in selecting the right items for the many and varied needs of the entire building. I will definitely choose Almani Lighting for our future projects.", 'name' => 'Engr. Jaree Ali', 'position'=> 'Managing Partner, CVTEC Consulting Engineers', 'stars' => [ 1, 2, 3, 4, 5 ] ];
    $testimonials[] = [ 'desc' => "When Almani Lighting explained the many benefits of replacing our old car park lights with their LED tube lights, I soon realised that it was indeed a wise investment. The longer life span of these high quality LED units means lower maintenance costs for us, as well as improving safety and creating a great first impression of our premises. We are very happy that our parking area is now very well lit and that our tenants are giving us excellent feedback about it.", 'name' => 'Mr. Arindam Bose', 'position'=> 'Dubai Dar Facilities Management Manager','stars' => [ 1, 2, 3, 4 ] ];
    $testimonials[] = [ 'desc' => "In a busy hotel, the lights are always shining 24/7, so our energy consumption has always been high. However,  Almani Lighting's LED bulbs have given us a great opportunity to make huge savings. The bulbs also last far longer and come with a 3-year warranty. We have installed Almani's LED bulbs in the lobby, corridors and bedrooms and we couldn't be more satisfied with them.", 'name' => 'Engr. Haider Rizvi', 'position'=> 'Materials Manager,  Time Oaks Hotel & Suites', 'stars' => [ 1, 2, 3, 4, 5 ] ];
    $testimonials[] = [ 'desc' => "As a contractor, we always look for innovative ways to increase cost-efficiency and productivity. When building a factory recently, we chose Almani Lighting to supply LED industrial lights for many reasons such as the units'  5-year warranty, excellent cost-effectiveness and their water- and dust-proof qualities which are hugely beneficial given the factory's location. The LED units are also eco-friendly as they contain no mercury or other contaminants. We will definitely work with Almani again in the future.", 'name' => 'Engr. Prabu Rajappa', 'position'=> 'Lake Stone Contracting', 'stars' => [ 1, 2, 3, 4 ] ];
    $testimonials[] = [ 'desc' => "I ordered Almani Lighting's LED panel lights for my office and I must say it never looked this bright before! Almani also gave very good customer service and made sure everything was delivered on time. I am very pleased with both products and the entire team, and I will recommend them warmly to my friends.", 'name' => 'Mr. Khalid Almutawa', 'position'=> 'CEO, ASAS Holdings LLC', 'stars' => [ 1, 2, 3, 4, 5 ] ];
    $testimonials[] = [ 'desc' => "As a private home owner, I had no consultants to advise me on suitable LED lighting for my villa. Fortunately, Almani Lighting has great LED expertise! They were very helpful and handled everything with real professionalism. I would say they've raised the standards in the LED market here in Dubai and I was right to trust them to turn my house into a home!", 'name' => 'Mr. Khalid Abdullah', 'position'=> 'Private Sector, Dubai Resident', 'stars' => [ 1, 2, 3, 4 ] ];

?>

    <h1 class="title main_title" style="">
        <a class="button is-primary">Testimonials</a>
    </h1>
    <div class="container testimonials_container">
        <div class="owl-carousel owl-theme">
            @foreach($testimonials as $testimonial)
                <div class="item">
                        <div class="testimonial">
                            <center>
                                <div class="img">
                                    <?php $count = 0; ?>
                                    @foreach($testimonial['stars'] as $testimonial_stars)
                                            <span class="fa fa-star checked"></span>
                                            <?php $count++; ?>
                                    @endforeach
                                    @if($count == 4)
                                        <span class="fa fa-star"></span>
                                    @endif
                                </div>
                            </center>
                                    <div class="testimonies testimonies-mb"> {{ $testimonial['desc'] }} </div>
                                    <div class="profile">
                                        <p>
                                                <b> {{ $testimonial['name'] }}</b>
                                                <br>
                                                {{ $testimonial['position'] }}  
                                        </p>
                                </div>
                        </div>
                </div>
                @endforeach
        </div>
    </div>
</section>






<div class="bar_text" id="almaniapproach" style="background-image: url({{App\Util::asset('assets/images/aboutus/almani-approach.jpg')}});">
    <div class="backdrop"></div>
    <h1 class="title">
        The Almani Approach
    </h1>        
    <p class="title_p" style="">
        Quite simply, we aim to be and deliver the very best, every time. This is why we work exclusively with LED lighting products sourced only from meticulously vetted suppliers. Our quality control process is of prime importance to us and underpins our mission, vision and values. 
        <br><br>
        Our LED lights and systems are carefully selected to give the best quality light, excellent product durability and service and the greatest cost efficiency. We constantly update our product range and designs in line with technological advances and latest trends. Crucially, LED lighting also helps reduce your carbon footprint. 
        <br><br>
        We not only work with the finest and most environmentally friendly products, we also aspire to offer the highest standards of customer care and service. When we find an outstanding manufacturer who meets our high standards, we work with them closely to build sound, productive relationships and secure consistently excellent value. We are the sole link between the manufacturers and you, our client: you can view products in our Dubai showroom but we have no expensive retail outlets and no middle men to inflate costs. We believe that by keeping the supply chain simple, we can make certain that as an Almani client, you enjoy the best possible experience and outcomes. 
        <br><br>
        Friendly, helpful and always professional, we always go the extra distance to make Almani Lighting your global lighting partner of choice.
    </p>
</div>



<div class="container" id="clients">

<?php
    $client_logos = [];
    $client_logos[] = 'CVTEC-LOGO.png';
    $client_logos[] = 'Enova_logo.png';
    $client_logos[] = 'ifa-logo.png';
    $client_logos[] = 'radissonblu.png';
    $client_logos[] = 'movenpick_hotels_resorts_logo.png';
    $client_logos[] = 'Smart-Living-Logo.png';
    $client_logos[] = 'rcc.png';
    $client_logos[] = 'ejadah-logo.png';
    $client_logos[] = 'aandp.png';
    $client_logos[] = 'Damac_logo.png';
    $client_logos[] = 'emaar.png';
    $client_logos[] = 'landmark.png';
    $client_logos[] = 'delta.png';
    $client_logos[] = 'chirag_logo.png';
    $client_logos[] = 'Hafeet.png';
    $client_logos[] = 'asas.png';
    $client_logos[] = 'Al-Futtaim-Engineering-_-Technology_logo.png';
    $client_logos[] = 'gec-logo.png';
    $client_logos[] = 'edmac.png';
    $client_logos[] = 'civilmatrix.png';
    $client_logos[] = 'adce.jpg';
    $client_logos[] = 'rosehomes.png';
    $client_logos[] = 'emrill.png';
    $client_logos[] = 'alandalous.png';
    $client_logos[] = 'aswaaq.png';
    $client_logos[] = 'holidayinn.png';
    $client_logos[] = 'dubaimunicipality_logo.jpg';
    $client_logos[] = 'rotana.png';
    $client_logos[] = 'abdulrahim.png';
    $client_logos[] = 'deyaar.png';
    $client_logos[] = '90degree.png';
    $client_logos[] = 'CL_logo.png';
    $client_logos[] = 'Inaya-Logo.png';
    $client_logos[] = 'zayed.png';
    $client_logos[] = 'pharmgenomics.png';
    $client_logos[] = 'zoomdna-offwhite.png';
    $client_logos[] = 'dubaiconsult.png';
    $client_logos[] = 'Nakheel_Properties.png';
    $client_logos[] = 'lineplex.png';
    $client_logos[] = 'arabtec.png';
    $client_logos[] = 'capital-logo.png';
    $client_logos[] = 'DEWA-bills.png';
    $client_logos[] = 'neb.png';
    $client_logos[] = 'Omniyat-Logo.png';
    $client_logos[] = 'rta.png';
    $client_logos[] = 'serveu.png';
    $client_logos[] = 'SEWA.png';
    $client_logos[] = 'wasl.png';
    $client_logos[] = 'ati.png';




?>

   <div class="client-logos" id="client-logos">
        <div class="owl-carousel owl-theme owl-loaded">
            @foreach($client_logos as $client_logo)
            <div class="item">     
                <img alt="client logo" src="<?=asset('assets/images/aboutus/clients-logo/'.$client_logo)?>">                
            </div>
            @endforeach
        </div>
    </div>

</div>
</section>

@endsection




@section('modal')
<!-- Modal -->
<!-- //////////////////////////////////////////////////////// -->
<div class="modal fade youtube_video_modal" id="video_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
    <div class="modal-content">          
      <div class="modal-body">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <iframe class="video" style="" allowfullscreen></iframe>

        <div style="margin-top: 20px;">
            <button type="button" 
                class="btn-almani change_language" 
                data-target="#video_modal"
                data-language="english"
                data-video_url="https://www.youtube.com/embed/7-7jbUNA4PQ?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                English                    
            </button>
            <button type="button" 
                class="btn-almani change_language" 
                data-target="#video_modal"
                data-language="arabic"
                data-video_url="https://www.youtube.com/embed/ItkzAlzdX10?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                العَرَبِيَّة
            </button>                
        </div>

      </div>
    </div>
  </div>
</div>    
<!-- //////////////////////////////////////////////////////// -->
<!-- Modal -->
@endsection
