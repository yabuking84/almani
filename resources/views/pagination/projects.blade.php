@if ($paginator->hasPages())
    <nav aria-label="pagination Page navigation example">



    <ul class="pagination justify-content-center">

        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
        <li class="page-item disabled">            
            <a class="page-link pagination-next disabled" 
                tabindex="-1">
                Prev
            </a>
        </li>
        @else            
        <li class="page-item">        
            <a href="{{$paginator->previousPageUrl()}}" 
                class="pagination-previous page-link">
                Prev
            </a>
        </li>
        @endif


        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
            <li class="page-item">                
                <span class="pagination-ellipsis">&hellip;</span>
            </li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active">
                            <a class="page-link pagination-link is-current" 
                                aria-label="Page {{ $page }}" 
                                aria-current="page">
                                {{ $page }}

                            </a>
                        </li>
                    @else
                        <li class="page-item element">        
                            <a class="page-link pagination-link" 
                                aria-label="Goto page {{$page}}"
                                href="{{ $url }}">
                                {{ $page }}
                            </a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach


        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())            
            <li class="page-item">
                <a href="{{ $paginator->nextPageUrl() }}" 
                    class="pagination-next page-link">
                    Next
                </a>
            </li>
        @else
            <li class="page-item disabled">
                <a class="page-link pagination-next disabled" 
                    disabled>
                    Next
                </a>
            </li>
        @endif


    </ul>

    </nav>
@endif

