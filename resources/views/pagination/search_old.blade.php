@if ($paginator->hasPages())
    <nav class="pagination is-centered" role="navigation" aria-label="pagination">

    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
        <a class="pagination-previous disabled" disabled>Prev page</a>
    @else            
        <a data-href="{{$paginator->previousPageUrl()}}" class="pagination-previous">Prev page</a>
    @endif


    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())            
        <a data-href="{{ $paginator->nextPageUrl() }}" class="pagination-next">Next page</a>
    @else
        <a class="pagination-next disabled" disabled>Next page</a>
    @endif


    <ul class="pagination-list">

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li><span class="pagination-ellipsis">&hellip;</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li>
                            <a class="pagination-link is-current" 
                                aria-label="Page {{ $page }}" 
                                aria-current="page">
                            {{ $page }}
                            </a>
                        </li>
                    @else
                        <li>
                            <a class="pagination-link" 
                                aria-label="Goto page 45"
                                data-href="{{ $url }}">
                                {{ $page }}
                            </a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach
  
    </ul>

    </nav>
@endif


