@extends('main-layout', [
    'lightingdesign_menu' => 'active',
    'company_menu' => 'active',    
])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-lightingdesign.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-lightingdesign.js')}}"></script>
@endsection



@section('title')
Lighting Design{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')


@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif

<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Lighting Design</a>
    </h1>
</section>


<section class="section">


    <div class="container light_refits text_right" id="fullsitemodelling">
        <div class="div_container d1st">
            <div class="light_refits_img" style="background-image: url({{App\Util::asset('assets/images/lightingdesign/full-site-modelling.jpg')}});"></div>
        </div>
        <div class="div_container d2nd">
            <h1 class="title">Full Site Modeling</h1>
            <p>
                See for yourself how our high quality products will add value to your site! Our skilled LED lighting designers will create a full 3D design for your project.
                <br><br>
                A perfect visualisation for probing every angle. 
                <br><br>
                Your site model demonstrates lux distribution at ground level as well as light dispersal on walls, surfaces and objects. 
                <br><br>
                We design superior projects for commercial, industrial and public applications from offices, retail outlets and residential blocks to sports venues, car parks and street lighting.
            </p>
        </div>
    </div>



    <div class="container light_refits text_left" id="falsecolourrendering">
        <div class="div_container d2nd">
            <h1 class="title">False Colour Rendering</h1>
            <p>
                False colour displays are invaluable for simulating lighting distribution and intensity across your target site. 
                <br><br>
                Our design experts offer both 2D and 3D false colour views, along with all the guidance and advice you need to achieve the exact effects you’re looking for. 
            </p>
        </div>
        <div class="div_container d1st">
            <div class="light_refits_img" style="background-image: url({{App\Util::asset('assets/images/lightingdesign/false-colors.jpg')}});"></div>
        </div>
    </div>


    <div class="container light_refits text_right" id="lightinglayouts">
        <div class="div_container d1st">
            <div class="light_refits_img" style="background-image: url({{App\Util::asset('assets/images/lightingdesign/light-distribution.jpg')}});"></div>
        </div>
        <div class="div_container d2nd">
            <h1 class="title">Lighting Layouts</h1>
            <p>
                Invaluable for the final installation on site, your 2D luminaire layout plan shows the precise location of every individual light fitting and the light distribution from each luminaire.
            </p>
        </div>
    </div>


    <div class="container light_refits text_left" id="isolinesmapping">
        <div class="div_container d2nd">
            <h1 class="title">Isolines Mapping</h1>
            <p>
                Our accurate, 2D isoline plans give a clear and detailed picture of the relative lux levels and light distribution across your site.                 
            </p>
        </div>
        <div class="div_container d1st">
            <div class="light_refits_img" style="background-image: url({{App\Util::asset('assets/images/lightingdesign/iso-mapping.jpg')}});"></div>
        </div>
    </div>




    <div class="container light_refits text_right" id="valuegrid">
        <div class="div_container d1st">
            <div class="light_refits_img" style="background-image: url({{App\Util::asset('assets/images/lightingdesign/value-grid.jpg')}});"></div>
        </div>
        <div class="div_container d2nd">
            <h1 class="title">Value Grid</h1>
            <p>
                We can supply you with value grids 2D plans that show the exact lux level achieved at any specific point across a given surface or your entire site. 
            </p>
        </div>
    </div>


</section>
@endsection




