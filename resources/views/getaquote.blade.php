@extends('main-layout', ['getaquote_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-getaquote.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-getaquote.js')}}"></script>
@endsection



@section('title')
Get a Quote{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')

@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif




<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Get a Quote</a>
    </h1>
</section>


<section class="section">




<div class="container intro_texts">
    <div class="intro_text">
        <div class="img_bg" style="background-image: url({{App\Util::asset('assets/images/getaquote/getaquote7.jpg')}});"></div>
        
    </div>
    <div class="intro_text">
        <h3 class="title">
        </h3>
        <p class="title_p" style="">
            Almani Lighting is a one stop solutions provider for high quality LED Lights. 
            Whether it’s for residential or commercial projects, we are confident that we can assist you in achieving your desired outcome for your projects with the help of our LED Lighting experts! 
            <br><br>
            Get your <span>FREE Professional Consultation</span> by filling out the form below and our LED experts will get back to you! 
            <br><br>
            We will contact you directly for any further information or clarification. 
            <br><br>
            For general enquiries, contact us on <a href="tel:+97148873265">+971 4-887-3265</a> or email us at <a href="mailto:info@almani.ae">info@almani.ae</a>.

        </p>
    </div>
</div>


<!-- 
<div class="bar_text_backdrop" style="background-image: url({{App\Util::asset('assets/images/getaquote/getaquote7.jpg')}});">
    <div class="backdrop"></div>
    <p class="title_p" style="">
        Almani Lighting is a one stop solutions provider for high quality LED Lights. 
        Whether it’s for residential or commercial projects, we are confident that we can assist you in achieving your desired outcome for your projects with the help of our LED Lighting experts! 
        <br><br>
        Get your <span>FREE Professional Consultation</span> by filling out the form below and our LED experts will get back to you! 
        <br><br>
        We will contact you directly for any further information or clarification. 
        <br><br>
        For general enquiries, contact us on <a href="tel:+97148873265">+971 4-887-3265</a> or email us at <a href="mailto:info@almani.ae">info@almani.ae</a>.
    </p>
</div>
 -->



<div class="bar_text">
    <h3 class="title">
        LED Lighting Design and Professional Consultation
    </h3>
    <p class="title_p" style="">
        <!-- Get an extra 10% discount on all enquiries received before the 30th September, 2018 -->
    </p>
</div>



<div class="container">

<form class="form_quote" method="post" action="{{route('sendquote')}}" enctype="multipart/form-data">
{{ csrf_field() }}

 <input type="hidden" value="" name="recaptcha_response" id="recaptchaResponse">

    <div class="form-col">

        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-user"></i></span>            
                </div>
                <input name="full_name" class="form-control" type="text" placeholder="Full Name" >
            </div>        
        </div>


        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                </div>
                <input name="email" class="form-control email" type="email" placeholder="Email" required>
            </div>        
        </div>        

        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-phone"></i></span>
                </div>
                <input name="tel" class="form-control" type="tel" placeholder="Phone / Mobile" >
            </div>        
        </div>        

        <div class="form-group">     
            <textarea name="project_definition" class="form-control textarea" placeholder="Project definition.."></textarea>
        </div>        

    </div>














    <div class="form-col">

        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Project Type:</label>
            <div class="col-sm-9">
                <select name="project_type" class="form-control select_show_others" data-showfield="project_type_others">
                    <option selected="selected" disabled="" style="display:none;">Please specify ...</option>
                    <option>Commercial</option>
                    <option>Residential</option>
                    <option>Villa</option>
                    <option>Hotel</option>
                    <option>Industrial</option>
                    <option value="Others">Other</option>
                </select>
            </div>            
        </div>
        <div class="form-group row project_type_others" style="display: none;">
            <label class="col-sm-3 col-form-label"></label>
            <div class="col-sm-9">
                <input name="project_type_others" class="form-control" type="text" placeholder="other project type.." >                
            </div>            
        </div>




        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Construction Status:</label>
            <div class="col-sm-9">
                <select name="construction_status" class="form-control select_show_others" data-showfield="construction_status_others">
                    <option selected="selected" disabled="" style="display:none;">Please specify...</option>
                    <option>Pre-Launch</option>
                    <option>Launch</option>
                    <option>Excavation & Foundation</option>
                    <option>Under-Construction</option>
                    <option>Structure Half done</option>
                    <option>Topping out</option>
                    <option value="Others">Other</option>
                </select>
            </div>
        </div>
        <div class="form-group row construction_status_others" style="display: none;">
            <label class="col-sm-3 col-form-label"></label>
            <div class="col-sm-9">
                <input name="construction_status_others" class="form-control" type="text" placeholder="other construction status..">
            </div>
        </div>






        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Project Budget:</label>
            <div class="col-sm-9">
                <select name="project_budget_currency" class="form-control currency_aed" style="width:85px; display:inline-block;">
                      <option>AED</option>
                      <option>EUR</option>
                      <option>USD</option>
                </select>
                <input name="project_budget" class="form-control" type="text" placeholder="0.00" style="width:150px; height:38px; display:inline-block;">
            </div>            
        </div>





        <div class="form-group row project_type_others">
            <label class="col-sm-3 col-form-label">Location:</label>
            <div class="col-sm-9">
                <input name="location" class="form-control" type="text" placeholder="">
            </div>            
        </div>





        <div class="files">
            <label class="label is-small">Please upload BOQ, Building Plans, Specifications etc. for our design team to study and quote:</label>
            <div class="control">

                <div class="form-row file-row">
                    <div class="col-11">
                        <div class="custom-file ">
                            <input class="custom-file-input file_quote" type="file" name="file_quote[]">
                            <label class="custom-file-label" for="customFile">Choose file..</label>
                        </div>
                    </div>
                    <div class="col-1">
                        <button type="button" class="clear-file-input btn-almani">
                            <i class="fas fa-times-circle"></i>
                        </button>
                    </div>
                </div>

                <div class="form-row file-row">
                    <div class="col-11">
                        <div class="custom-file ">
                            <input class="custom-file-input file_quote" type="file" name="file_quote[]">
                            <label class="custom-file-label" for="customFile">Choose file..</label>
                        </div>
                    </div>
                    <div class="col-1">
                        <button type="button" class="clear-file-input btn-almani">
                            <i class="fas fa-times-circle"></i>
                        </button>
                    </div>
                </div>

                <div class="form-row file-row">
                    <div class="col-11">
                        <div class="custom-file ">
                            <input class="custom-file-input file_quote" type="file" name="file_quote[]">
                            <label class="custom-file-label" for="customFile">Choose file..</label>
                        </div>
                    </div>
                    <div class="col-1">
                        <button type="button" class="clear-file-input btn-almani">
                            <i class="fas fa-times-circle"></i>
                        </button>
                    </div>
                </div>

                <div class="form-row file-row">
                    <div class="col-11">
                        <div class="custom-file ">
                            <input class="custom-file-input file_quote" type="file" name="file_quote[]">
                            <label class="custom-file-label" for="customFile">Choose file..</label>
                        </div>
                    </div>
                    <div class="col-1">
                        <button type="button" class="clear-file-input btn-almani">
                            <i class="fas fa-times-circle"></i>
                        </button>
                    </div>
                </div>

                <div class="form-row file-row">
                    <div class="col-11">
                        <div class="custom-file ">
                            <input class="custom-file-input file_quote" type="file" name="file_quote[]">
                            <label class="custom-file-label" for="customFile">Choose file..</label>
                        </div>
                    </div>
                    <div class="col-1">
                        <button type="button" class="clear-file-input btn-almani">
                            <i class="fas fa-times-circle"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="submit_div" style="">
        <button type="submit" class="btn-almani submit_quote">Submit</button>
    </div>

</form>


    
</div>

</section>
@endsection




