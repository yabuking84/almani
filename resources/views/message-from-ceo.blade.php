@extends('main-layout', ['company_menu' => 'active' ,'message_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-message-from-ceo.css')}}" rel="stylesheet">  

@endsection

@section('javascript-lib')
    <script src="{{App\Util::asset('assets/js/master-message-from-ceo.js')}}"></script>
@endsection

@section('title')
        Message from the CEO{{App\Meta::webpageTitle()}}
@endsection
        
@section('main-content')

@if (session('error') !== null)
    <div class="container alert_container">
        <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
        <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
            {{session('message')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endif


            <section class="section section_title" style="">
                    <h1 class="title main_title" style="">
                        <a class="button is-primary">Almani Ligthing </a>
                    </h1>

                    <div class="intro_text">
                        <h1 class="title">            
                                Message from the CEO                      
                        </h1>
                    </div>
            </section>
            <section class="section_container"  style="background-image: url( {{App\Util::asset('assets/images/message/message_bg.jpg')}} );">
                <div class="backdrop"></div>
                <div class="lightbar_text d-none">
                        <p class="title_p" style="">
                            Enjoy all these major benefits when you choose us, the easiest and most reliable supplier in the market.
                        </p>
                    </div>
                <div class="container">
                        <div class="section_message">
                                <div class="section_picture">
                                    <img src="{{App\Util::asset('assets/images/message/johannes-eidens-aachen-germany-almani-lighting.png')}}" alt="Johannes Eidens Aachen Germany Almani Lighting"/>
                                </div>
                                <div class="section_text">
                                        <p>
                                                This has been an excellent year for Almani Lighting. A proud German and European, I learned from childhood that high standards
                                                of quality, professionalism, service and integrity were essential to building strong customer relationships, outstanding businesses
                                                and better lives for all. I’m therefore delighted that my early vision of sharing my German standards and approach with Almani’s
                                                rapidly growing client base across the UAE is proving both popular and successful!
                                        </p>
                                        <p>
                                                I am certain that our exciting growth is due to our absolute commitment to quality – quality that we can confidently guarantee
                                                because we manufacture every single product to our strict German specifications and standards, from the smallest chip to the
                                                largest industrial light fittings. Similarly, we have developed our own world-class project management software which allows our
                                                people to do the best job they possibly can and, most importantly, helps ensure the best possible experience and outcomes for
                                                our clients. And even though Almani Lighting offers only the finest LED quality in the world, our portfolio is one of the largest in the
                                                entire UAE region, with its many superb ranges and infinite bespoke options. Furthermore, we are innovators, eager to anticipate
                                                and adapt to changing technologies and market demands. 
                                                
                                        </p>
                                        <p>
                                                Our business now has around 50 staff, all trained, highly skilled and client-focused. At the time of writing, we are involved in more
                                                than 100 projects in Dubai alone. Many of these are large, complex and prestigious projects and we are proud of the work we do
                                                for every single client who works with us. We are one of a small number of companies in the UAE who can quote for an entire
                                                lighting project using just the one brand.
                                        </p>
                                        <p>
                                                Yes, we are proud. We love making a positive difference. We are deliberately raising the global bar in LED service and technology.
                                                But we are not complacent. We seek opportunities to improve in any way we can. We liaise closely with our clients to find out how
                                                we can do things better, because over time, every tiny increment adds up to significant progress. We manage the challenges of
                                                promoting high standards in an industry where poor quality is rife. And we intend to become a truly amazing organisation,
                                                celebrated worldwide for excellence!
                                        </p>
                                        <p>
                                                As the CEO of both Almani Lighting, I have many thanks to give: I am grateful to our staff, investors, partners and
                                                families for their support, hard work and commitment. And of course, I am immensely grateful to our wonderful clients for trusting
                                                us with their important and challenging projects: we are honoured to work with you, delighted to deliver beyond your expectations
                                                and we look forward to doing so for many years to come.
                                        </p>
                                                

                                        <p class="lbl-ceo"> Johannes N. Eidens (M.A.) <span class="role">  CEO and Founder  </span> </p>
                                </div>
                        </div>
                </div>
            </section>
@endsection




