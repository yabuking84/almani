@extends('main-layout', ['products_menu' => 'active'])


@section('meta-viewport')
<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=yes">
@endsection

<?php
// Product Name
//////////////////////////////////////
$product_name = ucwords(str_replace('[camel]', '', strtolower($product->product_name)));
//////////////////////////////////////
?>

@section('title')
{{$product->product_name}} | {{$category->category_name}}{{App\Meta::webpageTitle()}}
@endsection



@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-product.css')}}" rel="stylesheet">
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-product.js')}}"></script>
@endsection

@section('js-head-lib')
@parent
<script>
    var category_name = "{{$category->category_name}}";
    var product_code = "{{$product->product_code}}";
    var product_name = "{{$product_name}}";
    var get_model_code = "{{(isset($_GET['model']))?$_GET['model']:''}}";
</script>
@endsection


@section('meta')
{{-- META --}}
<?php /* <meta name="google-site-verification" content="" /> */ ?>
<meta name="description" content="<?= $meta['description'] ?>" />
<meta name="author" content="Almani Lighting LLC">
<meta name="keywords" content="<?= $meta['keywords'] ?>" />
<link rel="canonical" href="{{ URL::current() }}" />

<meta property="og:title" content="<?= ucwords($meta['title']) ?>">
<meta property="og:type" content="article">
<meta property="og:url" content="{{route('product_page', [
                              'category_alias' => strtolower($category->category_alias),
                              'product_code' => $product->product_code,
                        ])}}">
<meta property="og:image" content="https://manage.almani.ae/thumbnailer.php?d=1000x525&i=<?= $product->mainImage() ?>">
<meta property="og:description" content="<?= $meta['description'] ?>">
<?php $x=0; ?>
@foreach(array_reverse($product->galleryImages('has_sketches')) as $images)
<meta property="og:image" content="<?=$images['gallery_img']?>" data-count="<?=$x?>">
<?php $x++; ?>
@endforeach
{{-- /META --}}
@endsection

@section('breadcrumb')
<div class="container breadcrumb_container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">

            @if($category->parentCategory)
            <li class="breadcrumb-item">
                <a href="{{route('catalog_landing_page')}}">
                    CATEGORIES
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('catalog_product_page',['category_alias'=>$category->parentCategory->category_alias])}}">
                    {{$category->parentCategory->category_name}}
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('catalog_product_page',['category_alias'=>$category->category_alias])}}">
                    {{$category->category_name}}
                </a>
            </li>
            <li class="breadcrumb-item active">{{$product->product_name}}</li>
            @else
            <li class="breadcrumb-item">
                <a href="{{route('catalog_landing_page')}}">
                    CATEGORIES
                </a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('catalog_product_page',['category_alias'=>$category->category_alias])}}">
                    {{$category->category_name}}
                </a>
            </li>
            <li class="breadcrumb-item active">{{$product->product_name}}</li>
            @endif


        </ol>
    </nav>

    <button class="btn btn-sm btn-almani contact_us_enquire" data-toggle="modal" data-target="#contact_us_modal"
        data-message="Hi, I would like to enquire about <?=$product->product_name?> (<?=$product->product_code?>) and would appreciate a call back as soon as possible."
        style="margin: -10px 0 0 5px;">
        <i class="fa fa-envelope"></i> Enquire
    </button>   
    @if(count($sameProduct))
        <a href="{{route('product_page', [
            'category_alias' => strtolower($category->category_alias),
            'product_code' => $sameProduct[0]->product_code,
        ])}}" class="btn btn-sm btn-almani" 
        style="margin: -10px 0 0 5px;">
            Economical Product
        </a> 
    @endif



    @if (session('login_led_designer'))
       <a class="btn btn-sm btn-almani" id="get_modal_datasheet_btn" data-toggle="modal" data-target="#get_datasheet_modal" data-href="https://manage-camel.almani.ae/datasheet/create/<?=$product->product_code?>" href="javascript:void(0);" target="_blank" title="Download Datasheet" style="margin: -10px 0 0 5px;">          
           <i class="fa fa-download"></i>
            Download Datasheet
        </a>
    @endif


    @if(session('login_led_designer'))
    <h1 style="font-weight: 500;font-size: 11px; margin: 5px;">
        <?php if($product->supplier)
        echo $product->supplier->supplier_company;
    else 
        echo "NO SUPPLIER";
    ?>

        <?=$product->epOptions();?>
    </h1>
    @endif



</div>
@endsection




@section('main-content')


@if (session('error') !== null)
<div class="container mt-1">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif

<div class="main-content container">

    <div class="gallery">
        <div class="mainImage">
            <div class="easyzoom easyzoom--overlay easyzoom-thumbnails">
                <a href="{{$product->mainImage()}}">
                    <img src="{{$product->mainImage()}}" alt="" class="mainImage-img" />
                </a>
            </div>
        </div>

        @if(count($product->galleryImages("has_sketches")) > 1 || $product->defaultSketch)
        <div class="thumbnails">
            <ul class="owl-carousel-js owl-carousel owl-theme owl-loaded" id="products_gallery">
                @php
                $selected = 'selected'; foreach ($product->galleryImages("has_sketches") as $key => $val) {
                @endphp
                <li class="{{$selected}}">
                    {{-- <div class="backdrop"></div> --}}
                    <a href="{{$val['gallery_img']}}" data-standard="{{$val['gallery_img']}}">
                        <img src="{{$val['gallery_th_img']}}" alt="" />
                    </a>
                </li>
                @php
                $selected = ''; }
                @endphp
            </ul>
        </div>
        @endif
    </div>


    <div class="description">


        @if(count($product->getApplicationAreasIcons()))
        <div class=" app_areas">
            <h3>
                Application Areas:
            </h3>
            <div class="owl-carousel-appareas owl-carousel owl-theme owl-loaded">
                @foreach($product->getApplicationAreasIcons() as $val)
                <div class="col-auto app_area_icon">
                    <img src="{{App\Util::asset('assets/images/datasheets/application_areas/'.$val['icon'].'.'.$val['extension'])}}"
                        alt="">
                    <span>
                        <?=ucwords($val['title'])?></span>
                </div>
                @endforeach
            </div>
        </div>
        @endif


        <div class="tab_links">
            <ul>
                <li class="active" data-tab="tab1">
                    <a>Technical Data Summary</a>
                </li>
                <li data-tab="tab2">
                    <a>Description</a>
                </li>
                <li data-tab="tab3">
                    <a>Additional Info</a>
                </li>
            </ul>

        </div>


        <div class="tabs">
            <div class="tab tab1 active">
                <div class="tech-data-summary">
                    <?php
                    $tds = $product->getTechnicalDataSummary();
                    $cnt = count($tds);
                    $numFieldsPerPanel = ceil($cnt/2);
                    $x=1;
                    echo '<div class="column no-bottom-space">';
                    foreach($tds as $key => $val) { 
                    ?>
                    <div>
                        <span class="tech-summary-label">
                            <?=$val['label']?>: </span>
                        <span class="tech-summary-value">
                            <?=App\Util::changeDash($val['value'])?></span>
                    </div>
                    <?php
                      
                      // group
                      if ($numFieldsPerPanel>=1 && $x % $numFieldsPerPanel == 0 && $cnt!=$x) { 
                          echo '</div>';
                          echo '<div class="column no-bottom-space">';
                      }
                      $x++;
                    } 
                    echo '</div>';            
                    ?>
                </div>
            </div>

            <div class="tab tab2">
                <div class="text-desc">
                    <p>
                        <?= ($product->product_desc)?nl2br($product->product_desc):'No product description available. To be updated soon..' ?>
                    </p>
                </div>
            </div>

            <div class="tab tab3">
                <div class="additional-info">
                    <?= ($product->product_desc2)?nl2br($product->product_desc2):'No Additional Info available. To be updated soon..' ?>
                </div>
            </div>
        </div>


    </div>


</div>




@if($product->product_active>=1)
{{-- ////////////////////////////////////////////////////////////// --}}
<div class="container table_container" style="">
 @if((session('login_led_designer') || session('login_sales')))
    <div class="currency_container">
        <label for="currency" class="col-form-label  col-form-label-sm" style="margin-right: 10px">Currency</label>
        <select class="form-control form-control-sm" id="currency" name="currency" style="width: 80px;">
            <option selected="selected">AED</option>
            <option>USD</option>
            <option>EUR</option>
        </select>
    </div>
    @endif

    <div class="models">
        <div id="models_table" class="table-scrollable-container table_type_1 table-scroll">
            <div class="table-wrap">
                <table class="table main-table" border="collapse">
                    <thead class="thead-dark">
                        <tr>
                            <th style="" class="fixed-col model_th"><span>Model Details</span></th>
                            <th style="" class="fixed-col-content model_th"><span>Model Details</span></th>
                            <th style="">Power</th>
                            <th style="">Efficiency</th>
                            <th style="">CRI</th>
                            <th style="">Size/Cut-Out</th>
                            <th>Delivered Lumen</th>
                            <th>Dim</th>
                            <th>CCTs</th>
                            <th>IPs</th>
                            <th>Beam Angles</th>
                            @if($product->product_wifi)
                            <th>Wifi</th>
                            @endif
                            <th class="">Warranty</th>
                            @if((session('login_led_designer') || session('login_sales')))
                            <th style="width: 180px;"><span>Price</span></th>
                            @else
                            <th>Enquire</th>
                            @endif
                            {{-- <th style="min-width: 200px;"><span>More</span></th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        
                <?php $start_heading = ""; ?>

                @foreach($product->getModelsForProductPage() as $modelKey => $model)
                <?php 

                // for headers
                if($model->model_heading) { 
                    $str = $model->model_heading;
                    $model_headingArr = explode('.', $str, 2);
                    $str0 =  (array_key_exists(0, $model_headingArr))?trim($model_headingArr[0]):"";;
                    $str1 =  (array_key_exists(1, $model_headingArr))?trim($model_headingArr[1]):"";

                    $strAll = "";
                    if($str1!="")
                    $strAll = $str1;
                    else
                    $strAll = $str0;

                    if($start_heading != $strAll)
                    echo '<tr class="model_heading_tr">'.
                            '<td class="fixed-col" colspan="13">'.
                                '<div class="model_heading" style="padding: 5px 5px 5px 10px;">'.
                                    $strAll.
                                '</div>'.
                            '</td>'.
                            '<td class="fixed-col-content" colspan="13">'.
                                '<div class="model_heading" style="padding: 5px 5px 5px 10px;">'.
                                    $strAll.
                                '</div>'.
                            '</td>'.
                        '</tr>';

                    $start_heading = $strAll;
                } 

                // for html extra
                $html_extra_style = ($model->model_extra!="")?'border-bottom: 0;':''; 

                ?>
                        <tr style="{{$html_extra_style}}" itemscope itemtype="http://schema.org/Product" class="<?=$model->model_group_code?> <?=$model->model_codes?>">

                            {{-- ///////////////////////////////////// --}}
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            <td class="fixed-col">
                                <div class="columns <?=$model->model_group_code?>">
                                    <div class="column model_tech_data_name">
                                        <div class="model_tech_data model_name" data-model_name="<?=$product->product_name.' ('.$model->model_group_code.')'?>"
                                            style="">

                                            <span class="zoom-gallery">
                                                <a href="{{$model->thumbnail()['href']}}" title="{{$model->model_group_code}}">
                                                    <img class="" alt="{{$model->model_group_code}}" src="{{$model->thumbnail()['src']}}"
                                                        style="height: 30px;" />
                                                </a>
                                            </span>

                                            &nbsp;&nbsp;

                                            <span class="is-pulled-right model_title" style="">
                                                <?=$model->model_group_code?>
                                            </span>


                                            @if($model->model_extra!="")
                                            &nbsp;&nbsp;
                                            <span class="show_model_extra btn btn-dark btn-sm" style="">
                                                <i class="fas fa-caret-up"></i>
                                                <i class="fas fa-caret-down hide_group"></i>
                                            </span>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="fixed-col-content">

                                {{-- /////////////// --}}
                                {{-- schema.org tags --}}
                                {{-- ///////////////////////////////////////////// --}}
                                <span style="display: none;">
                                    <span itemprop="brand" class="is-hidden">Almani Lighting</span>
                                    <span itemprop="name" class="is-hidden">
                                        <?=$model->product->product_name?>
                                        <?=$model->model_group_code?></span>
                                    <span itemprop="description" class="is-hidden">
                                        <?=strip_tags($model->product->product_desc)?></span>
                                    <span itemprop="url" class="is-hidden">
                                        {{route('productmodel_page', [
                                        'category_alias' => strtolower($model->product->category->category_alias),
                                        'product_code' => $model->product->product_code,
                                        'model_code' => $model->model_group_code,
                                        ])}}
                                    </span>

                                    <img itemprop="image"alt="{{$model->model_group_code}}" src="{{ $product->thumb($model->model)['gallery_img'] }}"

                                        class="is-hidden" />
                                    <span itemprop="model" class="is-hidden">
                                        <?=$model->model_group_code?></span>

                                    <span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="is-hidden">
                                        <meta itemprop="priceCurrency" content="AED" />
                                        <span itemprop="price">
                                            <?=$model->priceOptions()['def_price']?></span>
                                        <link itemprop="availability" href="http://schema.org/InStock" />In stock
                                    </span>
                                </span>
                                {{-- ///////////////////////////////////////////// --}}
                                {{--/ schema.org tags --}}
                                {{-- /////////////// --}}


                                <div class="columns">
                                    <div class="column model_tech_data_name">
                                        <div class="model_tech_data model_name" data-model_name="<?=$product->product_name.' ('.$model->model_group_code.')'?>"
                                            style="">

                                            <span class="zoom-gallery">
                                                <a href="https://manage.almani.ae/thumbnailer.php?thumbnail=1&d=250x250&i={{ $product->thumb($model->model)['gallery_img'] }}" title="{{$model->model_group_code}}">
                                                    <img class="model_th" alt="{{$model->model_group_code}}" src="{{ $product->thumb($model->model)['gallery_img'] }}"
                                                        style="height: 30px;" />
                                                </a>
                                            </span>

                                            &nbsp;&nbsp;

                                            <span class="is-pulled-right model_title" style="">
                                                <?=$model->model_group_code?>
                                            </span>


                                            @if($model->model_extra!="")
                                            &nbsp;&nbsp;
                                            <span class="show_model_extra btn btn-dark btn-sm" style="">
                                                <i class="fas fa-caret-up"></i>
                                                <i class="fas fa-caret-down hide_group"></i>
                                            </span>
                                            @endif

                                        </div>
                                    </div>
                                </div>

                            </td>
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            {{-- ///////////////////////////////////// --}}










                            {{-- ///////////////////////////////////// --}}
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            <td>
                                <div style="display: flex;">
                                    <div class="columns model_tech_data_field" data-model_details="<?=''?>"
                                        data-model_code="{{$model->model_group_code}}" style="">
                                        @if( (float)$model->model_power )
                                              @if (!empty($model->model_no_leds))
                                                    <div class="model_tech_data "><span class="model_tech_value">  <?=$model->model_power = $model->model_no_leds . 'x' . round(intval($model->model_power) / intval($model->model_no_leds), 0); ?> (W)</span></div>
                                              @elseif(!empty($model->model_no_heads))
                                                    <div class="model_tech_data "><span class="model_tech_value">  <?=$model->model_power = $model->model_no_heads . 'x' . round(intval($model->model_power) / intval($model->model_no_heads), 0); ?> (W)</span></div>
                                             @elseif($model->model_up_down)
                                                    <div class="model_tech_data "><span class="model_tech_value">  <?=$model->model_power = '2' . 'x' . round(intval($model->model_power) / 2 ) ?> (W)</span></div>
                                              @else
                                                    <div class="model_tech_data "><span class="model_tech_value">  <?=(float)$model->model_power ?> (W)</span></div>
                                              @endif
                                        @else
                                            <div class="model_tech_data ">
                                                {{-- Power: --}}
                                                <span class="model_tech_value">N/A</span>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            </td>
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            {{-- ///////////////////////////////////// --}}










                            {{-- ///////////////////////////////////// --}}
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            <td>
                                <div style="display: flex;">
                                    <div class="columns model_tech_data_field" data-model_details="<?=''?>"
                                        data-model_code="{{$model->model_group_code}}" style="">

                                        @if($model->model_lm_efficiency)
                                        <div class="model_tech_data ">
                                            {{-- Efficiency: --}}
                                            <span class="model_tech_value">
                                                <?=$model->model_lm_efficiency?> (Lm/W)</span>
                                        </div>
                                        @else
                                        <div class="model_tech_data ">
                                            {{-- Efficiency: --}}
                                            <span class="model_tech_value">N/A</span>
                                        </div>
                                        @endif

                                    </div>
                                </div>
                            </td>
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            {{-- ///////////////////////////////////// --}}










                            {{-- ///////////////////////////////////// --}}
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            <td>
                                <div style="display: flex;">
                                    <div class="columns model_tech_data_field" data-model_details="<?=''?>"
                                        data-model_code="{{$model->model_group_code}}" style="">

                                        @if($model->cri())
                                        <div class="model_tech_data ">
                                            {{-- CRI: --}}
                                            <span class="model_tech_value">
                                                <?=$model->cri()?></span>
                                        </div>
                                        @else
                                        <div class="model_tech_data ">
                                            {{-- CRI: --}}
                                            <span class="model_tech_value">N/A</span>
                                        </div>
                                        @endif

                                    </div>
                                </div>
                            </td>
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            {{-- ///////////////////////////////////// --}}










                            {{-- ///////////////////////////////////// --}}
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            <td>
                                <div style="display: flex;">
                                    <div class="columns model_tech_data_field" style="flex-direction: column;">

                                        <?php if($model->modelSize()|| $model->model_cutout) { ?>
                                        <div class="model_tech_data multi_lines">
                                            {{-- Size/Cut-Out: --}}
                                            <?php if($model->modelSize()) { ?>
                                            <div class="model_tech_data_sub">
                                                <?=$model->modelSize()?>
                                            </div>
                                            <?php } ?>

                                            <?php if($model->model_cutout) { ?>
                                            <div class="model_tech_data_sub">Cutout
                                                <?=$model->model_cutout?>
                                            </div>
                                            <?php } ?>

                                        </div>
                                        <?php } ?>

                                        <?php if($model->model_lamp_holder!='' && $model->model_lamp_holder!='N/A') { ?>
                                        <div class="model_tech_data ">
                                            {{-- Lamp Holder: --}}
                                            <span class="model_tech_value">
                                                <?=$model->model_lamp_holder?></span></div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </td>
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            {{-- ///////////////////////////////////// --}}










                            {{-- ///////////////////////////////////// --}}
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            <td>
                                <div style="display: flex;">
                                    <div class="columns model_tech_data_field" data-model_details="<?=''?>"
                                        data-model_code="{{$model->model_group_code}}" style="">

                                        @if($model->cri())
                                        <div class="model_tech_data ">
                                            {{-- LUMEN: --}}
                                            <span class="model_tech_value">
                                                <?=$model->lumen?>
                                                </span>
                                              
                                        </div>
                                        @else
                                        <div class="model_tech_data ">
                                            {{-- LUMEN: --}}
                                            <span class="model_tech_value">N/A</span>
                                        </div>

                                        @endif

                                    </div>
                                </div>
                            </td>
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            {{-- ///////////////////////////////////// --}}










                            {{-- ///////////////////////////////////// --}}
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}

                            <td>
                                <div class="option_group_container">

                                    <?php 
                                    $dimOptions = $model->dimOptions();
                                    if($dimOptions['has_dim_triac'] || $dimOptions['has_dim_010v'] || $dimOptions['has_dim_dali']) { 
                                ?>
                                    <div class="option_group dim_group change_price" style="">
                                        {{-- <h6 class="title is-6">Dim</h6> --}}
                                        <?php foreach ($dimOptions['dim_options'] as $key => $val) { ?>
                                        <?php if($val['enabled']) { ?>
                                        <label class="button is-small" title="<?=$val['price_title']?>">
                                            <input type="radio" name="dim_<?=strtolower($model->model_group_code)?>"
                                                value="<?=$key?>" data-uae_price="<?=$val['price']?>" />
                                            <?=$val['title']?>
                                        </label>
                                        <?php } else { ?>
                                        <label class="button is-small disabled" disabled>
                                            <?=$val['title']?>
                                        </label>
                                        <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <?php } else { ?>
                                    <div class="model_tech_data ">
                                        {{-- DIM: --}}
                                        <span class="model_tech_value">N/A</span>
                                    </div>
                                    <?php } ?>

                                </div>
                            </td>

                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            {{-- ///////////////////////////////////// --}}










                            {{-- ///////////////////////////////////// --}}
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            <td>
                                <div class="option_group_container">

                                    <?php 
                                    $cctOptions = $model->cct_options;
                                    if(count($cctOptions)) { 
                                ?>
                                    <div class="option_group cct_group change_base_price" style="">
                                        {{-- <h6 class="title is-6">CCTs</h6> --}}
                                        <?php foreach ($cctOptions as $cctOption_val) { ?>
                                        @if($cctOption_val['cct'])
                                        <label class="button is-small" title="">
                                            <input type="radio" name="cct_<?=strtolower($model->model_group_code)?>"
                                                value="<?=$cctOption_val['cct']?>" data-uae_price="<?=$cctOption_val['price']?>" />
                                            <?=$cctOption_val['cct']?>
                                        </label>
                                        @else
                                        <div class="model_tech_data ">
                                            {{-- CCTS: --}}
                                            <span class="model_tech_value">N/A</span>
                                        </div>
                                        @endif
                                        <?php } ?>
                                    </div>
                                    <?php } else {  ?>
                                    <div class="model_tech_data ">
                                        {{-- CCTS: --}}
                                        <span class="model_tech_value">N/A</span>
                                    </div>
                                    <?php } ?>
                                </div>
                            </td>
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            {{-- ///////////////////////////////////// --}}










                            {{-- ///////////////////////////////////// --}}
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            <td>
                                <div class="option_group_container">

                                    <?php 
                                    $ipOptions = $model->ipOptions();
                                    if($ipOptions['has_more_ips']) { 
                                ?>
                                    <div class="option_group ip_group change_price" style="">
                                        {{-- <h6 class="title is-6">IPs</h6> --}}
                                        <?php $cnt=0; foreach ($ipOptions['ip_ratingArr'] as $key => $val) { $cnt++;?>
                                        <label class="button is-small <?=($cnt==1)?'isSelected default':''?>" title="+<?=$val['price']?> AED">
                                            <input type="radio" name="ip_<?=strtolower($model->model_group_code)?>"
                                                value="<?=$val['ip']?>" data-uae_price="<?=$val['price']?>" />
                                            IP
                                            <?=$val['ip']?>
                                        </label>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                            </td>
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            {{-- ///////////////////////////////////// --}}










                            {{-- ///////////////////////////////////// --}}
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            <td>
                                <div class="option_group_container">

                                    <?php 
                                    $beamAngleOptions = $model->beamAngleOptions();
                                    if(count($beamAngleOptions)) { 
                                ?>
                                    <div class="option_group beam_angle_group" style="">
                                        {{-- <h6 class="title is-6">Beam Angles</h6> --}}
                                        <?php foreach ($beamAngleOptions as $key => $val) { ?>
                                        <label class="button is-small" title="0.00 AED">
                                            <input type="radio" name="beam_angle_<?=strtolower($model->model_group_code)?>"
                                                value="<?=$val['beam_angle']?>" data-uae_price="0" />
                                            <?=$val['beam_angle']?>@if(is_numeric($val['beam_angle']))&deg;@endif
                                        </label>
                                        <?php } ?>
                                    </div>
                                    <?php } else {  ?>
                                    <label class="button is-small" title="0.00 AED">
                                        N/A
                                    </label>
                                    <?php } ?>
                                </div>
                            </td>
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            {{-- ///////////////////////////////////// --}}










                            {{-- ///////////////////////////////////// --}}
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            @if($product->product_wifi)
                            <td>
                                <div class="option_group_container">

                                    <?php 
                                    $wifiOptions = $model->wifiOptions();
                                    if($wifiOptions['has_wifi_option']) { 
                                ?>
                                    <div class="option_group wifi_group" style="
                                              border: 0px;
                                              display: flex;
                                              align-items: center;">
                                        {{-- <h6 class="title is-6">WIFI</h6> --}}
                                        <label class="button is-small" title="<?=$wifiOptions['wifi_tip']?>">
                                            <input type="radio" name="wifi_<?=strtolower($model->model_group_code)?>"
                                                value="wifi" data-uae_price="0" />
                                            <i class="fa fa-wifi" style="margin-right: 3px;"></i>
                                            Wifi
                                        </label>
                                    </div>
                                    <?php } else {  ?>
                                    <label class="button is-small" title="<?=$wifiOptions['wifi_tip']?>">
                                        N/A
                                    </label>
                                    <?php } ?>
                                </div>
                            </td>
                            @endif
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            {{-- ///////////////////////////////////// --}}






                            {{-- ///////////////////////////////////// --}}
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            <td class="">
                                <div class="option_group_container">
                                    <?php $warrantyOptions = $model->warrantyOptions(); ?>
                                    <div class="option_group warranty_group change_warranty" style=" ;">
                                        {{-- <h6 class="title is-6">Warranties</h6> --}}
                                        <?php //print_r($warrantyOptions['warranties'])?>

                                        <?php foreach ($warrantyOptions['warranties'] as $key => $val) { ?>
                                        <label class="button is-small {{($val['default'])?'isSelected default':''}}">
                                            <input type="radio" name="warr_<?=strtolower($model->model_group_code)?>"
                                                value="<?=$val['warranty']?>" />
                                            <?=$val['warranty']?>
                                            <?=($val['warranty']==1)?'yr&nbsp;':'yrs'?>
                                        </label>
                                        <?php } ?>
                                    </div>


                                </div>
                            </td>
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            {{-- ///////////////////////////////////// --}}












            {{-- ///////////////////////////////////// --}}
            {{-- ////////////////////////////////////////////////////////////////////////// --}}
            <td>

                @if((session('login_led_designer') || session('login_sales')))
                        @if(session('login_led_designer'))
                            <?php $priceOptions = $model->priceOptions(); ?>
                            <span style="float:left;" class="price_container" data-def_uae_price="<?=$priceOptions['def_price']/ 2?>" data-discount="<?=$priceOptions['discount']?>">
                                <span class="price_container_show">
                                    <?=$priceOptions['price_to_display']?>
                                </span>
                                <span class="price_container_loading" style=" font-size: 15px;
                                                  display: none;
                                                  text-align: center;
                                                  margin-bottom: 5px;">
                                    <i class="fa fa-spinner fa-spin"></i>
                                </span>
                            @if ($model->invOptions() > 0 || $model->stockOptions() > 0 )
                                <span class="in_stock" style="display:block; font-size: 11px; color: green;">
                                    In stock
                                </span>
                            @endif
                        @elseif (session('login_sales'))
                            <?php $priceOptions = $model->priceOptions(); ?>
                            <span style="float:left;" class="price_container" data-def_uae_price="<?=$priceOptions['def_price']?>" data-discount="<?=$priceOptions['discount']?>">
                                <span class="price_container_show">
                                    <?=$priceOptions['price_to_display']?>
                                </span>
                                <span class="price_container_loading" style=" font-size: 15px;
                                                  display: none;
                                                  text-align: center;
                                                  margin-bottom: 5px;">
                                    <i class="fa fa-spinner fa-spin"></i>
                                </span>
                        @else 
                        @endif
    
                </span>


                                {{-- Enquire --}}
                                <a class="btn btn-dark btn-sm model_action_button show_modal enquire" style="float: right;"
                                    title="Enquire" data-toggle="modal" data-target="#enquire_modal" data-message="">
                                    <i class="fa fa-envelope"></i>
                                </a>
                                {{-- Enquire --}}


                                <br><br>


                                <?php if(session('edit_items')) { ?>
                                {{-- edit product if admin --}}
                                <div class="edit_item" style="padding-left: 5px;">
                                    <?=$model->emOptions()?>
                                </div>
                                <?php } ?>


                    @else

                                    {{-- Enquire --}}
                                <a class="btn btn-dark btn-sm model_action_button show_modal enquire" style="float: left;"
                                    title="Enquire" data-toggle="modal" data-target="#enquire_modal" data-message="">
                                    <i class="fa fa-envelope"></i>
                                </a>
                                {{-- Enquire --}}

                                <br><br>


                                <?php if(session('edit_items')) { ?>
                                {{-- edit product if admin --}}
                                <div class="edit_item" style="padding-left: 5px;">
                                    <?=$model->emOptions()?>
                                </div>
                                <?php } ?>
                            @endif

                            </td>
                            {{-- ////////////////////////////////////////////////////////////////////////// --}}
                            {{-- ///////////////////////////////////// --}}













                            {{-- <td class="model_action_column">
                            </td> --}}

                        </tr>

                        <?php if($model->model_extra!="") { ?>
                        <tr class="model_extra_tr" style="">
                            <td class="" colspan="12" style="">
                                <div class="model_extra" style="padding: 5px 20px;">
                                    <?=$model->model_extra?>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>

                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <span class="side-scrolling"><i class="fas fa-info-circle"></i> slide to scroll <i class="fas fa-arrow-left"></i> </span>
    </div>
</div>

@php
@endphp




{{-- SIMILAR PRODUCT --}}
@if (!empty($similar_products))
<br>
<br>
    <div class="container similar_container" id="similar_container">
        <div class="header-section">
                <h1 class="">You might be interested in:</h1>
        </div>
        <div class="smlar-container owl-carousel owl-theme owl-loaded" id="smlar-container ">
            @foreach ($similar_products as $similar_product)
            @php

                // Image
                //////////////////////////////////////
                // $products_img = $prod->image('l');

                    $products_img = $similar_product->image('m');
                
                //////////////////////////////////////


                // Product Code
                //////////////////////////////////////
                $prod_code = '<span class="prod_code">'.$similar_product->product_code.'</span>';


                // Product IP
                //////////////////////////////////////
                $prod_ip = ($similar_product->product_ip)?'<span class="prod_ip">IP'.$similar_product->product_ip.'</span>':'';
                //////////////////////////////////////


                // Product Name
                //////////////////////////////////////
                $prod_name = "<span class='prod_name'>".ucwords(str_replace('[camel]', '', strtolower($similar_product->product_name)))."</span>";
                //////////////////////////////////////


                // if no LED is present
                //////////////////////////////////////
                $alt = $prod_name.' '.$similar_product->product_code.' '.$similar_product->category->category_name;
                if (strpos($alt, 'LED') == false) {
                    $alt = $alt.' LED';
                }
                $alt = strip_tags($alt);
                //////////////////////////////////////

            @endphp

            <div class="prod_block">

                    <a class="item" href="{{route('product_page', [
                                                        'category_alias' => strtolower($category->category_alias),
                                                        'product_code' => $similar_product->product_code,
                                                    ])}}">

                        @if(App\Util::checkIfNew($similar_product->product_date_added))
                        <img class="new_icon" src="{{App\Util::asset('assets/images/icons/new.png')}}">
                        @endif

                    <div class="more_details hide_group">
                        <div class="specs_legend">
                            <div class="specs_legend_lifespan">
                                <img alt="Lifespan" src="{{App\Util::asset('assets/images/icons/unit_icons/time.png')}}">
                                <label>Lifespan</label>
                            </div>

                            <div>
                                <img alt="Power" src="{{App\Util::asset('assets/images/icons/unit_icons/power.png')}}">
                                <label>Power</label>
                            </div>

                            <div>
                                <img alt="Kelvin" src="{{App\Util::asset('assets/images/icons/unit_icons/kelvin.png')}}">
                                <label>Kelvin</label>
                            </div>

                            <div>
                                <img alt="Lumens" src="{{App\Util::asset('assets/images/icons/unit_icons/lumens.png')}}">
                                <label>Lumens</label>
                            </div>

                        </div>

                        <div class="specs">
                            <span class="mini-specs" title="Lifespan" data-lifespan="50,000">
                                <img alt="time" src="{{App\Util::asset('assets/images/icons/unit_icons/time.png')}}">
                                {{ $similar_product->product_lifespan }}
                            </span>
                            <span class="mini-specs" title="Power" data-power="5 – 7">
                                <img alt="power" src="{{App\Util::asset('assets/images/icons/unit_icons/power.png')}}">
                                {{App\Util::changeDash((trim($similar_product->product_power)=='-')?'N/A':$similar_product->product_power)}}
                            </span>
                            <span class="mini-specs" title="Kelvin" data-kelvin="3000 – 5000">
                                <img alt="kelvin" src="{{App\Util::asset('assets/images/icons/unit_icons/kelvin.png')}}">
                                {{App\Util::changeDash((trim($similar_product->kelvin())=='-')?'N/A':$similar_product->kelvin())}}
                            </span>
                            <span class="mini-specs" title="Lumens" data-lumens="400 – 565">
                                <img alt="lumens" src="{{App\Util::asset('assets/images/icons/unit_icons/lumens.png')}}">
                            {{ $similar_product->product_lumen }}
                            </span>
                            <div class="view_product" title="View Product">
                                <button class="btn btn-sm btn-almani goto_product">View</button>
                            </div>
                        </div>
                    </div>
            
                    <div class="img_container col-sm-12">
                            <img src="{{App\Util::asset($products_img)}}" alt="{{$alt}}">
                    </div>

                    <div class="prod_title">
                            <?=$prod_ip?>
                            <?=$prod_name?>
                            <?=$prod_code?>
                            <div class="prod_group">
                                 @if((session('login_led_designer') || session('login_sales')))
                                @if($similar_product->minPrice()=="AVAILABLE UPON REQUEST") 
                                    AVAILABLE UPON REQUEST 
                                @elseif (session('login_sales'))
                                    <div class="price_container" data-value="<?=$similar_product->minPrice() * 2?>" data-price_value_uae="<?=$similar_product->minPrice() * 2?>">
                                        <?=$similar_product->minPrice() * 2?> AED  
                                    </div>
                                @else
                                       <div class="price_container" data-value="<?=$similar_product->minPrice()?>" data-price_value_uae="<?=$similar_product->minPrice()?>">
                                        <?=$similar_product->minPrice()?> AED  
                                    </div>
                                @endif
                                @else
                                <br>

                                @endif

                            </div>                
                    </div>

                </a>
            </div>
            @endforeach
        </div>
    </div>
   
@endif
{{-- END SIMILAR PRODUCT --}}

{{-- ////////////////////////////////////////////////////////////// --}}
@else
{{-- ////////////////////////////////////////////////////////////// --}}
<div class="container table_container not_available" style="">
    Sorry, this product is currently not available.
    Check our other lights at <a href="{{route('catalog_product_page', ['category_alias'=>$category->category_alias])}}"><strong>{{$category->category_name}}</strong></a>

    For more information, please call us at <a href="tel:+97148873265"><strong>+971 4-887-3265</strong></a>.
    or <a class="btn-almani btn-sm send_message" data-toggle="modal" data-target="#contact_us_modal">send us a message</a>
    to help you with perfect alternatives.
</div>
{{-- ////////////////////////////////////////////////////////////// --}}
@endif



@endsection





@section('modal')

{{-- enquire modal --}}
{{-- ///////////////////////////////////////////////////// --}}
<div class="modal fade" id="enquire_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <form class="" method="post" action="{{route('sendenquiry')}}"> {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fa fa-envelope"></i>&nbsp;&nbsp; Enquire</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input name="email_type" type="hidden" value="enquire">
                    <input name="category_alias" type="hidden" value="{{strtolower($category->category_alias)}}">
                    <input name="category_name" type="hidden" value="{{$category->category_name}}">
                    <input name="product_name" type="hidden" value="{{$product->product_name}}">
                    <input name="product_code" type="hidden" value="{{$product->product_code}}">
                    <input name="model_code" type="hidden" value="">

                    <div class="form-group row">
                        <div class="col-sm">
                            <input type="text" class="form-control" name="full_name" id="full_name" placeholder="Full Name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            <input name="email" class="form-control" type="email" placeholder="Email" required="required">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            <input name="tel" id="tel" class="form-control" type="text" placeholder="Telephone/Mobile">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            <textarea class="form-control" name="message" id="message" placeholder="Your Enquiry" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            {{-- <label class="checkbox">
                                <input name="do_subscribe" type="checkbox"> Subscribe now to benefit from offers and
                                discounts
                            </label> --}}
                            <br><i>A copy of the enquiry will also be sent to you.</i><br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark" type="submit">Enquire</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- ///////////////////////////////////////////////////// --}}
{{-- enquire modal --}}




{{-- get_datasheet modal --}}
{{-- ///////////////////////////////////////////////////// --}}
<div class="modal fade" id="datasheet_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <form class="" method="post" action="{{route('requestdatasheet')}}"> {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title"><i class="far fa-file-alt"></i>&nbsp;&nbsp; Request Datasheet</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input name="email_type" type="hidden" value="enquire">
                    <input name="category_alias" type="hidden" value="{{strtolower($category->category_alias)}}">
                    <input name="category_name" type="hidden" value="{{$category->category_name}}">
                    <input name="product_name" type="hidden" value="{{$product->product_name}}">
                    <input name="product_code" type="hidden" value="{{$product->product_code}}">
                    <input name="model_code" type="hidden" value="">

                    <div class="form-group row">
                        <div class="col-sm">
                            <input type="text" class="form-control" name="full_name" id="full_name" placeholder="Full Name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            <input name="email" class="form-control" type="email" placeholder="Email" required="required">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            <input name="tel" id="tel" class="form-control" type="text" placeholder="Telephone/Mobile">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            <textarea class="form-control" name="message" id="message" placeholder="Your Mesage" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            {{-- <label class="checkbox">
                                <input name="do_subscribe" type="checkbox"> Subscribe now to benefit from offers and
                                discounts
                            </label> --}}
                            <br><i>A copy of the enquiry will also be sent to you.</i><br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark" type="submit">Request Datasheets</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- ///////////////////////////////////////////////////// --}}
{{-- get_datasheet modal --}}




{{-- FOR LOGIN DESIGNER --}}

{{-- get_datasheet modal --}}
{{-- ///////////////////////////////////////////////////// --}}
<div class="modal fade" id="get_datasheet_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <form class="get_download_sheet" target="_blank" method="post" name="get_download_sheet"> 
                {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title"><i class="far fa-file-alt"></i>&nbsp;&nbsp; Download Datasheet</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group row">

                        <div class="col-sm">
                            <input type="text" class="form-control" name="project_name" id="project_name" placeholder="Project Name">
                        </div>

                    </div>

                    <div class="form-group row">

                        <div class="col-sm">
                            <input name="project_location" id="project_location" class="form-control" type="Project Location" placeholder="Project Location">
                        </div>
                    </div>

                    <div class="form-group row">

                        <div class="col-sm">
                            <input name="project_reference_name" id="project_reference_name" class="form-control" type="text" placeholder="Project Reference Name">
                        </div>

                        <div class="col-sm">
                            <input name="project_reference_value" id="project_reference_value" class="form-control" type="text" placeholder="Project Reference Value">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            <select class="form-control" name="brand">
                              <option value="default">Default</option>
                              <option value="almani">Almani</option>
                              <option value="camel">CamelLED</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">Close</button>
                    <button id="get_download_datasheet" class="btn btn-dark">Get Datasheet</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- ///////////////////////////////////////////////////// --}}
{{-- get_datasheet modal --}}


{{-- FOR LOGIN DESIGNER --}}




{{-- technical_file_modal modal --}}
{{-- ///////////////////////////////////////////////////// --}}
<div class="modal fade" id="technical_file_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <form class="" method="post" action="{{route('requesttechnicalfiles')}}"> {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fa fa-wrench"></i>&nbsp;&nbsp; Request Technical Files</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input name="email_type" type="hidden" value="enquire">
                    <input name="category_alias" type="hidden" value="{{strtolower($category->category_alias)}}">
                    <input name="category_name" type="hidden" value="{{$category->category_name}}">
                    <input name="product_name" type="hidden" value="{{$product->product_name}}">
                    <input name="product_code" type="hidden" value="{{$product->product_code}}">
                    <input name="model_code" type="hidden" value="">

                    <div class="form-group row">
                        <div class="col-sm">
                            <input type="text" class="form-control" name="full_name" id="full_name" placeholder="Full Name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            <input name="email" class="form-control" type="email" placeholder="Email" required="required">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            <input name="tel" id="tel" class="form-control" type="text" placeholder="Telephone/Mobile">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            <textarea class="form-control" name="message" id="message" placeholder="Your Mesage" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            {{-- <label class="checkbox">
                                <input name="do_subscribe" type="checkbox"> Subscribe now to benefit from offers and
                                discounts
                            </label> --}}
                            <br><i>A copy of the enquiry will also be sent to you.</i><br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark" type="submit">Request Technical Files</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- ///////////////////////////////////////////////////// --}}
{{-- technical_file_modal modal --}}





{{-- share via email modal --}}
{{-- ///////////////////////////////////////////////////// --}}
<div class="modal fade" id="share_via_email_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <form class="" method="post" action="{{route('shareviaemail')}}"> {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-share-alt"></i></i>&nbsp;&nbsp; Share Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input name="email_type" type="hidden" value="enquire">

                    <div class="form-group row">
                        <div class="col-sm">
                            <input type="text" class="form-control" name="full_name" id="full_name" placeholder="Full Name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            <input name="email" class="form-control" type="email" placeholder="Email" required="required">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            <input name="tel" id="tel" class="form-control" type="text" placeholder="Telephone/Mobile">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            <input name="email_to_share" class="form-control" type="email" placeholder="Email to share to "
                                required="required">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            <textarea class="form-control" name="message" id="message" placeholder="Your Mesage" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm">
                            {{-- <label class="checkbox">
                                <input name="do_subscribe" type="checkbox"> Subscribe now to benefit from offers and
                                discounts
                            </label> --}}
                            <br><i>A copy of the enquiry will also be sent to you.</i><br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Share</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- ///////////////////////////////////////////////////// --}}
{{-- share via email modal --}}




@endsection















@section('floating-icons')

<!-- <span class="icons back-to-top" title="Back to top">
    <i class="fa fa-chevron-up"></i>
</span> -->

<span class=" icons open_floating_container messaging_btn_product" title="Messaging" data-target="messaging_container_product">
    <i class="far fa-comment"></i>
</span>

<div class="floating-container messaging_container_product">

    {{-- Chats --}}
    {{-- //////////////////////////////////////////////////////////////////////////////// --}}
    <a href="https://api.whatsapp.com/send?phone=0971521043640" target="_blank" class="whatsapp btn btn-dark btn-sm model_action_button">
        <i class="fab fa-whatsapp"></i>
    </a>
    <a href="https://m.me/almani.lighting" target="_blank" class="btn btn-dark btn-sm model_action_button">
        <i class="fab fa-facebook-messenger"></i>
    </a>
    {{-- //////////////////////////////////////////////////////////////////////////////// --}}
    {{-- Chats --}}
</div>



<span class="icons contact_us" title="Contact" data-toggle="modal" data-target="#contact_us_modal" data-message="Hi, I would like to enquire about <?=$product->product_name?> (<?=$product->product_code?>) and would appreciate a call back as soon as possible.">
    <i class="fas fa-phone""></i>
    </span>



    <span class=" icons open_floating_container share_product_btn"
        title="Share <?=$product->product_name?>" data-target="share_products">
        <i class="fas fa-share-alt""></i>
    </span>
    {{-- Share product --}}
    <div class=" floating-container
            share_products">

            <a style="" class="btn btn-dark btn-sm model_action_button share_product" title="Share Product in Facebook"
                data-share_url="<?=$product->slOptions()?>" href="http://www.facebook.com/sharer.php?u=<?=$product->slOptions()?>"
                target="_blank">
                <i class="fab fa-facebook-f"></i>
            </a>
            <a style="" class="btn btn-dark btn-sm model_action_button share_product" title="Tweet this product"
                data-share_url="<?=$product->slOptions()?>" href="https://twitter.com/home?status=<?=$product->slOptions()?>"
                target="_blank">
                <i class="fab fa-twitter"></i>
            </a>
            <a style="" class="btn btn-dark btn-sm model_action_button share_product" title="Share Product on Linkedin"
                data-share_url="<?=$product->slOptions()?>" href="https://www.linkedin.com/shareArticle?mini=true&url=<?=$product->slOptions()?>&title=<?=$product->product_code?>&source="
                target="_blank">
                <i class="fab fa-linkedin"></i>
            </a>
            <a style="" class="btn btn-dark btn-sm model_action_button share_product share_via_email_btn" title="Share Product via Email"
                data-toggle="modal" data-message="I believe you would be interested with this product: <?=$product->product_name?> (<?=$product->product_code?>) and would appreciate a call back as soon as possible."
                data-target="#share_via_email_modal">
                <i class="fas fa-share-alt"></i>
            </a>
            <a style="" class="btn btn-dark btn-sm model_action_button share_product" title="Share Product via Whatsapp"
                data-share_url="<?=$product->slOptions()?>" href="https://api.whatsapp.com/send?text=We believe you would be interested with this product: <?=$product->product_name?> (<?=$product->product_code?>) and for more information you can visit this -> <?=$product->slOptions()?> .">
                <i class="fab fa-whatsapp"></i>
            </a>
            </div>




            <span class="icons open_floating_container option_products_btn" title="Request Files" data-target="other_options_products">
                <i class="fa fa-download"></i>
            </span>
            <div class="floating-container other_options_products">
                {{-- Datasheets --}}
                {{-- //////////////////////////////////////////////////////////////////////////////// --}}
                @if(session('edit_items'))
                <a class="btn btn-dark btn-sm model_action_button get_datasheet" title="Download Datasheet <?=$product->product_name?> (<?=$product->product_code?>)"
                    data-href="{{route('create_datasheet', ['product_code' => $product->product_code])}}">
                    <i class="far fa-file-alt"></i>
                </a>
                @else
                <a class="btn btn-dark btn-sm model_action_button request_datasheet" title="Request Datasheet for <?=$product->product_name?> (<?=$product->product_code?>)"
                    data-toggle="modal" data-target="#datasheet_modal" data-message="Hi, I would like to request DATASHEET for <?=$product->product_name?> (<?=$product->product_code?>) and would appreciate a call back as soon as possible.">
                    <i class="far fa-file-alt"></i>
                </a>
                @endif
                {{-- //////////////////////////////////////////////////////////////////////////////// --}}
                {{-- Datasheets --}}


                {{-- Technical Files --}}
                {{-- //////////////////////////////////////////////////////////////////////////////// --}}
                <a class="btn btn-dark btn-sm model_action_button request_technicalfiles" title="Request Technical File <?=$product->product_name?> (<?=$product->product_code?>)"
                    data-toggle="modal" data-target="#technical_file_modal" data-message="Hi, I would like to request TECHNICAL FILES for <?=$product->product_name?> (<?=$product->product_code?>) and would appreciate a call back as soon as possible.">
                    <i class="fa fa-wrench"></i>
                </a>
                {{-- //////////////////////////////////////////////////////////////////////////////// --}}
                {{-- Technical Files --}}
            </div>


            @endsection


@section('hint')

<div class="custom-social-proof catalog-product-hint">
    <div class="custom-notification">
        <div class="custom-notification-container">
            <div class="custom-notification-content-wrapper">
                <p class="custom-notification-content">
                    <small>Hello! Visit and Explore <a href="https://camel.almani.ae/catalog/{{$category->category_alias}}" target="_blank">Camel LED</a>. </small>
                </p>
            </div>
            <div class="custom-notification-image-wrapper">
                <a href="https://camel.almani.ae/catalog/{{$category->category_alias}}" target="_blank"> <img src="{{App\Util::asset('assets/images/logo/camel-logo-colored-sm.png')}}">
                </a>
            </div>
        </div>
        <div class="custom-close"></div>
    </div>
</div>

@endsection

