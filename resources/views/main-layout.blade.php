<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name=”format-detection” content=”telephone=no”>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	@section('meta-viewport')
	<meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
	@show

	@section('meta-csrf-token')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	@show

	@section('meta')
		@include('sections.meta')
	@show

	<link rel="icon" type="image/png" href="{{App\Util::asset('favicon.ico')}}" />

	<title>
		@section('title')
		Almani Lighting
		@show
	</title>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-KXSLKHM');</script>
		<!-- End Google Tag Manager -->

	<script>
		var $glb_allmenu_urls = "{{route('showallhtmlsubmenu')}}";

		var $glb_menu_urls = {
			'company_menu_container': "{{route('showhtmlsubmenu',['submenu'=>'company_menu_container'])}}",
			'products_menu_container': "{{route('showhtmlsubmenu',['submenu'=>'products_menu_container'])}}",
			'downloads_menu_container': "{{route('showhtmlsubmenu',['submenu'=>'downloads_menu_container'])}}",
		};

		var $glb_mobile_menu_urls = {
			'product-container': "{{route('showmobilehtmlsubmenu',['submenu'=>'mobile-category-submenus'])}}",
			'company-container': "{{route('showmobilehtmlsubmenu',['submenu'=>'mobile-company-submenus'])}}",
			'download-container': "{{route('showmobilehtmlsubmenu',['submenu'=>'mobile-download-submenus'])}}",
			'legal-container': "{{route('showmobilehtmlsubmenu',['submenu'=>'mobile-legal-submenus'])}}",
		};

		var $super_base = "{{App\Util::assetBaseURL('')}}";
		var $super_base2 = "{{asset('')}}";
		var $camel_url = "https://camel.almani.ae";


		var $glbl_currency = [];
		$glbl_currency['AED'] = <?=App\Currency::getAEDMulti('AED')?>;
		$glbl_currency['USD'] = <?=App\Currency::getAEDMulti('USD')?>;
		$glbl_currency['EUR'] = <?=App\Currency::getAEDMulti('EUR')?>;
	</script>

	@section('js-head-lib')
	<script>
		@if(isset($anchor) && $anchor)
	    var anchor = '{{$anchor}}';
	    @else
	    var anchor = '';
	    @endif
	</script>
	@show


	@yield('css-lib')
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt"
	 crossorigin="anonymous">
	
	 {{-- version 2 --}}
	 {{-- <script src='https://www.google.com/recaptcha/api.js'></script> --}}
	 {{-- version 3 --}}
	 <script src='https://www.google.com/recaptcha/api.js?render={{ env('CAPTCHA_KEY') }}'></script>
     {{-- version 3 --}}

</head>

<body id="body">
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KXSLKHM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<div id="super-main-container">


		<!-- preloader -->
		@section('preloader')
		<div id="preloader" style="display: none;">
			<div class="top_preloader"></div>
			<div class="bottom_preloader">
				<img style="height: 100px;" src="{{App\Util::asset('assets/images/icons/loadingdots.gif')}}">
			</div>
		</div>
		@show
		<!-- preloader -->

@if(\Request::is('promotions'))
@else
		<!--   Top Bar   -->
		<section id="topbar" class="d-none d-lg-block">
			<div class="container clearfix">
				<div class="contact-info float-left">
					<a href="mailto:info@almani.ae"><i class="fa fa-envelope"></i> info@almani.ae</a>
					<a href="tel:+97148873265"><i class="fa fa-phone"></i> +971 4-887-3265 </a>

					<?php /*
					<div class="google_translate_container" style="display: inline-block;margin-left: 20px;">
						<div id="google_translate_element"></div>
						<script type="text/javascript">
							function googleTranslateElementInit() {
							new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'de', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
							}
						</script>
						<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
					</div>
					*/ ?>


				</div>
				<div class="social-links float-right">
					<a href="https://www.facebook.com/almani.lighting" target="_blank" class="facebook"><i class="fab fa-facebook-square"></i></a>
					<a href="https://www.instagram.com/almani_lighting" target="_blank" class="instagram"><i class="fab fa-instagram"></i></a>
					<a href="https://www.linkedin.com/company/almani-lighting" target="_blank" class="linkedin"><i class="fab fa-linkedin"></i></a>
					<a href="https://twitter.com/Almani_Lighting" target="_blank" class="twitter"><i class="fab fa-twitter"></i></a>
				</div>
			</div>
		</section>
		<!--   Top Bar   -->




		<!--   Header   -->
		<header id="header">
			<div class="container">
				@include('sections.navmenu')
			</div>
		</header>
		<!--   Header   -->



		<!--   For Mobile Menu   -->
		<nav>
			<div class="d-none" id="nav-menu-container-for-mobile">
				@include('sections.nav-menu-mobile')
			</div>
		</nav>
		<!--   For Mobile Menu   -->
@endif



		<!-- Content -->
		<div class="main" id="main">
			@yield('breadcrumb')

			<!-- <div id="main-content-container"> -->
			<!-- for sidebar menu -->
			@yield('main-content')
		
			<!-- </div> -->

			@yield('gen-info')
		</div>
		
		  <div class="popup" id="popup">

        <div class="popup-inner">
          <div style="background-image: url({{App\Util::asset('/assets/images/homeowners/promo2.jpg')}}); background-size: cover; background-position: center; background-repeat: no-repeat; height: 100vh; width: 100%;" class="d-flex justify-content-center align-items-center">
  <div class="container d-flex justify-content-center">
    <div class="size">
      <div class="text-center pb-3 promo-logo">
        <img src="{{App\Util::asset('/assets/images/logo/logo-white.png')}}" class="img-size">
        <h5 class="mt-3 text-white">Almani Lighting - the UAE finest LED Choice </h5>
      </div>
      <h5 class="text-center text-white">
        <b>
          Please select which of the following fits you best?
        </b>
      </h5>
      <div>
        <div class="card-deck">
          <div class="card-size property-owner" style="cursor: pointer">
            <div class="card card-body promo-height d-flex justify-content-center align-items-center" style="background-color: black; box-shadow: 12px 9px 20px 0px #232323; border: 1px solid white;">
              <div class="text-white text-center">
                <h4 class="mb-1">
                  <b>
                    <div class="link-text">
                      I am a Property Owner
                    </div>
                    <div class="link-icon">
                      <i class="fas fa-arrow-right"></i> 
                    </div>
                  </b>
                </h4>
                <p class="m-0">Villa, Tower, Apartment, Hotel etc. </p>
              </div>
            </div>
          </div>
          <div class="card-size contractor" style="cursor: pointer">
            <div class="card card-body promo-height d-flex justify-content-center align-items-center" style="background-color: black; box-shadow: 12px 9px 20px 0px #232323; border: 1px solid white;">
              <div class="text-white text-center">
                <h4 class="mb-1">
                  <b>
                    <div class="link-text">
                      I am a Contractor
                    </div>
                    <div class="link-icon">
                      <i class="fas fa-arrow-right"></i> 
                    </div>
                  </b>
                </h4>
                <p class="m-0">
                  MEP Contractor, Developer etc.
                </p>
              </div>
            </div>
          </div>
          <div class="card-size consultant second-row" style="margin-top: 30px; cursor: pointer">
            <div class="card card-body promo-height d-flex justify-content-center align-items-center" style="background-color: black; box-shadow: 12px 9px 20px 0px #232323; border: 1px solid white;">
              <div class="text-white text-center">
                <h4 class="mb-1">
                  <b>
                    <div class="link-text">
                      I am a Consultant
                    </div>
                    <div class="link-icon">
                      <i class="fas fa-arrow-right"></i> 
                    </div>
                  </b>
                </h4>
                <p class="m-0">
                  Project Consultant, Interior Designer etc.
                </p>
              </div>
            </div>
          </div>
          <div class="card-size other second-row" style="margin-top: 30px; cursor: pointer;">
            <div class="card card-body promo-height d-flex justify-content-center align-items-center" style="background-color: black; box-shadow: 12px 9px 20px 0px #232323; border: 1px solid white;">
              <div class="text-white text-center">
                <h4 class="mb-1">
                  <b>
                    <div class="link-text">
                      Other
                    </div>
                    <div class="link-icon">
                      <i class="fas fa-arrow-right"></i> 
                    </div>
                  </b>
                </h4>
                <p class="m-0">
                  Supplier, Manufacturer, Job Seeker etc.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
          <!-- <a class="popup__close" href="javascript:;">X</a> -->
        </div>
      </div>


		<!-- @yield('hint') -->
		@yield('modal')

		<!-- Contact_us modal -->
		<div class="modal fade" id="contact_us_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div class="modal-content">

					<form class="" method="post" action="{{ route('sendmessage') }}">
						 {{ csrf_field() }}

						<input type="hidden" value="{{route('sendmessage')}}" id="send_message_url" class="send_message_url">

						<div class="modal-header">
							<h5 class="modal-title"><i class="fas fa-phone"></i>&nbsp;&nbsp; Contact Us</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<input name="email_type" type="hidden" value="contact_us">

							<div class="reach_us">
								<span>You will reach us here:</span>

								<span><a style="font-weight:bold;" href="mailto:info@almani.ae"><i class="fa fa-envelope"></i> info@almani.ae</a></span>

								<span><a style="font-weight:bold;" href="tel:+97148873265"><i class="fa fa-phone"></i> +971 4-887-3265 </a></span>

								<span>Or, kindly submit the form below and we will get back to you very soon.</span>
							</div>


							<div class="form-group row">
								<div class="col-sm">
									<input type="text" class="form-control" name="full_name" id="full_name" placeholder="Full Name">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm">
									<input name="email" class="form-control" type="email" placeholder="Email" required="required">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm">
									<input name="tel" id="tel" class="form-control" type="text" placeholder="Telephone/Mobile">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm">
									<textarea class="form-control" name="message" id="message" placeholder="Your Message" rows="3" required="required"></textarea>
								</div>
							</div>

							<input type="hidden" name="key" id="key">
                       	 <!--    <input type="hidden" value="" name="recaptcha_response" id="recaptchaResponse"> -->
					
					

							<div class="form-group row mb-decreased">
								<div class="col-sm">
									<?php /* <label class="checkbox">
									<input name="do_subscribe" type="checkbox"> Subscribe now to benefit from offers and discounts
								</label> */?>
									<br><i>A copy of this contact email will also be sent to you.</i><br>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-dark sendenquiry" type="submit" disabled="disabled">Send</button>
						</div>
					</form>

					
				</div>
			</div>
		</div>
		<!-- Contact_us modal -->



		<!-- Content -->
		@if(\Request::is('promotions'))
@else
		<!-- <div id="footer-container">		 -->
		@section('footer')
		@include('sections.footer')
		@show
		<!-- </div> -->


		<span class="floating-icons animate animated ">

			@section('floating-icons')

		<!-- 	<span class="icons back-to-top">
				<i class="fa fa-chevron-up"></i>
			</span> -->

			<span class="social-media-icons mb-facebook">
				<a href="https://www.facebook.com/almani.lighting" target="_blank" class="facebook"><i class="fab fa-facebook-square"></i></a>
			</span>

			<span class="social-media-icons mb-instagram">
				<a href="https://www.instagram.com/almani_lighting" target="_blank" class="instagram"><i class="fab fa-instagram"></i></a>
			</span>

			<span class="social-media-icons mb-linkedin">
				<a href="https://www.linkedin.com/company/almani-lighting" target="_blank" class="linkedin"><i class="fab fa-linkedin"></i></a>
			</span>
			<span class="social-media-icons mb-twitter">
				<a href="https://twitter.com/Almani_Lighting" target="_blank" class="linkedin"><i class="fab fa-twitter"></i></a>
			</span>

			<span class="icons contact_us" data-toggle="modal" data-target="#contact_us_modal">
				<i class="fas fa-phone"></i>
			</span>

			<span class="icons open_floating_container messaging_btn" title="data messaging" data-target="messaging_container">
				<i class="far fa-comment"></i>
			</span>

			<div class="floating-container messaging_container" >

					{{-- Chats --}}
					{{-- //////////////////////////////////////////////////////////////////////////////// --}}
						<a href="https://api.whatsapp.com/send?phone=0971521043640" target="_blank" class="whatsapp btn btn-dark btn-sm model_action_button">
							<i class="fab fa-whatsapp"></i>
						</a>
						<a href="https://m.me/almani.lighting" target="_blank" class="btn btn-dark btn-sm model_action_button">
							<i class="fab fa-facebook-messenger"></i>
						</a>
					{{-- //////////////////////////////////////////////////////////////////////////////// --}}
					{{-- Chats --}}
			</div>


			@show

		</span>
@endif

		<!-- Cookie Modal -->
		<div class="cookie-modal animate animated" id="cookie-modal" style="display:none;">
			<div class="cookie-modal-dialog">
				<div class="cookie-modal-content">
					<div class="cookie-modal-body">
						<button type="button" data-dismiss="modal" aria-label="Close" class="close cookie-close"> <span aria-hidden="true">&times;</span></button>
						<div class="cookie-container">
							<div class="cookie-text">
								<h6>
									We use cookies to offer you a better browsing experience, analyze site traffic, personalize content, and serve
									targeted advertisements. <br />
									Read about how we use cookies and how you can control them on our <a class="cookie_private" href="{{route('privacypolicy_page')}}/cookies">Privacy
										Policy.</a>
									If you continue to use this site, you consent to our use of cookies.
								</h6>
							</div>

							{{-- <div class="coo0kie-items cookie-btn" style="margin-top">
								<button type="submit" id="btn-cookie-agree" class="btn-almani">Agree</button>
							</div> --}}
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Coookie Modal -->










		<div id="html-templates" style="display: none !important;">
			@yield('html-templates')
		</div>


		{{-- <div class="browser_notification" id="browser_notification">
			<div class="notification is-danger">
				<button class="delete"></button>
				<p>
					For a full rich experience, <br>
					we suggest to use one of the following browsers:
				</p>
				<img src="assets/images/home/browsers/browser-logos.png">
				<ul>
					<li><img src="https://almani.ae/assets/images/home/browsers/chrome.png"> Google Chrome</li>
					<li><img src="https://almani.ae/assets/images/home/browsers/firefox.png"> Firefox</li>
					<li><img src="https://almani.ae/assets/images/home/browsers/edge.png"> Microsoft Edge</li>
					<li><img src="https://almani.ae/assets/images/home/browsers/safari.png"> Safari</li>
					<li><img src="https://almani.ae/assets/images/home/browsers/opera.png"> Opera</li>
				</ul>
			</div>
		</div> --}}


		@yield('javascript-lib')

				<script type="text/javascript">

					   grecaptcha.ready(function() {
					   		grecaptcha.execute( '{{ env('CAPTCHA_KEY') }}' , { action: 'contact' } )
							    .then(function(token) {
							          var recaptchaResponse = document.getElementById('recaptchaResponse');

							         	if(recaptchaResponse !== null) {
						              		recaptchaResponse.value = token;
						              		$('.sendenquiry').removeAttr('disabled','disabled');
							         		}

						    });
					    });

					   	   grecaptcha.ready(function() {
					   		grecaptcha.execute( '{{ env('CAPTCHA_KEY') }}' , { action: 'contact' } )
							    .then(function(token) {
							          var recaptchaResponse = document.getElementById('recaptchaResponse2');
							          if(recaptchaResponse !== null) {
						              		recaptchaResponse.value = token;
						              		$('.sendenquiry2').removeAttr('disabled','disabled');
							          }


						    });
					    });


			 </script>

	</div>



<!-- LIVE CHAT -->
<script type="text/javascript">function add_chatinline(){var hccid=61153924;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
add_chatinline(); </script>
<!-- LIVE CHAT -->


</body>

</html>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt"
 crossorigin="anonymous">