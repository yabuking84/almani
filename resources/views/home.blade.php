@extends('main-layout', ['home_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-home.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-home.js')}}"></script>
@endsection




@section('title')
    Shop with the Leading LED lights suppliers in Dubai UAE | Almani Lighting
@endsection


@section('meta')

<?php
    $meta_keywords = "Lighting companies dubai, Led lighting companies dubai, LED lights uae, LED lights abu dhabi, LED lights sharjah, LED lights ajman, LED lights ras al khaimah, LED b2b, LED retail, LED wholesale, LED business to business, LED to Own, rent to own, rent LED, LED projects, LED building, LED benefits, LED quality control, LED shop online, LED store dubai, LED store UAE, LED shop UAE, LED dubai";
    $page_desc = "LED Lighting Store Dubai - Almani Lighting is one of the most reputable LED lighting companies in Dubai, UAE. Best quality and prices. LED lights Dubai.";
?>
<!-- META -->
<meta name="description" content="<?= $page_desc ?>"/>
<meta name="author" content="Almani Lighting LLC">
<meta name="keywords" content="<?= $meta_keywords ?>" />
<meta name="google-site-verification" content="G5gSgLSUfMDmKtzKAKnEmfMRgw9Q19IeGcbKU8JxXbo" />

{{-- add canoncal if index.php is being apprehended --}}
@if (strpos($_SERVER['REQUEST_URI'] , 'index.php') !== false)
    <link rel="canonical" href="{{ URL::current() }}" />  
@endif



<meta property="og:title" content="Shop with the Leading LED lights suppliers in Dubai UAE | Almani Lighting">
<meta property="og:type" content="article">
<meta property="og:url" content="https://almani.ae">
<meta property="og:image" content="https://almani.ae/assets/images/logo/logo-colored-og.png">
<meta property="og:description" content="<?= $page_desc ?>">


<!-- /META -->
@endsection


@section('preloader')
<!-- preloader -->

<div id="preloader" style="">
  <div class="top_preloader"></div>
  <div class="bottom_preloader">
     <img style="height: 100px;" src="{{App\Util::asset('assets/images/icons/loadingdots.gif')}}">
  </div>
</div>

<!-- preloader -->
@endsection


@section('main-content')

        @if (session('error') !== null)
            <?php $alert_type = (!session('error'))?"notify-success":"notify-failure"?>
           <div class="success {{ $alert_type }}">
               <span class="success-content">
                   <span class="checkboz"></span>  
                    {{session('message')}}
               </span> 
           </div>
        @endif


    <section id="intro">

        <div id="intro-carousel" class="owl-carousel" >
            @foreach(App\Util::getImages('assets/images/home/carousel',3) as $img)
                <div class="item" style="background-image: url({{App\Util::asset($img)}});"></div>
            @endforeach
        </div>

        <div style="z-index: -1; position: absolute; top: 0;">
            <h1>LED Lighting Dubai UAE</h1>
            <h2>Raising the bar on quality, reliability and value</h2>
        </div>

        <div id="strap_lines" class="owl-carousel" style="">
            <div class="item"><h2>Almani Lighting – the UAE’s finest LED choice</h2></div>
            <div class="item"><h2>Zero Tolerance for defects</h2></div>
            <div class="item"><h2>The Ultimate in LED. Guaranteed.</h2></div>
            <div class="item"><h2>Standard or Bespoke: We’ll work with you, your way</h2></div>
            <div class="item"><h2>Exceptional German quality every step of the way</h2></div>
            <div class="item"><h2>Raising the bar on quality, reliability and value</h2></div>
            <div class="item" style="display: none;"><h2>LED Lighting Dubai, UAE</h2></div>
        </div>

    </section><!-- #intro -->

@endsection




 @section('popup-adds')
      
      <style type="text/css">

        .default_small_heading {
            color: #989898;
            margin: 0;
            word-spacing: 2px;
            line-height: 32px;
        }

        .default_text {
            color: #676767;
            font-size: 14px;
            margin: 0;
            word-spacing: 2px;
            line-height: 17px;
        }

        .font_600 {
             font-weight: 900;
              font-size: 37px;

          }

          .blink_me {
              animation: blinker 1s linear infinite;
            }

            @keyframes blinker {
              50% {
                opacity: 0.2;
              }
            }

      </style>

    

      
      <script>
        
      </script>

 @endsection








