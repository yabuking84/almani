@extends('main-layout', ['company_menu' => 'active','b2bportal_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-b2bportal.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-b2bportal.js')}}"></script>
@endsection



@section('title')
B2B Portal{{App\Meta::webpageTitle()}}
@endsection

@section('main-content')

@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif

<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="is-primary">Business to Business Portal</a>
    </h1>
</section>


<section class="section">

    <div class="container">

        <div class="intro_text">
            <h1 class="title">            
                LED Purchasing Made Easier!
            </h1>
            <p class="title_p" style="">
                As an innovative company, we continuously strive to find ways of improving our customers’ experience and satisfaction. Our insight into the spiraling popularity of LED lighting in the market was the inspiration for our unique, B2B, LED buyers’ portal designed especially for major local and international developers. 
                <br><br>
                Our unique system means you can say goodbye to time-consuming paper ordering processes, phone calls and faxes. Instead, you can place your order speedily and simply via the portal. Setting up your account is a one-step application that takes seconds. Once registered, you have access to all of our products at attractive, pre-agreed prices.
            </p>
        </div>
    </div>


    <div class="container">
        <div class="intro_text">
            <!-- <h1 class="title"></h1> -->
            <p class="title_p click_here" style="">
                If you already have a B2B Portal account please <a href="https://portal.almani.ae">CLICK HERE</a>.
            </p>
        </div>
    </div>


    <div class="container">    
        <div class="video_container" style="">
            <div class="video_intro_img" 
                    data-toggle="modal" 
                    data-target="#video_modal"
                    data-video_url="https://www.youtube.com/embed/SEK0ERhjjSw?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                <img src="{{App\Util::asset('assets/images/icons/youtube_play_icon.png')}}" alt="play">
                <p>
                    Business to Business Portal
                </p>
            </div>

        </div>
    </div>


    <div class="items_bg" style="background-image: url({{App\Util::asset('assets/images/b2bportal/items_bg1.jpg')}});">
        <div class="backdrop"></div>

        <div class="lightbar_text">
            <p class="title_p" style="">
                Enjoy all these major benefits when you choose us, the easiest and most reliable supplier in the market.
            </p>
        </div>
        
            <div class="container">
        <div class="img_text_boxes">
                <?php
                    $items = [];
                    $items[] = ['image'=>'hasslefree.jpg','title'=>'Hassle-free transactions, 24/7','text'=>'Your quotes and orders are just a click or two away.', 'anchor' => 'hasslefreetransactions247'];
                    $items[] = ['image'=>'investment.jpg','title'=>'Great prices, exclusive to you','text'=>'We’ll agree generous discounts upfront – no need to negotiate again and again.', 'anchor' => 'greatpricesexclusivetoyou'];
                    $items[] = ['image'=>'transparent.jpg','title'=>'Quick, easy and transparent','text'=>'Trustworthy, clear and simple to use, with live online support to answer your questions.','anchor' => 'greatpricesexclusivetoyou'];
                    $items[] = ['image'=>'latest.jpg','title'=>'Instant access to latest developments','text'=>'No more sorting through piles of books with limited ranges… Our world-class portfolio and daily updates on latest products and technologies keep you ahead of the competition.', 'anchor' => 'instantaccesstolatestdevelopments'];
                    $items[] = ['image'=>'freeconsultations.jpg','title'=>'Free consultations','text'=>'Our expert LED designers will guide and support your team ensuring best results and maximum value for your project.', 'anchor' => 'freeconsultations'];
                ?>
                @foreach($items as $item)
                <div class="img_text_box" id="{{ $item ['anchor'] }}">
                    <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/b2bportal/'.$item['image'])}});">
                    </div>
                    <div class="text_box">
                        <h1><?=$item['title']?></h1>                        
                        <p><?=$item['text']?></p>
                    </div>
                </div>
                @endforeach 
        </div>
            </div>
    </div>



    <div class="container" id="applynow">
        <div class="contact_form_container">
            <div class="contact_form">
                <form class="form_quote" method="post" action="{{route('sendmessage')}}" enctype="multipart/form-data" style="justify-content: center;">
                {{ csrf_field() }}
                    <div class="form-col">

                        <div class="form-group" style="text-align: center;">
                            <label for="full_name" style="font-weight: 500; border-bottom: 1px solid; font-size: 20px;">
                                Apply Now
                            </label>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-user"></i></span>            
                                </div>
                                <input name="full_name"  id="full_name" class="form-control" type="text" placeholder="Full Name" >
                            </div>        
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                </div>
                                <input name="email" class="form-control email" type="email" placeholder="Email" required>
                            </div>        
                        </div>        

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                </div>
                                <input name="tel" class="form-control" type="tel" placeholder="Phone / Mobile" >
                            </div>        
                        </div>

                        <div class="form-group">     
                            <textarea name="message" class="form-control textarea message" placeholder="Message.." style="height: 110px;"></textarea>
                        </div>        

                    </div>

		           <input type="hidden" value="" name="recaptcha_response" id="recaptchaResponse">

                    
                    
                    <div class="submit_div" style="">
                        <button type="submit" class="btn-almani">Submit</button>
                    </div>

                </form>            
            </div>
        </div>
    </div>


    <div class="spacer" style="border-top: 1px solid gainsboro"></div>

    <div class="container" id="downloadables">    
             <div class="download-container">
            <div class="download-item">
                    <div class="img-container">
                        {{-- <a href="{{ route('downloadpricematchpromise') }}"> --}}
                        <a target="_blank" href="{{ asset('assets/pdfs/b2b.pdf') }}">
                            <img class="animate flipInY animated" src="{{ App\Util::asset('assets/images/covers/b2b-cover.png') }}" alt="Download Price Match Promise" class="download-img-center"
                                class="almani-catgalog">
                            <span class="dl-button"><button class="btn-almani">Download B2B</button> </span>
                        </a>
                    </div>
            </div>
	    </div>
	</div>





</section>




@endsection






@section('modal')

            <!-- Modal -->
            <!-- //////////////////////////////////////////////////////// -->
            <div class="modal fade youtube_video_modal" id="video_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
                <div class="modal-content">          
                  <div class="modal-body">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <iframe class="video" style="" allowfullscreen></iframe>

                    <div style="margin-top: 20px;">
                        <button type="button" 
                            class="btn-almani change_language" 
                            data-target="#video_modal"
                            data-language="english"
                            data-video_url="https://www.youtube.com/embed/SEK0ERhjjSw?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                            English                    
                        </button>
                        <button type="button" 
                            class="btn-almani change_language" 
                            data-target="#video_modal"
                            data-language="arabic"
                            data-video_url="https://www.youtube.com/embed/KcKIuKtZ7so?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                            العَرَبِيَّة
                        </button>                
                    </div>

                  </div>
                </div>
              </div>
            </div>    
            <!-- //////////////////////////////////////////////////////// -->
            <!-- Modal -->
@endsection
