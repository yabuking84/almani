<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">

	<url> 
		<loc>https://almani.ae</loc>
		<changefreq>monthly</changefreq>
		<priority>0.5</priority>
	</url>


{{-- image with almani lighting in filename Google SEO test --}}
{{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}}
{{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}}

@foreach($categories as $category)
	@foreach($category->products as $product)
		<url> 
			<loc>{{route('product_page',['category_alias'=>$category->category_alias,'product_code'=>$product->product_code])}}</loc>				
			<changefreq>weekly</changefreq>
			<priority>0.5</priority>
			<image:image>
				<image:loc>{{asset($product->image('seo'))}}</image:loc>
				<image:caption>Almani Lighting {{$product->product_name}} {{$product->product_code}}</image:caption>
			</image:image>
		</url>
	@endforeach	
@endforeach

 
{{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}}
{{-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --}}
{{-- image with almani lighting in filename Google SEO test --}}







	@foreach($categories as $category)
		<url> 
			<loc>{{route('catalog_product_page',['category_alias'=>$category->category_alias])}}</loc>
			<changefreq>weekly</changefreq>
			<priority>0.9</priority>
			<image:image>
				<image:loc>{{asset('assets/images/cats/'.$category->category_id.'.jpg')}}</image:loc>
				<image:caption>Category {{htmlspecialchars($category->category_name)}}</image:caption>
			</image:image>
		</url>
		<url> 
			<loc>{{route('catalog_product_page',['category_alias'=>$category->category_alias])}}</loc>
			<changefreq>weekly</changefreq>
			<priority>0.9</priority>
		</url>

		@foreach($category->products as $product)
			<url> 
				<loc>{{route('product_page',['category_alias'=>$category->category_alias,'product_code'=>$product->product_code])}}</loc>				
				<changefreq>weekly</changefreq>
				<priority>0.9</priority>
				<image:image>
					<image:loc>{{asset($product->image())}}</image:loc>
					<image:caption>{{$product->product_name}} {{$product->product_code}}</image:caption>
				</image:image>
			</url>
			<url> 
				<loc>{{route('product_page',['category_alias'=>$category->category_alias,'product_code'=>$product->product_code])}}</loc>								
				<changefreq>weekly</changefreq>
				<priority>0.9</priority>
			</url>
		@endforeach
	
	@endforeach

 



	@foreach($pages as $page)
	<url> 
	    <loc>{{route($page)}}</loc>
	    <changefreq>monthly</changefreq>
	    <priority>0.7</priority>
  	</url>
  	@endforeach



</urlset>
  