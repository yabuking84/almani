




<!-- Strat Footer Area -->
<footer class="section-gap footer-widget-area footer">
	<div class="overlay overlay-bg"></div>
	<div class="container">
		<div class="row">
			<div class="footer_group">
				<p class="footer_text">
					<!-- <span style="font-size: 13px;margin-bottom: 4px;font-weight: 500;">Designed in Germany</span> -->
					<!-- <span>318612518714 &nbsp;&nbsp; 99198 &nbsp;&nbsp; 71427321893 </span> -->
					<span>
						&copy;
						Almani Lighting
						<!-- <span style="font-size: 14px; vertical-align: text-top;">&reg;</span>  -->
						GmbH, Germany. <span class="rights_reserved"> All rights reserved. </span>
					</span>
				</p>
			</div>
	<!-- 		<div class="footer_group">
				<a href="https://buyanylight.com" style="text-align: center;">
					<span style="margin-bottom: 0px; font-size: 1.25rem;">Introducing</span>&nbsp;
					<img src="{{App\Util::asset('assets/images/logo/logo-black.png')}}" width="20%" style="padding-bottom: 3px;">
				</a>
			</div> -->
			<div class="footer_group">	
				<ul class="footer_links">
					<li class="li_text"><a href="{{route('terms_page')}}">Terms & Conditions</a></li>
					<li class="li_separator"><i class="fas fa-circle"></i></li>
					<li class="li_text"><a href="{{route('privacypolicy_page')}}">Privacy Policy</a></li>
					<li class="li_separator"><i class="fas fa-circle"></i></li>
					<li class="li_text"><a href="{{route('careers_page')}}">Careers</a></li>
					<li class="li_separator"><i class="fas fa-circle"></i></li>
					<li class="li_text"><a href="https://portal.almani.ae">B2B Portal</a></li>
				<!--     <li class="li_text camel_logo">
					   	<a href="https://camel.almani.ae">
				     		<img src="{{App\Util::asset('assets/images/logo/camel-footer.png')}}" alt="Camel LED" title="Visit Camel LED!" class="camel_logo">
				       	</a>
		        	</li> -->
				</ul>
				
				<!-- <div class="footer_img">
					<img src="https://camel.almani.ae/assets/images/logos/camel.png" alt="Camel LED" title="Visit Camel LED!" style="">
				</div> -->
			</div>
		</div>

	</div>
</footer>
<!-- End Footer Area -->




