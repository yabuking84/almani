
<div id="logo">
    <a href="{{asset('')}}">
        <img class="almani_logo-mobile" src="{{App\Util::asset('assets/images/logo/logo-colored.png')}}" alt="Almani Lighting Logo">
        <img class="almani_logo"  src="{{App\Util::asset('assets/images/logo/logo-colored-xm.png')}}" alt="Almani Lighting Logo">
    </a>
</div>

<!-- ///////////////////////////////////////////// -->
<nav id="nav-menu-container-mobile">
    <button type="button" class="btn btn-light open_menus" id="open_menus">
        <i class="fas fa-bars"></i>
    </button>
</nav>
<!-- ///////////////////////////////////////////// -->
