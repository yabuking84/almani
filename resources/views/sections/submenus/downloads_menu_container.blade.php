<div id="downloads_menu_container" class="menu_container">
    <div class="hide_group menu" data-parent_menu=".downloads_menu">


        <div class="menu_group by_downloads how_hide_group always_show">
            <div class="menu_group_content">
                <?php
                    $route_name = 'aboutus_page_goto';
                    $items = [];
                    $items[] = ['route'=>asset('assets/pdfs/almani-catalog.pdf'),'title'=>'Almani Lighting Catalog','text'=>'Download Almani Lighting Catalog'];
                    $items[] = ['route'=>asset('assets/pdfs/company-profile.pdf'),'title'=>'Company Profile','text'=>'Download Company Profile'];
                    $items[] = ['route'=>asset('assets/pdfs/pre-qualifications.pdf'),'title'=>'Company Pre-qualification','text'=>'Download Company Pre-qualification'];
                    // $items[] = ['route'=>asset('assets/pdfs/camelled-catalog.pdf'),'title'=>'CamelLED Catalog','text'=>'Download CamelLED Catalog'];
                    $items[] = ['route'=>route('flyers_page'), 'title' => 'Flyers', 'text' => 'Download Almani Flyers'];
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="<?=$item['route']?>" target="_blank">
                            <h5><?=$item['title']?></h5>
                            <p><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
