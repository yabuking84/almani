<div id="company_menu_container" class="menu_container">


    <div class="hide_group menu" data-parent_menu=".company_menu">

        <div class="group_by">
                
            <div class="{{isset($welcometoalmani_menu)?'current_page':''}}" data-show_menu="by_welcome_almani">
                        <a href="{{route('welcome_to_almani')}}">
                            <span>Welcome to Almani</span>
                        </a>
            </div>

            <div class="{{isset($aboutus_menu)?'current_page':''}}" data-show_menu="by_aboutus">
                <a href="{{route('aboutus_page')}}">
                    <span>About</span>        
                </a>
            </div>
            <div class="{{isset($whyled_menu)?'current_page':''}}" data-show_menu="by_whyled">
                <a href="{{route('whyled_page')}}">
                    <span>Why LED</span>        
                </a>
            </div>
            <div class="{{isset($sectorsweserve_menu)?'current_page':''}}" data-show_menu="by_sectorsweserve">
                <a href="{{route('sectorsweserve_page')}}">
                    <span>Sectors We Serve</span>        
                </a>
            </div>
            <div class="{{isset($whatwedo_menu)?'current_page':''}}" data-show_menu="by_whatwedo">
                <a href="{{route('whatwedo_page')}}">
                    <span>What We Do</span>        
                </a>
            </div>
            <div class="{{isset($lightingdesign_menu)?'current_page':''}}" data-show_menu="by_lightingdesign">
                <a href="{{route('lightingdesign_page')}}">
                    <span>Lighting Design</span>
                </a>
            </div>
            <div class="{{isset($qualitycontrol_menu)?'current_page':''}}" data-show_menu="by_qualitycontrol">
                <a href="{{route('qualitycontrol_page')}}">
                    <span>Quality Control</span>
                </a>
            </div>
            <div class="{{isset($qualityassurance_menu)?'current_page':''}}" data-show_menu="by_qualityassurance">
                <a href="{{route('qualityassurance_page')}}">
                    <span>Quality Assurance</span>
                </a>
            </div>
            <div class="{{isset($servicepromise_menu)?'current_page':''}}" data-show_menu="by_servicepromise">
                <a href="{{route('servicepromise_page')}}">
                    <span>Service Promise</span>  
                </a>
            </div>
            <div class="{{isset($pricematchpromise_menu)?'current_page':''}}" data-show_menu="by_pricematchpromise">
                <a href="{{route('pricematchpromise_page')}}">
                    <span>Price Match Promise</span>
                </a>
            </div>
            <div class="{{isset($b2bportal_menu)?'current_page':''}}" data-show_menu="by_b2bportal">
                <a href="{{route('b2bportal_page')}}">
                    <span>Business to Business Portal</span>
                </a>
            </div>
            <div class="{{isset($ledtoown_menu)?'current_page':''}}" data-show_menu="by_ledtoown">
                <a href="{{route('ledtoown_page')}}">
                    <span>LED to Own</span>
                </a>
            </div>
            <div class="{{isset($approvedsupplier_menu)?'current_page':''}}" data-show_menu="by_approvedsupplier">
                <a href="{{route('approvedsupplier_page')}}">
                    <span>Approved Supplier</span>
                </a>
            </div>
            <div class="{{isset($customprojects_menu)?'current_page':''}}" data-show_menu="by_customprojects">
                <a href="{{route('customprojects_page')}}">
                    <span>Custom Projects</span>
                </a>
            </div>
       


            <div class="{{isset($message_menu)?'current_page':''}}" data-show_menu="by_message">
                <a href="{{route('message_page')}}">
                    <span>Message from the CEO</span>
                </a>
            </div>

            <div class="{{isset($careers_menu)?'current_page':''}}" data-show_menu="by_careers">
                <a href="{{route('careers_page')}}">
                    <span>Careers</span>
                </a>
            </div>

        </div>









































        <div class="menu_group by_message show_hide_group {{isset($message_menu)?'':'hide_group'}}">        
            <div class="menu_group_content">
                <?php 
                    $route_name = "message_page";
                    $route_name_goto = "message_page_goto";
                    $items = [];
                    $items[] = ['title'=>'CEO and Founder','text'=>'Johannes N. Eidens','css_p'=>'','route'=>route($route_name)];
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="<?=$item['route']?>">
                            <h5><?=$item['title']?></h5>
                            <p style="{{$item['css_p']}}"><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        

        <div class="menu_group by_welcome_almani show_hide_group {{isset($welcometoalmani_menu)?'':'hide_group'}}">        
                <div class="menu_group_content">
                    <?php 
                        $route_name = "welcome_to_almani";
                        $route_name_goto = "welcome_to_almani_page_goto";
                        $items = [];
                        $items[] = ['title'=>'Welcome to Almani Lighting','text'=>' Almani Lighting is committed to quality in all that we do.','css_p'=>'','route'=>route($route_name)];
                    ?>
                    <ul>
                        @foreach($items as $item)
                        <li>
                            <a href="<?=$item['route']?>">
                                <h5><?=$item['title']?></h5>
                                <p style="{{$item['css_p']}}"><?=$item['text']?></p>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>

        <div class="menu_group by_customprojects show_hide_group {{isset($customprojects_menu)?'':'hide_group'}}">        
            <div class="menu_group_content">
                <?php 

                    $route_name = "customprojects_page";
                    $route_name_goto = "customprojects_page_goto";
                    $items = [];
                    $items[] = ['title'=>'Why choose Almani for your Custom Projects','text'=>'Our clients know that the reasons are many and varied, but we can provide 5 prime reasons for choosing Almani.','css_p'=>'','route'=>route($route_name_goto,['anchor'=>'whychoosealmani'])];
                    $items[] = ['title'=>'Unique solutions for complex challenges','text'=>'Leading on LED quality and excellence across the UAE.','css_p'=>'','route'=>route($route_name_goto,['anchor' => 'uniquesolutions'])];
                ?>

                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="<?=$item['route']?>">
                            <h5><?=$item['title']?></h5>
                            <p style="{{$item['css_p']}}"><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>


        <div class="menu_group by_approvedsupplier show_hide_group {{isset($approvedsupplier_menu)?'':'hide_group'}}">        
            <div class="menu_group_content">
                <?php 

                    $route = "approvedsupplier_page";
                    $items = [];

                    $items[] = ['title'=>'Choose Almani as an Approved Supplier','text'=>'We at Almani Lighting have an absolute commitment to high German standards and guaranteed quality at lowest possible prices.','css_p'=>'white-space:nowrap;'];
                    $items[] = ['title'=>'Download','text'=>'Approved Supplier','css_p'=>'white-space:nowrap;'];

                ?>

                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="{{route($route)}}">
                            <h5><?=$item['title']?></h5>
                            <p><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="menu_group by_servicepromise show_hide_group {{isset($servicepromise_menu)?'':'hide_group'}}">        
            <div class="menu_group_content">
                <!-- <h5><a href="{{route('servicepromise_page')}}">Service Promise</a></h5> -->
                <?php 
                    $route = "servicepromise_page_goto";
                    $items = [];
                    $items[] = ['title'=>'Exclusive Warranty for 5 Years','text'=>'We aim to deliver outstanding service.','anchor'=>'exclusivewarrantyfor5years'];
                    $items[] = ['title'=>'Dependable Service','text'=>'From your initial enquiry to after-sales and support.','anchor'=>'dependableservice'];
                    $items[] = ['title'=>'Simple, Speedy, Replacement','text'=>'Your lights will be shining again within 24-48 hours.','anchor'=>'simplespeedyreplacement'];
                    $items[] = ['title'=>'Systematic Storage','text'=>'We keep spare stocks of every item you purchase.','anchor'=>'systematicstorage'];
                    $items[] = ['title'=>'True to Our Promise','text'=>'Our word is our bond.','anchor'=>'truetoourpromise'];
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="{{route($route,['anchor'=>$item['anchor']])}}">
                            <h5><?=$item['title']?></h5>
                            <p><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="menu_group by_sectorsweserve show_hide_group {{isset($sectorsweserve_menu)?'':'hide_group'}}">
            <div class="menu_group_content">
                <?php
                    $route = "sectorsweserve_page_goto";
                    $items = [];
                    $items[] = ['title'=>"Residential",'text'=>"Luxury lighting designs and sumptuous products.",'anchor'=>"residential"];
                    $items[] = ['title'=>"Commercial",'text'=>"Almani maximises both quality and value.",'anchor'=>"commercial"];
                    $items[] = ['title'=>"Hotels, Leisure and Tourism",'text'=>"With beautifully designed and stunning lighting.",'anchor'=>"hotelsleisureandtourism"];
                    $items[] = ['title'=>"Industrial",'text'=>"Reliable, Durable, Great value.",'anchor'=>"industrial"];
                    $items[] = ['title'=>"Transport Hubs",'text'=>"Air, rail, road or water.",'anchor'=>"transporthubs"];
                    $items[] = ['title'=>"Street Lighting",'text'=>"Almani lights the way for the UAE.",'anchor'=>"streetlighting"];
                    $items[] = ['title'=>"Decorative and Seasonal Lighting",'text'=>"Magical lights and effects for special occasions.",'anchor'=>"decorativeandseasonallighting"];
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="{{route($route,['anchor'=>$item['anchor']])}}">
                            <h5><?=$item['title']?></h5>
                            <p><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="menu_group by_qualityassurance show_hide_group {{isset($qualityassurance_menu)?'':'hide_group'}}">
            <div class="menu_group_content">
                <!-- <h5><a href="{{route('qualityassurance_page')}}">Quality Assurance</a></h5> -->
                <?php
                    $route = "qualityassurance_page_goto";
                    $items = [];
                    $items[] = ['title'=>"German",'text'=>"Rooted in our German heritage.",'anchor'=>"german"];
                    $items[] = ['title'=>"Honest and Transparent",'text'=>"Made to perform perfectly for years and years. Guaranteed.",'anchor'=>"honestandtransparent"];
                    $items[] = ['title'=>"Best LED Brand in UAE",'text'=>"5-year warranty on every single Almani product.",'anchor'=>"bestledbrandinuae"];                    
                ?>                
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="{{route($route,['anchor'=>$item['anchor']])}}">
                            <h5><?=$item['title']?></h5>
                            <p><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach

                    <li>
                        <a href="{{route($route,['anchor'=>'video'])}}">
                            <?php /* <h5>Watch our Video</h5> */?>
                            <p>
                                <img src="{{App\Util::asset('assets/images/icons/youtube_play_icon.png')}}" alt="play">
                                Quality Assurance
                            </p>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
        <div class="menu_group by_qualitycontrol show_hide_group {{isset($qualitycontrol_menu)?'':'hide_group'}}">
            <div class="menu_group_content">
                <!-- <h5><a href="{{route('qualitycontrol_page')}}">Quality Control</a></h5> -->
                <?php
                    $route = "qualitycontrol_page_goto";
                    $items = [];
                    $items[] = ['title'=>'Five-Year Warranty','text'=>'Guaranteed each one is the best available on the market.','anchor'=>'fiveyearwarranty'];
                    $items[] = ['title'=>'Quality Control Process','text'=>'We work only with the best.','anchor'=>'qcprocess'];
                    $items[] = ['title'=>'Zero Tolerance for Defects','text'=>'Our products are of superb quality and we are committed to zero defects.','anchor'=>'zerotolerancefordefects'];
                    $items[] = ['title'=>'Certifications & Brands','text'=>'Find out what partnering with Almani means and what you will get.','anchor'=>'certificationsandbrands'];
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="{{route($route,['anchor'=>$item['anchor']])}}">
                            <h5><?=$item['title']?></h5>
                            <p><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                </ul>

            </div>
        </div>
        <div class="menu_group by_lightingdesign show_hide_group {{isset($lightingdesign_menu)?'':'hide_group'}}">
            <div class="menu_group_content">
                <!-- <h5><a href="{{route('lightingdesign_page')}}">Lighting Design</a></h5> -->
                <?php
                    $route = "lightingdesign_page_goto";
                    $items = [];
                    $items[] = ['title'=>'Full Site Modelling','text'=>'Our skilled LED lighting designers will create a full 3D design for your project.','anchor'=>'fullsitemodelling'];
                    $items[] = ['title'=>'False Colour Rendering','text'=>'Our design experts offer both 2D and 3D false colour views.','anchor'=>'falsecolourrendering'];
                    $items[] = ['title'=>'Lighting Layouts','text'=>'Invaluable for the final installation on site.','anchor'=>'lightinglayouts'];
                    $items[] = ['title'=>'Isolines Mapping','text'=>'We give clear and detailed picture of the relative lux levels and light distribution.','anchor'=>'isolinesmapping'];
                    $items[] = ['title'=>'Value Grid','text'=>'We can supply you with value grids 2D plans.','anchor'=>'valuegrid'];
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="{{route($route,['anchor'=>$item['anchor']])}}">
                            <h5><?=$item['title']?></h5>
                            <p><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                </ul>

            </div>
        </div>

        <div class="menu_group by_pricematchpromise show_hide_group {{isset($pricematchpromise_menu)?'':'hide_group'}}">
            <div class="menu_group_content">
                <!-- <h5><a href="{{route('pricematchpromise_page')}}">Price Match Promise</a></h5> -->
                <?php
                    $route = "pricematchpromise_page_goto";
                    $items = [];
                    $items[] = ['title'=>'Best LED Lighting Prices','text'=>'We’ll match the price, guaranteed!','anchor'=>'bestledlightingprices'];
                    $items[] = ['title'=>'Controlled Manufacture','text'=>'We carefully control the manufacture of all our products.','anchor'=>'controlledmanufacture'];
                    $items[] = ['title'=>'From The Manufacturer Direct to You','text'=>'We work directly with you, avoiding extra layers of costs.','anchor'=>'fromthemanufacturerdirecttoyou'];
                    $items[] = ['title'=>'In Fine Company','text'=>'We manufacture in the same places as some of the world’s most celebrated LED brands.','anchor'=>'infinecompany'];
                      $items[] = ['title'=>'Download','text'=>'Price Match Promise Flyer','anchor'=>'downloadables'];
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="{{route($route,['anchor'=>$item['anchor']])}}">
                            <h5><?=$item['title']?></h5>
                            <p><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                    <li>
                        <a href="{{route($route,['anchor'=>'video'])}}">
                            <?php /* <h5>Watch our Video</h5> */ ?>
                            <p>
                                <img src="{{App\Util::asset('assets/images/icons/youtube_play_icon.png')}}" alt="play">
                                Price Match Promise
                            </p>
                        </a>
                    </li>

                </ul>
            </div>
        </div>

        <div class="menu_group by_ledtoown show_hide_group {{isset($ledtoown_menu)?'':'hide_group'}}">
            <div class="menu_group_content">
                <!-- <h5><a href="{{route('ledtoown_page')}}">LED to Own</a></h5> -->
                <?php
                    $route = "ledtoown_page_goto";
                    $items = [];
                    $items[] = ['title'=>'Free Led Lights','text'=>'Get your free LED lights today!','anchor'=>'freeledlights'];
                    $items[] = ['title'=>'Without Any Investment','text'=>'Discover how much you can save without investing a single dirham.','anchor'=>'withoutanyinvestment'];
                    $items[] = ['title'=>'With Zero Risk','text'=>'Save money and reduce your carbon footprint without any risk.','anchor'=>'withzerorisk'];
                    $items[] = ['title'=>'With a Long Warranty','text'=>'We guarantee a hassle free time.','anchor'=>'withalongwarranty'];
                    $items[] = ['title'=>'Download','text'=>'Led to Own Flyer','anchor'=>'downloadables'];
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="{{route($route,['anchor'=>$item['anchor']])}}">
                            <h5><?=$item['title']?></h5>
                            <p><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                    <li>
                        <a href="{{route($route,['anchor'=>'video'])}}">
                            <?php /* <h5>Watch our Video</h5> */ ?>
                            <p>
                                <img src="{{App\Util::asset('assets/images/icons/youtube_play_icon.png')}}" alt="play">
                                LED to Own
                            </p>
                        </a>
                    </li>

                </ul>

            </div>
        </div>
        <div class="menu_group by_whyled show_hide_group {{isset($whyled_menu)?'':'hide_group'}}">
            <div class="menu_group_content">
                <?php
                    $route = "whyled_page_goto";
                    $items = [];
                    $items[] = ['title'=>'Saves On Energy Costs','text'=>'Conventional light bulbs and tubes are around 20% efficient, while the energy efficiency of LED lights is between 60-90%.','anchor'=>'savesonenergycosts'];
                    $items[] = ['title'=>'Longer Life','text'=>'Bulb replacement is a constant 40x as long as an incandescent bulb and 6x as long as a fluorescent unit.','anchor'=>'longerlife'];
                    $items[] = ['title'=>'Cooler','text'=>'LED lighting produces very little infrared (heat) energy.','anchor'=>'cooler'];
                    $items[] = ['title'=>'Greener','text'=>'LED lighting brings numerous ecological benefits.','anchor'=>'greener'];
                    $items[] = ['title'=>'Design Flexibility','text'=>'LEDs allow maximum flexibility.','anchor'=>'designflexibility'];
                    $items[] = ['title'=>'Smarter','text'=>'The future of smart LED technologies is vast.','anchor'=>'smarter'];
                    $items[] = ['title'=>'Instant Light','text'=>'Giving full brightness from the moment they\'re switched on.','anchor'=>'instantlight'];
                    $items[] = ['title'=>'Safety','text'=>'LED lights generate virtually no heat.','anchor'=>'safety'];
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="{{route($route,['anchor'=>$item['anchor']])}}">
                            <h5><?=$item['title']?></h5>
                            <p><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                </ul>

            </div>
        </div>
        <div class="menu_group by_whatwedo show_hide_group {{isset($whatwedo_menu)?'':'hide_group'}}">
            <div class="menu_group_content">
                <!-- <h5><a href="{{route('whatwedo_page')}}">What We Do</a></h5> -->
                <?php
                    $route = "whatwedo_page_goto";
                    $items = [];
                    $items[] = ['title'=>'Overview','text'=>'Almani Lighting is proud to deliver consistently exceptional standards of LED lighting design, supply and installation.','anchor'=>'overview'];
                    $items[] = ['title'=>'Lighting Advice and Consultancy','text'=>' Our skilled professionals will give you all the help you want.','anchor'=>'lightingadvice'];
                    $items[] = ['title'=>'LED Lighting Design','text'=>'Create superb, innovative and effective solutions.','anchor'=>'lightingdesign'];
                    $items[] = ['title'=>'The Almani Lighting Range','text'=>'LED lighting projects of all kinds.','anchor'=>'ligthingrange'];
                    $items[] = ['title'=>'LED Lighting Installation','text'=>'Design your lighting installations to match your brief and budget.  ','anchor'=>'ledinstall'];
                    $items[] = ['title'=>'LED Lighting Refits','text'=>'LED lighting refits and retrofits.','anchor'=>'lightrefits'];
                    $items[] = ['title'=>'Lighting Rental','text'=>'Install and run their new lighting system.','anchor'=>'lightingrental'];
                    $items[] = ['title'=>'LED Smarthome Solutions','text'=>'Cloud-controlled home comfort, convenience and cost efficiency.','anchor'=>'smartsolution'];
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="{{route($route,['anchor'=>$item['anchor']])}}">
                            <h5><?=$item['title']?></h5>
                            <p><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                </ul>

            </div>
        </div>



        <div class="menu_group by_careers show_hide_group {{isset($careers_menu)?'':'hide_group'}}">
            <div class="menu_group_content">                
                <?php
                    $route = "careers_page_goto";
                    $items = [];
                    $items[] = ['title'=>' Join Us','text'=>'Why should we work with you? Surprise us, convince us and we welcome you to the probably most amazing team!','anchor'=>'joinus', 'css_p'=>'white-space:nowrap;'];
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="{{route($route,['anchor'=>$item['anchor']])}}">
                            <h5><?=$item['title']?></h5>
                            <p style="{{$item['css_p']}}"><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                </ul>

            </div>
        </div>



        <div class="menu_group by_aboutus show_hide_group {{isset($aboutus_menu)?'':'hide_group'}}">
            <div class="menu_group_content">
                <?php
                    $route_name = 'aboutus_page_goto';
                    $items = [];
                    $items[] = ['route'=>route($route_name,['anchor'=>'whoweare']),'title'=>'Who we are','text'=>'Friendly, helpful and professional, we at Almani Lighting aspires to offer the highest standards of customer care and service.'];
                    $items[] = ['route'=>route($route_name,['anchor'=>'mission']),'title'=>'Our Mission','text'=>'Almani Lighting designs, supplies and installs finest quality LED lighting.'];
                    $items[] = ['route'=>route($route_name,['anchor'=>'vision']),'title'=>'Our Vision','text'=>'We will earn our place as a global leader in LED lighting design.'];
                    $items[] = ['route'=>route($route_name,['anchor'=>'values']),'title'=>'Our Values','text'=>'The values which inspire and guide all that we do at Almani Lighting.'];
                    $items[] = ['route'=>route($route_name,['anchor'=>'whychooseus']),'title'=>'Why choose us','text'=>'We always go the extra distance to make Almani Lighting your global lighting partner of choice.'];
                    $items[] = ['route'=>route($route_name,['anchor'=>'almanibenefits']),'title'=>'Almani Benefits','text'=>'Excellence, Reliability, Integrity, Positivity, Value, Environmental care, Partnership and Dedication.'];
                    $items[] = ['route'=>route($route_name,['anchor'=>'almaniapproach']),'title'=>'Almani Approach','text'=>'We at Almani aim to be and deliver the very best, every time. '];
                    $items[] = ['route'=>route($route_name,['anchor'=>'testimonials']),'title'=>'Testimonials','text'=>'See what our satisified customers has to say.'];
                    $items[] = ['route'=>route($route_name,['anchor'=>'clients']),'title'=>'Clients','text'=>'Companies who trust Almani.'];
                    $items[] = ['route'=>route('downloadprofile_page'),'title'=>'Company Profile','text'=>'Download Company Profile'];
                    $items[] = ['route'=>route('downloadprequalification_page'),'title'=>'Company Pre-qualification','text'=>'Download Company Pre-qualification'];
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="<?=$item['route']?>">
                            <h5><?=$item['title']?></h5>
                            <p><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                    <li>
                        <a href="{{route('aboutus_page_goto',['anchor'=>'video'])}}">
                            <?php /*<h5>Watch our Video</h5>*/?>
                            <p>
                                <img src="{{App\Util::asset('assets/images/icons/youtube_play_icon.png')}}" alt="play">
                                Why Choose us?
                            </p>
                        </a>
                    </li>

                </ul>

            </div>
        </div>


        <div class="menu_group by_b2bportal show_hide_group {{isset($b2bportal_menu)?'':'hide_group'}}">
            <div class="menu_group_content">
                <?php
                    $route = "b2bportal_page_goto";
                    $items = [];
                    $items[] = ['title'=>'Hassle-Free Transactions, 24/7','text'=>'Your quotes and orders are just a click or two away.','anchor'=>'hasslefreetransactions247'];                
                    $items[] = ['title'=>'Great Prices, exclusive to you','text'=>'We’ll agree generous discounts upfront – no need to negotiate again and again.','anchor'=>'greatpricesexclusivetoyou'];                
                    $items[] = ['title'=>'Quick, easy and transparent','text'=>'Trustworthy, clear and simple to use, with live online support to answer your questions.','anchor'=>'quickeasyandtransparent'];                
                    $items[] = ['title'=>'Instant Access to Latest Developments','text'=>'No more sorting through piles of books with limited ranges.','anchor'=>'instantaccesstolatestdevelopments'];                
                    $items[] = ['title'=>'Free Consultations','text'=>'Our expert LED designers will guide and support your team.','anchor'=>'freeconsultations'];    
                    $items[] = ['title'=>'Download','text'=>'B2B Flyer','anchor'=>'downloadables'];                   
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="{{route($route,['anchor'=>$item['anchor']])}}">
                            <h5><?=$item['title']?></h5>
                            <p><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                    <li>
                        <a href="{{route($route,['anchor'=>'video'])}}">
                            <?php /*<h5>Watch our Video</h5>*/?>
                            <p>
                                <img src="{{App\Util::asset('assets/images/icons/youtube_play_icon.png')}}" alt="play">
                                B2B Portal
                            </p>
                        </a>
                    </li>

                </ul>

            </div>
        </div>

    </div>
</div>
