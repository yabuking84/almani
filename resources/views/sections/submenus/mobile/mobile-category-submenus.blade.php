{{-- CATEGORIES Drop Down --}}
{{-- MAIN CATEGORY OF CATEGORIES --}}
<a class="dd-menu" href="javascript:void(0);">
    <li>Categories <span class="toggle-info"> <i class="fas fa-caret-right"></i> </span> </li>
</a>
{{-- MAIN CATEGORY OF CATEGORIES --}}
@include('sections.static.mobile.categories-links-mobile')
{{-- CATEGORIES Drop Down --}}

{{-- Application Drop Down --}}
<a class="dd-menu" href="javascript:void(0);">
    <li> Application Areas <span class="toggle-info"><i class="fas fa-caret-right"></i></span> </li>
</a>

@include('sections.static.mobile.application_areas-links-mobile')
{{-- Application Drop Down --}}

<!-- <a href="{{ route('catalog_brand_page') }}">
    <li>Brands</li>
</a> -->
<a href="{{ route('roi_page') }}">
    <li>Return on Investment Calculator</li>
</a>
<a href="{{ route('downloadcatalog_page') }}">
    <li>Download Catalog</li>
</a>

