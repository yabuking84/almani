<a href="{{ route('download_page') }}"><li><button class="btn btn-sm btn-dark btn-border">View All</button></li></a>
<a href="{{ asset('assets/pdfs/almani-catalog.pdf') }}"><li>Almani Lighting Catalog</li></a>
<a href="{{ asset('assets/pdfs/company-profile.pdf') }}"><li>Company Profile</li></a>
<a href="{{ asset('assets/pdfs/pre-qualifications.pdf') }}"><li>Company Pre-qualification</li></a>
<!-- <a href="{{ asset('assets/pdfs/camelled-catalog.pdf') }}"><li>CamelLED Catalog</li></a> -->
<a href="{{ route('flyers_page') }}"><li>Flyers</li></a>
