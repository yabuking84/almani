
            <ul class="company_menu_ul">
                <li class="{{isset($welcometoalmani_menu)?'current_page':''}}">
                        <a href="{{route('welcome_to_almani')}}">Welcome to Almani</a>
                </li>
                <li class="{{isset($aboutus_menu)?'current_page':''}}">
                    <a href="{{route('aboutus_page')}}">About</a>
                </li>                           
                <li class="{{isset($catalogdownload_menu)?'current_page':''}}">
                    <a href="{{route('downloadprofile_page')}}">Company Profile</a>
                </li>
                <li class="{{isset($catalogdownload_menu)?'current_page':''}}">
                    <a href="{{route('downloadprequalification_page')}}">Company Pre-qualification</a>
                </li>
                <li class="{{isset($whyled_menu)?'current_page':''}}">
                    <a href="{{route('whyled_page')}}">Why LED</a>
                </li>                           
                <li class="{{isset($sectorsweserve_menu)?'current_page':''}}">
                    <a href="{{route('sectorsweserve_page')}}">Sectors We Serve</a>
                </li>
                <li class="{{isset($whatwedo_menu)?'current_page':''}}">
                    <a href="{{route('whatwedo_page')}}">What We Do</a>
                </li>                           
                <li class="{{isset($lightingdesign_menu)?'current_page':''}}">
                    <a href="{{route('lightingdesign_page')}}">Lighting Design</a>
                </li>                           
                <li class="{{isset($qualitycontrol_menu)?'current_page':''}}">
                    <a href="{{route('qualitycontrol_page')}}">Quality Control</a>
                </li>                           
                <li class="{{isset($qualityassurance_menu)?'current_page':''}}">
                    <a href="{{route('qualityassurance_page')}}">Quality Assurance</a>
                </li>
                <li class="{{isset($servicepromise_menu)?'current_page':''}}">
                    <a href="{{route('servicepromise_page')}}">Service Promise</a>
                </li>
                <li class="{{isset($pricematchpromise_menu)?'current_page':''}}">
                    <a href="{{route('pricematchpromise_page')}}">Price Match Promise</a>
                </li>                           
                <li class="{{isset($b2bportal_menu)?'current_page':''}}">
                    <a href="{{route('b2bportal_page')}}">Business to Business Portal</a>
                </li>
                <li class="{{isset($ledtoown_menu)?'current_page':''}}">
                    <a href="{{route('ledtoown_page')}}">LED to Own</a>
                </li>
                <li class="{{isset($approvedsupplier_menu)?'current_page':''}}">
                    <a href="{{route('approvedsupplier_page')}}">Approved Supplier</a>
                </li>
                <li class="{{isset($customprojects_menu)?'current_page':''}}">
                    <a href="{{route('customprojects_page')}}">Custom Projects</a>
                </li>
               
                <li class="{{isset($message_menu)?'current_page':''}}">
                    <a href="{{route('message_page')}}">Message from the CEO</a>
                </li>
                <li class="{{isset($careers_menu)?'current_page':''}}">
                    <a href="{{route('careers_page')}}">Careers</a>
                </li>
            </ul>
