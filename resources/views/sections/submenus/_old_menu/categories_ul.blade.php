
            <ul class="categories">
                <li class="menu-has-children">
                    <a>
                        Categories
                        <i class="fa fa-caret-down caret_down hide_group"></i>
                        <i class="fa fa-caret-right caret_right"></i>
                    </a>

                    @include('sections.static.categories-links-mobile')



                </li>
                <li class="menu-has-children">
                    <a>
                        Application Areas
                        <i class="fa fa-caret-down caret_down hide_group"></i>
                        <i class="fa fa-caret-right caret_right"></i>
                    </a>

                    @include('sections.static.application_areas-links-mobile')

                </li>
                <li class="{{isset($catalog_brand_menu)?'current_page':''}}">
                    <a href="{{route('catalog_brand_page')}}">Brands</a>
                </li>

                <li class="{{isset($roi_menu)?'current_page':''}}">
                    <a href="{{route('roi_page')}}">Return On Investment Calculator</a>
                </li>

                <li class="{{isset($catalogdownload_menu)?'current_page':''}}">
                    <a href="{{route('downloadcatalog_page')}}">Download Catalog</a>
                </li>

            </ul>
