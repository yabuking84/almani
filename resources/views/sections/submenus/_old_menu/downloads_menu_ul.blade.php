<ul class="downloads_menu_ul">
    <li class="">
        <a href="{{route('downloadcatalog_page')}}">
            Almani Lighting Catalog
        </a>
    </li>
    <li class="">
        <a href="{{route('downloadprofile_page')}}">
            Company Profile
        </a>
    </li>
    <li class="">
        <a href="{{route('downloadprequalification_page')}}">
            Company Pre-qualification
        </a>
    </li>
    <li class="">
        <a href="{{route('downloadcatalog_camel_page')}}">
            CamelLED Catalog
        </a>
    </li>
</ul>
