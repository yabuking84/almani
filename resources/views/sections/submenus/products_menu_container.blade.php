
<div id="products_menu_container">

    <div id="products_menu" class="hide_group">

        <div class="group_by">
        	<div class="selected cursor_default" data-show_menu="by_category">
                <span><a href="{{route('catalog_landing_page')}}">Categories</a></span>
            </div> 

            <div data-show_menu="by_application_areas">
                <span>
                    <a href="{{route('application_area_landing_page')}}">
                        Application Areas             
                    </a>
                </span> 
            </div>

    <!--         <div data-show_menu="by_catalog_brand">
                <span>
                    <a href="{{route('catalog_brand_page')}}">Brands</a>
                </span>            
            </div> -->

            <div data-show_menu="by_downloadcatalog">
                <span>
                    <a href="{{asset('assets/pdfs/almani-catalog.pdf')}}">Catalog</a>
                </span>            
            </div>

            <div data-show_menu="by_roi">
                <span>
                    <a href="{{route('roi_page')}}">Return On Investment Calculator</a>
                </span>
	        </div>


        </div>




        <div class="product_group by_downloadcatalog show_hide_group hide_group menu_group">
            <div class="menu_group_content">
                <?php
                    // $route_name = 'downloadcatalog_page';
                    $items = [];
                    $items[] = ['title'=>'Catalog','text'=>'Download Almani Catalog','css_p'=>'','route'=>asset('assets/pdfs/almani-catalog.pdf')];
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="<?=$item['route']?>">
                            <h5><?=$item['title']?></h5>
                            <p style="{{$item['css_p']}}"><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                </ul>

            </div>
        </div>


        <div class="product_group by_catalog_brand show_hide_group hide_group menu_group">
            <div class="menu_group_content">
                <?php
                    $route = "catalog_brand_page_goto";
                    $items = [];
                    $items[] = ['title'=>'Almani Lighting','text'=>'Almani Lighting offers a watertight 5-year warranty on every one of our beautiful products.','anchor'=>'almani','css_p'=>''];
                    $items[] = ['title'=>'CamelLED','text'=>'We created CamelLED to deliver good quality and exceptional value.','anchor'=>'camelled','css_p'=>''];
                    
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="{{route($route,['anchor'=>$item['anchor']])}}">
                            <h5><?=$item['title']?></h5>
                            <p style="{{$item['css_p']}}"><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                    <li>
                            <a href="{{route($route,['anchor'=>'video'])}}">
                                <p>
                                    <img src="{{App\Util::asset('assets/images/icons/youtube_play_icon.png')}}" alt="play">
                                     Explore which brand is for you!
                                </p>
                            </a>
                        </li>
                </ul>

            

            </div>
        </div>

        <div class="product_group by_roi show_hide_group hide_group menu_group">
            <div class="menu_group_content">
                <?php
                    $route = "roi_page_goto";
                    $items = [];
                    $items[] = ['title'=>'Calculate Your ROI Payback Period','text'=>'Calculate your Return on Investment (ROI) payback period for installing Almani Lighting LED lights.','anchor'=>'calculateyourroipaybackperiod','css_p'=>'white-space: nowrap;'];
                    
                ?>
                <ul>
                    @foreach($items as $item)
                    <li>
                        <a href="{{route($route,['anchor'=>$item['anchor']])}}">
                            <h5><?=$item['title']?></h5>
                            <p style="{{$item['css_p']}}"><?=$item['text']?></p>
                        </a>
                    </li>
                    @endforeach
                </ul>

            </div>
        </div>



        @include('sections.static.categories-links')

        @include('sections.static.application_areas-links')


    </div>
</div>
