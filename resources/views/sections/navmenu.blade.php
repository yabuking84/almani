
       
<!-- ///////////////////////////////////////////// -->
<nav id="nav-menu-container-mobile">
    <button type="button" class="btn btn-light open_menu">
        <i class="fas fa-bars"></i>
    </button>
</nav>
<!-- ///////////////////////////////////////////// -->











<div id="logo">
    <a href="{{asset('')}}">
        <!-- <img src="{{App\Util::asset('assets/images/logo/logo-colored.jpg')}}" alt="Almani Lighting Logo"> -->
        <img class="almani_logo-mobile" src="{{App\Util::asset('assets/images/logo/logo-colored.png')}}" alt="Almani Lighting Logo">
        <img class="almani_logo"  src="{{App\Util::asset('assets/images/logo/logo-colored-xm.png')}}" alt="Almani Lighting Logo">
    </a>
</div>







{{-- for desktop screen sizes --}}
<nav id="nav-menu-container">
    <ul class="nav-menu">

        
        <li class="{{isset($home_menu)?'current_page':''}}">
        	<a href="{{route('home_page')}}">Home</a>
        </li>
    	

        
        <li class="menu-has-children {{isset($company_menu)?'current_page':''}}">
        	<a class="show_menu c_default company_menu" data-target_menu_container="#company_menu_container">
        		Company 
        		<i class="fa fa-caret-down caret_down hide_group"></i>
        		<i class="fa fa-caret-right caret_right"></i>	                		
        	</a>

            <!-- For mobile only -->
            {{-- <span id="company_menu_ul_here">company_menu_ul here</span> --}}
            <!-- For mobile only -->
        </li>
    	

        
        <li class="{{isset($products_menu)?'current_page':''}}" id="show_category_menu">
            <a href="{{route('catalog_landing_page')}}" id="show_category"> 
                Products
                <i class="fa fa-caret-down caret_down hide_group"></i>
                <i class="fa fa-caret-right caret_right"></i>
            </a>

            <!-- For mobile only -->
            {{-- <span id="categories_ul_here">categories_ul here</span> --}}
            <!-- For mobile only -->
        </li>
        
        <li class="{{isset($projects_menu)?'current_page':''}}">
            <a href="{{route('projects_page')}}">Projects</a>
        </li>


        
        <li class="menu-has-children {{isset($downloads_menu)?'current_page':''}}">
            <a href="{{ route('download_page') }}" class="show_menu downloads_menu" data-target_menu_container="#downloads_menu_container">
                Downloads
                <i class="fa fa-caret-down caret_down hide_group"></i>
                <i class="fa fa-caret-right caret_right"></i>                           
            </a>

            <!-- For mobile only -->
            {{-- <span id="downloads_menu_ul_here">downloads_menu_ul here</span> --}}
            <!-- For mobile only -->
        </li>



        <li class="{{isset($getaquote_menu)?'current_page':''}}">
        	<a href="{{route('getaquote_page')}}">Quote</a>
        </li>	                

        <li class="{{isset($contactus_menu)?'current_page':''}}">
            <a href="{{route('contactus_page')}}">Contact</a>
        </li>    

        <li class="{{isset($login_menu)?'current_page':''}}">
            <a href="{{route('clientlogin')}}">Login</a>
        </li>                
        
        {{-- Disable for now --}}
        {{-- <li class="{{isset($blog_menu)?'current_page':''}}">
            <a href="{{route('blog_page')}}">Blog</a>
        </li> --}}



        <!-- For mobile only -->
        <li class="{{isset($legal_menu)?'current_page':''}} menu-has-children legal_menu">
            <a>
                Legal
                <i class="fa fa-caret-down caret_down hide_group"></i>
                <i class="fa fa-caret-right caret_right"></i>
            </a>
            <ul>
                <li class="{{isset($terms_page)?'current_page':''}}">
                    <a href="{{route('terms_page')}}">Terms & Conditions</a>
                </li>                           
                <li class="{{isset($privacypolicy_page)?'current_page':''}}">
                    <a href="{{route('privacypolicy_page')}}">Privacy Policy</a>
                </li>
            </ul>
        </li>
        <!-- For mobile only -->



        <!-- For mobile only -->
        <li class="{{isset($camel_menu)?'current_page':''}} camel_menu">
            <a href="https://camel.almani.ae">
                {{-- <img src="https://camel.almani.ae/assets/images/logos/camel.png" alt="Camel LED" title="Visit Camel LED!" class="camel_logo" style="height: 25px;"> --}}
            </a>
        </li>	                
        <!-- For mobile only -->


        <li class="{{isset($search_menu)?'current_page':''}}" style="display: inline-flex;" id="open_search_container">
        	<a href="{{route('search_page')}}" class="open_search"><i class="fas fa-search search"></i></a>
            <form method="post" action="{{route('search_nav_page')}}" id="open_search_form">
                {{ csrf_field() }}
                <div id="search_container" class="search_container" style="display: none;">
                    <div class="input-group search_div">
                        <input type="text" class="form-control form-control-sm" placeholder="Search Product Name.." name="search_fld">
                        <div class="input-group-append">                
                            <button type="submit" class="btn btn-dark btn-almani"><i class="fas fa-search search"></i></button>
                        </div>
                    </div>
                </div>            
            </form>
        </li>



    </ul>
</nav>
{{-- for desktop screen sizes --}}



























<!-- for desktop menu -->
<span id="products_menu_container_here">{{-- products_menu_container here --}}</span>
<?php /*@include('sections.submenus.products_menu_container')*/ ?>


<span id="company_menu_container_here">{{-- company_menu_container here --}}</span>
<?php /*@include('sections.submenus.company_menu_container')*/ ?>


<span id="downloads_menu_container_here">{{-- downloads_menu_container here --}}</span>
<?php /*@include('sections.submenus.downloads_menu_container')*/ ?>
<!-- for desktop menu -->
