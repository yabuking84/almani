
<!-- META -->
<!-- <meta name="google-site-verification" content="" /> -->
<meta name="description" content="<?= $meta['description'] ?>"/>
<meta name="author" content="Almani Lighting LLC">
<meta name="keywords" content="<?= $meta['keywords'] ?>" />
<link rel="canonical" href="{{ URL::current() }}" />

@section('meta-title') 
	@if(isset($meta_title)) 
	<meta property="og:title" content="<?= $meta_title ?> | Almani Lighting">
	@else
	<meta property="og:title" content="<?= $meta['title'] ?>">
	@endif
@show

<meta property="og:type" content="article">
<meta property="og:url" content="<?=URL::current()?>">


<?php 
if($meta['url']) {
	echo '<meta property="og:url" content="'.$meta['url'].'">';
} else {
	$route_name = Request::route()->getName();
	$route_name = str_replace("_goto","",$route_name);

	if(	$route_name!='catalog_product_page' && $route_name=='product_page' && $route_name=='productmodel_page')
	echo '<meta property="og:url" content="'.route($route_name).'">';
}
?>


@section('meta-image') 
<meta property="og:image" content="<?= $meta['image_url'] ?>">
@show

<meta property="og:description" content="<?= $meta['description'] ?>">
<!-- /META -->


