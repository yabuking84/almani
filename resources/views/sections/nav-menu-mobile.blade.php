<div class="menu-mobile">
	<ul>
		<a href="{{route('home_page')}}">
			<li class="{{isset($home_menu)?'active':''}} menu-list">Home</li>
		</a>

		{{-- COMPANY Drop Down --}}
		<a class="dd-menu" href="javascript:void(0);">
			<li class="{{isset($company_menu)?'active':''}} menu-list">
				Company <span class="toggle-info"><i class="fas fa-caret-right"></i></span>
			</li>
		</a>

		<!-- mobile menu -->
		<!-- //////////////////////////////// -->
		<ul class="menu-list-down" id="company-container_here" data-id="company-container">
			{{-- @include('sections.submenus.mobile.mobile-company-submenus') --}}
		</ul>
		<!-- //////////////////////////////// -->
		<!-- mobile menu -->



		{{-- PRODUCT Drop Down --}}
		<a class="dd-menu" href="javascript:void(0);">
			<li class="menu-list {{isset($products_menu)?'active':''}}">
				Products <span class="toggle-info"><i class="fas fa-caret-right"></i></span>
			</li>
		</a>


		<!-- mobile menu -->
		<!-- //////////////////////////////// -->
		<ul class="menu-list-down" id="product-container_here" data-id="product-container">
			{{-- @include('sections.submenus.mobile.mobile-category-submenus') --}}
		</ul>
		<!-- //////////////////////////////// -->
		<!-- mobile menu -->


		{{-- PRODUCT Drop Down --}}
		<a href="{{route('projects_page')}}" style="border-top: 1px solid gainsboro;">
			<li class="{{isset($projects_menu)?'active':''}} menu-list">Projects</li>
		</a>

		{{-- DOWNLOADS Drop Down --}}
		<a class="dd-menu" href="javascript:void(0);">
			<li class="menu-list {{isset($downloads_menu)?'active':''}}">
				Downloads <span class="toggle-info"><i class="fas fa-caret-right"></i></span>
			</li>
		</a>

		<!-- mobile menu -->
		<!-- //////////////////////////////// -->
		<ul class="dd-menu menu-list-down" id="download-container_here" data-id="download-container">
			{{-- @include('sections.submenus.mobile.mobile-download-submenus') --}}
		</ul>
		<!-- //////////////////////////////// -->
		<!-- mobile menu -->

		<a href="{{route('getaquote_page')}}">
			<li class="{{isset($getaquote_menu)?'active':''}} menu-list">Quote</li>
		</a>

		{{-- CONTACT --}}
		<a href="{{route('contactus_page')}}">
			<li class="{{isset($contactus_menu)?'active':''}} menu-list">Contact</li>
		</a>

		{{-- CONTACT --}}
		<a href="{{route('clientlogin')}}">
			<li class="{{isset($clientlogin_menu)?'active':''}} menu-list">Login</li>
		</a>

		{{-- BLOG --}}
		{{-- Disable for now --}}
		{{-- <a href="{{route('blog_page')}}">
			<li class="menu-list {{isset($blog_menu)?'active':''}}">Blog</li>
		</a> --}}


		<a class="dd-menu" href="javascript:void(0);">
			<li class="menu-list {{isset($legal_menu)?'active':''}}">
				Legal <span class="toggle-info"><i class="fas fa-caret-right"></i></span>
			</li>
		</a>

		<!-- mobile menu -->
		<!-- //////////////////////////////// -->
		<ul class="menu-list-down" id="legal-container_here" data-id="legal-container">
			{{-- @include('sections.submenus.mobile.mobile-legal-submenus') --}}
		</ul>
		<!-- //////////////////////////////// -->
		<!-- mobile menu -->


		<a href="{{route('search_page')}}" class="dd-menu open_search">
			<li class="{{isset($search_menu)?'active':''}} menu-list">
				<p style="margin:0;"><span style="font-size: 16px;display: block;float:right;"> <i class="fas fa-search search"></i>
					</span> Search</p>
			</li>
		</a>
	<!-- 	<a href="https://camel.almani.ae">
			<li class="{{isset($camel_menu)?'active':''}} menu-list"> <img style="height: 27px;" src="{{App\Util::asset('assets/images/logo/camel-sm.png')}}"
				 alt=""> </li>
		</a> -->
		
	</ul>
</div>