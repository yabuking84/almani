<div>

	@if(!$isMobile && !$isTablet) 

		<!-- for desktop menu -->
		<span id="products_menu_container_here">
			@include('sections.submenus.products_menu_container')
		</span>

		<span id="company_menu_container_here">
			@include('sections.submenus.company_menu_container')
		</span>

		<span id="downloads_menu_container_here">
			@include('sections.submenus.downloads_menu_container')
		</span>
		<!-- for desktop menu -->


    @else


		<!-- Submenu Old Mobile-->
		{{-- 
		<span id="company_menu_ul_here">
			@include('sections.submenus.company_menu_ul')
		</span>

		<span id="categories_ul_here">
			@include('sections.submenus.categories_ul') 
		</span>

		<span id="downloads_0enu_ul_here">
			@include('sections.submenus.downloads_menu_ul')
		</span>
		 --}}
		<!-- Submenu Old Mobile-->
	
		

		<!-- Mobile only -->
		<ul class="menu-list-down" id="company-container_here" data-id="company-container">
			@include('sections.submenus.mobile.mobile-company-submenus')
		</ul>

		<ul class="menu-list-down" id="product-container_here" data-id="product-container">
			@include('sections.submenus.mobile.mobile-category-submenus')
		</ul>

		<ul class="dd-menu menu-list-down" id="download-container_here" data-id="download-container">
			@include('sections.submenus.mobile.mobile-download-submenus')
		</ul>

		<ul class="menu-list-down" id="legal-container_here" data-id="legal-container">
			@include('sections.submenus.mobile.mobile-legal-submenus')
		</ul>
		<!-- Mobile only -->

	@endif

</div>