@extends('main-layout', ['clientlogin_menu' => 'active', 'clientlogin_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-clientlogin.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-clientlogin.js')}}"></script>
@endsection



@section('title')
Client Sign-in {{App\Meta::webpageTitle()}}
@endsection



@section('main-content')

@if (session('error') !== null)

<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>

@endif


<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Client Sign-in</a>
    </h1>
</section>



<section class="section">

	<div class="container">

		<div class="col-md-4 offset-md-4">

			<form class="form_quote" id="form_quote" method="post" action="{{route('clientloginprocess')}}" enctype="multipart/form-data" style="justify-content: center;">

				{{ csrf_field() }}

				    <div class="form-col">

				        <div class="form-group">
				            <div class="input-group">
				                <div class="input-group-prepend">
				                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
				                </div>
				                <input name="email" class="form-control email" type="email" placeholder="Email" required>
				            </div>        
				        </div>        

				        <div class="form-group">
				            <div class="input-group">
				                <div class="input-group-prepend">
				                    <span class="input-group-text"><i class="fa fa-key"></i></span>
				                </div>
				                <input name="password" class="form-control" type="password" placeholder="Password" >
				            </div>        
				        </div>
				        
				    
				         <div style="text-align: center;font-size: 12px">
					    	This site is protected by reCAPTCHA and the Google
						    <a href="https://policies.google.com/privacy">Privacy Policy</a> and
						    <a href="https://policies.google.com/terms">Terms of Service</a> apply.
					    </div>

				    </div>

				  
				    <div class="submit_div" style="">
				        <button type="submit" class="btn-almani sendenquiry">Login</button>
				    </div>
			</form>

		</div>

	</div>

</section>


@endsection




