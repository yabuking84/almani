@extends('main-layout', [
    'whatwedo_menu' => 'active',
    'company_menu' => 'active',    
])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-whatwedo.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-whatwedo.js')}}"></script>
@endsection



@section('title')
What We Do{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')


@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif


<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">What We Do</a>
    </h1>
</section>


<section class="section">
    <div class="container">
        <div class="overview_poor" >
            <div class="backdrop"></div>
            <div class="overview" id="overview">
                <h1 class="title">Overview</h1>
                <p>
                    With the pressing need across the GCC and neighbouring countries for real, 
                    reliable quality in technical supply chains, Almani Lighting is proud to deliver 
                    consistently exceptional standards of LED lighting design, supply and installation. 
                    <br><br>
                    The GCC countries and their neighbours are striving hard to innovate and grow, 
                    and Almani is eager to support the many ambitious developments in these remarkable nations. 
                    Commerce, industry, leisure, aviation and transport infrastructure as well as premium 
                    residential developments all need and deserve outstanding LED lighting products, 
                    skills and service that excel for the long term. With Almani you can depend on complete 
                    and seamless service from concept to project fulfilment, ready to serve as your 
                    lighting partner for the long term.
                </p>
            </div>
            <div class="poor_quality">
                <p>
                    "Poor quality never saves money. Yet it's also true that great quality doesn't have to be expensive. 
                    At Almani, we're all about value: by selecting our products and people wisely and harnessing our Group's 
                    excellent buying power, we can offer superb quality at competitive prices. True value!"                
                </p>
            </div>
        </div>
    </div>

    <div class="lighting_advice" id="lightingadvice" style="background-image: url({{App\Util::asset('assets/images/whatwedo/lighting_advice.jpg')}});">
        <div class="backdrop"></div>
        <div class="lighting_advice_content">
            <h1 class="title" style="color: #000;">
                Lighting Advice and Consultancy
            </h1>        
            <p class="title_p" style="color: #000;">
                Perhaps you already know the effect or the products that you want from your lighting project. 
                Or maybe you need ideas, information and inspiration. 
                Our skilled professionals will give you all the help you want to make the choices that deliver on all your objectives. 
                <br><br>
                No matter what the scale of your project, from a single room to a vast commercial tower or a mission-critical runway, 
                we can offer the time and expertise you need to find your ideal solutions.
            </p>
        </div>
    </div>


    <div class="container design_range" id="lightingdesign">
        <div class="design_range_content d1st">
            <h1 class="title">LED Lighting Design</h1>
            <p>
                At Almani, we handle LED lighting projects of all kinds. 
                Our specialist designers work with you to understand every detail of your brief, budget and schedule. 
                They then create superb, innovative and effective solutions, designed uniquely to match your needs and wishes. 
                <br><br>
                Of course, our design teams also work alongside our asrchitects, surveyors, 
                engineers and other experts to ensure seamless, successful projects that deliver exceptional returns.
                <br><br>
                Almani Lighting has been part of the development team at the fabulous Sherena Residence from Day One, 
                providing specialist advice, design and installation services for all interior and exterior lighting.
            </p>
        </div>
        
        <div class="separator"></div>

        <div class="design_range_content d2nd" id="ligthingrange">
            <h1 class="title">The Almani Lighting Range</h1>
            <p>
                At Almani, we handle LED lighting projects of all kinds. 
                Our specialist designers work with you to understand every detail of your brief, budget and schedule. 
                They then create superb, innovative and effective solutions, designed uniquely to match your needs and wishes. 
                <br><br>
                Of course, our design teams also work alongside our asrchitects, surveyors, 
                engineers and other experts to ensure seamless, successful projects that deliver exceptional returns.
                <br><br>
                Almani Lighting has been part of the development team at the fabulous Sherena Residence from Day One, 
                providing specialist advice, design and installation services for all interior and exterior lighting.
            </p>
        </div>
    </div>


    <div class="led_install">
        <div class="led_install_content" id="ledinstall">
            <h1 class="title">LED Lighting Installation</h1>
            <p>
                Often beginning our work with clients at the concept or design stage, 
                we can advise on and design your lighting installations to match your brief and budget. 
                <br><br>
                When it's time to fit our superb products, we have the all-encompassing 
                capability and capacity to cover installation projects of all sizes, 
                from a single residential home to a vast skyscraper, airport runway or exhibition venue. 
            </p>
            <br>
            <p>
                For the finest quality lighting installation projects, we're your first port of call.                 
            </p>
            <p class="talk_to">
                <a href="tel:+97148873265">Talk to Almani Professionals Today!</a>
            </p>
        </div>
    </div>


    

    <div class="container light_refits" id="lightrefits">
        <div class="div_container d1st">
            <div class="light_refits_img" style="background-image: url({{App\Util::asset('assets/images/whatwedo/light_refits.jpg')}});"></div>
            <p>
                "Almani Lighting is proud to be part of the development team at the 29-storey Ontario Tower 
                in Dubai's prestigious Business Bay. Originally fitted with traditional lighting, 
                we are now helping to cut energy costs, reduce the environmental impact and make huge 
                cost savings for the owners and tenants of this sumptuous tower with a complete interior 
                and exterior lighting refit."
            </p>
        </div>
        
        <div class="separator"></div>

        <div class="div_container d2nd" id="lightrefits">
            <h1 class="title">LED Lighting Refits</h1>
            <p>
                LED lighting refits and retrofits are increasingly popular with organisations who want to cut their costs, reduce their carbon footprints and harness the many opportunities and benefits of integrated technologies. 
                <br><br>
                Without question, LED lighting is far more energy-efficient, cost-saving and environmentally-friendly than traditional incandescent or fluorescent light bulbs. Refitting your lighting estate with LED cuts your costs by up to 90%. LEDs are also cleaner, 'greener', allow greater design flexibility and last much, much longer. 
                <br><br>
                In addition, LED development (particularly in the area of smart control technologies) opens up a world of potential for adding yet more value: remote control, dimmability, photosensors, occupancy detection and other options can reduce usage, generate vital data and improve user convenience. 
                <br><br>
                Our LED refit specialists will work closely with you to ensure superb results and returns on your refit projects. For more detail on LED's immense benefits and savings, visit our Why choose LED? page.             </p>
        </div>
    </div>





    <div class="container rental_solution" id="lightingrental">
        <div class="div_container d2nd">
            <h1 class="title">Lighting Rental</h1>
            <p>
                As a forward-thinking, innovative and client-focused company, we know that for some organisations, the option to install and run their new lighting system for a competitive monthly payment is extremely attractive. In simple terms, here’s how it works...
                <br><br>
                Our client – say, a supermarket chain owner – wants to replace the entire lighting estate within a particular supermarket outlet. He opts to pay for the project on a monthly rental basis for an agreed contract period. At the end of the agreed period, the supermarket owner can either return the lights to Almani or he can buy them at a favourable rate. 
                <br><br>
                Alternatively, the supermarket owner could choose to pay the monthly electricity bill to Almani Lighting for an agreed contract period of, say, three years. At the end of the three-year period, the LED lighting installation becomes the property of the supermarket owner – an acquisition that represents excellent value for our client. 
                <br><br>
                We’re proud that these agreements offer our clients outstanding flexibility, cashflow and win-win benefits and we can tailor LED lighting rental plans to suit a wide variety of needs and products.                
            </p>
        </div>
        <div class="div_container d1st" id="smartsolution">
            <h1 class="title">LED Smarthome Solutions by Almani Lighting</h1>
            <div class="light_refits_img" style="background-color: transparent;">
                <iframe style="" src="https://www.youtube.com/embed/ULe60BUTJoo?modestbranding=1&autoplay=1&showinfo=0&rel=0" allowfullscreen=""></iframe>
            </div>
            <p>
                Smart LED lighting opens up new horizons for cloud-controlled home comfort, convenience and cost efficiency. Almani Lighting is proud to present some exciting Smarthome options for the ultimate luxury home.
            </p>
        </div>
    </div>




</section>
@endsection




