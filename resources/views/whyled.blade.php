@extends('main-layout', ['whyled_menu' => 'active', 'company_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-whyled.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-whyled.js')}}"></script>
@endsection



@section('title')
Why LED?{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')

@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif


<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Why LED?</a>
    </h1>
</section>


<section class="section">


<?php
    $items = [];
    $items[] = ['img'=>'savesonenergycosts.jpg','title'=>'Saves On Energy Costs','text'=>"Conventional light bulbs and tubes are around 20% efficient, while the energy efficiency of LED lights is between 60-90%. This makes LED lights far cheaper to run and delivers massive reductions in your electricity bills.", 'anchor'=>'savesonenergycosts'];
    $items[] = ['img'=>'longerlife.jpg','title'=>'Longer Life','text'=>"Bulb replacement is a constant 40x as long as an incandescent bulb and 6x as long as a fluorescent unit. In addition, LED units are robust, durable and can withstand even the toughest conditions.", 'anchor'=>'longerlife'];
    $items[] = ['img'=>'cooler.jpg','title'=>'Cooler','text'=>"LED lighting produces very little infrared (heat) energy, making it ideal for heat-sensitive materials and environment such as art galleries, museums, hospitals and laboratories (which also benefit from its negligible UV emissions).", 'anchor'=>'cooler'];
    $items[] = ['img'=>'greener.jpg','title'=>'Greener','text'=>"LED lighting brings numerous ecological benefits. Unlike conventional lighting, it is free from toxic chemicals, is 100% recyclable, reduces consumption of materials and energy and makes a major reduction in your carbon footprint.", 'anchor'=>'greener'];
    $items[] = ['img'=>'designflexibility.jpg','title'=>'Design Flexibility','text'=>"A designer's delight, LEDs allow maximum flexibility in configuration, light, shape colour, mood, and distribution.", 'anchor'=>'designflexibility'];
    $items[] = ['img'=>'smarter.jpg','title'=>'Smarter','text'=>"Occupancy detection and control technologies today: tomorrow, the world! The future of smart LED technologies is vast and offers all kinds of opportunities for new efficiency savings, services, and revenue streams.", 'anchor'=>'smarter'];
    $items[] = ['img'=>'instantlight.jpg','title'=>'Instant Light','text'=>"Giving full brightness from the moment they're switched on, LEDs also maintain that brightness throughout their entire lifetime and provide better, safer user environments.", 'anchor'=>'instantlight'];
    $items[] = ['img'=>'safety.jpg','title'=>'Safety','text'=>"LED lights generate virtually no heat therefore they are cool to the touch and can be left on for hours without incident or consequence if touched.", 'anchor'=>'safety'];
?>


<!-- <div class="item_boxes" style="background-image: url({{App\Util::asset('assets/images/whyled/whyled2.jpg')}});"> -->
<div class="item_boxes">
    <div class="backdrop"></div> 
    <div class="items">
        @foreach($items as $item)
        <div class="item" id="{{$item['anchor']}}">
            <div class="item_img" style="background-image: url({{App\Util::asset('assets/images/whyled/items/'.$item['img'])}});"></div>
            <h5>{{$item['title']}}</h5>
            <p>{{$item['text']}}</p>
        </div>
        @endforeach
    </div>
</div>




</section>
@endsection




