@extends('main-layout', ['downloads_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-downloads.css')}}" rel="stylesheet">

@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-downloads.js')}}"></script>
@endsection

@section('title')
 Flyers{{App\Meta::webpageTitle()}}
@endsection

@section('main-content')

<div class="section">
    <section class="section section_title">
        <h1 class="title main_title">
            <a class="button is-primary">Flyers</a>
        </h1>
    </section>
</div>


@php
    
    $downloads = [];
    $downloads[] = ['route' => asset('assets/pdfs/led-to-own.pdf'), 'cover' => App\Util::asset('assets/images/covers/ledtoown-cover.png'), 'title' => 'Download LED to OWN'  ];
    $downloads[] = ['route' => asset('assets/pdfs/b2b.pdf'), 'cover' => App\Util::asset('assets/images/covers/b2b-cover.png'), 'title' => 'Download B2B'  ];
    $downloads[] = ['route' => asset('assets/pdfs/approved-supplier.pdf'), 'cover' => App\Util::asset('assets/images/covers/approvedsupplier-cover.png'), 'title' => 'Download Approved Supplier'];
    $downloads[] = ['route' => asset('assets/pdfs/price-match-promise.pdf'), 'cover' => App\Util::asset('assets/images/covers/price-match-promise-cover.png'), 'title' => 'Download Price Match Promise'];

    
@endphp

<section class="container">

    <div class="download-container">
        @foreach ($downloads as $downloaditems)
            <div class="download-item">
                    <div class="img-container">
                        <a href="{{ $downloaditems['route'] }}" target="_blank">
                            <img class="animate flipInY animated" src="{{ $downloaditems['cover'] }}" alt="{{ $downloaditems['title'] }}" class="download-img-center"
                                class="almani-catgalog">
                            <span class="dl-button"><button class="btn-almani">{{ $downloaditems['title'] }}</button> </span>
                        </a>
                    </div>
            </div>
        @endforeach
    </div>
</section>

<br>

@endsection