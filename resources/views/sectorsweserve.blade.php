@extends('main-layout', ['sectorsweserve_menu' => 'active', 'company_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-sectorsweserve.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-sectorsweserve.js')}}"></script>
@endsection



@section('title')
Sectors We Serve{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')

@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif

<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Sectors We Serve</a>
    </h1>
</section>


<section class="section">
<div class="container white_bar">

    <div class="intro_text">
        <p class="title_p" style="">
            Almani Lighting provides quality LED lighting solutions for a wide variety of settings and purposes. 
            <br><br>
            No matter what the sector, scope and challenge of the project, every Almani client enjoys the expertise, 
            care and absolute professional commitment that we would want and expect for ourselves. 
        </p>
    </div>


</div>
<!-- 
<div class="backdrop_bar" style="background-image: url({{App\Util::asset('assets/images/aboutus/almani-approach.jpg')}});">
    <div class="backdrop"></div>   
    <p class="title_p" style="">
        Almani Lighting provides quality LED lighting solutions for a wide variety of settings and purposes. 
        No matter what the sector, scope and challenge of the project, every Almani client enjoys the expertise, 
        care and absolute professional commitment that we would want and expect for ourselves. 
    </p>
</div>
 -->

<?php
    $items = [];
    $items[] = ['img'=>'<i class="fas fa-home"></i>','title'=>'Residential','text'=>"Luxury lighting designs and sumptuous products: any mood, any style - pure opulence!", 'anchor'=>'residential'];
    $items[] = ['img'=>'<i class="fas fa-shopping-bag"></i>','title'=>'Commercial','text'=>"Almani maximises both quality and value, delivering superb solutions for offices, malls, exhibition venues and retail outlets of every kind.", 'anchor'=>'commercial'];
    $items[] = ['img'=>'<i class="fas fa-hotel"></i>','title'=>'Hotels, Leisure and Tourism','text'=>"With beautifully designed and stunning lighting for hotels, villa developments and compounds, we help your brand make a great first impression.", 'anchor'=>'hotelsleisureandtourism'];
    $items[] = ['img'=>'<i class="fas fa-industry"></i>','title'=>'Industrial','text'=>"Reliable. Durable. Great value. Whatever the setting, we have the ideal industrial LED lighting services for you.", 'anchor'=>'industrial'];
    $items[] = ['img'=>'<i class="fas fa-subway"></i>','title'=>'Transport Hubs','text'=>"Air, rail, road or water – Almani goes the extra mile on capability, reliability, quality and value.", 'anchor'=>'transporthubs'];
    $items[] = ['img'=>'<i class="fas fa-road"></i>','title'=>'Street Lighting','text'=>"Almani lights the way for the UAE's streets, amenities and public areas.", 'anchor'=>'streetlighting'];
    $items[] = ['img'=>'<i class="fas fa-magic"></i>','title'=>'Decorative and Seasonal Lighting','text'=>"Magical lights and effects for special occasions.", 'anchor'=>'decorativeandseasonallighting'];
?>


<div class="item_boxes" style="background-image: url({{App\Util::asset('assets/images/sectorsweserve/sectorsweserve4.jpg')}});">
    <div class="backdrop"></div> 
    <div class="items">
        @foreach($items as $item)
        <div class="item" id="{{$item['anchor']}}">
            <span><?=$item['img']?></span>
            <h5>{{$item['title']}}</h5>
            <p>{{$item['text']}}</p>
        </div>
        @endforeach
    </div>
</div>




</section>
@endsection




