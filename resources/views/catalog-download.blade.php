<?php 
    if($pages === 'catalog') {
        $paramsNav = ['catalogdownload_menu' => 'active', 'downloads_menu' => 'active']; 
    } else {
        $paramsNav = ['aboutus_menu' => 'active','downloads_menu' => 'active']; 
    }
?>
@extends('main-layout', $paramsNav)


@section('meta-title')
    @if($pages === 'catalog')    
    <meta property="og:title" content="Download Almani Catalog | Almani Lighting">
    @elseif($pages === 'camel-catalog')    
    <meta property="og:title" content="Download CamelLED Catalog | Almani Lighting">
    @elseif($pages === 'profile')    
    <meta property="og:title" content="Download Company Profile | Almani Lighting">
    @elseif($pages === 'pre_qualifications')    
    <meta property="og:title" content="Download Pre-Qualification | Almani Lighting">
    @elseif($pages === 'b2b')    
    <meta property="og:title" content="Download Business to Business | Almani Lighting">
    @elseif($pages === 'ledtoown')
    <meta property="og:title" content="Download LED to Own | Almani Lighting">
    @elseif($pages === 'approved-supplier')
    <meta property="og:title" content="Download  | Almani Lighting">
    @elseif($pages === 'price-match-promise')
    <meta property="og:title" content="Download Business to Business | Almani Lighting">
    @else
        @if(isset($meta_title))
        <meta property="og:title" content="<?= $meta_title ?> | Almani Lighting">
        @else
        <meta property="og:title" content="<?= $meta['title'] ?>">
        @endif
    @endif
@endsection


@section('meta-image')
    @if($pages === 'catalog')    
    <meta property="og:image" content="{{ asset('assets/images/covers/almani-cover.png')}}">
    @elseif($pages === 'camel-catalog')    
    <meta property="og:image" content="{{ asset('assets/images/covers/camel-cover.png')}}">
    @elseif($pages === 'profile')    
    <meta property="og:image" content="{{ asset('assets/images/covers/company-cover.png')}}">
    @elseif($pages === 'pre_qualifications')    
    <meta property="og:image" content="{{ asset('assets/images/covers/almani-cover.png')}}">
    @elseif($pages === 'b2b')    
    <meta property="og:image" content="{{ asset('assets/images/covers/b2b-cover.png')}}">
    @elseif($pages === 'approved-supplier')
    <meta property="og:image" content="{{ asset('assets/images/covers/approved-supplier-cover.png')}}">
    @elseif($pages === 'ledtoown')
    <meta property="og:image" content="{{ asset('assets/images/covers/ledtoown-cover.png')}}">
    @elseif($pages === 'price-match-promise')
    <meta property="og:image" content="{{ asset('assets/images/covers/price-match-promise-cover.png')}}">
    @else
    <meta property="og:image" content="<?= $meta['image_url'] ?>">
    @endif
@endsection

@section('css-lib')
 <link href="{{App\Util::asset('assets/css/master-catalog-download.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-catalog-download.js')}}"></script>
@endsection


@section('title')
  Download {{$name}}{{App\Meta::webpageTitle()}}
@endsection

@section('main-content')
   
@if (session('error') !== null)
    <div class="container alert_container">
        <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
        <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
                {{session('message')}}
                
        @if (session('is_dl') && session('error') != true)
            <br>
            <small>Please check your Spam Filter or Junk Mail Folder</small>
        @endif

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
        </button>
        </div>
    </div>
@endif

    <section class="section section_title heading-first" stul>
        <h1 class="title main_title">
            <a class="button is-primary mb-titile">{{ $name }}</a>
        </h1>
    </section>


    <section class="section heading-second">
        <div class="container intro_texts">
                <div class="intro_text">
                    {{-- remove display none to --}}
                    <h3 class="title">
                         @if ($pages === 'profile' || $pages === 'pre_qualifications')
                                <span>
                                    Please fill the very basic form to begin the download.                  
                                </span>
                            @endif
                    </h3>
                </div>
            </div>
    </section>


    <section class="section download">

        {{-- hide the main form for the revision  --}}
        <div class="container download-section">

            @if($pages === 'catalog')
                    <div class="download-item form1 animate flipInY animated" id="catalog">
                        <a href="javascript:void(0);" data-name="Almani Catalog" data-download="almani-catalog" class="bsn-modals">
                                <img src="{{ asset('assets/images/covers/almani-cover.png')}}" alt="" class="download-img-center" class="almani-catgalog" >
                        </a>    
                    </div>
            @elseif($pages === 'camel-catalog')
                    <div class="download-item form1 animate flipInY animated" id="catalog">
                        <a href="javascript:void(0);" data-name="Almani Catalog" data-download="almani-catalog" class="bsn-modals">
                                <img src="{{ asset('assets/images/covers/camel-cover.png')}}" alt="" class="download-img-center" class="almani-catgalog" >
                        </a>    
                    </div>
            @elseif($pages === 'profile')
                    <div class="download-item animate flipInY animated " id="companyprofile">
                        <a href="javascript:void(0)" data-name="Company Profile" data-download="company-download" class="bsn-modals" >
                                <img src="{{ asset('assets/images/covers/company-cover.png')}}" alt="" class="company-catalog" >
                        </a>
                    </div>
            @elseif($pages === 'pre_qualifications')
                    <div class="download-item animate flipInY animated" id="companyprequalification">
                        <a href="javascript:void(0);" data-name="Pre-Qualifications" data-download="pq" class="bsn-modals">
                                <img src="{{ asset('assets/images/covers/pq-cover.png')}}" alt="" class="download-img-center" class="almani-catgalog" >
                        </a>
                    </div>
           @elseif($pages === 'b2b')
                    <div class="download-item animate flipInY animated" id="b2b">
                        <a href="javascript:void(0);" data-name="Pre-Qualifications" data-download="pq" class="bsn-modals">
                                <img src="{{ asset('assets/images/covers/b2b-cover.png')}}" alt="" class="download-img-center" class="almani-catgalog" >
                        </a>
                    </div>
           @elseif($pages === 'ledtoown')
                    <div class="download-item animate flipInY animated" id="ledtoown">
                        <a href="javascript:void(0);" data-name="Pre-Qualifications" data-download="pq" class="bsn-modals">
                                <img src="{{ asset('assets/images/covers/ledtoown-cover.png')}}" alt="" class="download-img-center" class="almani-catgalog" >
                        </a>
                    </div>
            @elseif($pages === 'approved-supplier')
                    <div class="download-item animate flipInY animated" id="approved-supplier">
                        <a href="javascript:void(0);" data-name="Pre-Qualifications" data-download="pq" class="bsn-modals">
                                <img src="{{ asset('assets/images/covers/approvedsupplier-cover.png')}}" alt="" class="download-img-center" class="almani-catgalog" >
                        </a>
                    </div>
            @elseif($pages === 'price-match-promise')
                    <div class="download-item animate flipInY animated" id="pricematchprice">
                        <a href="javascript:void(0);" data-name="Pre-Qualifications" data-download="pq" class="bsn-modals">
                                <img src="{{ asset('assets/images/covers/price-match-promise-cover.png')}}" alt="" class="download-img-center" class="almani-catgalog" >
                        </a>
                    </div>
            @endif
             


                {{-- <div class=" download-item form2">
                    <div class="warning-content">
                        <div class="svg-container">
                             <div class="container">
                                    <div class="msg-container">
                                        <h1>Coming<span class="soon"> Soon </span> </h1>
                                        <p>
                                            Forgive us for the Inconvenience. <br />
                                            We are currently updating our <span class="bold"> Blog. </span>
                                            <span class="boldup"> In the mean time please check out other things in our Site. </span> 
                                        </p>
                                        <p class="bold">
                                            We're Excited to Launch Soon!
                                        </p>
                                    </div>
                             </div>
                        </div>
                    </div>
                </div> --}}

             {{-- remove the display none to show it to the live --}}
                <div class="download-item form2">
    
        {{-- @if ($pages === 'catalog' || $pages === 'camel-catalog') --}}
        @if (false)


        
        <div class="warning-content">
            <div class="svg-container">
                 <div class="container">
                        <div class="msg-container">
                            <h1>Coming<span class="soon"> Soon </span> </h1>
                              <hr>
                            <p>
                                Forgive us for the Inconvenience. <br />
                                We are currently updating our <span class="bold"> Catalog Section. </span>
                                <br/>
                                <br/>

                                <span class="boldup"> In the mean time please check out other things in our site. </span> 
                            </p>
                        </div>
                 </div>
            </div>
        </div>

        @else

           <div class="download-modal-form" id="download-modal-form" style="margin-bottom:41px;">
                                    <div class="modal-dialog modal-dialog-centered  modal-md" role="document">
                                    <div class="modal-content">  
                                            <div class="modal-header">
                                                    <h6 class="modal-title" id="modal-title">Please fill the Form to download {{ $name }} </h6>                                                    
                                                    </div>        
                                        <div class="modal-body">
                                                {{-- form start here --}}
                                                <form class="download-form" id="download-form" method="post" action="{{route('requestdownloads')}}" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <div class="form-group">
                                                        <div class="input-group">   
                                                            <div class="input-group-prepend">   
                                                                <span class="input-group-text"><i class="fa fa-user"></i></span>            
                                                            </div>
                                                            <input name="full_name" id="full_name" class="form-control" type="text" placeholder="Full Name" required>
                                                        </div>        
                                                    </div>
                                                    <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                                                </div>
                                                                <input name="phone" id="phone" class="form-control" type="text" placeholder="Phone" required>
                                                            </div>        
                                                        </div> 
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                                            </div>
                                                            <input name="email" id="email" class="form-control" type="email" placeholder="Email" required>
                                                            <input type="hidden" value="{{ $type }}" name="catalogType" id="catalogType">
                                                        </div>        
                                                    </div>        
                                                    <div class="download-buttom">
                                                        <button type="submit" id="btn-download"  class="btn-download btn-almani">Submit</button>
                                                </div>
                                                
                                                      <input type="hidden" value="" name="recaptcha_response" id="recaptchaResponse">

                                                </form>
                                                {{-- form end here --}}
                                            
                                        </div>
                                    </div>  
                                    </div>
                            </div>  

    @endif

                </div>
        </div>


 
    </section>




 


@endsection


