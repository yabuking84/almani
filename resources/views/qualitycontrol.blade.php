@extends('main-layout', ['qualitycontrol_menu' => 'active', 'company_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-qualitycontrol.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-qualitycontrol.js')}}"></script>
@endsection



@section('title')
Quality Control{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')


@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif

<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Quality Control</a>
    </h1>
</section>


<section class="section">
<div class="container">


    <div class="intro_text">
        <!-- <h3 class="title">WHERE your LED is made is irrelevant.<br>Ask instead, WHO made it?</h3> -->
        <p class="title_p" style="">
            When you choose Almani Lighting for your LED projects, you choose peace of mind, quality, value and absolute reliability. <br><br>
            With our German heritage, quality is always uppermost in our minds at Almani. <br>
            It's the reason we invest a great deal of time, care and effort in making certain that you can depend on every single Almani product and service.
        </p>
    </div>

    <div class="img_text_boxes">
        <div class="img_text_box" id="fiveyearwarranty">
            <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/qualitycontrol/fiveyearwarranty.jpg')}});">
                <!-- <img src="{{App\Util::asset('assets/images/quality-assurance/quality.jpg')}}"> -->
            </div>
            <div class="text_box">
                <h1>Five-year warranty</h1>
                <p>Testament to our passion and commitment to quality, our products all come with a full five-year warranty; we're certain that each one is the best available on the market.</p>            
            </div>
        </div>
        <div class="img_text_box" id="qcprocess">
            <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/qualitycontrol/ourqcprocess.jpg')}});">
                <!-- <img src="{{App\Util::asset('assets/images/quality-assurance/trust.jpg')}}"> -->
            </div>
            <div class="text_box">
                <h1>Our QC process</h1>
                <p>How can we be so sure about our product quality? Well, we buy only from hand-picked manufacturers, each one of which has been thoroughly vetted and is continually checked to ensure compliance with our rigorous quality standards. We accept only the very best. In addition, we test and check every product in our laboratories and testing facilities in Germany and Dubai, as well as commissioning carefully selected and highly reputable partners such VDE and TÜV Süd to support our extensive testing operations.</p>
            </div>
        </div>
        <div class="img_text_box" id="zerotolerancefordefects">
            <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/qualitycontrol/zerotolerance.jpg')}});">
                <!-- <img src="{{App\Util::asset('assets/images/quality-assurance/award.jpg')}}"> -->
            </div>
            <div class="text_box">
                <h1>Zero tolerance for defects</h1>
                <p>Because we go to great lengths to test, check and monitor Almani quality, we are confident that our products are of superb quality and we are committed to zero defects. However, while we strive for perfection we know that it's never completely possible. We therefore give you our absolute assurance: should you ever encounter a defect in your Almani product, we will replace it immediately, without any hesitation. We want to know that every Almani Lighting product works hard for you and delivers on every objective – no exceptions.</p>
            </div>
        </div>
    </div>


</div>



<div class="container">
<?php
    $client_logos = [];
    $client_logos[] = 'tuvrheinland.png';
    $client_logos[] = 'vde.png';
    $client_logos[] = 'tuv.png';
    $client_logos[] = 'ce.png';
    $client_logos[] = 'rohs.png';
    $client_logos[] = 'saa.png';
    $client_logos[] = 'gs.png';
    $client_logos[] = 'ul.png';
    $client_logos[] = 'etl.png';
    $client_logos[] = 'philips.png';
    $client_logos[] = 'bridgelux.png';
    $client_logos[] = 'cree.png';
    $client_logos[] = 'epistar.png';
    $client_logos[] = 'citizen.png';
    $client_logos[] = 'samsung.png';
    $client_logos[] = 'meanwell.png';
    $client_logos[] = 'osram.png';
    $client_logos[] = 'luminus.png';
    $client_logos[] = 'nichia.png';
    $client_logos[] = 'lumileds.png';
    $client_logos[] = 'moso.png';
    $client_logos[] = 'edison.png'; 
    $client_logos[] = 'easeic.png';
    $client_logos[] = 'lifud.png';
    $client_logos[] = 'euchips.png';
    $client_logos[] = 'inventronics.png';
    $client_logos[] = 'tridonic.png';
?>
   <div class="client-logos" id="certificationsandbrands">
        <div class="owl-carousel owl-theme owl-loaded">
            @foreach($client_logos as $client_logo)
            <div class="item">     
                <img alt="client logo" src="<?=asset('assets/images/qualitycontrol/certificate_logos/'.$client_logo)?>">                
            </div>
            @endforeach
        </div>
    </div>
</div>
{{-- style="min-height: 27px;max-height: 100px;" --}}

</section>
@endsection




