@extends('main-layout', ['qualityassurance_menu' => 'active', 'company_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-quality-assurance.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-quality-assurance.js')}}"></script>
@endsection



@section('title')
Quality Assurance{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')


@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif

<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Quality Assurance</a>
    </h1>
</section>


<section class="section">
<div class="container">


    <div class="intro_text">
        <h3 class="title">WHERE your LED is made is irrelevant.<br>Ask instead, WHO made it?</h3>
        <p class="title_p" style="">
            At Almani, we are proud to be:
        </p>
    </div>

    <div class="img_text_boxes">
        <div class="img_text_box" id="german">
            <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/quality-assurance/quality.jpg')}});">
                <!-- <img src="{{App\Util::asset('assets/images/quality-assurance/quality.jpg')}}"> -->
            </div>
            <div class="text_box">
                <h1>German</h1>
                <p>Rooted in our German heritage, high standards control the quality of every single LED component and unit that we produce.</p>
                
                <div class="video_container" style="">
                    <div class="video_intro_img" 
                            data-toggle="modal" 
                            data-target="#video_modal"
                            data-video_url="https://www.youtube.com/embed/bdRJgok7R0E?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                        <img src="{{App\Util::asset('assets/images/icons/youtube_play_icon.png')}}" alt="play">
                        <p>
                            Quality Assurance
                        </p>
                    </div>
{{-- xxxxxxxxxxxxxxxxx --}}

                </div>

            </div>
        </div>
        <div class="img_text_box" id="honestandtransparent">
            <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/quality-assurance/trust.jpg')}});">
                <!-- <img src="{{App\Util::asset('assets/images/quality-assurance/trust.jpg')}}"> -->
            </div>
            <div class="text_box">
                <h1>Honest and Transparent</h1>
                <p>
                    We’re happy to tell you what most LED manufacturers try to hide... It’s this: 99% of the world’s LED products are made in China, and, whatever they might pretend, every LED brand sources most of their goods there. Quite simply, that’s the truth. But we’re not like other brands. 
                    <br><br>
                    'Made in China by Almani' means 'Made to perfection'. Made to the highest German standards. Made to perform perfectly for years and years. Guaranteed.                
                </p>
            </div>
        </div>
        <div class="img_text_box" id="bestledbrandinuae">
            <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/quality-assurance/award.jpg')}});">
                <!-- <img src="{{App\Util::asset('assets/images/quality-assurance/award.jpg')}}"> -->
            </div>
            <div class="text_box">
                <h1>The Best LED Brand in the UAE</h1>
                <p>
                    Our exceptional production standards are proof that it’s not where a product is made, but who makes it, that determines its quality. Product perfection is the only reason why we can offer a solid gold, 5-year warranty on every single Almani product. 
                    <br><br>
                    What’s more, by manufacturing where skills are high but far less costly, and by cutting out the middle men, we also keep our prices 5-10 times lower than their European equivalents.                
                </p>
            </div>
        </div>
    </div>


</div>





<div class="bar_text">
    <p class="title_p" style="">
        Highest quality, lowest prices and a 5-year warranty on every item.
    </p>
    <h3 class="title">
        Perfect.
    </h3>        
</div>





</section>
@endsection


@section('modal')

<!-- Modal -->
<!-- //////////////////////////////////////////////////////// -->
<div class="modal fade youtube_video_modal" id="video_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
    <div class="modal-content">          
      <div class="modal-body">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <iframe class="video" style="" allowfullscreen></iframe>

        <div style="margin-top: 20px;">
            <button type="button" 
                class="btn-almani change_language" 
                data-target="#video_modal"
                data-language="english"
                data-video_url="https://www.youtube.com/embed/bdRJgok7R0E?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                English                    
            </button>
            <button type="button" 
                class="btn-almani change_language" 
                data-target="#video_modal"
                data-language="arabic"
                data-video_url="https://www.youtube.com/embed/PMvZh8SVYn8?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                العَرَبِيَّة
            </button>                
        </div>

      </div>
    </div>
  </div>
</div>    
<!-- //////////////////////////////////////////////////////// -->
<!-- Modal -->
@endsection
