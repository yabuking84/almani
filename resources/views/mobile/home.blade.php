@extends('mobile-layout', ['home_menu' => 'active'])

@section('css-lib')
{{--
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css" /> --}}
<link href="{{App\Util::asset('assets/css/master-home-mobile.css')}}" rel="stylesheet">
@endsection

@section('javascript-lib')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/slideout/1.0.1/slideout.min.js"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js"></script> --}}

<script src="{{App\Util::asset('assets/js/master-home-mobile.js')}}"></script>
@endsection

@section('title')
Shop with the Leading LED lights suppliers in Dubai UAE | Almani Lighting
@endsection



@section('meta')
<?php
    $meta_keywords = "Lighting companies dubai, Led lighting companies dubai, LED lights uae, LED lights abu dhabi, LED lights sharjah, LED lights ajman, LED lights ras al khaimah, LED b2b, LED retail, LED wholesale, LED business to business, LED to Own, rent to own, rent LED, LED projects, LED building, LED benefits, LED quality control, LED shop online, LED store dubai, LED store UAE, LED shop UAE, LED dubai";
    $page_desc = "LED Lighting Store Dubai - Almani Lighting is one of the most reputable LED lighting companies in Dubai, UAE. Best quality and prices. LED lights Dubai.";
?>
<!-- META -->
<meta name="description" content="<?= $page_desc ?>" />
<meta name="author" content="Almani Lighting LLC">
<meta name="keywords" content="<?= $meta_keywords ?>" />
<meta name="google-site-verification" content="G5gSgLSUfMDmKtzKAKnEmfMRgw9Q19IeGcbKU8JxXbo" />
<meta property="og:title" content="Shop with the Leading LED lights suppliers in Dubai UAE | Almani Lighting">
<meta property="og:type" content="article">
<meta property="og:url" content="https://almani.ae">
<meta property="og:image" content="https://almani.ae/assets/img/og/og2.jpg">
<meta property="og:description" content="<?= $page_desc ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

{{--
<meta name=viewport content="width=device-width, initial-scale=1.0, minimum-scale=0.5 maximum-scale=1.0"> --}}

{{-- apple touch --}}
{{--
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"> --}}


{{-- homescreen icons --}}
{{--
<link rel="apple-touch-icon" href="/static/images/identity/HTML5_Badge_64.png" />
<link rel="apple-touch-icon-precomposed" href="/static/images/identity/HTML5_Badge_64.png" /> --}}


<!-- /META -->
@endsection

@section('main-content')


    @if (session('error') !== null)
            <?php $alert_type = (!session('error'))?"notify-success":"notify-failure"?>
           <div class="success {{ $alert_type }}">
               <span class="success-content">
                   <span class="checkboz"></span>  
                    {{session('message')}}
               </span> 
           </div>
        @endif
<input type="hidden" id="isSuccess" value="{{ session('error') }}">

<div class="main" id="main">
    <section id="intro">

        <div id="intro-carousel" class="owl-carousel">
            @foreach(App\Util::randomImage('assets/images/home/carousel',3) as $img)
            <div class="item" style="background-image: url({{$img}});"></div>
            @endforeach
        </div>

        <div style="z-index: -1; position: absolute; top: 0;">
            <h1>LED Lighting Dubai UAE</h1>
            <h2>Raising the bar on quality, reliability and value</h2>
        </div>

        <div id="strap_lines" class="owl-carousel" style="">
            <div class="item">
                <h2>Almani Lighting – the UAE’s finest LED choice</h2>
            </div>
            <div class="item">
                <h2>Zero Tolerance for defects</h2>
            </div>
            <div class="item">
                <h2>The Ultimate in LED. Guaranteed.</h2>
            </div>
            <div class="item">
                <h2>Standard or Bespoke: We’ll work with you, your way</h2>
            </div>
            <div class="item">
                <h2>Exceptional German quality every step of the way</h2>
            </div>
            <div class="item">
                <h2>Raising the bar on quality, reliability and value</h2>
            </div>
            <div class="item" style="display: none;">
                <h2>LED Lighting Dubai, UAE</h2>
            </div>
        </div>
        {{-- we hide this section cause we transfer it to the tabbale section --}}
        <div class="welcome-message d-none">
            <div class="content-message">
                <h1>Welcome to Almani Lighting!</h1>
                <div class="message">
                    <p>By choosing to study our website, we conclude that you are a discerning LED procurer of LED for
                        whom
                        quality matters. Your choice is therefore very wise…</p>
                    <p>Almani Lighting is committed to quality in all that we do. Indeed, it is the reason we
                        established
                        our business in 2015. Appalled at the poor standard of LED products and service across the GCC
                        region, we launched Almani Lighting to raise the bar and offer consistent LED excellence.</p>
                    <div class="button-div">
                        <a href="javascript:void(0);" class="btn-more btn btn-sm btn-dark btn-border">More</a>
                    </div>
                    <span class="more-message">
                        <p>We are proud to design, manufacture and install superb yet reasonably priced LED units and
                            systems which deliver exceptional lighting, energy and cost savings, and longevity. That
                            commitment to quality extends also to the way we do business: we build excellent, trusting
                            relationships with clients who know they can rely on us for the right advice, support and
                            services, every time.</p>
                        <p>Excelling in large and complex LED challenges, we are delighted to invest our time,
                            knowledge
                            and care in your project from concept to completion and beyond. You can put your trust in
                            us
                            with 100% confidence, for we are at your service now and long into the future.</p>
                        <p>Please browse our catalogue, explore our website and of course, call us at any time. We
                            aspire
                            to be your trusted LED partner, at your service whenever you need us.</p>
                        <div class="button-div">
                            <a href="javascript:void(0);" class="btn-close btn btn-sm btn-dark btn-border">Close</a>
                        </div>
                    </span>
                </div>
            </div>
        </div>

        {{-- search bar --}}
        <form method="post" action="{{route('search_nav_page')}}">
            <div class="search" id="search">
                {{ csrf_field() }}
                <div class="search-container" id="search_container" style="">
                    <div class="input-group search_div search_div2">
                        <input type="text" class="form-control form-control-sm" placeholder="Search Product Name.."
                            name="search_fld">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-dark btn-almani"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        {{-- end search --}}

        {{-- Recommended Product --}}
        <section class="recommended-products">
            <div class="col-12">
                <div class="section-header">
                    <div class="section-label ">
                        <h1 class="title">Featured Products </h1>
                    </div>
                </div>
                <div class="section-content open">
                    <div class="section-heading product-list " id="product-list">
                        <div class="swiper-container swiper-horizontal">
                            <div class="swiper-wrapper">

                                @foreach ($featured_prods as $featured_prod)
                                <div class="swiper-slide" style="margin-top:9px !important;">
                                    <a href="{{route('product_page', [
                                                    'category_alias' => $featured_prod->category->category_alias,
                                                    'product_code' => $featured_prod->product_code,
                                                ])}}">
                                        <div class="list-item">
                                            <div class="item">
                                                @php
                                                $products_img = $featured_prod->image();
                                                @endphp
                                                <div class="img-container">
                                                    <img src="{{App\Util::asset($products_img)}}" alt="{{$featured_prod['name']}}">
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="name-container">
                                                    @php
                                                    $product_names = '';
                                                    $product_name = '';
                                                    if(strlen($featured_prod['product_name']) >= 9) {
                                                    $product_names = substr($featured_prod['product_name'], 0, 7);
                                                    $product_name = $product_names.'..';
                                                    } else {
                                                    $product_name = $featured_prod['product_name'];
                                                    }
                                                    @endphp
                                                    <div class="items">
                                                        <h1 class="prodname fleft">{{ $product_name }}</h1>
                                                        @if((session('login_led_designer') || session('login_sales')))
                                                        @if(session('login_sales'))
                                                        <h1 class="prodprice fleft" price_value_uae="{{ substr($featured_prod['minPrice_eager'], 0, 6) *2}}"
                                                            data-value="{{  substr($featured_prod['minPrice_eager'], 0, 6) *2}}">{{
                                                            substr($featured_prod['minPrice_eager'], 0, 6) *2}}
                                                            AED</h1>
                                                        @else
                                                           <h1 class="prodprice fleft" price_value_uae="{{ substr($featured_prod['minPrice_eager'], 0, 6) }}"
                                                            data-value="{{  substr($featured_prod['minPrice_eager'], 0, 6) }}">{{
                                                            substr($featured_prod['minPrice_eager'], 0, 6) }}
                                                            AED</h1>

                                                        @endif
                                                        @endif
                                                    </div>
                                                    <div class="items" style="display:block;">
                                                        <h1 class="prodcode fright">{{$featured_prod['product_code']}}</h1>
                                                        <h1 class="prodip fright">IP{{$featured_prod['product_ip']}}</h1>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                @endforeach

                            </div>
                            <div class="swiper-pagination"></div>
                            <br />
                        </div>
                        <div class="button-div">
                            <a href="{{route('catalog_landing_page')}}" class="btn btn-sm btn-dark btn-border">Show All
                                Category</a>
                        </div>
                    </div>



                </div>
            </div>
        </section>
        {{-- Recommended Product --}}


        <section class="welcome">
            <div class="section-header" data-open="false">
                <div class="section-label">
                    <h1 class="title">Welcome to Almani Lighting</h1>
                </div>
                <div class="section-label">
                    <span class="toggle-info">
                        <i class="fas fa-sort-down"></i>
                    </span>
                </div>
            </div>

            <div class="section-content">
                <div class="ceo-container">
                    <div class="item-ceo">
                        <div class="image-container">
                            <div class="item">
                                <div class="img-shadow-cont">
                                    <img class="img-shadow" src="{{asset('assets/images/welcometoalmani/johannes-eidens-aachen-germany-almani-lighting3.png')}}"
                                        alt="Johannes Eidens Aachen Germany Almani Lighting" />
                                </div>
                            </div>
                            <div class="item">
                                <p class="lbl-ceo"> <span class="name">Johannes N. Eidens (M.A.) </span> <span class="role">
                                        CEO and Founder </span> <span class="company"> ALMANI</span> </p>
                            </div>
                        </div>
                    </div>

                    <div class="item-ceo">
                        <div class="image-container">
                            <div class="item">
                                <div class="img-shadow-cont">
                                    <img class="img-shadow" src="{{asset('assets/images/welcometoalmani/khalid-almutawa-partner-and-chairman-asas-holding-group.png')}}"
                                        alt="Khalid Almutawa Partner and Chairman ASAS Holding Group" />
                                </div>
                            </div>
                            <div class="item">
                                <p class="lbl-ceo"> <span class="name"> Khalid Almutawa </span> <span class="role">
                                        Partner and Chairman </span> <a href="https://asasholding.ae/" target="_blank"><span
                                            class="company"> ASAS Holding Group </span> </a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 content-message">
                    <p>By choosing to study our website, we conclude that you are a discerning LED procurer of LED for
                        whom
                        quality matters. Your choice is therefore very wise…</p>
                    <p>Almani Lighting is committed to quality in all that we do. Indeed, it is the reason we
                        established
                        our business in 2015. Appalled at the poor standard of LED products and service across the GCC
                        region, we launched Almani Lighting to raise the bar and offer consistent LED excellence.</p>

                    <span class="more-welcome-messages">
                        <p>We are proud to design, manufacture and install superb yet reasonably priced LED units and
                            systems which deliver exceptional lighting, energy and cost savings, and longevity. That
                            commitment to quality extends also to the way we do business: we build excellent, trusting
                            relationships with clients who know they can rely on us for the right advice, support and
                            services, every time.</p>
                        <p>Excelling in large and complex LED challenges, we are delighted to invest our time,
                            knowledge
                            and care in your project from concept to completion and beyond. You can put your trust in
                            us
                            with 100% confidence, for we are at your service now and long into the future.</p>
                        <p>Please browse our catalogue, explore our website and of course, call us at any time. We
                            aspire
                            to be your trusted LED partner, at your service whenever you need us.</p>
                    </span>
                    <div class="button-div">
                        <a href="javascript:void(0);" data-read="false" class="btn-more btn btn-sm btn-dark btn-border">Read
                            More</a>
                    </div>
                </div>


            </div>
        </section>

        {{-- About Area --}}
        <section class="vision-mission">
            <div class="section-header">
                <div class="section-label ">
                    <h1 class="title">About us </h1>
                </div>
                <div class="section-label">
                    <span class="toggle-info">
                        <i class="fas fa-sort-down"></i>
                    </span>
                </div>
            </div>
            <div class="section-content">
                <div class="vis-mis-tab">
                    <div class="col-12">
                        <ul class="nav nav-pills nav-fill" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-mission"
                                    role="tab" aria-controls="pills-home" aria-selected="true">Our Mission
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-vision" role="tab"
                                    aria-controls="pills-profile" aria-selected="false">Our Vision
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-values" role="tab"
                                    aria-controls="pills-home" aria-selected="true">Our Values
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-mission" role="tabpanel" aria-labelledby="pills-home-tab">
                                <div class="content-container">
                                    <div class="item-content">
                                        <div class="img-container">
                                            <img src="https://almani.ae/assets/images/aboutus/mission.jpg" alt="">
                                        </div>
                                        Almani Lighting designs, supplies and installs finest quality LED lighting,
                                        delivering maximum energy- and cost-efficiency to commercial and industrial
                                        clients
                                        throughout the GCC region and neighbouring countries.
                                    </div>
                                </div>


                            </div>
                            <div class="tab-pane fade" id="pills-vision" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="content-container">
                                    <div class="item-content">
                                        <div class="img-container">
                                            <img src="https://almani.ae/assets/images/aboutus/vision.jpg" alt="">
                                        </div>
                                        We will earn our place as a global leader in LED lighting design and systems,
                                        trusted to deliver the highest quality and value every time.
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-values" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="content-container">
                                    <div class="item-content">
                                        <div class="img-container">
                                            <img src="https://almani.ae/assets/images/aboutus/values.jpg" alt="">
                                        </div>
                                        The values which inspire and guide all that we do at Almani Lighting are
                                        heavily
                                        influenced by our German heritage.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-div">
                        <a href="{{route('aboutus_page')}}" class="btn-border btn btn-sm btn-dark">Know More About Us</a>
                    </div>



                </div>
            </div>
        </section>

        {{-- Application Area --}}
        <section class="application-areas">

            <div class="section-header">
                <div class="section-label">
                    <h1 class="title">Applications Areas</h1>
                </div>
                <div class="section-label">
                    <span class="toggle-info">
                        <i class="fas fa-sort-down"></i>
                    </span>
                </div>
            </div>
            <div class="section-content">
                <div class="col-12">
                    <div class="app-container">
                        @foreach ($application_areas as $application_area)
                        {{-- @if ($app_products->count() > 0) --}}
                        <div class="app-item">
                            <a href="{{route('application_area_page', ['app_areas' => $application_area['app_slug'] ] ) }}">
                                <div class="item">
                                    <div class="img-container">
                                        <img src="{{App\Util::asset('assets/images/datasheets/application_areas/'. $application_area['app_image'].'.'.$application_area['app_extension'] )}}"
                                            alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="name-container">
                                        @php
                                        $app_names = '';
                                        $app_name = '';
                                        if(strlen($application_area['app_title']) >= 15) {
                                        $app_names = substr($application_area['app_title'], 0, 14);
                                        $app_name = $app_names.'..';
                                        } else {
                                        $app_name = $application_area['app_title'];
                                        }
                                        @endphp
                                        <p>{{$app_name}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        {{-- @endif --}}
                        @endforeach
                    </div>
                    <div class="button-div">
                        <a href="{{route('application_area_landing_page')}}" class="btn-border btn btn-sm btn-dark">
                            Show All Application Areas
                        </a>
                    </div>
                </div>
            </div>
        </section>
        {{-- Application Area --}}


        {{-- Youtube Video --}}
        <section class="youtube-view">
            <div class="section-header choose-us" data-open="false">
                <div class="section-label">
                    <h1 class="title">Why Choose us ?</h1>
                </div>
                <div class="section-label">
                    <span class="toggle-info">
                        <i class="fas fa-sort-down"></i>
                    </span>
                </div>
            </div>

            <div class="section-content">
                <div class="col-12">
                    <div class="video-container">
                        <div class="video-item" id="video-item">
                            <iframe id="asdasd" width="100%" height="100%" src="https://www.youtube.com/embed/7-7jbUNA4PQ"
                                frameBorder="0">
                            </iframe>
                        </div>
                    </div>
                    <div class="button-div">
                        <button data-target="#video-item" data-video-url="https://www.youtube.com/embed/7-7jbUNA4PQ?modestbranding=1&autoplay=1&showinfo=0&rel=0"
                            class="btn-border change_languaged item btn btn-sm btn-dark">English</button>
                        <button data-target="#video-item" data-video-url="https://www.youtube.com/embed/ItkzAlzdX10?modestbranding=1&autoplay=1&showinfo=0&rel=0"
                            class="btn-border change_languaged item btn btn-sm btn-dark">العَرَبِيَّة</button>
                    </div>
                </div>
            </div>
        </section>
        {{-- Youtube Video --}}

        {{-- Almani benefits --}}
        <div class="benefits" id="almanibenefits">

            <?php
                $benefits = [];
                $benefits[] = ['img'=>'5yrs.png','text'=>'Almani excellence - a five-year warranty on every product'];
                $benefits[] = ['img'=>'quality.png','text'=>'Exceptional German Quality Control'];
                $benefits[] = ['img'=>'nodefects.png','text'=>'Zero tolerance for defects'];
                $benefits[] = ['img'=>'check.png','text'=>'Raising the bar on quality, reliability and value'];
                $benefits[] = ['img'=>'idea.png','text'=>'LED from Almani: brighter, greener, and maximum value'];
                $benefits[] = ['img'=>'world.png','text'=>'World-beating products, crafted to the highest standards'];
                $benefits[] = ['img'=>'like.png','text'=>'Your ideal LED lighting partner: experienced, insightful, involved'];
                $benefits[] = ['img'=>'gears.png','text'=>'We give you the edge in LED technologies'];
                $benefits[] = ['img'=>'edit.png','text'=>'Comprehensive LED lighting services, tailored for you'];
                $benefits[] = ['img'=>'work.png','text'=>'Standard or bespoke: we’ll work with you, your way'];
            ?>

            <div class="section-header">
                <div class="section-label">
                    <h1 class="title">Almani Benefits</h1>
                </div>
                <div class="section-label">
                    <span class="toggle-info">
                        <i class="fas fa-sort-down"></i>
                    </span>
                </div>
            </div>

            <div class="section-content benefits_items">
                @foreach($benefits as $benefit)
                <div class="benefits_item">
                    <img src="{{App\Util::asset('assets/images/aboutus/benefits/'.$benefit['img'])}}">
                    <p>
                        {{$benefit['text']}}
                    </p>
                </div>
                @endforeach
            </div>
        </div>
        {{-- Almani benefits --}}


        {{-- Almani Approach --}}
        <section class="almani-approach">
            <div class="section-header">
                <div class="section-label heading-label">
                    <h1 class="title">Almani Approach</h1>
                </div>
                <div class="section-label">
                    <span class="toggle-info">
                        <i class="fas fa-sort-down"></i>
                    </span>
                </div>
            </div>
            <div class="section-content" style="background-image: url(https://almani.ae/assets/images/aboutus/almani-approach.jpg);">
                <div class="backdrop"></div>
                <div class="approach-container">
                    <div class="approach-item">
                        <p>
                            Quite simply, we aim to be and deliver the very best, every time. This is why we work
                            exclusively with LED lighting products sourced only from meticulously vetted suppliers. Our
                            quality control process is of prime importance to us and underpins our mission, vision and
                            values.
                            <br><br>
                            Our LED lights and systems are carefully selected to give the best quality light, excellent
                            product durability and service and the greatest cost efficiency. We constantly update our
                            product range and designs in line with technological advances and latest trends. Crucially,
                            LED
                            lighting also helps reduce your carbon footprint.<span class="dot-effect">...</span>
                            <br><br>
                            <span class="second-par" style="display: none;">
                                We not only work with the finest and most environmentally friendly products, we also
                                aspire
                                to offer the highest standards of customer care and service. When we find an
                                outstanding
                                manufacturer who meets our high standards, we work with them closely to build sound,
                                productive
                                relationships and secure consistently excellent value. We are the sole link between the
                                manufacturers and you, our client: you can view products in our Dubai showroom but we
                                have
                                no
                                expensive retail outlets and no middle men to inflate costs. We believe that by keeping
                                the
                                supply chain simple, we can make certain that as an Almani client, you enjoy the best
                                possible
                                experience and outcomes.
                                <br><br>
                                Friendly, helpful and always professional, we always go the extra distance to make
                                Almani
                                Lighting your global lighting partner of choice.
                            </span>
                        </p>
                    </div>
                    <div class="approach-item">
                        <div class="button-div">
                            <a data-read="false" href="javascript:void(0)" id="approach-read-more" class="btn-border btn btn-sm btn-dark">
                                Read More</a>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        {{-- Almani Approach --}}

        {{-- Price Match --}}
        @php
        $pricematchs = [];
        $pricematchs[] = ['img' => 'https://almani.ae/assets/images/pricematchpromise/guarantee.jpg', 'title' =>
        'GUARANTEES THE BEST LED LIGHTING PRICES IN UAE', 'text' => ' You’ve found a cheaper offer? Just send us the
        competitor’s quotation. If the items are similar in specification and quality, we’ll match the price,
        guaranteed!'];
        $pricematchs[] = ['img' => 'https://almani.ae/assets/images/pricematchpromise/controlled.jpg', 'title' =>
        'CONTROLLED MANUFACTURE', 'text' => ' We carefully control the manufacture of all our products. Every single
        one is
        made to our high German standards and our efficient manufacturing agreements keep costs low.'];
        $pricematchs[] = ['img' => 'https://almani.ae/assets/images/pricematchpromise/manufacturer_direct.jpg', 'title'
        =>
        'FROM THE MANUFACTURER (THAT’S US!) DIRECT TO YOU', 'text' => ' There are no costly middle men in our supply
        chain.
        We work directly with you, avoiding extra layers of costs for distributors, agents or retailers. We also avoid
        the
        burden of many expensive showrooms. If you want to see our goods or project sites, simply get in touch!'];
        $pricematchs[] = ['img' => 'https://almani.ae/assets/images/pricematchpromise/fine_company.jpg', 'title' => 'IN
        FINE COMPANY', 'text' => ' We manufacture in the same places as some of the world’s most celebrated LED brands,
        such as Philips and Osram. There’s no difference in our quality, but you’ll be amazed at the contrast in
        prices!
        '];
        @endphp

        <section class="price-match">
            <div class="section-header">
                <div class="section-label heading-label">
                    <h1 class="title">Price Match Promise </h1>
                </div>
                <div class="section-label">
                    <span class="toggle-info">
                        <i class="fas fa-sort-down"></i>
                    </span>
                </div>
            </div>
            <div class="section-content">
                <div class="header-touch">
                    <h1 class="title">High quality and low prices.. <br> Is that possible?</h1>
                    <div class="sub-title">
                        <span>At Almani Lighting, YES, it certainly is! Here’s why.. </span>
                    </div>
                </div>
                <div class="price-match-container">
                    <div class="swiper-container swiper-vertical mh-50 oh">
                        <div class="swiper-wrapper">
                            @foreach ($pricematchs as $pricematch)
                            <div class="swiper-slide">
                                <div class="price-match-item">
                                    <div class="match-item">
                                        <div class="img-container">
                                            <img src="{{$pricematch['img']}}" alt="">
                                        </div>
                                    </div>
                                    <div class="match-item">
                                        <div class="title-container">
                                            <h1>{{$pricematch['title']}}</h1>
                                        </div>
                                    </div>
                                    <div class="match-item">
                                        <div class="text-container">
                                            <p>{{$pricematch['text']}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <br>
                        <!-- If we need pagination -->
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="video_container" style="">
                        <div class="video_intro_img" data-toggle="modal" data-target="#video_modal" data-video_url="https://www.youtube.com/embed/MKSedNHx11k?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                            <img src="{{App\Util::asset('assets/images/icons/youtube_play_icon.png')}}" alt="play">
                            <p> Price Match Promise</p>
                        </div>
                    </div>
                </div>

                <div class="bar_text">
                    <p class="title_p" style="">
                        That’s why you can be sure..
                    </p>
                    <h3 class="title">
                        WE WON’T BE BEATEN ON PRICE!
                    </h3>
                </div>

            </div>



        </section>
        {{-- Price Match --}}

        {{-- Approved Supplier --}}
        @php
        $approved_suppliers = [];
        $approved_suppliers[] = ['img' => 'https://almani.ae/assets/images/approvedsupplier/manufactures.jpg', 'text'
        =>
        'Almani Lighting GmbH manufactures, supplies and installs only the highest quality LED units on the market –
        equal
        to or better than the famous brands.' ];
        $approved_suppliers[] = ['img' => 'https://almani.ae/assets/images/approvedsupplier/prices.jpg', 'text' =>
        'Almani
        Lighting LED prices are much, much lower than the well-known brands.'];
        $approved_suppliers[] = ['img' => 'https://almani.ae/assets/images/approvedsupplier/customercare.jpg', 'text'
        =>
        'Almani service and customer care are the finest on the market: just ask our clients!' ];
        $approved_suppliers[] = ['img' => 'https://almani.ae/assets/images/approvedsupplier/approved.jpg', 'text' =>
        'You
        need to make sure that Almani Lighting is on your "Approved Suppliers" list! '];
        @endphp

        <section class="approved-supplier">
            <div class="section-header">
                <div class="section-label heading-label">
                    <h1 class="title">Approved Supplier</h1>
                </div>
                <div class="section-label">
                    <span class="toggle-info">
                        <i class="fas fa-sort-down"></i>
                    </span>
                </div>
            </div>

            <div class="section-content">

                <div class="header-touch">
                    <h1>If you are a construction client or consultant, then this information could save you a FORTUNE!</h1>
                    <span>These are the facts – plain and simple..</span>
                </div>

                <div class="apd-item-container">
                    <div class="swiper-container swiper-vertical mh-50 oh">
                        <div class="swiper-wrapper">
                            @foreach ($approved_suppliers as $approved_supplier)
                            <div class="swiper-slide">
                                <div class="apd-item">
                                    <div class="item">
                                        <div class="img-container">
                                            <img src="{{$approved_supplier['img']}}" alt="">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="text-container">
                                            <p>{{$approved_supplier['text']}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <br>
                        <!-- If we need pagination -->
                        <div class="swiper-pagination"></div>
                    </div>

                </div>
                <div class="approved-supplier-container" style="background-image: url(https://almani.ae/assets/images/approvedsupplier/approvedsupplier.jpg);">
                    <div class="backdrop"></div>
                    <div class="approved-supplier-item">
                        <div class="title">
                            <h2>WHY IT PAYS TO CHOOSE ALMANI AS AN APPROVED SUPPLIER</h2>
                        </div>
                        <div class="text">
                            <p>
                                No matter how far into the construction planning process you might be, adding Almani
                                Lighting to your Approved Suppliers list is the wisest move you can make.
                                <br><br>
                                You see, most clients and consultants assume that the only way to buy quality LED is to
                                specify the biggest brands. But with Almani Lighting’s absolute commitment to high
                                German
                                standards and guaranteed quality at lowest possible prices, there’s no longer any need
                                to
                                pay a high price for the finest LED products.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="almani-led-benefits">
                    <h1>At Almani, we value large LED projects of all kinds, making certain that your project benefits
                        from:
                    </h1>
                    <ul>
                        <li>Products that match or exceed your big-brand specification</li>
                        <li>Major cost savings</li>
                        <li>Exceptional value </li>
                        <li>One single supplier able to fulfill ALL your LED needs</li>
                        <li>Full compliance </li>
                        <li>Excellent guarantees</li>
                    </ul>
                    <div class="img-container">
                        <img src="https://almani.ae/assets/images/approvedsupplier/valuelargeledprojects.jpg" alt="">
                    </div>

                </div>

                <div class="bar_text">
                    <p class="title_p" style="">
                        Want to know more? <br>
                        <a class="prompt_callback" data-toggle="modal" data-target="#contact_us_modal">CLICK HERE</a>
                        for a
                        prompt callback.
                    </p>
                    <h3 class="title"></h3>
                </div>
            </div>

        </section>
        {{-- Approved Supplier --}}

        {{-- Quality Control --}}
        @php
        $qualitycontrols = [];
        $qualitycontrols[] = ['img' => 'https://almani.ae/assets/images/qualitycontrol/fiveyearwarranty.jpg', 'title'
        =>
        'Five-year warranty', 'text' => 'Testament to our passion and commitment to quality, our products all come with
        a
        full five-year warranty; were certain that each one is the best available on the market.'];
        $qualitycontrols[] = ['img' => 'https://almani.ae/assets/images/qualitycontrol/ourqcprocess.jpg', 'title' =>
        'Our
        QC process', 'text' => ' How can we be so sure about our product quality? Well, we buy only from hand-picked
        manufacturers, each one of which has been thoroughly vetted and is continually checked to ensure compliance
        with
        our rigorous quality standards. We accept only the very best. In addition, we test and check every product in
        our
        laboratories and testing facilities in Germany and Dubai, as well as commissioning carefully selected and
        highly
        reputable partners such VDE and TÜV Süd to support our extensive testing operations.'];
        $qualitycontrols[] = ['img' => 'https://almani.ae/assets/images/qualitycontrol/zerotolerance.jpg', 'title' =>
        'Zero
        tolerance for defects', 'text' => 'Because we go to great lengths to test, check and monitor Almani quality, we
        are
        confident that our products are of superb quality and we are committed to zero defects. However, while we
        strive
        for perfection we know that its never completely possible. We therefore give you our absolute assurance: should
        you
        ever encounter a defect in your Almani product, we will replace it immediately, without any hesitation. We want
        to
        know that every Almani Lighting product works hard for you and delivers on every objective – no exceptions.'];
        @endphp

        <section class="quality-control">
            <div class="section-header">
                <div class="section-label heading-label">
                    <h1 class="title">Quality Control</h1>
                </div>
                <div class="section-label">
                    <span class="toggle-info">
                        <i class="fas fa-sort-down"></i>
                    </span>
                </div>
            </div>
            <div class="section-content">
                <div class="quality-control-container">
                    <div class="quality-control-message">
                        <p>When you choose Almani Lighting for your LED projects, you choose peace of mind, quality,
                            value
                            and absolute reliability.
                            With our German heritage, quality is always uppermost in our minds at Almani.
                            It's the reason we invest a great deal of time, care and effort in making certain that you
                            can
                            depend on every single Almani product and service.
                        </p>
                    </div>
                    <div class="swiper-container swiper-vertical mh-50 oh">
                        <div class="swiper-wrapper">
                            @foreach ($qualitycontrols as $qualitycontrol)
                            <div class="swiper-slide">
                                <div class="quality-control-item">
                                    <div class="quality-item">
                                        <div class="img-container">
                                            <img src="{{$qualitycontrol['img']}}" alt="">
                                        </div>
                                    </div>
                                    <div class="quality-item">
                                        <div class="title-container">
                                            <h1>{{$qualitycontrol['title']}}</h1>
                                        </div>
                                    </div>
                                    <div class="quality-item">
                                        <div class="text-container">
                                            <p>{{$qualitycontrol['text']}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <!-- If we need pagination -->
                        <br>
                        <div class="swiper-pagination"></div>
                    </div>
                    {{-- <div class="button-div" style="margin-bottom:10px;">
                        <a href="{{route('qualitycontrol_page')}}" class="btn-border btn btn-sm btn-dark">
                            Check our Quality Control</a>
                    </div> --}}
                </div>

            </div>
        </section>
        {{-- Quality Control --}}

        {{-- Quality Assurance --}}
        @php
        $qualityassurances = [];
        $qualityassurances[] = ["img" => "https://almani.ae/assets/images/quality-assurance/quality.jpg", "title" =>
        "German", "text" => "Rooted in our German heritage, high standards control the quality of every single LED
        component and unit that we produce."];
        $qualityassurances[] = ["img" => "https://almani.ae/assets/images/quality-assurance/trust.jpg", "title" =>
        "Honest
        and Transparent", "text" => "We’re happy to tell you what most LED manufacturers try to hide... It’s this: 99%
        of
        the world’s LED products are made in China, and, whatever they might pretend, every LED brand sources most of
        their
        goods there. Quite simply, that’s the truth. But we’re not like other brands. 'Made in China by Almani' means
        'Made
        to perfection'. Made to the highest German standards. Made to perform perfectly for years and years.
        Guaranteed."];
        $qualityassurances[] = ["img" => "https://almani.ae/assets/images/quality-assurance/award.jpg", "title" => "The
        Best LED Brand in the UAE", "text" => "Our exceptional production standards are proof that it’s not where a
        product
        is made, but who makes it, that determines its quality. Product perfection is the only reason why we can offer
        a
        solid gold, 5-year warranty on every single Almani product. What’s more, by manufacturing where skills are high
        but
        far less costly, and by cutting out the middle men, we also keep our prices 5-10 times lower than their
        European
        equivalents."];
        @endphp

        <section class="quality-assurance">
            <div class="section-header">
                <div class="section-label heading-label">
                    <h1 class="title">Quality Assurance</h1>
                </div>
                <div class="section-label">
                    <span class="toggle-info">
                        <i class="fas fa-sort-down"></i>
                    </span>
                </div>
            </div>
            <div class="section-content">

                <div class="header-section">
                    <h1>WHERE your LED is made is irrelevant.Ask instead, WHO made it?</h1>
                    <span>At Almani, we are proud to be:</span>
                </div>
                <div class="quality-assurance-container">
                    <div class="swiper-container swiper-vertical mh-50 oh">
                        <div class="swiper-wrapper">
                            @foreach ($qualityassurances as $qualityassurance)
                            <div class="swiper-slide">
                                <div class="quality-assurance-item">
                                    <div class="quality-item">
                                        <div class="img-container">
                                            <img src="{{$qualityassurance['img']}}" alt="">
                                        </div>
                                    </div>
                                    <div class="quality-item">
                                        <div class="title-container">
                                            <h1>{{$qualityassurance['title']}}</h1>
                                        </div>
                                    </div>
                                    <div class="quality-item">
                                        <div class="text-container">
                                            <p>{{$qualityassurance['text']}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <!-- If we need pagination -->
                        <br>
                        <div class="swiper-pagination"></div>
                    </div>

                </div>
                <div class="video_container" style="">
                    <div class="video_intro_img" data-toggle="modal" data-target="#video_modal" data-video_url="https://www.youtube.com/embed/bdRJgok7R0E?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                        <img src="{{App\Util::asset('assets/images/icons/youtube_play_icon.png')}}" alt="play">
                        <p>
                            Quality Assurance
                        </p>
                    </div>


                </div>

                <div class="bar_text">
                    <p class="title_p" style="">
                        Highest quality, lowest prices and a 5-year warranty on every item.
                    </p>
                    <h3 class="title">
                        Perfect.
                    </h3>
                </div>

            </div>
        </section>
        {{-- Quality Assurance --}}


        {{-- Message From CEO --}}
        <div class="section message-from-ceo">
            <div class="section-header">
                <div class="section-label heading-label" id="message-from-ceo">
                    <h1 class="title">Message From the CEO</h1>
                </div>
                <div class="section-label">
                    <span class="toggle-info">
                        <i class="fas fa-sort-down"></i>
                    </span>
                </div>
            </div>
            <div class="section-content">
                <div class="message-from-ceo-container">
                    <div class="message-from-ceo-item">

                        <div class="message-item">

                            <div class="column-2">
                                <div class="cols-1 ceo-image">
                                    <div class="img-container">
                                        <img src="https://almani.ae/assets/images/message/johannes-eidens-aachen-germany-almani-lighting.png"
                                            alt="">
                                    </div>
                                </div>
                                <div class="cols-1 ceo-msg">
                                    <p>
                                        This has been an excellent year for Almani Lighting. A proud German and
                                        European, I
                                        learned from childhood that high standards
                                        of quality, professionalism, service and integrity were essential to building
                                        strong
                                        customer relationships, outstanding businesses
                                        and better lives for all. I’m therefore delighted that my early vision of
                                        sharing
                                        my
                                        German standards and approach with Almani’s
                                        rapidly growing client base across the UAE is proving both popular and
                                        successful!
                                    </p>
                                </div>
                            </div>




                        </div>

                        <div class="name-container">
                            <p class="lbl-ceo"> Johannes N. Eidens (M.A.) <span class="role"> CEO and Founder
                                </span> </p>
                        </div>





                        <div class="message-item">
                            <div class="text-container">


                                <p>
                                    I am certain that our exciting growth is due to our absolute commitment to quality
                                    –
                                    quality that we can confidently guarantee
                                    because we manufacture every single product to our strict German specifications and
                                    standards, from the smallest chip to the
                                    largest industrial light fittings. Similarly, we have developed our own world-class
                                    project management software which allows our
                                    people to do the best job they possibly can and, most importantly, helps ensure the
                                    best possible experience and outcomes for
                                    our clients. And even though Almani Lighting offers only the finest LED quality in
                                    the
                                    world, our portfolio is one of the largest in the
                                    entire UAE region, with its many superb ranges and infinite bespoke options.
                                    Furthermore, we are innovators, eager to anticipate
                                    and adapt to changing technologies and market demands.<span class="dot-message d-none">...</span>
                                </p>

                                <span class="more-message" style="display: none">
                                    <p>
                                        Our business now has around 50 staff, all trained, highly skilled and
                                        client-focused. At the time of writing, we are involved in more
                                        than 100 projects in Dubai alone. Many of these are large, complex and
                                        prestigious
                                        projects and we are proud of the work we do
                                        for every single client who works with us. We are one of a small number of
                                        companies in the UAE who can quote for an entire
                                        lighting project using just the one brand (or two, if we also involve our
                                        recently
                                        launched daughter brand, CamelLED).
                                    </p>
                                    <p>
                                        Yes, we are proud. We love making a positive difference. We are deliberately
                                        raising the global bar in LED service and technology.
                                        But we are not complacent. We seek opportunities to improve in any way we can.
                                        We
                                        liaise closely with our clients to find out how
                                        we can do things better, because over time, every tiny increment adds up to
                                        significant progress. We manage the challenges of
                                        promoting high standards in an industry where poor quality is rife. And we
                                        intend
                                        to become a truly amazing organisation,
                                        celebrated worldwide for excellence!
                                    </p>
                                    <p>
                                        As the CEO of both Almani Lighting and CamelLED, I have many thanks to give: I
                                        am
                                        grateful to our staff, investors, partners and
                                        families for their support, hard work and commitment. And of course, I am
                                        immensely
                                        grateful to our wonderful clients for trusting
                                        us with their important and challenging projects: we are honoured to work with
                                        you,
                                        delighted to deliver beyond your expectations
                                        and we look forward to doing so for many years to come.
                                    </p>
                                </span>

                            </div>

                        </div>

                        <div class="button-div" style="display: flex;justify-content: center;margin: 0 auto;">
                            <button id="btn-msg-read" data-read="false" class="btn btn-sm btn-dark btn-border">Read
                                More</button>
                        </div>




                        {{-- <div class="button-div" style="display: flex;justify-content: center;margin: 0 auto;">
                            <button id="" class="btn btn-sm btn-dark btn-border">view more</button>
                        </div> --}}
                    </div>
                </div>
            </div>

        </div>
        {{-- Message From CEO --}}

    </section> <!-- #intro -->

</div>
{{-- price match promise --}}
<div class="video_container">
    <!-- Modal -->
    <!-- //////////////////////////////////////////////////////// -->
    <div class="modal fade youtube_video_modal" id="video_modal" tabindex="-1" role="dialog" aria-labelledby=""
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <iframe class="video" style="" allowfullscreen></iframe>

                    <div style="margin-top: 20px;">
                        <button type="button" class="btn-almani change_language" data-target="#video_modal"
                            data-language="english" data-video_url="https://www.youtube.com/embed/bdRJgok7R0E?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                            English
                        </button>
                        <button type="button" class="btn-almani change_language" data-target="#video_modal"
                            data-language="arabic" data-video_url="https://www.youtube.com/embed/PMvZh8SVYn8?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                            العَرَبِيَّة
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- //////////////////////////////////////////////////////// -->
    <!-- Modal -->
</div>

@endsection


 @section('popup-adds')
      
      <style type="text/css">

        .default_small_heading {
            color: #989898;
            margin: 0;
            word-spacing: 2px;
            line-height: 32px;
        }

        .default_text {
            color: #676767;
            font-size: 14px;
            margin: 0;
            word-spacing: 2px;
            line-height: 17px;
        }

        .font_600 {
             font-weight: 900;
              font-size: 37px;

          }

          .blink_me {
              animation: blinker 1s linear infinite;
            }

            @keyframes blinker {
              50% {
                opacity: 0.2;
              }
            }

        }

      </style>

      
      <script>
        
      </script>

 @endsection

