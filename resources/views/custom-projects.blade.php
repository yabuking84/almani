@extends('main-layout', [    
    'customprojects_menu' => 'active',
    'company_menu' => 'active'
])
 
    @section('css-lib')
     <link href="{{App\Util::asset('assets/css/master-custom-projects.css')}}" rel="stylesheet">  
    @endsection

    @section('javascript-lib')
    <script src="{{App\Util::asset('assets/js/master-custom-projects.js')}}"></script>
    @endsection

@section('title')
    Custom Projects{{App\Meta::webpageTitle()}}
@endsection

@section('main-content')

    @if (session('error') !== null)
    <div class="container alert_container">
        <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
        <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
            {{session('message')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
    @endif


    <section class="section section_title heading-first">
        <h1 class="title main_title">
            <a class="button is-primary mb-titile">Custom Projects </a>
        </h1>
    </section>

    <section class="section content-holder" id="uniquesolutions">
            <div class="container">
                    <div class="intro_text">
                        <h1 class="title">            
                            Unique Solutions for Complex Challenges
                        </h1>
                        <p class="title_p" style="">
                            LED is the jewel in the crown of many complex and challenging projects. Leading on LED quality and excellence across the UAE, Almani has therefore worked with highly skilled professionals from many disciplines who share our passion for creating spectacular custom projects
                            <br><br>
                            By partnering with the best of these talented craftsmen, we are now very proud to offer the finest, fully managed concept to completion service for any premium private or commercial project. The range is limitless, and the more complex your requirements, the more we love the challenge!
                        </p>
                    </div>
            </div>
    </section>

    <section class="section">
                <?php
                $items = [];
                $items[] = ['img'=>'custom_kitchen.jpg','title'=>' Custom Kitchen'];
                $items[] = ['img'=>'smart_automation.jpg','title'=>'Smart Automation'];
                $items[] = ['img'=>'luxury_suites.jpg','title'=>'Luxury Suites'];
                $items[] = ['img'=>'hote_refurbishment.jpg','title'=>'Hotel Refurbishments'];
                ?>

                <div class="item_boxes">
                <div class="items">
                    @foreach($items as $item)
                    <div class="item">
                        <div class="item_img" style="background-image: url({{App\Util::asset('assets/images/customprojects/'.$item['img'])}});"></div>
                        <h5>{{$item['title']}}</h5>
                    </div>
                    @endforeach
                </div>
                </div>
    </section>
    <section>
            <div class="bar_text">
                    <p class="title_p" style="">
                        Let Almani Take Care of your Custom Project. Any Size. Any Services. Delivered to Perfections.
                    </p>
                    <h3 class="title">
                            Talk to us Today On  <span class="tel-text"> <a href="tel:+971 4-887-3265">+971 4-887-3265</a>  </span> 
                    </h3>          
               </div>
    </section>

    

    <div class="items_bg">

        <div class="container" id="whychoosealmani">
                <div class="intro_text">
                        <h1 class="title">            
                                Why choose Almani for your Custom Project?
                        </h1>
                        <p class="title_p" style="">
                                Our clients know that the reasons are many and varied, but here are  5 prime reasons for choosing Almani.                       
                        </p>
                    </div>

        <div class="img_text_boxes">

                <?php
                    $items = [];
                    $items[] = ['image'=>'finest_services.jpg','title'=>'The Finest Products and Services','text'=>'Your project will exude Almani’s outstanding quality from start to finish.'];
                    $items[] = ['image'=>'dreams_fullfiled.jpg','title'=>'Your Dreams Fulfilled','text'=>'No matter what your dream project - if it can be designed and accomplished, we will create it!'];
                    $items[] = ['image'=>'right_people.jpg','title'=>'The Right People for the Job','text'=>'All our installers are fully trained, skilled and qualified to the latest standards.'];
                    $items[] = ['image'=>'management.jpg','title'=>'Seamless Management','text'=>'Excellent project management ensures safety, efficiency and superb results.'];
                    $items[] = ['image'=>'goal.jpg','title'=>'Our Goal - Your Delight','text'=>'We expect the very best in all that we do: delighted customers are our mark of success!'];
                ?>
                @foreach($items as $item)
                <div class="img_text_box">
                    <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/customprojects/'.$item['image'])}});">
                    </div>
                    <div class="text_box">
                        <h1><?=ucfirst($item['title']);?></h1>                        
                        <p><?=$item['text']?></p>
                    </div>
                </div>
                @endforeach 
        </div>
            </div>
    </div>

@endsection


