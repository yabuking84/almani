@extends('main-layout', [    
    'products_menu' => 'active',
])
 
    @section('css-lib')
    <link href="{{App\Util::asset('assets/css/master-catalog-brands.css')}}" rel="stylesheet">  
    @endsection

    @section('javascript-lib')
    <script src="{{App\Util::asset('assets/js/master-catalog-brands.js')}}"></script>
    @endsection

@section('title')
    Brands{{App\Meta::webpageTitle()}}
@endsection

@section('main-content')

    @if (session('error') !== null)
    <div class="container alert_container">
        <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
        <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
            {{session('message')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
    @endif


    <section class="section section_title heading-first">
        <h1 class="title main_title">
            <a class="button is-primary">Brands</a>
        </h1>
    </section>

    <section class="section heading-second">
        <div class="container">
            <div class="intro_text">
                <h3 class="title">      
                    Every project is unique. We have your ideal solution! <br/>
                    Begin by choosing the brand for you...
                </h3>
            </div>
        </div>
    </section>

    <section class="section content-section-heading">
        <div class="container">
                <div class="img_text_boxes">

                        <div class="img_text_box" id="almani">
                            <a href="https://almani.ae/catalog">
                                <div class="img_box " style="background-image: url('https://camel.almani.ae/assets/images/home/aboutus/img1.jpg');">
                                        <div class="almani-img">
                                           <img src="{{App\Util::asset('assets/images/logo/logo-colored.png')}}" alt="img">
                                       </div>
                                </div>
                              </a>
                                <div class="text_box">
                                    <h1>Almani Lighting</h1>
                                    <p>  Renowned for the fine quality in all that we do, Almani Lighting offers 
                                            a watertight 5-year warranty on every one of our beautiful products.
                                            We’re proud to raise the bar in LED lighting across the GCC and beyond.
                                     </p>  
                                     <p>Looking for the ultimate in satisfaction and ROI? 
                                         <span style="display: block;">Almani Lighting is your perfect LED partner!</span>
                                     </p>
                                </div>
                        </div>

                        <div class="img_text_box" id="camelled">
                            <a href="https://camel.almani.ae/catalog">
                                <div class="img_box" style="background-image: url('https://camel.almani.ae/assets/images/home/camelled_approach.jpg');">
                                      <div class="camelled">
                                            <img src="https://camel.almani.ae/assets/images/home/camel-white.png" alt="img">
                                        </div>
                                </div>
                             </a>
                                <div class="text_box">
                                        <h1>CamelLED</h1>
                                    <p>  A smaller budget shouldn’t mean you have to accept mediocrity.
                                            We created CamelLED to deliver good quality and exceptional value.
                                    </p>
                                    <p>
                                            Want flexible warranty options from 1-3 years, reliable LED products and great customer service?
                                            <span style="display: block;">CamelLED is made for you!</span>
                                    </p>
                                    
                                </div>
                        </div>
                </div>
        </div>
            
    </section>

    <section class="section brand-view" id="video">
            <div class="container">
                    <div class="video_container" style="">
                        <div class="video_intro_img" 
                                data-toggle="modal" 
                                data-target="#video_modal"
                                data-video_url="https://www.youtube.com/embed/gjaRpKVEydY?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                            <img src="{{App\Util::asset('assets/images/icons/youtube_play_icon.png')}}" alt="play">
                            <p>
                                Explore which brand is for you!
                            </p>
                        </div>
            
                    </div>    
            </div>
    </section>

    <?php
    $client_logos = [];
    $client_logos[] = 'tuvrheinland.png';
    $client_logos[] = 'vde.png';
    $client_logos[] = 'tuv.png';
    $client_logos[] = 'ce.png';
    $client_logos[] = 'rohs.png';
    $client_logos[] = 'saa.png';
    $client_logos[] = 'gs.png';
    $client_logos[] = 'ul.png';
    $client_logos[] = 'etl.png';
    $client_logos[] = 'philips.png';
    $client_logos[] = 'bridgelux.png';
    $client_logos[] = 'cree.png';
    $client_logos[] = 'epistar.png';
    $client_logos[] = 'citizen.png';
    $client_logos[] = 'samsung.png';
    $client_logos[] = 'meanwell.png';
    $client_logos[] = 'osram.png';
    $client_logos[] = 'luminus.png';
    $client_logos[] = 'nichia.png';
    $client_logos[] = 'lumileds.png';
    $client_logos[] = 'moso.png';
    $client_logos[] = 'edison.png';
    $client_logos[] = 'easeic.png';
    $client_logos[] = 'lifud.png';
    $client_logos[] = 'euchips.png';
    $client_logos[] = 'inventronics.png';
    $client_logos[] = 'tridonic.png';
?>

    <section class="section brand-images">
        <div class="container">
            <div class="client-logos">
                <div class="owl-carousel owl-theme owl-loaded">
                    @foreach($client_logos as $client_logo)
                        <div class="item">     
                            <img alt="client logo" src="<?=asset('assets/images/qualitycontrol/certificate_logos/'.$client_logo)?>">                
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>


@endsection



@section('modal')


                        <!-- ///////////////////////// Modal ////////////////////////////// -->
                        <div class="modal fade youtube_video_modal" id="video_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
                            <div class="modal-content">          
                              <div class="modal-body">
            
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <iframe class="video" style="" allowfullscreen></iframe>
            
                                <div style="margin-top: 20px;">
                                    <button type="button" 
                                        class="btn-almani change_language" 
                                        data-target="#video_modal"
                                        data-language="english"
                                        data-video_url="https://www.youtube.com/embed/gjaRpKVEydY?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                                        English                    
                                    </button>
                                    <button type="button" 
                                        class="btn-almani change_language" 
                                        data-target="#video_modal"
                                        data-language="arabic"
                                        data-video_url="https://www.youtube.com/embed/TU998jaAdls?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                                        العَرَبِيَّة
                                    </button>              
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>    
                        <!-- ///////////////////////// End Modal /////////////////////////////// -->

@endsection
                        