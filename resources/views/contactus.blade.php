@extends('main-layout', ['contactus_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-contactus.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-contactus.js')}}"></script>
@endsection



@section('js-head-lib')
@parent
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuqUdjg_q8ZGWgtAgUo82w4IYj3WGGY6k" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js" async="" defer="" type="text/javascript"></script>
@endsection


@section('title')
Contact Us{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')

@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif




<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Contact Us</a>
    </h1>
</section>




<section class="section">




<div class="container intro_texts">
    <div class="intro_text">
        <h3 class="title">
            <span>
            Do you have a question, a problem, an idea or a new lighting challenge? 
            <br>
            The Almani Lighting team would be delighted to hear from you. 
            </span>            
        </h3>
        <p class="title_p" style="">
            <span>Call or email and let's get talking!</span>
        </p>
    </div>
</div>

<h5 class="single_line google_maps_h5">
    <a class="show_map" data-map="germany">Open Google Maps</a>
    <a class="close_map" style="display: none;">Close Google Maps</a>
</h5>
   

<div class="google_maps">
    <div class="map_section" style="display: none;">
        <div id="map_container"></div>
        <span><i class="fa fa-remove close_map"></i></span>
    </div>
</div>



<div class="bar_text">

    <div>
        <h4 class="bar_text_title">Germany, Aachen</h4>
        <div>
            <div class="contact_div managing_director">      
                <div class="contact_div_title">            
                    <i class="fas fa-user-tie"></i>
                    <h4>Managing Director</h4>
                </div>  
                <p class="contact_div_text">
                    Martin Heyen
                </p>
            </div>
            <div class="contact_div address">
                <div class="contact_div_title">            
                    <i class="fas fa-globe-africa"></i>
                    <h4>Address</h4>
                    <span class="show_map" data-map="germany">Show in Map</span>
                </div>
                <p class="contact_div_text">
                    <a class="show_map_mobile germany" data-map="germany" href="whatsapp://send?text=https://goo.gl/maps/UPsjZGoxVst">Share via Whatsapp</a>
                    Almani Lighting GmbH <br>
                    Heyder Feldweg 50 <br>
                    52072 Aachen <br>
                    Germany
                </p>
            </div>
            <div class="contact_div phone">
                <div class="contact_div_title">            
                    <i class="fas fa-phone"></i>
                    <h4>Phone</h4>
                </div>
                <p class="contact_div_text">
                    Tel: +49-2407-5656460 <br>
                    Fax: +49-2407-5656461
                </p>
            </div>

            <div class="contact_div email">
                <div class="contact_div_title">            
                    <i class="far fa-envelope"></i>
                    <h4>Email</h4>
                </div>
                <p class="contact_div_text">
                    info@almani.ae
                </p>
            </div>

        </div>

    </div>


    <div>
        <h4 class="bar_text_title">UAE, Dubai</h4>
        <div>
            <div class="contact_div managing_director">
                <div class="contact_div_title">            
                    <i class="fas fa-user-tie"></i>
                    <h4>Managing Director</h4>
                </div>  
                <p class="contact_div_text">
                    Johannes N. Eidens (M.A.)
                </p>
            </div>
            <div class="contact_div address">
                <div class="contact_div_title">            
                    <i class="fas fa-globe-africa"></i>
                    <h4>Address</h4>
                    <span class="show_map" data-map="dubai">Show in Map</span>
                </div>
                <p class="contact_div_text">
                    <a class="show_map_mobile dubai" data-map="dubai" href="whatsapp://send?text=https://goo.gl/maps/FWiZerjRFBq">Share via Whatsapp</a>
                    Almani Lighting L.L.C. <br>
                    Showroom 10, <br>
                    Dubai Investment Park 1, <br>
                    Opposite Green Community East, <br>
                    Beside Park N' Shop <br>
                    P.O. BOX 3219 <br>
                    Dubai, UAE
                </p>
            </div>
            <div class="contact_div phone">
                <div class="contact_div_title">            
                    <i class="fas fa-phone"></i>
                    <h4>Phone</h4>
                </div>
                <p class="contact_div_text">
                    Tel: +971-4-887-3265 <br>
                    Fax: +971-4-887-4550
                </p>
            </div>
            <div class="contact_div email">
                <div class="contact_div_title">            
                    <i class="far fa-envelope"></i>
                    <h4>Email</h4>
                </div>
                <p class="contact_div_text">
                    info@almani.ae
                </p>
            </div>
        </div>
    </div>



</div>



<div class="container">

<form class="form_quote" id="form_quote" method="post" action="{{route('sendcontactdetails')}}" enctype="multipart/form-data" style="justify-content: center;">

{{ csrf_field() }}

    <input type="hidden" class="form_url" id="form_url" value="" name="">

    <div class="form-col">

        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-user"></i></span>            
                </div>
                <input name="full_name" class="form-control" type="text" placeholder="Full Name" >
            </div>        
        </div>


        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                </div>
                <input name="email" class="form-control email" type="email" placeholder="Email" required>
            </div>        
        </div>        

        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-phone"></i></span>
                </div>
                <input name="tel" class="form-control" type="tel" placeholder="Phone / Mobile" >
            </div>        
        </div>
        
        <div class="form-group">     
            <textarea name="message" class="form-control textarea message" placeholder="Message.."></textarea>
        </div>   

           <input type="hidden" value="" name="recaptcha_response" id="recaptchaResponse">

        {{-- <div style="text-align: center;font-size: 12px">
	    	This site is protected by reCAPTCHA and the Google
		    <a href="https://policies.google.com/privacy">Privacy Policy</a> and
		    <a href="https://policies.google.com/terms">Terms of Service</a> apply.
	    </div> --}}

    </div>

   
    <div class="submit_div" style="">
        <button type="submit" class="btn-almani sendenquiry" disabled="disabled">Submit</button>
    </div>


</form>


    
</div>

<br>

</section>
@endsection




