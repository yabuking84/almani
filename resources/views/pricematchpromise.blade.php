@extends('main-layout', ['pricematchpromise_menu' => 'active', 'company_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-pricematchpromise.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-pricematchpromise.js')}}"></script>
@endsection



@section('title')
Price Match Promise{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')

@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif

<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Price Match Promise</a>
    </h1>
</section>


<section class="section">

    <div class="container">
        <div class="intro_text">
            <h1 class="title">            
                High quality and low prices.. 
                <br>
                Is that possible?
            </h1>

            <p class="title_p" style="">
                At Almani Lighting, YES, it certainly is! Here’s why..
            </p>
        </div>
    </div>

    <br>

    <div class="container">

        <div class="img_text_boxes" id="bestledlightingprices">

            <div class="img_text_box">
                <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/pricematchpromise/guarantee.jpg')}});">
                </div>
                <div class="text_box">
                    <h1>GUARANTEES THE BEST LED LIGHTING PRICES IN UAE</h1>
                    <p>
                        You’ve found a cheaper offer? Just send us the competitor’s quotation. If the items are similar in specification and quality, we’ll match the price, guaranteed!
                    </p>
                </div>
            </div>

            <div class="img_text_box" id="controlledmanufacture">
                <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/pricematchpromise/controlled.jpg')}});">
                </div>
                <div class="text_box">
                    <h1>CONTROLLED MANUFACTURE</h1>
                    <p>
                        We carefully control the manufacture of all our products. Every single one is made to our high German standards and our efficient manufacturing agreements keep costs low.
                    </p>
                </div>
            </div>

            <div class="img_text_box" id="fromthemanufacturerdirecttoyou">
                <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/pricematchpromise/manufacturer_direct.jpg')}});">
                </div>
                <div class="text_box">
                    <h1>FROM THE MANUFACTURER (THAT’S US!) DIRECT TO YOU</h1>
                    <!-- <h1>FROM THE MANUFACTURER DIRECT TO YOU</h1> -->
                    <p>
                        There are no costly middle men in our supply chain. We work directly with you, avoiding extra layers of costs for distributors, agents or retailers. We also avoid the burden of many expensive showrooms. If you want to see our goods or project sites, simply get in touch!
                    </p>
                </div>
            </div>

            <div class="img_text_box" id="infinecompany">
                <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/pricematchpromise/fine_company.jpg')}});">
                </div>
                <div class="text_box">
                    <h1>IN FINE COMPANY</h1>
                    <p>
                        We manufacture in the same places as some of the world’s most celebrated LED brands, such as Philips and Osram. There’s no difference in our quality, but you’ll be amazed at the contrast in prices!
                    </p>
                </div>
            </div>

        </div>
    </div>



    <div class="container">    
        <div class="video_container" style="">
            <div class="video_intro_img" 
                    data-toggle="modal" 
                    data-target="#video_modal"
                    data-video_url="https://www.youtube.com/embed/MKSedNHx11k?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                <img src="{{App\Util::asset('assets/images/icons/youtube_play_icon.png')}}" alt="play">
                <p>
                    Price Match Promise
                </p>
            </div>

        </div>
    </div>




    <div class="bar_text">
        <p class="title_p" style="">
            That’s why you can be sure.. 
        </p>
        <h3 class="title">
            WE WON’T BE BEATEN ON PRICE!
        </h3>        
    </div>


    <div class="container" id="downloadables">    
             <div class="download-container">
            <div class="download-item">
                    <div class="img-container">
                        <a href="{{ asset('assets/pdfs/price-match-promise.pdf') }}" target="_blank">
                            <img class="animate flipInY animated" src="{{ App\Util::asset('assets/images/covers/price-match-promise-cover.png') }}" alt="Download Price Match Promise" class="download-img-center"
                                class="almani-catgalog">
                            <span class="dl-button"><button class="btn-almani">Download Flyer</button> </span>
                        </a>
                    </div>
            </div>
    </div>
        
</div>




</section>
@endsection




@section('modal')

            <!-- Modal -->
            <!-- //////////////////////////////////////////////////////// -->
            <div class="modal fade youtube_video_modal" id="video_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
                <div class="modal-content">          
                  <div class="modal-body">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <iframe class="video" style="" allowfullscreen></iframe>

                    <div style="margin-top: 20px;">
                        <button type="button" 
                            class="btn-almani change_language" 
                            data-target="#video_modal"
                            data-language="english"
                            data-video_url="https://www.youtube.com/embed/MKSedNHx11k?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                            English                    
                        </button>
                        <button type="button" 
                            class="btn-almani change_language" 
                            data-target="#video_modal"
                            data-language="arabic"
                            data-video_url="https://www.youtube.com/embed/IDCn4mwYpKs?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                            العَرَبِيَّة
                        </button>                
                    </div>

                  </div>
                </div>
              </div>
            </div>    
            <!-- //////////////////////////////////////////////////////// -->
            <!-- Modal -->
@endsection
