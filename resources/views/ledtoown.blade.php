@extends('main-layout', ['ledtoown_menu' => 'active','company_menu' => 'active'])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-ledtoown.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-ledtoown.js')}}"></script>
@endsection



@section('title')
LED to Own{{App\Meta::webpageTitle()}}
@endsection

@section('main-content')

@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif

<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">LED to Own</a>
    </h1>
</section>


<section class="section">

    <div class="container">

        <div class="intro_text" id="freeledlights">
            <h1 class="title">            
                Get your free LED lights today!
            </h1>
            <p class="title_p" style="">
                You already know that LED lights are great for cutting energy consumption and maintenance, 
                increasing longevity and delivering fantastic illumination and effects...
                So what is it that prevents people from embracing this amazing planet-friendly, 
                cost-saving technology? 
                <br><br>
                The answer is simple. Most people think that LED will cost them lots of money. 
                But they’d be wrong… So wrong, you can’t imagine. So let me help. 
                What if I tell you that changing your lighting setup to LED saves you up to 90% in 
                annual energy and service costs? Sounds interesting?
            </p>
        </div>
    </div>


    <div class="container">    
        <div class="video_container" style="">
            <div class="video_intro_img" 
                    data-toggle="modal" 
                    data-target="#video_modal"
                    data-video_url="https://www.youtube.com/embed/hn3IlyFUUUA?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                <img src="{{App\Util::asset('assets/images/icons/youtube_play_icon.png')}}" alt="play">
                <p>
                    LED to Own
                </p>
            </div>

        </div>
    </div>


    <div class="items_bg" style="background-image: url({{App\Util::asset('assets/images/ledtoown/items_bg1.jpg')}});">
        <div class="backdrop"></div>

        <div class="lightbar_text">
            <p class="title_p" style="">
                Well how would you like a state-of-the-art LED lighting solution:
            </p>
        </div>
        
        <div class="img_text_boxes" id="withoutanyinvestment">
            <div class="img_text_box">
                <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/ledtoown/investment.jpg')}});">
                </div>
                <div class="text_box">
                    <h1>WITHOUT ANY INVESTMENT</h1>
                    <p>
                        Discover how much you can save in maintenance and energy costs without investing a single dirham.
                    </p>
                </div>
            </div>

            <div class="img_text_box" id="withzerorisk">
                <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/ledtoown/risk.jpg')}});">
                </div>
                <div class="text_box">
                    <h1>WITH ZERO RISK</h1>
                    <p>
                        Save money and reduce your carbon footprint without any risk because our LED solution doesn’t cost you anything.
                    </p>
                </div>
            </div>

            <div class="img_text_box" id="withalongwarranty">
                <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/ledtoown/long.jpg')}});">
                </div>
                <div class="text_box">
                    <h1>WITH A LONG WARRANTY</h1>
                    <p>
                        We guarantee a hassle free time with our no-quibble 6-year warranty.
                    </p>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="middle_text">
            <p class="title_p" style="">
                I know what you’re thinking. This sounds too good to be true, right?
            </p>
            <h1 class="title">
                Well, it’s not.
            </h1>
        </div>
    </div>


    <div class="items_bg" style="background-image: url({{App\Util::asset('assets/images/ledtoown/items_bg2.jpg')}});   border-top: 1px solid gainsboro;">
        <div class="backdrop" style="background-color: rgba(255, 255, 255, 0.8);"></div>
        
        <div class="explain_steps">
            <p class="title_p" style="">
                Let me explain why our concept is a no-brainer for you in just THREE simple steps.
            </p>
        </div>
        <div class="img_text_boxes">

            <div class="img_text_box">
                <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/ledtoown/step1.jpg')}});">
                </div>
                <div class="text_box">
                    <h1>Step 1</h1>
                    <p>
                        Almani Lighting assesses your current lighting setup and potential energy savings.
                    </p>
                </div>
            </div>

            <div class="img_text_box">
                <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/ledtoown/step2.jpg')}});">
                </div>
                <div class="text_box">
                    <h1>Step 2</h1>
                    <p>
                        Once our design experts have custom designed your new system, 
                        we deliver the products on time and provide full technical advice for the installer. 
                        What’s more, your products will carry a full 6-year warranty.
                    </p>
                </div>
            </div>

            <div class="img_text_box">
                <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/ledtoown/step3.jpg')}});">
                </div>
                <div class="text_box">
                    <h1>Step 3</h1>
                    <p>
                        With at least 50% less energy consumption and no maintenance for more than 12 years, 
                        you start saving right away.
                    </p>
                </div>
            </div>
        </div>
    </div>



    <div class="container">
        <div class="intro_text">            
            <p class="title_p" style="">
                <br><br>
                So if we give you the lights for free and you start saving immediately without any prior investment, 
                how do we pay our bills? It’s actually beautifully simple.
                <br><br>
                Say you start saving 70% monthly on your current bills. 
                We’ll allocate a small portion of those savings each month to cover our costs. 
                This means that you enjoy all the many benefits of your brand new LED lighting for 
                less than you paid to run your old system. 
                <br><br>
                The reason we can do it is simply that LED is so much cheaper to run and maintain than traditional lighting. 
                Better still, after six years, 
                the entire installation and every saving your LED continues to make will be yours alone.
            </p>
        </div>
    </div>

    <div class="container">
        <div style="display: flex; flex-wrap: wrap; margin: 45px auto;">
            <div class="joinus_text">
                <h1 class="title" style=";">
                    <span>
                        Join our many Happy Customers and save Tens of Thoursands of Dirhams Every Year!
                    </span>
                </h1>
                <p class="title_p" style="">
                    Cut your costs and help protect our planet: change to LED today. 
                    No need to save up.
                    <br><br>
                    Call (<a href="tel:+97148873265" class="contact_details">+971 4-887-3265</a>) 
                    or 
                    email (<a href="mailto:info@almani.ae" class="contact_details">info@almani.ae</a>) 
                    us today and let's talk.
                </p>
            </div>

            <div class="contact_form">
                <form class="form_quote" method="post" action="{{route('sendcontactdetails')}}" enctype="multipart/form-data" style="justify-content: center;">
                {{ csrf_field() }}
                    <div class="form-col">

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-user"></i></span>            
                                </div>
                                <input name="full_name" class="form-control" type="text" placeholder="Full Name" >
                            </div>        
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                </div>
                                <input name="email" class="form-control email" type="email" placeholder="Email" required>
                            </div>        
                        </div>        

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                </div>
                                <input name="tel" class="form-control" type="tel" placeholder="Phone / Mobile" >
                            </div>        
                        </div>

                        <div class="form-group">     
                            <textarea name="message" class="form-control textarea message" placeholder="Message.." style="height: 110px;"></textarea>
                        </div>        

                    </div>


                    
                    
                    <div class="submit_div" style="">
                        <button type="submit" class="btn-almani">Submit</button>
                    </div>

                </form>            
            </div>
        </div>
    </div>

        <div class="spacer" style="border-top: 1px solid gainsboro"></div>

        <div class="container" id="downloadables">    
             <div class="download-container">
            <div class="download-item">
                    <div class="img-container">
                        <a href="{{ asset('assets/pdfs/led-to-own.pdf') }}" target="_blank">
                            <img class="animate flipInY animated" src="{{ App\Util::asset('assets/images/covers/ledtoown-cover.png') }}" alt="Download Price Match Promise" class="download-img-center"
                                class="almani-catgalog">
                            <span class="dl-button"><button class="btn-almani">Download Flyer</button> </span>
                        </a>
                    </div>
            </div>
    </div>
</div>


</section>
@endsection






@section('modal')

            <!-- Modal -->
            <!-- //////////////////////////////////////////////////////// -->
            <div class="modal fade youtube_video_modal" id="video_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
                <div class="modal-content">          
                  <div class="modal-body">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <iframe class="video" style="" allowfullscreen></iframe>

                    <div style="margin-top: 20px;">
                        <button type="button" 
                            class="btn-almani change_language" 
                            data-target="#video_modal"
                            data-language="english"
                            data-video_url="https://www.youtube.com/embed/hn3IlyFUUUA?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                            English                    
                        </button>
                        <button type="button" 
                            class="btn-almani change_language" 
                            data-target="#video_modal"
                            data-language="arabic"
                            data-video_url="https://www.youtube.com/embed/fnn0jp0wz1I?modestbranding=1&autoplay=1&showinfo=0&rel=0">
                            العَرَبِيَّة
                        </button>                
                    </div>

                  </div>
                </div>
              </div>
            </div>    
            <!-- //////////////////////////////////////////////////////// -->
            <!-- Modal -->
@endsection
