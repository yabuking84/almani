@extends('main-layout', [
    'servicepromise_menu' => 'active',
    'company_menu' => 'active',
])

@section('css-lib')
<link href="{{App\Util::asset('assets/css/master-service-promise.css')}}" rel="stylesheet">  
@endsection

@section('javascript-lib')
<script src="{{App\Util::asset('assets/js/master-service-promise.js')}}"></script>
@endsection



@section('title')
Service Promise{{App\Meta::webpageTitle()}}
@endsection



@section('main-content')


@if (session('error') !== null)
<div class="container alert_container">
    <?php $alert_type = (!session('error'))?"alert-success":"alert-danger"?>
    <div class="alert {{$alert_type}} alert-dismissible fade show" role="alert" style="text-align: center;">
        {{session('message')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif

<section class="section section_title" style="">
    <h1 class="title main_title" style="">
        <a class="button is-primary">Service Promise</a>
    </h1>
</section>


<section class="section">


<div class="container">
    <div class="intro_text">
        <p class="title_p" style="">
            Committed to delivering outstanding products and a superb customer experience, 
            we’re proud to grow Almani Lighting by earning our clients’ confidence and trust.
        </p>
        <p class="title_p" style="">
            Our LED lighting products are carefully selected for their excellent quality, design, durability, energy efficiency and value.             
            With Almani by your side, your absolute satisfaction is assured.
        </p>
    </div>
</div>

<div class="bar_text">
    <p>When you place your trust in us, here’s what you can expect in return</p>
</div>

<div class="container">

    <div class="img_text_boxes">
        <div class="img_text_box" id="exclusivewarrantyfor5years">
            <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/service-promise/warranty.jpg')}}); background-size: 100%;">
            </div>
            <div class="text_box">
                <h1>Exclusive warranty for 5 years</h1>
                <p>Our exceptional warranty is a rare find in the LED lighting world, but the fine quality of our products means we can confidently guarantee each one.</p>
            </div>
        </div>

        <div class="img_text_box" id="dependableservice">
            <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/service-promise/dependable-service.jpg')}});">
            </div>
            <div class="text_box">
                <h1>Dependable service</h1>
                <p>From your initial enquiry to after-sales care and support, we aim to deliver outstanding service that builds your assurance and trust.</p>
            </div>
        </div>

        <div class="img_text_box" id="simplespeedyreplacement">
            <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/service-promise/replacement.jpg')}});">
            </div>
            <div class="text_box">
                <h1>Simple, speedy replacement</h1>
                <p>When you need replacement units, we’re just a call away: your lights will be shining again within 24-48 hours.</p>
            </div>
        </div>

        <div class="img_text_box" id="systematicstorage">
            <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/service-promise/storage.jpg')}});">
            </div>
            <div class="text_box">
                <h1>Systematic storage</h1>
                <p>In case you ever need a replacement, we keep spare stocks of every item you purchase – all from the same production batch to ensure perfect consistency.</p>
            </div>
        </div>

        <div class="img_text_box" id="truetoourpromise">
            <div class="img_box" style="background-image: url({{App\Util::asset('assets/images/service-promise/true-promise.jpg')}});">
            </div>
            <div class="text_box">
                <h1>True to our promise</h1>
                <p>Our word is our bond. From product advice and design to installation service, working with Almani Lighting is a seamless, enjoyable and productive experience with exceptional results for you, time after time.</p>
            </div>
        </div>
    </div>

</div>





</section>
@endsection




