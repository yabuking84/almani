$(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 120;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "read more &nbsp;&nbsp;<i class='fa fa-angle-up'></i>";
    var lesstext = "show less &nbsp;&nbsp;<i class='fa fa-angle-down'></i>";

    
    // apply only when mobile
    if($(window).width() > 780) {
        var showChar = 320;  // How many characters are shown by default
    }

    $('.more').each(function() {
        var content = $(this).html();
        
        console.log(content.length+" -- "+(showChar+50));
        console.log("content[showChar] = "+content[showChar]);

        if(content.length > showChar+50) {


            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = 
            c + 
            '<span class="moreellipses">' + ellipsestext+ '</span>'+
            '<span class="morecontent">'+ h +'</span>'+
            '<div class="morelink_container">'+
                '<a class="morelink button is-success">' + moretext + '</a>'+
            '</div>';

            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        var $morelink = $(this);
        if($morelink.hasClass("less")) {
            console.log('less');
            $morelink.removeClass("less");
            $morelink.html(moretext);
            $morelink.closest('.more').find('.morecontent').fadeOut(function(){
                $morelink.closest('.more').find('.moreellipses').fadeIn();
            });
            
            // $morelink.closest('.more').find('.moreellipses').show();
            // $morelink.closest('.more').find('.morecontent').slideUp();
        } else {
            console.log('not less');
            console.log(lesstext);
            $morelink.addClass("less");
            $morelink.html(lesstext);
            $morelink.closest('.more').find('.moreellipses').fadeOut(function(){
                $morelink.closest('.more').find('.morecontent').fadeIn();
            });
            
            
            // $morelink.closest('.more').find('.moreellipses').hide();
            // $morelink.closest('.more').find('.morecontent').slideDown();
        }
        // $morelink.parent().prev().slideToggle();
        // $morelink.prev().slideToggle();

        return false;
    });


});