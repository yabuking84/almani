$(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 120;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "show more &nbsp;&nbsp;<i class='fa fa-angle-double-up'></i>";
    var lesstext = "show less &nbsp;&nbsp;<i class='fa fa-angle-double-down'></i>";

    console.log('readmore');
    
    // apply only when mobile
    if($(window).width() > 780) {
        var showChar = 320;  // How many characters are shown by default
    }

        $('.more').each(function() {
            var content = $(this).html();
     
            if(content.length > showChar) {
     
                var c = content.substr(0, showChar);
                var h = content.substr(showChar, content.length - showChar);
     
                var html = 
                c + 
                '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span>'+
                    '<span class="morecontent">'+
                    '<span>' + h + '</span>&nbsp;&nbsp;'+
                    '<div class="morelink_container">'+
                        '<a class="morelink button is-success">' + moretext + '</a>'+
                    '</div>'+
                '</span>';

                $(this).html(html);
            }
     
        });
     
        $(".morelink").click(function(){
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().slideToggle();
            $(this).prev().slideToggle();
            return false;
        });


});