$(document).ready(function(){

	$('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:30,
	    rewind: false,
		nav:true,
		dots:false,
		autoWidth:false,
		autoplay:true,
		rewind:false,
	    autoplayTimeout:2000,
	    autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:3,
			    margin:30,
            },
            600:{
                items:3,
			    margin:30,
	        },
	        1000:{
	            items:11,
	        }
	    }
	})


});

//
