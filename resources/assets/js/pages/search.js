$(document).ready(function () {


	$('.category_id').select2({
		placeholder: "Categories..",
		allowClear: true,
	});

	$('.product_id').select2({
		placeholder: "Products..",
		allowClear: true,
	});

	$('.application_id').select2({
		placeholder: "Application Areas..",
		allowClear: true,
	});

	$('.select2-selection--single').click(function () {
		$('.brand_options .all_brand').addClass('selected');
		$('.brand_options .camel_brand').removeClass('selected');
		$('.brand_options .almani_brand').removeClass('selected');
	});


	if ($(window).width() < 780) {

		$('body').on("click", ".prod_block .item", function (e) {
			e.preventDefault();
			$(this).find('.more_details').toggleClass('hide_group');
		});

		$('body').on("click", ".prod_block .item button.goto_product", function () {
			var href = $(this).closest('a.item').attr('href');
			window.location.href = href;
		});

		$('body').click(function (e) {
			if (!$(e.target).closest('.item').attr('class')) {
				$('.prod_block .item .more_details').addClass('hide_group');
			}
		});

	} else {

		$("body").on("mouseenter", ".prod_block .item", function () {
			$(this).find(".more_details").stop().removeClass("hide_group");
		});

		$("body").on("mouseleave", ".prod_block .item", function (e) {
			$(this).find(".more_details").stop().addClass("hide_group");
		});

	}



	$('.submit_search').click(function () {
		$('.submit_search i').show().css('display', 'inline-block');
		var href = $('.search_tab .href').val();
		var data = getResults(href, getSearchData());
	});

	// $('.brand_options .option').click(function () {
	// 	$(this).toggleClass('selected');
	// });

	$('.options .option').click(function () {
		$(this).toggleClass('selected');
	});

	$('.open_search_tab').click(function () {
		$('.search_tab').toggleClass('show_tab');
		$('.search_tab .category_name').focus();

		if ($('.products').find('.alert-container').length > 0) {
			$('.products').slideUp(function () {
				$('.products').empty();
			});
		}
	});

	$('.close_search_tab').click(function () {
		$('.search_tab').removeClass('show_tab');
	});

	$('.search_tab input').keypress(function (ev) {
		var keycode = (ev.keyCode ? ev.keyCode : ev.which);
		if (keycode == '13') {
			$('.search_tab .submit_search').click();
		}
	})

	$('.pagination_nav').on('click', '.pagination-next:not(disabled), .pagination-previous:not(disabled)', function () {

		// console.log('click');
		var href = $(this).attr('data-href');
		var search = getSearchData();
		getResults(href, search);

	});

	$('.pagination_nav').on('click', '.pagination-link:not(.is-current)', function () {

		// console.log('click');
		var href = $(this).attr('data-href');
		var search = getSearchData();
		getResults(href, search);
	});


	// check if seach_nav_fld then submit
	//////////////////////////////////////////////////////
	if (search_fld) {
		// $('.product_name').val(search_fld);
		// $('.search_tab .submit_search').click();

		if ($('.product_id').find("option[value='" + search_fld.toLowerCase() + "']").length) {
			$('.product_id').val(search_fld.toLowerCase()).trigger('change');
			$('.brand_options .all_brand').addClass('selected');
			$('.brand_options .camel_brand').removeClass('selected');
			$('.brand_options .almani_brand').removeClass('selected');
			$('.search_tab .submit_search').click();
		} else {
			$('.search_tab').removeClass('show_tab');
			// $('.products').html('<h5 class="none_found">No products found.. Please try again..</h5>');
			var error = '<div class="alert-container col-sm-4 col-md-4">';
				error += '<div class="alert-message alert-message-danger">';
				error += '<h4>No Products Found.. </h4>';
				error += '<p>Please Try Again!</p>';
				error += '</div></div>';


			$('.products').html(error);
		}
		cnsl(search_fld.toLowerCase());
	}
	//////////////////////////////////////////////////////
	// check if seach_nav_fld then submit

	// clear fields reason is when one
	$('.select2-selection--multiple').on('click', function () {
		$(".product_id").val(null).trigger('change');
		$('.model_code').val('');
	});

	$('.select2-selection--single').on('click', function () {
		$(".category_id").val([]).trigger('change');
		$(".application_id").val([]).trigger('change');
		$('.model_code').val('');
	});

	$('.model_code').click(function () {
		$(".category_id").val([]).trigger('change');
		$(".application_id").val([]).trigger('change');
		$(".product_id").val(null).trigger('change');
	});

	$('.model_code').focus(function () {
		$(".category_id").val([]).trigger('change');
		$(".application_id").val([]).trigger('change');
		$(".product_id").val(null).trigger('change');
	});


	/////////////////// start from catalog searching ////////////////////////

	if (frcatalog) {

		$('.category_name').val(category_name);
		// $('.product_name').val(product_name);

		// var $product_select = $(".product_id").select2();
		// $product_select.val(product_name).trigger("change");

		$('.model_code').val(model_code);

		$('.cct_min').val(cct_min);
		$('.cct_max').val(cct_max);

		$('.lumen_min').val(lumen_min);
		$('.lumen_max').val(lumen_max);

		$('.power_min').val(power_min);
		$('.power_max').val(power_max);

		(dimm === '0') ? $('.dim_options .dimm').addClass('selected'): '';
		(triac === '0') ? $('.dim_options .triac').addClass('selected'): '';
		(v0_10 === '0') ? $('.dim_options .v0_10').addClass('selected'): '';
		(dali === '0') ? $('.dim_options .DALI').addClass('selected'): '';

		(almani === '1') ? $('.brand_options .almani_brand').addClass('selected'): '';
		(camel === '1') ? $('.brand_options .camel_brand').addClass('selected'): '';

		(emergency === '0') ? $('.emergency').addClass('selected'): '';
		(wifi === '0') ? $('.wifi').addClass('selected'): '';

		$('.ip_rating_min').val(ip_rating_min);
		$('.ip_rating_max').val(ip_rating_min);

		$('.price_min').val(price_min)
		$('.price_max').val(price_max);
		$('.currency').val(currency);

		$('.search_tab .submit_search').click();

	}


	/////////////////// end from catalog searching ////////////////////////


	// when body is clicked
	// bodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclick
	$('body').click(function (e) {
		if (!$(e.target).closest('.open_search_tab').length &&
			!$(e.target).closest('.search_tab').length &&
			!$(e.target).closest('.select2-selection__choice__remove').length) {
			$('.search_tab').removeClass('show_tab');
		}
	});
	// bodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclick
	// when body is clicked



	// if mobile reduce number of pages
	////////////////////////////////////////////////////////////////////////////////
	if ($(window).width() < 780) {
		$('.pagination_nav .element:gt(4)').css('color', 'red');
	}
	////////////////////////////////////////////////////////////////////////////////
	// if mobile reduce number of pages




	///////////////////////
	// Options as Radio //
	////////////////////////////////////////////////////////////////////////////////////////////

	$('.search_tab  .option_group_container .option_group label.button input').on('click', function (e) {

		// $(this).closest('.option_group').find('label.button:not(.isSelected)').removeClass('isSelected');
		$(this).closest('label.button').siblings('label.button').removeClass('isSelected');

		$(this).closest('label.button').toggleClass('isSelected');
	});

	////////////////////////////////////////////////////////////////////////////////////////////
	// Options as Radio //
	/////////////////////





});
// docreadydocreadydocreadydocreadydocreadydocreadydocreadydocready
// docreadydocreadydocreadydocreadydocreadydocreadydocreadydocready
// Document Ready



























































function getSearchData() {
	var search = {};

	$('.search_tab').find('.brand_group label.isSelected').each(function (i) {
		search.brand_name = $.trim($(this).text());
	});

	search.brand = {};
	search.brand.almani = ($('.search_tab .brand_options .almani_brand').hasClass('selected') ? '1' : '0');
	search.brand.camel = ($('.search_tab .brand_options .camel_brand').hasClass('selected') ? '1' : '0');
	search.brand.all = ($('.search_tab .brand_options .all_brand').hasClass('selected') ? '1' : '0');

	// we comment this for adding a feature of select multiple
	// search.category_name = $('.search_tab .category_name').val();
	// search.product_name = $('.search_tab .product_name').val();

	search.product = {};
	search.product.category_id = $('.category_id').val();
	search.product.product_id = $('.product_id').val();
	search.product.application_area = $('.application_id').val();


	search.model_code = $('.search_tab .model_code').val();
	search.supplier_code = $('.search_tab .supplier_code').val();

	search.cct = {};
	search.cct.min = $('.search_tab .cct_min').val();
	search.cct.max = $('.search_tab .cct_max').val();

	search.lumen = {};
	search.lumen.min = $('.search_tab .lumen_min').val();
	search.lumen.max = $('.search_tab .lumen_max').val();

	search.power = {};
	search.power.min = $('.search_tab .power_min').val();
	search.power.max = $('.search_tab .power_max').val();

	search.dim_options = {};
	// search.dim_options.dimm = ($('.search_tab .dim_options .dimm.selected').data('value')) ? 1 : 0;
	// search.dim_options.triac = ($('.search_tab .dim_options .triac.selected').data('value')) ? 1 : 0;
	// search.dim_options.v0_10 = ($('.search_tab .dim_options .v0_10.selected').data('value')) ? 1 : 0;
	// search.dim_options.dali = ($('.search_tab .dim_options .dali.selected').data('value')) ? 1 : 0;

	search.dim_options.dimm = ($('.search_tab .dim_options .dimm').hasClass('selected')) ? '1' : '0';
	search.dim_options.triac = ($('.search_tab .dim_options .triac').hasClass('selected')) ? '1' : '0';
	search.dim_options.v0_10 = ($('.search_tab .dim_options .v0_10').hasClass('selected')) ? '1' : '0';
	search.dim_options.dali = ($('.search_tab .dim_options .dali').hasClass('selected')) ? '1' : '0';




	search.warranty = {};
	search.warranty.year_1 = 0;
	search.warranty.year_2 = 0;
	search.warranty.year_3 = 0;
	search.warranty.year_4 = 0;
	search.warranty.year_5 = 1;


	// search.emergency = ($('.search_tab .emergency.selected').data('value')) ? 1 : 0;
	// search.wifi = ($('.search_tab .wifi.selected').data('value')) ? 1 : 0;

	search.emergency = ($('.search_tab .emergency').hasClass('selected')) ? '1' : '0';
	search.wifi = ($('.search_tab .wifi').hasClass('selected')) ? '1' : '0';



	search.ip_rating = {};
	search.ip_rating.min = $('.search_tab .ip_rating_min').val();
	search.ip_rating.max = $('.search_tab .ip_rating_max').val();

	search.price = {};
	search.price.min = $('.search_tab .price_min').val();
	search.price.max = $('.search_tab .price_max').val();
	search.price.currency = $('.search_tab .currency').val();

	cnsl(JSON.stringify(search, '', 2));
	return search;
}

function getResults(href, search) {

	var csrf_token = $('meta[name="csrf-token"]').attr('content');

	// clear results section
	// scrollToTopResults();
	// test run ..
	$('.products').slideUp(function () {
		// $('.products').empty();
	});

	$('.pagination_nav').empty();

	$(".loading_results").fadeIn(500);

	// cnsl("href = "+href);
	// scrollToTopResults();
	$.post(
			href, {
				'_token': csrf_token,
				'search': JSON.stringify(search)
			},
			function (data) {

				data = JSON.parse(data);

				// cnsl("sss1 ");
				// cnsl(search);
				// cnsl("sss1 ");
				// cnsl("xxx1");
				cnsl("data search results",data);
				// cnsl("xxx1");

				$(".loading_results").fadeOut();
				populateSearchColumn(data);
				$('.search_tab').removeClass('show_tab');
				$('.submit_search i').hide();
				$('.products').slideDown();

				scrollTo('.products', -350);

				if ($(window).width() < 780) {
					// $('.pagination_nav .element:gt(3)').css('color','red');
					$('.pagination_nav .element:gt(2)').addClass('hide_group');
					$('.pagination_nav .element:gt(-2)').removeClass('hide_group');
				}


			})
		.fail(function (e) {
			cnsl(e.responseText);
		});
}





function populateSearchColumn(data) {

	var paginate = data.paginate;
	var links = data.links;
	var currency = data.currency;
	var rate = data.rate;

	// console.log(paginate.data);

	var model_html = '';
	cnsl(paginate.data);
	cnsl(paginate.data.length);

	//clear the html when it reach here
	$('.products').empty();

	if (paginate.data.length > 0) {
		// if (paginate.data.total > 0) {

		$('.products').empty();
		$('.pagination_nav').html(links);
		setTimeout(function () {

			$.each(paginate.data, function (key, product) {

				if (product.camel == '1') {
					a_href = $camel_url + '/catalog/' + product.category.category_alias + '/' + product.product_code;
					imgBrand = $super_base2 + '/assets/images/search/camel-logo-colored-xm.png';
				} else {
					imgBrand = $super_base2 + '/assets/images/search/almani-logo-colored-xm.png';

					a_href = $super_base2 + 'catalog/' + product.category.category_alias + '/' + product.product_code;
				}

				var product_img = product.image_eager;
				var product_name = product.product_name;
				var product_code = product.product_code;
				var category_name = product.category.category_name;
				var product_id = product.product_id;
				var product_active = product.product_active;


				lifespan = product.lifespan_eager;
				// power = model.product.power_eager;
				power = (product.power_eager && product.power_eager != "-") ? product.power_eager : 'N/A';
				// kelvin = model.product.kelvin_eager;
				kelvin = product.kelvin_eager;
				// lumens = model.product.lumens_eager;
				lumens = product.product_lumen;

				ip_rating = product.product_ip;


				// Price
				////////////////////////////////////////////////////////
				minPrice = parseFloat(product.minPrice_eager) * parseFloat(rate);
				minPrice = minPrice.toFixed(2);
				minPriceText = product.minPriceText_eager;
				price_text = "";
				if (minPriceText == "AVAILABLE UPON REQUEST") {
					price_text = "AVAILABLE UPON REQUEST ";
				} else {
					price_text = '<div class="price_container" data-price_value_uae="' + minPrice + '" style="display: inline-block;">' + minPrice + ' ' + currency + '</div>';
				}
				////////////////////////////////////////////////////////


				var $product = $('#html-templates > .product > div').clone();

				$product.find(".item").attr('href', a_href);
				$product.find(".img_container img").attr('src', product_img);
				$product.find(".prod_title .prod_ip").html('IP' + ip_rating);
				$product.find(".prod_title .prod_name").html(product_name);
				$product.find(".prod_title .prod_code").html(product_code);
				$product.find('.image-brand-identifier').attr('src', imgBrand);

				$product.find('.specs #lifespan').html(lifespan);
				$product.find('.specs #power').html(power);
				$product.find('.specs #wattage').html(kelvin);
				$product.find('.specs #lumens').html(lumens);

				$product.find(".prod_title .prod_price_grp").html(price_text);
				

				var prod_active_text = '&nbsp;';
				if(!product_active)
				prod_active_text = '<span style="color:red;font-weight:bold;">ITEM UNAVAILABLE</span>';

				$product.find(".prod_title .prod_active").html(prod_active_text);


				$('.products').append($product);

			});
		}, 100);


	} else {
		// $('.products').html('<h5 class="none_found">No products found.. Please try again..</h5>');

		var error = '<div class="alert-container col-sm-4 col-md-4">';
			error += '<div class="alert-message alert-message-danger">';
			error += '<h4>No Products Found.. </h4>';
			error += '<p>Please Try Again!</p>';
			error += '</div></div>';
		

		$('.products').html(error);
	}



}




function scrollTo(element, addY) {

	if (addY)
		addY = parseInt(addY);
	else
		addY = 0

	var position = $(element).offset().top + addY;

	$('html, body').animate({
		scrollTop: position
	}, 1000);

}