$(document).ready(function(){

	$(".file_quote").on('change',function(){
		var filename = $(this).val().replace(/.*(\/|\\)/, '');
		if (filename){
			$(this).next('label').html(filename);
			console.log(filename);
		}
	});
	$('.clear-file-input').click(function(){

		var $this = $(this).closest('.form-row').find('.file_quote');
		$this.value = "";
		$(this).closest('.form-row').find('.custom-file-label').html('Choose file..');
		
	});

	$('.submit_quote').on('click',function() {
		$(this).addClass('is-loading');  		
	});

	$('.select_show_others').on('change',function() {
		var val = $(this).val();
		console.log('val = '+val);
		console.log('showfield = '+'.'+$(this).attr('data-showfield'));
		if(val=="Others")
		$('.'+$(this).attr('data-showfield')).show("fast");
		else
		$('.'+$(this).attr('data-showfield')).hide("fast");
	});

    $('form').submit(function(){
        $(this).find('button[type=submit]').html('<i class="fas fa-circle-notch fa-spin"></i>');
    });


});



