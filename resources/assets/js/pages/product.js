$(document).ready(function(){

     setCookie('is_product_page','yes');

    // Easy zoom
    /////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    // If Mobile
    //////////////////////////////////////////
    // Disable easyzoom when mobile
    if(getDevice()!='desktop') {

        var $easyzoom = $('.easyzoom').easyZoom({
            loadingNotice:"<i class='fas fa-spinner fa-spin '></i>",
            errorNotice:'Image not loaded..',
            onShow: function(e){
                cnsl('onShow');
                $(this)[0].teardown();
            },
            onHide: function(){
                cnsl('onHide');
            },
            onMove: function(){
                cnsl('onMove');
            },
        });
        
        var $api_thumbnails = $easyzoom.filter('.easyzoom-thumbnails').data('easyZoom');

        // Stop EasyZoom
        $api_thumbnails.teardown();
        // Start EasyZoom
        // $api_thumbnails._init();

        // Disable href on mainImage and stop easyzoom
        $('.mainImage > div > a > img').click(function(e){
            e.preventDefault();
            e.stopPropagation();
        });


        $('.thumbnails').on('click', 'a', function(e) {
            var $this = $(this);
            e.preventDefault();

            // start easyzoom for the loading feature when swapping images
            $api_thumbnails._init();

            // Use EasyZoom's `swap` method
            $api_thumbnails.swap($this.data('standard'), $this.attr('href'));

            // To start the loading
            $api_thumbnails.show(false,true);

            $('.gallery .thumbnails li').removeClass('selected');
            $(this).closest('li').addClass('selected');

        });


    } 
    //////////////////////////////////////////
    // If Mobile

    // if Desktop
    //////////////////////////////////////////
    else {

        var $easyzoom = $('.easyzoom').easyZoom({
            loadingNotice:"<i class='fas fa-spinner fa-spin '></i>",
            errorNotice:'Image not loaded..',
        });
        var $api_thumbnails = $easyzoom.filter('.easyzoom-thumbnails').data('easyZoom');


        $('.thumbnails').on('click', 'a', function(e) {
            var $this = $(this);
            e.preventDefault();
            
            // Use EasyZoom's `swap` method
            $api_thumbnails.swap($this.data('standard'), $this.attr('href'));

            // To start the loading
            $api_thumbnails.show(false,true);

            $('.gallery .thumbnails li').removeClass('selected');
            $(this).closest('li').addClass('selected');

        });
        // To start the loading
        $api_thumbnails.show(false,true);

    }
    //////////////////////////////////////////
    // if Desktop

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////
    // Easy zoom


    
    // changing currency in similar products 
    $('#similar_container .smlar-container .prod_block .item .prod_title .price_container').each(function(){
        var total = 0;
        var price = 0;

        var price = parseFloat($(this).attr('data-value')); 
        total = total+price;
        
        //// currency ////////////////////////////////////////////
        total = total*$glbl_currency['EUR'];
        //////////////////////////////////////////////////////////
    
       total = total.toFixed(2);

       $(this).html(total + ' EUR');
    });

    


 // more details page of the similar products
 if($(window).width() < 780) {

     $('.prod_block .item').click(function(e){
         e.preventDefault();
         // $('.prod_block .item .more_details').addClass('hide_group');
         $(this).find('.more_details').toggleClass('hide_group');
     });

     $('.prod_block .item button.goto_product').click(function(e){
         var href = $(this).closest('a.item').attr('href');
         window.location.href = href;
     });

     $('body').click(function(e){
         // cnsl($(e.target).closest('.item').attr('class'));
         if(!$(e.target).closest('.item').attr('class')) {
             $('.prod_block .item .more_details').addClass('hide_group');
         }
     });

 } else {

     $('.prod_block .item').mouseenter(function(){
         $(this).find('.more_details').stop().removeClass('hide_group');
     }).mouseleave(function(){
         $(this).find('.more_details').stop().addClass('hide_group');
     });

 }
 
 // more details page of the similar products


   // similar product
   $('.smlar-container').owlCarousel({
    loop:true,
    margin:5,
    stagePadding: 50,
    nav:false,
    dots:false,
    rewind: true,
    mouseDrag: true,
    touchDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
        },
        750:{
            items:1,
        },
        1000:{
            items:5,
            margin:2
        }
    }
});


    $('.owl-carousel-js').owlCarousel({
        loop:false,
        margin:20,
        nav:true,
        dots:true,
        rewind: false,
        mouseDrag: false,
        touchDrag: false,
        responsiveClass:true,
        responsive:{
            0:{
                items:3,
            },
            750:{
                items:5,
            },
            1000:{
                items:5
            }
        }
    })



	$('.owl-carousel-appareas').owlCarousel({
	    loop:false,
	    margin:20,
	    nav:true,
        dots:true,
        rewind: false,
        mouseDrag: false,
        touchDrag: false,
        responsiveClass:true,
        responsive:{
            0:{
                items:3,
            },
            750:{
                items:5,
	        },
	        1000:{
	            items:5
	        }
	    }
	})



	///////////////////////
    // Options as Radio //
    ////////////////////////////////////////////////////////////////////////////////////////////

    $('.option_group_container .option_group label.button input').on('click',function(e){
    	
    	// $(this).closest('.option_group').find('label.button:not(.isSelected)').removeClass('isSelected');
    	$(this).closest('label.button').siblings('label.button').removeClass('isSelected');

    	$(this).closest('label.button').toggleClass('isSelected');
    });

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Options as Radio //
	/////////////////////






    // Scroll to model on page if model exist
    /////////////////////////////////////////////////////////////////////////////
    var href = window.location.href;
    var href_arr = href.split('/')
    var model_code = href_arr[href_arr.length-1];
    var is_model = (href_arr[href_arr.length-2]=="model" || get_model_code)?true:false;
    var mcont = "tr";

    model_code = (get_model_code)?get_model_code:model_code;

    // cnsl('is_model = '+is_model);
    // cnsl('model_code = '+model_code);
    // cnsl('get_model_code = '+get_model_code);
    // cnsl($(mcont+'.'+model_code).offset());

    if(is_model && $(mcont+'.'+model_code).offset()) {
        // cnsl($super_base2);
        // cnsl(href_arr);
        // cnsl(href_arr.length);
        // cnsl(model_code);

        var model_position = 0;

        model_position = $(mcont+'.'+model_code).offset().top - 90;

        $(mcont+'.'+model_code).addClass('highlight_from_url');

        // $(mcont+':not(:hidden) .'+model_code+'').css('background-color','red');

        // cnsl('xxxx '+model_code);
        // cnsl('.mobile-table-value .'+model_code+'');
        // cnsl('mcont = '+mcont+' .'+model_code+'');
        // cnsl('model_position = '+model_position);

        $('html, body').animate({
            scrollTop: model_position
        }, 1500);
    }

    /////////////////////////////////////////////////////////////////////////////
    // Scroll to model on page







    $('.tab_links li').click(function(){
        $('.tab_links li').removeClass('active');
        $(this).addClass('active');

        var tab = $(this).attr('data-tab');
        $('.tabs .tab').removeClass('active');
        $('.tabs .'+tab).addClass('active');

    });








    // $('.show_modal').on('click',function(){
    //     var modal_id = $(this).attr('data-modal_id');

        
    //     $('.modal#'+modal_id).addClass('is-active');
    //     $('.modal#'+modal_id).css({
    //         opacity: 0, 
    //         // display: 'flex',
    //     }).animate({
    //             opacity: 1,
    //     }, 500);        

    // });








    $('.zoom-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true,
            titleSrc: function(item) {
                return item.el.attr('title');
            }
        },
        gallery: {
            enabled: false
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function(element) {
                return element.find('img');
            }
        }
    });





    // add loading animation when form submit on modal
    $('.modal form').submit(function(){
        $(this).find('button[type=submit]').html('<i class="fas fa-circle-notch fa-spin"></i>');
    });








    // enquire
    ////////////////////////////////////////////////////////////////////
    $('.model_action_button.enquire').on('click',function(){ 
        
        // check if tableMobilizer is shown
        var $tr = "";
        if($(this).closest('table').is('.mobile-table'))
        $tr = $(this).closest('table');
        else
        $tr = $(this).closest('tr');



        var model_name = $.trim($tr.find('.model_name').attr('data-model_name'));
        var model_details = $.trim($tr.find('.model_details').attr('data-model_details'));

        // var model_price = $.trim($tr.find('.best_price .price_value').text());
        // model_price+= " "+$.trim($tr.find('.best_price .currency').text());

        var model_options_dim = "";
        $tr.find('.dim_group label.isSelected').each(function(i){
            model_options_dim+= $.trim($(this).text());
        });

        var model_options_ips = "";
        $tr.find('.ip_group label.isSelected').each(function(i){
            model_options_ips+= $.trim($(this).text());
        });

        var model_options_ccts = "";
        $tr.find('.cct_group label.isSelected').each(function(i){
            model_options_ccts+= $.trim($(this).text());
        });

        var model_options_wifi = "";
        $tr.find('.wifi_group label.isSelected').each(function(i){
            model_options_wifi+= $.trim($(this).text());
        });

        var model_options_beam_angles = "";
        $tr.find('.beam_angle_group label.isSelected').each(function(i){
            model_options_beam_angles+= $.trim($(this).text());
        });

        var model_options_warranty = "";
        $tr.find('.warranty_group label.isSelected').each(function(i){
            model_options_warranty+= $.trim($(this).text());
        });

        var model_in_stock = $.trim($tr.find('.in_stock').text());
        

        var message = 
        "Hi, I would like to enquire about "+model_name+
        " and would appreciate a call back as soon as possible.";

        if(model_options_dim!="" || model_options_ips!="" || model_options_wifi!="") {
            message+= "\n\nProduct should have the following options:"; 
        }

        if(model_options_dim!="")
        message+= "\nDim: "+model_options_dim.replace(/\s+/g, " ").replace(/^\s|\s$/g, "");
        if(model_options_ips!="")
        message+= "\n"+model_options_ips.replace(/\s+/g, " ").replace(/^\s|\s$/g, "");;
        if(model_options_ccts!="")
        message+= "\nCCT: "+model_options_ccts.replace(/\s+/g, " ").replace(/^\s|\s$/g, "");+"K";
        if(model_options_beam_angles!="")
        message+= "\nBeam Angle: "+model_options_beam_angles.replace(/\s+/g, " ").replace(/^\s|\s$/g, "");;
        if(model_options_warranty!="")
        message+= "\nWarranty: "+model_options_warranty.replace(/\s+/g, " ").replace(/^\s|\s$/g, "");;
        if(model_options_wifi!="")
        message+= "\nWifi Enabled";

        message+= "\n\n";

        var lines = message.split("\n");  


        // cnsl("lines "+lines.length+"px");
        // cnsl("lines* "+lines.length*30+"px");
        $('#enquire_modal textarea').css('height',(lines.length*30+30)+"px");
        // var new_message = message.replace(/\s+/g, " ").replace(/^\s|\s$/g, "");
        // cnsl(new_message);

        // cnsl("model_code = "+model_code);
        $('#enquire_modal textarea').val(message);
        
        
        var model_code = $.trim($tr.find('.model_details').attr('data-model_code'));
        $('#enquire_modal input[name="model_code"]').val(model_code);

    });
    ////////////////////////////////////////////////////////////////////
    // enquire 






    // Datasheet
    ////////////////////////////////////////////////////////////////////
    $('.model_action_button.request_datasheet').on('click',function(){
        // check if tableMobilizer is shown
        var $tr = "";
        if($(this).closest('table').is('.mobile-table'))
        $tr = $(this).closest('table');
        else
        $tr = $(this).closest('tr');

        var message = $(this).attr('data-message');

        var model_code = $.trim($tr.find('.model_details').attr('data-model_code'));
        $('#datasheet_modal input[name="model_code"]').val(model_code);

        $('#datasheet_modal textarea').val(message);
    });
    ////////////////////////////////////////////////////////////////////
    // Datasheet






    // Technical Files
    ////////////////////////////////////////////////////////////////////
    $('.model_action_button.request_technicalfiles').on('click',function(){
        // check if tableMobilizer is shown
        var $tr = "";
        if($(this).closest('table').is('.mobile-table'))
        $tr = $(this).closest('table');
        else
        $tr = $(this).closest('tr');
                

        var message = $(this).attr('data-message');

        var model_code = $.trim($tr.find('.model_details').attr('data-model_code'));
        $('#technical_file_modal input[name="model_code"]').val(model_code);

        $('#technical_file_modal textarea').val(message);


    });
    ////////////////////////////////////////////////////////////////////
    // Technical Files



    // Share via Email
    ////////////////////////////////////////////////////////////////////
    $('.model_action_button.share_via_email_btn').on('click',function(){ 

        var message = $(this).attr('data-message');
        $('#share_via_email_modal textarea').val(message);

    });
    ////////////////////////////////////////////////////////////////////
    // Share via Email









    // Custom Contact Us for Product Page
    ////////////////////////////////////////////////////////////////////
    $('.icons.contact_us, .contact_us_enquire').on('click',function(){                

        var message = $(this).attr('data-message');
        $('#contact_us_modal textarea').val(message);

    });
    ////////////////////////////////////////////////////////////////////
    // Custom Contact Us for Product Page

















    // Higlight when clicked
    //////////////////////////////////////////////////////////////////
    $('.table tr:not(.model_extra_tr)').click(function(e){

        // cnsl($(e.target).closest('.btn').attr('class'));

        if(!$(e.target).closest('.btn').hasClass('btn') && !$(e.target).closest('.zoom-gallery').hasClass('zoom-gallery')) {
            $(this).toggleClass('highlight');
            $(this).next('.model_extra_tr').toggleClass('highlight');
        }
    });

    $('.table tr.model_extra_tr').click(function(){
            $(this).toggleClass('highlight');
            $(this).prev('tr').toggleClass('highlight');
    });


    // Applicable only for desktop
    // if($(window).width() > 780) {
    if(getDevice()!='desktop') {
        // hover
        $('.table tr:not(.model_extra_tr)').hover(function(e){

            $(this).toggleClass('hover');
            $(this).next('.model_extra_tr').toggleClass('hover');
        });

        $('.table tr.model_extra_tr').hover(function(){
                $(this).toggleClass('hover');
                $(this).prev('tr').toggleClass('hover');
        });
    }

    //////////////////////////////////////////////////////////////////
    // Higlight when clicked









    // Show model's extra details
    $('table').on('click','.show_model_extra',function(e){
        $(this).closest('tr').next('tr').stop().slideToggle();
        $(this).find('i').toggleClass('hide_group');

    });











    $('.send_message').on('click',function(){
        var message = 'Hi, I would like to enquire about '+product_name+' ('+product_code+') and would appreciate a call back as soon as possible.';
        $('#contact_us_modal textarea').val(message);
    });


    



















    // currency
    /////////////////////////////////////////////////////////////////////////////////
    $('#currency').change(function(){
        var currency = $(this).val();
        setCookie('almani_currency',currency);

        // Convert all prices
        $('.models .table tr .price_container .price_container_show').each(function(){
            changePrice($(this).closest('tr'));
        });

    });

    var curr = getCookie("almani_currency");
    curr = (!curr)?"AED":curr;

    $('#currency').val(curr);
    $('#currency').trigger('change');
    /////////////////////////////////////////////////////////////////////////////////
    // currency








    // Change price
    /////////////////////////////////////////////////////////////////////////////////
    $('.option_group.change_price .button input').click(function(){
        changePrice($(this).closest('tr'));
    });

    function changePrice($tr){

        var $selected = $tr.find('.option_group.change_price .isSelected');
        
        // cnsl('change_price');
        var total = 0;
        var price = 0;
        $selected.each(function(e){
            price = parseFloat($(this).find('input').attr('data-uae_price'));
            total = total+price;
        });
        
        var def_uae_price =  parseFloat($tr.find('.price_container').attr('data-def_uae_price'));

        // warranty_group
        ////////////////////////////////////
        ////////////////////////////////////
        var wty_dflt = $tr.find('.option_group.warranty_group .default input').val();
        wty_dflt = (wty_dflt)?wty_dflt:0;

        // if no selected wty then select default wty
        var wty_slctd = $tr.find('.option_group.warranty_group .isSelected input').val();
        

        wty_slctd = (wty_slctd)?wty_slctd:0;
        if(wty_slctd==0) {
            $tr.find('.option_group.warranty_group .default').addClass('isSelected');
            wty_slctd = wty_dflt;
        }
        var wty_multi = wtyMulti(wty_dflt)[wty_slctd+'_yrs'];

        // cnsl(wty_slctd);
        // cnsl(wty_dflt);
        // cnsl(wtyMulti(wty_dflt));
        // cnsl(wty_multi);
        ////////////////////////////////////
        ////////////////////////////////////
        // warranty_group
    

        total = (total+def_uae_price)*wty_multi;
        
        // currency
        //////////////////////////////////////////////////////////
        total = total*$glbl_currency[getCookie("almani_currency")];
        //////////////////////////////////////////////////////////

        total = total.toFixed(2);
        // cnsl("total === "+(total));

        $tr.find('.price_container_show .price_value').attr('data-value',total).html(total);
        $tr.find('.price_container_show .currency').html(getCookie("almani_currency"));    
    }
    /////////////////////////////////////////////////////////////////////////////////
    // Change price
     





    // Change base price
    /////////////////////////////////////////////////////////////////////////////////
    $('.option_group.change_base_price .button input').click(function(){
        var $tr = $(this).closest('tr');
        var new_price = $(this).attr('data-uae_price');
        $tr.find('.price_container').attr('data-def_uae_price',new_price);

        changePrice($tr);
    });
    /////////////////////////////////////////////////////////////////////////////////
    // Change base price
     




    // Change warranty
    /////////////////////////////////////////////////////////////////////////////////
    $('.option_group.change_warranty .button input').click(function(){
        var $tr = $(this).closest('tr');
        changePrice($tr);
    });
    /////////////////////////////////////////////////////////////////////////////////
    // Change warranty
     

     $('#get_modal_datasheet_btn').click(function(){  
         $('#get_datasheet_modal form').attr('action',  $(this).attr('data-href'));
     });

     $('#get_download_datasheet').click(function() {
        $('.get_download_datasheet').html('Get Datasheet');
        $('#get_datasheet_modal get_download_sheet').submit();
        $('#get_datasheet_modal').modal('hide');
     });


    // if($(window).width() < 780) {

    //     $('body').click(function(e){
    //         cnsl(e.target);
    //     });
    // }



}); // document.ready





