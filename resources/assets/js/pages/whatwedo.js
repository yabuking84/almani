$(document).ready(function(){

	$('.owl-carousel').owlCarousel({
	    loop:false,
	    margin:50,
	    nav:false,
        dots:true,
		autoplay:true,
		rewind:true,
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:2,
            },
            600:{
                items:2,
	        },
	        1000:{
	            items:7
	        }
	    }
	})



});