$(document).ready(function(){
//////////////////////////////////
// Document.ready
//////////////////////////////////////////////////////////////////////

$('.morelink').click(function(){

    var data = $(this);

    if(data.hasClass('is-open')) {
        data.html('Read More <i class="fa fa-angle-down"></i>')
        data.addClass('is-close');
        data.removeClass('is-open')
        $('.moreellipses').fadeIn();
    } else {
        data.html('Show Less <i class="fa fa-angle-up"></i>')
        data.addClass('is-open');
        data.removeClass('is-close')
        $('.moreellipses').fadeOut();
    }

    $( ".morecontent" ).slideToggle( "slow" );
    return false;

});

$('.readmore').click(function(){
    // $('.category_desc').toggleClass('hide_group');
    $(this).find('span').toggleClass('hide_group');
    $('.category_desc').slideToggle();

});









//////////////////////////////////////////////////////////////////////
// Document.ready
//////////////////////////////////
});
