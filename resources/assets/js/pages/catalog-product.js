$(document).ready(function(){

setCookie('is_catalog_page','yes');

isotope();

if ($(window).width() < 780) {

		$('body').on("click", ".prod_block .item", function (e) {
			e.preventDefault();
			$(this).find('.more_details').toggleClass('hide_group');
		});

		$('body').on("click", ".prod_block .item button.goto_product", function () {
			var href = $(this).closest('a.item').attr('href');
			window.location.href = href;
		});

		$('body').click(function (e) {
			if (!$(e.target).closest('.item').attr('class')) {
				$('.prod_block .item .more_details').addClass('hide_group');
			}
		});

	} else {

		$("body").on("mouseenter", ".prod_block .item", function () {
			$(this).find(".more_details").stop().removeClass("hide_group");
		});

		$("body").on("mouseleave", ".prod_block .item", function (e) {
			$(this).find(".more_details").stop().addClass("hide_group");
		});

	}



    // if($(window).width() < 780) {

	   //  $('.prod_block .item').click(function(e){
	   //  	e.preventDefault();
	   //  	// $('.prod_block .item .more_details').addClass('hide_group');
	   //  	$(this).find('.more_details').toggleClass('hide_group');
	   //  });

	   //  $('.prod_block .item button.goto_product').click(function(e){
	   //  	var href = $(this).closest('a.item').attr('href');
	   //  	window.location.href = href;
	   //  });

	   //  $('body').click(function(e){
	   //  	// cnsl($(e.target).closest('.item').attr('class'));
	   //  	if(!$(e.target).closest('.item').attr('class')) {
		  //   	$('.prod_block .item .more_details').addClass('hide_group');
	   //  	}
	   //  });



    // } else {

	   //  $('.prod_block .item').mouseenter(function(){
	   //  	$(this).find('.more_details').stop().removeClass('hide_group');
	   //  }).mouseleave(function(){
	   //  	$(this).find('.more_details').stop().addClass('hide_group');
	   //  });

    // }

    $('.readmore').click(function(){
    	// $('.category_desc').toggleClass('hide_group');
    	$(this).find('span').toggleClass('hide_group');
    	$('.category_desc').slideToggle();

	});


	 // when body is clicked
    // bodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclick
    // $('body').click(function(e){
    //     if(!$(e.target).closest('.open_search_tab').length && 
    //     	!$(e.target).closest('.search_tab').length) {

	// 		$('.search_tab').removeClass('show_tab');
    //     }
		
    // });
    // bodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclick
    // when body is clicked




// application areas

			// var $grid = $('.grid').isotope({
			// 	itemSelector: '.item-container',
			// 	// layoutMode: 'fitRows',
			// 	getSortData: {
			// 	// set sortable value
			// 		price: '.price_container parseFloat',
			// 		power: '.power_container parseFloat',
			// 		lumen: '.lumen_container parseFloat'

			// 	},
			// 	// declare sortable values
			// 		// sortBy: [ 'original-order' ]
			// 	sortBy: [ 'price', 'power' , 'lumen' ]
			// });


	// sorting using isotope for the product page

	// sorting using isotope
	// $('.price').click(function(){
		$('.price').on('click', function() { 
		var $this = $(this);
		var method = $this.attr('data-sort');	
		if(method === 'asc') {
			$grid.isotope({ sortBy: 'price', sortAscending : true });
			$this.attr('data-sort','desc');
			$this.html('<i class="fas fa-money-bill-alt"></i> Price <i class="fas fa-sort-down"></i>');
		} else {
			$grid.isotope({ sortBy: 'price', sortAscending : false });
			$this.attr('data-sort','asc');
			$this.html('<i class="fas fa-money-bill-alt"></i> Price <i class="fas fa-sort-up"></i>');
		}
	});

	// $('.power').click(function(){
		$('.power').on('click', function() { 
		var $this = $(this);
		var method = $this.attr('data-sort');
		if (method === 'asc') {
			$grid.isotope({ sortBy: 'power', sortAscending : true });
			$this.attr('data-sort','desc');
			$this.html('<i class="fas fa-bolt"></i> Power <i class="fa fa-sort-down" aria-hidden="true"></i>')
		} else {
			$grid.isotope({ sortBy: 'power', sortAscending : false });
			$this.attr('data-sort','asc');
			$this.html('<i class="fas fa-bolt"></i> Power <i class="fa fa-sort-up" aria-hidden="true"></i>')
		}
	});

	// sorting using isotope
	// $('.lumens').click(function(){
		$('.lumens').click(function() { 
		var $this = $(this);
		var method = $this.attr('data-sort');
		if(method === 'asc') {
			$grid.isotope({ sortBy: 'lumens', sortAscending : true });
			$this.attr('data-sort','desc');
			$this.html('<i class="fas fa-sun"></i> Lumens <i class="fa fa-sort-down" aria-hidden="true"></i>')
		} else {
			$grid.isotope({ sortBy: 'lumens', sortAscending : false });
			$this.attr('data-sort','asc');
			$this.html('<i class="fas fa-sun"></i> Lumens <i class="fa fa-sort-up" aria-hidden="true"></i>')
		}
	});
	// sorting using isotope 

		// clasess of the sorting
	$('.sort-options .option').click(function(){
		 dataType = $(this).attr('data-type');
		if (dataType == 'price') {
			$('.sort-options .price').addClass('selected');
			$('.sort-options .power').removeClass('selected');
			$('.sort-options .lumens').removeClass('selected');
		} else if (dataType == 'power') {
			$('.sort-options .power').addClass('selected');
			$('.sort-options .price').removeClass('selected');
			$('.sort-options .lumens').removeClass('selected');
		} else if (dataType == 'lumens') {
			$('.sort-options .price').removeClass('selected');
			$('.sort-options .power').removeClass('selected');
			$('.sort-options .lumens').addClass('selected');
		}
		// cnsl(dataType);
	});



// end application areas










































	// var dataType;
	// var sortType;


	// var $grid = $('.grid').isotope({
	// 	itemSelector: '.item-container',
	// 	// layoutMode: 'fitRows',
	// 	getSortData: {
	// 	// set sortable value
	// 		power: '.power_container parseFloat',
	// 		lumen: '.lumen_container parseFloat'
	// 	},
	// 	// declare sortable values
	// 		// sortBy: [ 'original-order' ]
	// 	sortBy: [ 'price', 'power' , 'lumen' ]
	// });

	// // sorting using isotope for the product page


	// ------------------------------------- 

	$('.sorting-price').click(function(){
		var $this = $(this);
		productSort($this, '', 'price');
	});

	$('.sorting-lumen').click(function() {
		var $this = $(this);
		productSort($this, '', 'lumen');
	});

	$('.sorting-power').click(function() {
		var $this = $(this);
		productSort($this, '', 'power');
	});


	// ---------------------------------------


	function isotope() {

			var dataType;
			var sortType;


			var $grid = $('.grid').isotope({
				itemSelector: '.item-container',
				// layoutMode: 'fitRows',
				getSortData: {
				// set sortable value
					price: '.price_container parseFloat',
					power: '.power_container parseFloat',
					lumen: '.lumen_container parseFloat'

				},
				// declare sortable values
					// sortBy: [ 'original-order' ]
				sortBy: [ 'price', 'power' , 'lumen' ]
			});

	}

	// --------- MAIN FUNCTION SORTING ------------

	function productSort(element, obj = '', type) {


			var $this = $(element);

			var sort_type = $this.attr('data-sort');
			var sort_key = $this.attr('data-type');
			var category_alias  = $this.attr('data-category');

			var icon = $this.attr('data-icon');
			var name = $this.attr('data-name');

			var url = $this.attr('data-url');

				console.log(sort_type);
				console.log(sort_key);
				console.log(category_alias);

			if(obj === '') {
				setCookie('sort_type', sort_type);
				setCookie('sort_key', sort_key);
				setCookie('category_alias', category_alias);
			}

			//  overide variables if from paginate is true

			if(obj.paginate) {
				
				console.log(obj.paginate);
				sort_type = getCookie('sort_type');
				sort_key = getCookie('sort_key');
				category_alias  = getCookie('category_alias');
				url = obj.url;

				var shiftj = {
					sort_key:sort_key,
					sort_type:sort_type,
					category_alias:category_alias
				}

				console.log(shiftj);
			}

			var csrf_token = $('meta[name="csrf-token"]').attr('content');

			$this.html('<i class="'+ icon +'"></i> '+ name +' <i class="fas fa-spinner fa-pulse" aria-hidden="true"></i>')

			$('.product-holder').slideUp();

			$.post(url,
			{

				'_token':csrf_token,
				'category_alias': category_alias,
				'sort_key': sort_key,
				'sort_type': sort_type
				
			}, function(data) {

				if(data.success) {
					
					$('.pagination_nav').html(data.links);
					$('#product-container').html(data.product_html);
					// $grid.append
					$('#frst-phase').hide();
					$('#secnd-phase').show();

					 if (type === 'price') {
						// $grid.isotope({ sortBy: 'price', sortAscending : true });
						$( ".price" ).trigger( "click" );
					 } else if(type === 'lumen') {
					 	$( ".lumens" ).trigger( "click" );
						// $grid.isotope({ sortBy: 'lumen', sortAscending : true });
					 }else if(type === 'power') {
					 	$( ".power" ).trigger( "click" );
						// $grid.isotope({ sortBy: 'power', sortAscending : true });
					 }

				}

			}).fail(function(e) 
			{
				cnsl(e.responseText)
			});

	}

	// --------- MAIN FUNCTION SORTING ------------


	$('.pagination_nav').on('click', '.pagination-next:not(disabled), .pagination-previous:not(disabled)', function () {

		var href = $(this).attr('data-href');

		var paginate = true;

		var obj = {
			paginate:true,
			url:href
		}

		productSort('', obj);
		

	});

	$('.pagination_nav').on('click', '.pagination-link:not(.is-current)', function () {

		var href = $(this).attr('data-href');
		
		var paginate = true;

		var obj = {
			paginate:true,
			url:href
		}

		productSort('', obj);

	});



	// sorting using isotope for the application area page
	$('.sorting-application').click(function() {

		var $this = $(this);
		var sort_type = $this.attr('data-sort');
		var url = $this.attr('data-url');
		var category_alias  = $this.attr('data-category');
		var csrf_token = $('meta[name="csrf-token"]').attr('content');

		var _obj = {
			_token:csrf_token,
			url:url,
			category_alias: category_alias
		}

		productSort(_obj);

	});


	function sortinginApplication(_data) {
	}


	//  end of sorting thing here 
	
	/*

	$('.power').on('click', function(){

	    var $grid = $('.grid');
	    var $grid = $('.grid').isotope({
	        itemSelector: '.item-container',
	        // layoutMode: 'fitRows',
	        getSortData: {
	        // set sortable value
	            power: '.power_container parseFloat',
	            lumen: '.lumen_container parseFloat'
	        },
	        // declare sortable values
	        // sortBy: [ 'original-order' ]
	        sortBy: [ 'power' , 'lumen' ]
	    });


		var $this = $(this);
		var method = $this.attr('data-sort');
		if (method === 'asc') {
			$grid.isotope({ sortBy: 'power', sortAscending : true });
			$this.attr('data-sort','desc');
			$this.html('<i class="fas fa-bolt"></i> Power <i class="fa fa-sort-down" aria-hidden="true"></i>')
		} else {
			$grid.isotope({ sortBy: 'power', sortAscending : false });
			$this.attr('data-sort','asc');
			$this.html('<i class="fas fa-bolt"></i> Power <i class="fa fa-sort-up" aria-hidden="true"></i>')
		}

		console.log('diri ka');
	});

	// sorting using isotope

	$('.lumens').on('click', function(){

	    var $grid = $('.grid');

	    var $grid = $('.grid').isotope({
	        itemSelector: '.item-container',
	        // layoutMode: 'fitRows',
	        getSortData: {
	        // set sortable value
	            power: '.power_container parseFloat',
	            lumen: '.lumen_container parseFloat'
	        },
	        // declare sortable values
	            // sortBy: [ 'original-order' ]
	        sortBy: [ 'power' , 'lumen' ]
	    });

		var $this = $(this);
		var method = $this.attr('data-sort');
		if(method === 'asc') {
			$grid.isotope({ sortBy: 'lumens', sortAscending : true });
			$this.attr('data-sort','desc');
			$this.html('<i class="fas fa-sun"></i> Lumens <i class="fa fa-sort-down" aria-hidden="true"></i>')
		} else {
			$grid.isotope({ sortBy: 'lumens', sortAscending : false });
			$this.attr('data-sort','asc');
			$this.html('<i class="fas fa-sun"></i> Lumens <i class="fa fa-sort-up" aria-hidden="true"></i>')
		}
		console.log('diri ka alkjal');
	});

	*/

	// sorting using isotope 



	/// currency

	$('#currency').change(function(){

		var currency = $(this).val();
		setCookie('almani_currency',currency);

		$('.grid .price_container').each(function(){

			var total = 0;
			var price = 0;

			var def_uae_price = parseFloat($(this).attr('data-value')); 
			var price = parseFloat($(this).attr('data-price_value_uae')); 

			total = total+price;
			
			//// currency ////////////////////////////////////////////
			total = total*$glbl_currency[getCookie("almani_currency")];
			//////////////////////////////////////////////////////////
		
		   total = total.toFixed(2);

		   $(this).html(total + ' ' + getCookie('almani_currency'));

		});

		var curr = getCookie("almani_currency");
		curr = (!curr)?"AED":curr;

		$('#currency').val(curr);
	
	});

	/// currency

	// showing modal search
	$('.search').click(function(){
		$('.search_tab').toggleClass('show_tab');
	});

	$( "#form-search" ).submit(function() {
		$('#form-search .submit_search i').show().css('display','inline-block');
	  });

	// closing modal search
	$('.close_search_tab').click(function(){
		$('.search_tab').removeClass('show_tab');
	});


	$(".img_container").on("contextmenu",function(e){
        return false;
	});
	
	// clasess of the sorting
	$('.sort-options .option').click(function(){
		 dataType = $(this).attr('data-type');
		if (dataType == 'price') {
			$('.sort-options .price').addClass('selected');
			$('.sort-options .power').removeClass('selected');
			$('.sort-options .lumens').removeClass('selected');
		} else if (dataType == 'power') {
			$('.sort-options .power').addClass('selected');
			$('.sort-options .price').removeClass('selected');
			$('.sort-options .lumens').removeClass('selected');
		} else if (dataType == 'lumens') {
			$('.sort-options .price').removeClass('selected');
			$('.sort-options .power').removeClass('selected');
			$('.sort-options .lumens').addClass('selected');
		}
		cnsl(dataType);
	});

	$('.search_tab input').keypress(function (ev) {
        var keycode = (ev.keyCode ? ev.keyCode : ev.which);
        if (keycode == '13') {
			$('.search_tab .submit_search').click();
        }
	});

	$('.brand_options .option').click(function(){
		$(this).toggleClass('selected');
	});
	
	$('.dim_options .option').click(function(){
		$(this).toggleClass('selected');
	});

	$('.btn-emergency').click(function(){
		$(this).toggleClass('selected');
	});

	$('.btn-wifi').click(function(){
		$(this).toggleClass('selected');
	});

	$('.btn-dimm').click(function(){
		let val = ($(this).hasClass('selected')) ? '0' : '1';
		$('.dimm').val(val);
	});

	$('.btn-triac').click(function(){
		let val = ($(this).hasClass('selected')) ? '0' : '1';
		$('.triac').val(val);

	});

	$('.btn-v0_10').click(function(){
		let val = ($(this).hasClass('selected')) ? '0' : '1';
		$('.v0_10').val(val);

	});		

	$('.btn-dali').click(function(){
		let val = ($(this).hasClass('selected')) ? '0' : '1';
		$('.dali').val(val);
	});

	$('.btn-emergency').click(function(){
		let val = ($(this).hasClass('selected')) ? '0':'1';
		$('.emergency').val(val);
	});

	$('.btn-wifi').click(function(){
		let val = ($(this).hasClass('selected')) ? '0':'1';
		$('.wifi').val(val);
	});

	$('.btn-almani_brand').click(function(){
		let val = ($(this).hasClass('selected')) ? '1':'0';
		$('.almani').val(val);
	});

	$('.btn-camel_brand').click(function(){
		let val = ($(this).hasClass('selected')) ? '1':'0';
		$('.camel').val(val);
	});






    $('.send_message').on('click',function(){
        var message = 'Hi, I would like to enquire about '+category_name+' and would appreciate a call back as soon as possible.';
        $('#contact_us_modal textarea').val(message);
    });


	



    // Custom Contact Us for Product Page
    ////////////////////////////////////////////////////////////////////
    $('.icons.contact_us').on('click',function(){

        var message = 'Hi, I would like to enquire about '+category_name+' and would appreciate a call back as soon as possible.';
        $('#contact_us_modal textarea').val(message);

    });
    ////////////////////////////////////////////////////////////////////
    // Custom Contact Us for Product Page





    

    var almani_itm_pg = getCookie('almani_itm_pg');
    if(almani_itm_pg==""){
        setCookie('almani_itm_pg','15');
    } else {
        $('#itm_pg option').each(function(i){
            if($(this).attr('value')==almani_itm_pg)
            $(this).attr('selected','selected');
        });
        
        // $('#itm_pg').trigger('change');
    	setCookie('almani_itm_pg',almani_itm_pg);
        cnsl('itm+pg trigger change');
    }


    $('#itm_pg').change(function(){

    	var itm_pg = $(this).val();
    	setCookie('almani_itm_pg',itm_pg);
    	window.location.href = $glb_url;

    });




	///////////////////////
    // Options as Radio //
    ////////////////////////////////////////////////////////////////////////////////////////////

    $('.option_group_container .option_group label.button input').on('click',function(e){
    	
    	// $(this).closest('.option_group').find('label.button:not(.isSelected)').removeClass('isSelected');
    	$(this).closest('label.button').siblings('label.button').removeClass('isSelected');

    	$(this).closest('label.button').toggleClass('isSelected');
    });

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Options as Radio //
	/////////////////////


	$('#brand_almani').click(function(){
		if($(this).hasClass('isSelected')) {
			$('#brand_name').val('almani');
		} 
	});

	$('#brand_camel').click(function(){
		if($(this).hasClass('isSelected')) {
			$('#brand_name').val('camel');
		}
	});







});
