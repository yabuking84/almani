$(document).ready(function(){



  	$('.form_contactdetails').on('submit',function(e) {
  		$('.submit_contactdetails').addClass('is-loading');	
  	});


	$(".map_section .close_map").click(function(){
		$('.map_section').hide('slow');
	});

	$(".show_map").click(function(){
		if($('.google_maps').height()==0) {	
			var what = $(this).data("get");
			if(what == "germany"){
				lat = german_lat;
				lon = german_lon;
				country = "germany";
			} else {
				lat = dubai_lat;
				lon = dubai_lon;
				country = "dubai";
			}

			console.log(lat);
			console.log(lon);
			console.log(country);

			$("html, body").animate({ scrollTop: $(".google_maps").offset().top-130 }, "slow");

			$('.map_section').show('slow', function(){
				$("#map_container").css("background", "none");
				init_map(lat,lon,country);
			});
		} else {
			$('.map_section').hide('slow');
		}
	
	});


});



var german_lat = 50.84542391;
var german_lon = 6.05737;
// var dubai_lat = 25.185689962527203;
// var dubai_lon = 55.26248574256897;
// var dubai_lat = 25.0118336;
// var dubai_lon = 55.1602297;

var dubai_lat = 25.012028;
var dubai_lon = 55.1593233;




function init_map(lat, lon, country) {
  var var_location = new google.maps.LatLng(lat, lon);

  var var_mapoptions = {
    center: var_location,
    zoom: 14
  };
  
  var title = (country == "dubai")?"Almani Lighting L.L.C.":"Almani Lighting GmbH";
  var var_marker = new google.maps.Marker({
  position: var_location,
  map: var_map,
  title:title});

  var var_map = new google.maps.Map(document.getElementById("map_container"),
  var_mapoptions);

  var_marker.setMap(var_map); 

}

var lat = 0;
var lon = 0;
