$(document).ready(function(){



  	$('.form_quote').on('submit',function(e) {
  		$('.submit_quote').addClass('is-loading');	
  	});


    $('form').submit(function(){
        $(this).find('button[type=submit]').html('<i class="fas fa-circle-notch fa-spin"></i>');
    });



    // Google Maps
    //////////////////////////////////////////////////////
	$(".show_map").click(function(){
		$(".single_line .show_map").hide();
		$('.close_map').show();

		// if($('.google_maps').height()==0) {	
			var what = $(this).attr('data-map');
			if(what == "germany"){
				lat = german_lat;
				lon = german_lon;
				country = "germany";
			} else {
				lat = dubai_lat;
				lon = dubai_lon;
				country = "dubai";
			}


			$("html, body").animate({ scrollTop: $(".google_maps").offset().top-170 }, "slow");

			$('.map_section').show('slow', function(){
				$("#map_container").css("background", "none");
				init_map(lat,lon,country);
			});
		// } else {
		// 	$('.map_section').slideUp('fast');
		// }
	
	});
	$(".close_map").click(function(){
		$(this).hide();
		$('.show_map').show();
		$('.map_section').slideUp('slow');
	});
    //////////////////////////////////////////////////////
    // Google Maps


});








var german_lat = 50.84542391;
var german_lon = 6.05737;

var dubai_lat = 25.012028;
var dubai_lon = 55.1593233;





function init_map(lat, lon, country) {
  var var_location = new google.maps.LatLng(lat, lon);

  var var_mapoptions = {
    center: var_location,
    zoom: 14
  };
  
  var title = (country == "dubai")?"Almani Lighting L.L.C.":"Almani Lighting GmbH";
  var var_marker = new google.maps.Marker({
  position: var_location,
  map: var_map,
  title:title});

  var var_map = new google.maps.Map(document.getElementById("map_container"),
  var_mapoptions);

  var_marker.setMap(var_map); 

}

var lat = 0;
var lon = 0;
