//Globals
  var LEDTotalCost = new Array();
  var OLDTotalCost = new Array();
  var DifTotalCost = new Array();

  var LEDYearlyCost = new Array();
  var OLDYearlyCost = new Array();
  var DifYearlyCost = new Array();

  var ROI = null;

  //Format number function. c= number, d = decimal point, t = thousands seperator
  Number.prototype.formatMoney = function(c, d, t) {
      var n = this,
          c = isNaN(c = Math.abs(c)) ? 2 : c,
          d = d == undefined ? "," : d,
          t = t == undefined ? "." : t,
          s = n < 0 ? "-" : "",
          i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
          j = (j = i.length) > 3 ? j % 3 : 0;
      return s + '' + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
  };

  //Calculate cost of running lights
  function CalculateReturn() {
      var MaxMonthsGraph = 48;
      var MaxMonths = 120;

      var MaxYearsGraph = (MaxMonthsGraph / 12) + 1;

      var LEDWattsStr = document.LEDCalc.LEDWatts.value;
      if (!LEDWattsStr)
          LEDWattsStr = '0';

      var OLDWattsStr = document.LEDCalc.OLDWatts.value;
      if (!OLDWattsStr)
          OLDWattsStr = '0';

      var PwrCostStr = document.LEDCalc.PwrCost.value;
      if (!PwrCostStr)
          PwrCostStr = '0';

      var LEDLifetimeStr = document.LEDCalc.LEDLifetime.value;
      if (!LEDLifetimeStr)
          LEDLifetimeStr = '0';

      var OLDLifetimeStr = document.LEDCalc.OLDLifetime.value;

      if (!OLDLifetimeStr) {
          OLDLifetimeStr = '0';
      }

      var HoursUseStr = document.LEDCalc.HoursUse.value;
      if (!HoursUseStr)
          HoursUseStr = '0';

      var NumLightsStr = document.LEDCalc.NumLights.value;
      if (!NumLightsStr)
          NumLightsStr = '0';

      var LEDBulbCostStr = document.LEDCalc.LEDBulbCost.value;
      if (!LEDBulbCostStr)
          LEDBulbCostStr = '0';

      var OLDBulbCostStr = document.LEDCalc.OLDBulbCost.value;
      if (!OLDBulbCostStr)
          OLDBulbCostStr = '0';


      var LEDBulbCost = parseFloat(LEDBulbCostStr);
      var OLDBulbCost = parseFloat(OLDBulbCostStr);
      var LEDWatts = parseFloat(LEDWattsStr);
      var OLDWatts = parseFloat(OLDWattsStr);
      var PwrCost = parseFloat(PwrCostStr.replace("$", " "));
      var LEDLifetime = parseFloat(LEDLifetimeStr);
      var OLDLifetime = parseFloat(OLDLifetimeStr);
      var HoursUse = parseFloat(HoursUseStr);
      var NumLights = parseFloat(NumLightsStr);

      var LEDPwrConsumption = LEDWatts * HoursUse * NumLights * 30 / 1000;
      var OLDPwrConsumption = OLDWatts * HoursUse * NumLights * 30 / 1000;
      var DifPwrConsumption = OLDPwrConsumption - LEDPwrConsumption;
      document.LEDCalc.LEDPwrConsumption.value = LEDPwrConsumption.toFixed(1);
      document.LEDCalc.OLDPwrConsumption.value = OLDPwrConsumption.toFixed(1);
      document.LEDCalc.DifPwrConsumption.value = DifPwrConsumption.toFixed(1);
      //880g CO2 per KWH
      document.LEDCalc.LEDCO2.value = ((880 * LEDPwrConsumption / 1000) / 12).toFixed(1);
      document.LEDCalc.OLDCO2.value = ((880 * OLDPwrConsumption / 1000) / 12).toFixed(1);
      document.LEDCalc.DifCO2.value = ((880 * DifPwrConsumption / 1000) / 12).toFixed(1);

      var LEDRunningCost = (LEDPwrConsumption * PwrCost);
      var OLDRunningCost = (OLDPwrConsumption * PwrCost);
      var DifRunningCost = (OLDRunningCost - LEDRunningCost);
      document.LEDCalc.LEDRunningCost.value = LEDRunningCost.formatMoney(2, '.', ',');
      document.LEDCalc.OLDRunningCost.value = OLDRunningCost.formatMoney(2, '.', ',');
      document.LEDCalc.DifRunningCost.value = DifRunningCost.formatMoney(2, '.', ',');

      // cnsl(LEDTotalCost)
      var LEDMonthsInService = 0;
      var OLDMonthsInService = 0;
      
      var isRoi = false;
      for (var Month = 1; Month <= MaxMonths; Month++) {
          //Accumlate costs
          if (Month > 1) {
              LEDTotalCost[Month] = LEDTotalCost[Month - 1];
              OLDTotalCost[Month] = OLDTotalCost[Month - 1];
          } else {
              LEDTotalCost[Month] = 0;
              OLDTotalCost[Month] = 0;
          }

          LEDMonthsInService++;
          if (Month == 1 || LEDMonthsInService >= LEDLifetime) {
              LEDTotalCost[Month] += LEDBulbCost * NumLights;
              LEDMonthsInService = 0;
          }

          // Add this month running cost
          LEDTotalCost[Month] += LEDRunningCost;

          // Is a new set of Old globes needed this month?
          OLDMonthsInService++;

          if (Month == 1 || OLDMonthsInService >= OLDLifetime) {
              OLDTotalCost[Month] += OLDBulbCost * NumLights;
              OLDMonthsInService = 0;
          }
          // Add this years running cost
          OLDTotalCost[Month] += OLDRunningCost;

          DifTotalCost[Month] = OLDTotalCost[Month] - LEDTotalCost[Month];

          if (Month % 12 == 0) {
              // Compound running costs at 5% increase per year
              LEDRunningCost = (LEDRunningCost * 1.05);

              // Assume LED get 10% cheaper year on year
              LEDBulbCost = (LEDBulbCost * 0.9);

              LEDYearlyCost[Month / 12] = LEDTotalCost[Month];
              OLDYearlyCost[Month / 12] = OLDTotalCost[Month];
              DifYearlyCost[Month / 12] = DifTotalCost[Month];
          }

          // Check if roi
          if (!isRoi && DifTotalCost[Month] >= 0) {
              ROI = Month;
              isRoi = true;
          }
      }
      cnsl(ROI);

      //Assign values back to form
      document.LEDCalc.LEDTotalCost1.value = LEDTotalCost[6].formatMoney(2, '.', ',');
      document.LEDCalc.OLDTotalCost1.value = OLDTotalCost[6].formatMoney(2, '.', ',');
      document.LEDCalc.DifTotalCost1.value = DifTotalCost[6].formatMoney(2, '.', ',');
      document.LEDCalc.LEDTotalCost3.value = LEDTotalCost[12].formatMoney(2, '.', ',');
      document.LEDCalc.OLDTotalCost3.value = OLDTotalCost[12].formatMoney(2, '.', ',');
      document.LEDCalc.DifTotalCost3.value = DifTotalCost[12].formatMoney(2, '.', ',');
      document.LEDCalc.LEDTotalCost5.value = LEDTotalCost[36].formatMoney(2, '.', ',');
      document.LEDCalc.OLDTotalCost5.value = OLDTotalCost[36].formatMoney(2, '.', ',');
      document.LEDCalc.DifTotalCost5.value = DifTotalCost[36].formatMoney(2, '.', ',');
      document.LEDCalc.LEDTotalCost10.value = LEDTotalCost[60].formatMoney(2, '.', ',');
      document.LEDCalc.OLDTotalCost10.value = OLDTotalCost[60].formatMoney(2, '.', ',');
      document.LEDCalc.DifTotalCost10.value = DifTotalCost[60].formatMoney(2, '.', ',');

      if ($(window).width() > 999) {
          var isMobile = false,
              basis = 'month(s)',
              xAxisLabel = 'Months',
              xAxisMax = MaxMonthsGraph;
      } else {
          var isMobile = true,
              basis = 'year(s)',
              xAxisLabel = 'Years',
              xAxisMax = MaxYearsGraph;
      }

      //Update graphs
      var LEDSeries = [];
      var OLDSeries = [];
      var GraphOptions = {
          legend: {
              position: "nw",
              labelBoxBorderColor: "#C0C0C0",
              backgroundColor: "#E0E0E0",
              show: true,
              margin: 10,
              backgroundOpacity: 0.5
          },
          xaxis: {
              ticks: xAxisMax,
              min: 0,
              max: xAxisMax,
              tickDecimals: 2,
              tickFormatter: function(val, axis) {
                  return val < axis.max ? val : val + " " + xAxisLabel;
              }
          },
          yaxis: {
              tickDecimals: 2,
              tickFormatter: function(val) {
                  return "AED " + val.formatMoney(2, '.', ',');
              }
          },
          grid: {
              hoverable: true,
          },
      }

      //jQuery shortcut for $(document).ready(function(), to check document is loaded
      $(function() {
          
          if (isMobile) {
              for (var i = 1; i <= MaxYearsGraph; i++) {
                  LEDSeries.push([i, LEDYearlyCost[i]]);
                  OLDSeries.push([i, OLDYearlyCost[i]]);
              }
              // ROI = (ROI == null ? 'N/A' : ((ROI / 12).toFixed(1) + ' ' + basis))
          } else {
              for (var i = 1; i <= MaxMonthsGraph; i++) {
                  LEDSeries.push([i, LEDTotalCost[i]]);
                  OLDSeries.push([i, OLDTotalCost[i]]);
              }
              // ROI = (ROI == null ? 'N/A' : (ROI + ' ' + basis))
          }

          
          var GraphData = [{
                  data: LEDSeries,
                  label: "&nbsp;Almani Lighting cost",
                  color: "#00FF00",
                  points: {
                      show: true,
                      radius: 3
                  },
                  lines: {
                      show: true
                  }
              },
              {
                  data: OLDSeries,
                  label: "&nbsp;Old lighting cost",
                  color: "#FF0000",
                  points: {
                      show: true,
                      radius: 3
                  },
                  lines: {
                      show: true
                  }
              },
              {
                  data: [],
                  label: "<b>&nbsp;ROI: " + (isMobile ? (ROI / 12).toFixed(1) + ' year(s)' : (ROI + ' month(s)')) + "</b>",
                  color: "#f7bc16"
              },
          ];
          //Update graph
          $.plot($("#ReturnGraph"), GraphData, GraphOptions);

          //Deal with mouse over
          var previousPoint = null;
          $("#ReturnGraph").bind("plothover", function(event, pos, item) {
              if (item) {
                  if (previousPoint != item.dataIndex) {
                      previousPoint = item.dataIndex;

                      $("#tooltip").remove();
                      var MOTime = item.datapoint[0],
                          MOCost = item.datapoint[1],
                          Savings = isMobile ? DifYearlyCost[MOTime] : DifTotalCost[MOTime];

                      MOCost = MOCost.formatMoney(2, '.', ',');
                      Savings = Savings.formatMoney(2, '.', ',');

                      if (item.series.label == "&nbsp;Camel LED lighting cost") {
                          showTooltip(item.pageX, item.pageY, "Total cost of Almani Lighting at<br>" + MOTime + " " + basis + " is AED " + MOCost + "<br>Saving AED " + Savings);
                      } else {
                          showTooltip(item.pageX, item.pageY, "Total cost of existing old lighting at<br>" + MOTime + " " + basis + " is AED " + MOCost);
                      }
                  }
              } else {
                  $("#tooltip").remove();
                  previousPoint = null;
              }
          });

          //Display tooltip
          function showTooltip(x, y, contents) {
              $('<div id="tooltip">' + contents + '</div>').css({
                  'z-index': 10,
                  position: 'absolute',
                  display: 'none',
                  top: y + 5,
                  left: x + 5,
                  border: '1px solid #fdd',
                  padding: '2px',
                  'background-color': '#fee',
                  opacity: 0.80
              }).appendTo("body").fadeIn(200);
          }

      });
  }


  $( "#form_quote" ).submit(function() {
      $('#btn-almani').attr('disabled','disabled').html('<i class="fas fa-circle-notch fa-spin"></i>');
  });

  $(document).ready(function(){
    
    $(window).on('resize', function () {
        CalculateReturn();
    });

    CalculateReturn();
    cnsl('roi');
    console.log('this is great');

  });
