    var catalog, testEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var testNumber = /^[\+\-]?\d*\.?\d+(?:[Ee][\+\-]?\d+)?$/;
    
    function send() {
        
        var url = $('.form_post_url').val();
        var name = $('#full_name').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
    
        if(!name) {
            message('Full Name is Required');
        } else if(!phone) {
            message('Phone Number is Required');
        } else if(!testNumber.test(phone)) {
            message('Phone Number is Invalid');
        } else if(!email) {
            message('Email Address is Required')
        } else if(!testEmail.test(email)) {  
            message('Email Address is Invalid');
        } else {
            $('#message').addClass('d-none');
            $( "#download-form" ).submit();
            $('#btn-download').attr('disabled','disabled').html('<i class="fas fa-circle-notch fa-spin"></i>');
        }
    }


    $('form').submit(function(){
        $('#btn-download').attr('disabled','disabled').html('<i class="fas fa-circle-notch fa-spin"></i>');
    });
    
    // function message(msg, error = false) {
    //     if(!error) { 
    //         $('#message')   
    //             .removeClass('d-none alert-success')
    //                 .addClass('alert-danger')
    //                     .html(msg);
    //     } else {
    //         $('#message')
    //             .removeClass('d-none alert-danger')
    //                 .addClass('alert-success')
    //                     .html(msg);
    //      }
    
    // }
    
    function clear() {
        $('#full_name').val('');
        $('#phone').val('');
        $('#full_name').focus();
        $('#email').val('');
    }
    
    