$(document).ready(function(){

	$('#testimonials .owl-carousel').owlCarousel({
	    loop:true,
	    margin:10,
	    nav:true,
		dots:false,
		rewind: false,
		autoplay:true,
		autoWidth:false,
	    autoplayTimeout:3000,
	    autoplayHoverPause:true,
        responsiveClass:true,
     	 responsive:{
            0:{
                items:1,
            },
            600:{
                items:1,
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	$('#client-logos .owl-carousel').owlCarousel({
	    loop:true,
	    margin:45,
	    nav:true,
        dots:false,
		autoplay:true,
		rewind: false,
		autoWidth:false,
	    autoplayTimeout:2000,
	    autoplayHoverPause:true,
		responsiveClass:true,
        responsive:{
            0:{
                items:2,
            },
            600:{
                items:2,
	        },
	        1000:{
	            items:7
	        }
	    }
	});

});