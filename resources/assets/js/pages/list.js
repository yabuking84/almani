$(document).ready(function() {

	var cookie = getCookie('camelled_list');
	var list_items = (cookie)?JSON.parse(cookie):"";
	// console.log(list_items);


    // Initialize tableMobilizer if mobile
    if ($(window).width() < 780) {
		tableMobilizer($(".normal_table_mode .model_table"));    	
    }


	// checkboxes as button
	///////////////////////////////////////////////////////////////
	$(".button input").on("click", function(){
	  if ( $(this).attr("type") === "radio" ) {
	    $(this).parent().siblings().removeClass("isSelected");
	  	$(this).parent().toggleClass("isSelected");
	  }
	});	
	///////////////////////////////////////////////////////////////
	// checkboxes as button




  	// quantity
 	////////////////////////////////////////////////////////////////////
 	$('.quantity_sub').click(function(){
 		var $qc = $(this).closest('.quantiy_container');
 		var $qi = $qc.find('.quantity_input');
 		var quantity = parseInt($qi.val());

 		if(quantity>1) {
 			$qi.val(quantity-1);
 		} else {
 			$qi.val(1);
 		}
 		$qi.trigger('change')


 	});
 	$('.quantity_add').click(function(){
 		var $qc = $(this).closest('.quantiy_container');
 		var $qi = $qc.find('.quantity_input');
 		var quantity = parseInt($qi.val());

 		if(quantity>=1) {
 			$qi.val(quantity+1);
 		} else {
 			$qi.val(1); 			
 		}
 		$qi.trigger('change')

 	});

 	$('.quantity_input').on('change paste keyup',function(){
 		// console.log('change');
 		doTotal();
 	});
 	////////////////////////////////////////////////////////////////////
  	// quantity




  	// remove_from_list
 	////////////////////////////////////////////////////////////////////
 	////////////////////////////////////////////////////////////////////
 	////////////////////////////////////////////////////////////////////
	$('body').on('click','.remove_from_list',function(){
 		var model_code = $(this).attr('data-model_code');

		// check if tableMobilizer is shown		
        var $tr = "";
        if($(this).closest('table').is('.mobile-table'))
        $tr = $(this).closest('table');
        else
        $tr = $(this).closest('tr');


		var camelled_list = getCookie("camelled_list");
		if(camelled_list){
			camelled_list = JSON.parse(camelled_list);

			delete camelled_list[model_code];
			camelled_list = JSON.stringify(camelled_list);
			setCookie('camelled_list',camelled_list);

		} else {
			camelled_list = {};
		}

		// console.log("remove_from_list");
		// console.log(camelled_list);
		// console.log("remove_from_list");

    	$tr.fadeOut(700,function(){
    		$tr.remove();

			// Update Badge
		    /////////////////////////////////////////////////
		    // updateListBadge(camelled_list);
		    /////////////////////////////////////////////////
			// Update Badge

 			doTotal();

 			console.log("camelled list cnt "+Object.keys(camelled_list).length);
 			console.log("camelled list = "+camelled_list);
    		
		    // Check if no items then do the no items stuff
		    if(Object.keys(JSON.parse(camelled_list)).length <= 0) {
			    // Show empty table text and hide other fields
			    $('.table-scrollable-container').hide();
			    $('.action_buttons').hide();
			    $('.action_currency').hide();
			    $('.if_empty_table').show();
			}


    	});

 	});


	// Clear All
	$('.clear_all').on('click',function(){

		// confirmation modal
	    /////////////////////////////////////////////////
        $('#confirmation_clear_all').addClass('is-active');
        $('#confirmation_clear_all').css({
        	opacity: 0, 
        	// display: 'flex',
    	}).animate({
                opacity: 1,
        }, 500);		    
	    /////////////////////////////////////////////////
		// confirmation modal

 	});

  	// Clear All Confirmation Modal
 	////////////////////////////////////////////////////////////////////
  	$('#confirmation_clear_all .confirm_clear_all').click(function(){

        $('#confirmation_clear_all').closest('.modal').fadeOut();
        $('#confirmation_clear_all').closest('.modal').removeClass('is-active');

		clearAll();

  	});
 	////////////////////////////////////////////////////////////////////
  	// Clear All Confirmation Modal


 	////////////////////////////////////////////////////////////////////
 	////////////////////////////////////////////////////////////////////
 	////////////////////////////////////////////////////////////////////
  	// remove_from_list







  	// Enquire
 	////////////////////////////////////////////////////////////////////
    $('.actions .enquire').on('click',function(){
        var message ="";

        console.log('asd');

		// check if tableMobilizer is shown
    	if($(window).width() > 700) 		
		$tr = $('.table-scrollable-container .model_table tbody tr');
		else 
		$tr = $('.mobile-tables .mobile-table');

        message= "Hi, I would like to enquire about the following models and would appreciate a call back as soon as possible.\n";

	    var cnt = 0;
	    var products = [];
		$tr.each(function(i) {
			if(!$(this).find('.model_extra').length) {
				cnt++;
				// console.log(cnt);

		        var model_name = $.trim($(this).find('.model_name').attr('data-model_name'));

		        var category_alias = $.trim($(this).find('.model_details').attr('data-category_alias'));
		        var product_name = $.trim($(this).find('.model_details').attr('data-product_name'));
		        var product_code = $.trim($(this).find('.model_details').attr('data-product_code'));
		        var model_code = $.trim($(this).find('.model_details').attr('data-model_code'));
		        products.push({
		        	category_alias:category_alias,
		        	product_name:product_name,
		        	product_code:product_code,
		        	model_code:model_code,
		        });


		        var model_price = $.trim($(this).find('.best_price .price_value').text());
		        model_price+= " "+$.trim($(this).find('.best_price .currency').text());

		        var model_options_dim = "";
		        $(this).find('.dim_group label.isSelected').each(function(i){
		        	model_options_dim+= $.trim($(this).text());
		        });

		        var model_options_ips = "";
		        $(this).find('.ip_group label.isSelected').each(function(i){
		        	model_options_ips+= $.trim($(this).text());
		        });

		        var model_options_beam_angles = "";
		        $(this).find('.beam_angle_group label.isSelected').each(function(i){
		        	model_options_beam_angles+= $.trim($(this).text());
		        });

		        var model_options_warranty = "";
		        $(this).find('.warranty_group label.isSelected').each(function(i){
		        	model_options_warranty+= $.trim($(this).text());
		        });

		        var model_options_wifi = "";
		        $(this).find('.wifi_group label.isSelected').each(function(i){
		        	model_options_wifi+= $.trim($(this).text());
		        });

		        var model_in_stock = $.trim($(this).find('.in_stock').text());
		        
		        var quantity = $.trim($(this).find('.in_stock').text());
				

		        message+= "\n"+model_name;
		        message+= "\nQuantity: "+$(this).find('.quantity_input').val();

		        if(model_options_dim!="" || model_options_ips!="" || model_options_beam_angles!="" || model_options_wifi!="") {
		        	message+= "\nProduct should have the following options:";	
		        }

		        if(model_options_dim!="")
		        message+= "\nDim: "+model_options_dim;
		        if(model_options_ips!="")
		        message+= "\n"+model_options_ips;
		        if(model_options_beam_angles!="")
		        message+= "\nBeam Angle: "+model_options_beam_angles;
	    	    if(model_options_warranty!="")
		        message+= "\nWarranty: "+model_options_warranty;
		        if(model_options_wifi!="")
		        message+= "\nWifi Enabled";

		    	message+= "\n";
			}
		});

		// console.log(products);
		// console.log(JSON.stringify(products));


    	var lines = message.split("\n");  
		// console.log("lines "+lines.length+"px");
		// console.log("lines* "+lines.length*30+"px");
        $('#enquire textarea').css('height',(lines.length*30+30)+"px");

        $('#enquire input[name="products_json"]').val(JSON.stringify(products));
        $('#enquire textarea').val(message);

        $('#enquire').addClass('is-active');
        $('#enquire').css({
        	opacity: 0, 
        	// display: 'flex',
    	}).animate({
                opacity: 1,
        }, 500);    	

    });
 	////////////////////////////////////////////////////////////////////
  	// Enquire







































    /////////////////////////
    // price and currency //
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    ///////////////
    // Currency //
    /////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    $('.select_currency').change(function(e){


        var currency = $(this).val();

        // set cookie
        setCookie('camelled_currency',currency);

        // console.log("length = "+$('.model_table .price_container').length);

        $(' .price_container').each(function(key){
            var price = parseFloat($(this).find('.price_container_show .price_value').attr('data-uae_value'));
            price = price * $glbl_currency[currency];

            // check if tableMobilizer is shown
            var $tr = "";
            if($(this).closest('table').is('.mobile-table')) {
	            $tr = $(this).closest('table');            	
            }
            else {
	            $tr = $(this).closest('tr');
            }

            updatePrice($tr, $(this));



            // $(this).find('.price_value').html(splitCurrency(price.toFixed(2)));
            // $(this).find('.currency').html(currency);

            // console.log("select_currency = "+currency);
            // console.log("price = "+price);
            // console.log("glbl_currency["+currency+"] = "+$glbl_currency[currency]);
            // console.log($glbl_currency);
            // console.log("data = "+$(this).find('.price_value').attr('data-value'));
        });

    }); 
    //////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////
    // Currency //
    /////////////








    // when an option with .change_price or .change_base_price is clicked
    /////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    $('body').on('click','.option_group.change_price input, .option_group.change_base_price input',function() {
    // $('.option_group.change_price input, .option_group.change_base_price input').on('click',function() {

        // check if tableMobilizer is shown
        var $price_container = "";
        if($(this).closest('table').is('.mobile-table')) {
	        $price_container = $(this).closest('table').find('.price_container');
	        $tr = $(this).closest('table');
        }
        else {
	        $price_container = $(this).closest('tr').find('.price_container');        	
	        $tr = $(this).closest('tr');        }


        updatePrice($tr, $price_container);

    });
    //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////
    /////////////////////////////////////
    // when an option with .change_price or .change_base_price is clicked






    ///////////////////////////////////////////////////////////////////
    //  Logic Flow
    //  1. Get all options (no display changes)
    //  2. Convert to selected currency (no display changes)
    //  3. If has discount show discounted and un-discounted price
    //  4. Update the display
    //////////////////////////////////////////////////////////////////



    // Function according to Logic
    function updatePrice($tr, $price_container) {

    	// console.log('updatePrice');

        updateUAEPriceDataFromOptions($tr, $price_container);
        convertPriceDataFromCurrency($price_container);
        updatePriceContainer($price_container);
        
        checkAndUpdateDiscount($price_container);
        doTotal();
    }





    //////////////////////////////////////////////////
    // functions here should only touch data-value //
    ////////////////////////////////////////////////////////////////////////////////////////////////
    function updatePriceContainer($price_container) {
        $price_value = $price_container.find('.price_container_show .price_value');
        var final_price = parseFloat($price_value.attr('data-value'));
        // console.log((final_price);
        $price_value.html(splitCurrency(final_price.toFixed(2)));

        var currency = $('.select_currency').val();
        $price_container.find('.price_container_show .currency').html(currency);

    }

    function checkAndUpdateDiscount($price_container) {

        // add price without discount
        var discount = parseFloat($price_container.attr('data-discount'));
        if(discount) {
	        var price = parseFloat($price_container.find('.price_container_show .price_value').attr('data-value'));

	        var final_price = (price * 100)/(100-discount);

	        var currency = $('.select_currency').val();
	        $price_container.find('.price_container_no_discount .price_value')
					        .html(splitCurrency(final_price.toFixed(2)));
	        $price_container.find('.price_container_no_discount .currency')
					        .html(currency);
        	
        }

    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // functions here should only touch data-value //
    ////////////////////////////////////////////////




    //////////////////////////////////////////////////////////////////
    // functions here can touch both data-value and data-uae_value //
    ////////////////////////////////////////////////////////////////////////////////////////////////
    function convertPriceDataFromCurrency($price_container) {

            var currency = $('.select_currency').val();

            var price = parseFloat($price_container.find('.price_container_show .price_value').attr('data-uae_value'));
            price = price * $glbl_currency[currency];

            $price_container.find('.price_container_show .price_value').attr('data-value',price);

            // console.log(price+" "+$glbl_currency[currency]);
            // console.log("data = "+$price_container.find('.price_value').attr('data-value'));
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    // functions here can touch both data-value and data-uae_value //
    ////////////////////////////////////////////////////////////////





    //////////////////////////////////////////////////////
    // functions here should only touch data-uae_value //
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    function updateUAEPriceDataFromOptions($tr, $price_container) {
            // var isSelected = $(this).closest('.option_group').find('.isSelected input').attr('data-uae_price');

            // warranty_group
            ////////////////////////////////////
            var wty_dflt = $tr.find('.option_group.warranty_group .default input').val();
            wty_dflt = (wty_dflt)?wty_dflt:0;

            // if no selected wty then select default wty
            var wty_slctd = $tr.find('.option_group.warranty_group .isSelected input').val();
            

            wty_slctd = (wty_slctd)?wty_slctd:0;
            if(wty_slctd==0) {
                $tr.find('.option_group.warranty_group .default').addClass('isSelected');
                wty_slctd = wty_dflt;
            }
            ////////////////////////////////////
            // warranty_group

            var discount = parseFloat(100-$price_container.attr('data-discount'))/100;
            var def_uae_price = parseFloat($price_container.attr('data-def_uae_price'));
            

            console.log('');
            console.log('xxxxxxxxxxxxxxxxxxxxx');
            console.log('xxxxxxxxxxxxxxxxxxxxx');

            var name = $tr.find('.option_group.warranty_group .isSelected input').attr('name');

            console.log('name '+name);            
            console.log('discount '+discount);
            console.log('def_uae_price '+def_uae_price);
            console.log('wty_slctd '+wty_slctd);
            console.log('wty_dflt '+wty_dflt);
            console.log('---------------------');
            var uae_price = 0;
            var ttl_optn_price = 0;
            $tr.find('.option_group.change_price .isSelected input').each(function(i){
                uae_price = parseFloat($(this).attr('data-uae_price'));
                ttl_optn_price+= uae_price;
                console.log($(this).attr("name")+' == '+uae_price);

            });
            console.log('---------------------');
            console.log('ttl_optn_price = '+ttl_optn_price);

            
            var wty_multi = [];
            wty_multi = wtyMulti(wty_dflt);


            // console.log("("+def_uae_price+" + "+ttl_optn_price+" ) * "+wty_multi[wty_slctd+'_yrs']+" * "+discount);

            var final_price = (def_uae_price+ttl_optn_price)*wty_multi[wty_slctd+'_yrs']*discount;

            // $price_container.css('background-color','red');
            console.log("discount = "+discount);
            console.log("wty_slctd = "+wty_slctd+'_yrs = '+wty_multi[wty_slctd+'_yrs']);
            console.log("final_price = "+final_price);
            console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
            console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');

            $price_container.find('.price_value').attr('data-uae_value',final_price);

    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // functions here should only touch data-uae_value //
    ////////////////////////////////////////////////////



    //////////////
    // Summary //
    //////////////////////////////////////////////////////////////////////////////////////


	function doTotal() {
		var total = 0;
		var price = 0;
		var quantity = 0;
		var AllQty = 0;


	    if($(window).width() > 700) 
		tr = '.table-scrollable-container .model_table tbody tr';
		else 
		tr = '.mobile-tables .mobile-table';
		
		$(tr).each(function(key){
			if(!$(this).find('.model_extra').length) {
				price = $(this).find('.price_container_show .price_value').attr('data-value');
				quantity = parseFloat($(this).find('.quantity_input').val());
				total+= parseFloat(price*quantity);
				// console.log(price+" * "+quantity);
				AllQty = AllQty+quantity;
			}
		});

		total = (total).toFixed(2);

		$('.total_amount .value').html(splitCurrency(total));
		$('.total_amount .currency').html($('.select_currency').val());

		$('.total_items .value').html(AllQty);
		
	}


	// function recalculate(){

	//     if($(window).width() > 700) 
	// 	tr = '.table-scrollable-container .model_table tbody tr';
	// 	else 
	// 	tr = '.mobile-tables .mobile-table';


	// }


	function clearAll(){

		$('.remove_from_list').each(function(i){
			var model_code = $(this).attr('data-model_code');

			// check if tableMobilizer is shown		
		    var $tr = "";
		    if($(this).closest('table').is('.mobile-table'))
		    $tr = $(this).closest('table');
		    else
		    $tr = $(this).closest('tr');


			var camelled_list = getCookie("camelled_list");
			if(camelled_list){
				camelled_list = JSON.parse(camelled_list);

				delete camelled_list[model_code];
				camelled_list = JSON.stringify(camelled_list);
				setCookie('camelled_list',camelled_list);

			} else {
				camelled_list = {};
			}

			// console.log("remove_from_list");
			// console.log(camelled_list);
			// console.log("remove_from_list");

			$tr.fadeOut(700,function(){
				$tr.remove();
				$('.if_empty_table').fadeIn();
				// updateListBadge(camelled_list)
			});
		});
				
		// doTotal();

	    $('.total_items_amount .value').html("0");
	    $('.total_amount_container .value').html("0.00");

	    // Show empty table text and hide other fields
	    $('.table-scrollable-container').hide();
	    $('.action_buttons').hide();
	    $('.action_currency').hide();
	    $('.if_empty_table').show();


	}



    //////////////////////////////////////////////////////////////////////////////////////
    // Summary //
    ////////////


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // price and currency //
    ///////////////////////



















































///////////////////////////////////////////
// Run items for initialization of page //
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
getAndSetCurrency();
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// Run items for initialization of page //
/////////////////////////////////////////






}); // document.ready























































