$(document).ready(function(){

	// category menu
	/////////////////////////////////
	////////////////////////////////////////////////////////////////////////


	
	$(".category_overview").click(function() {

		scrollTo(".category_menu");

		$('.category_menu').addClass('is-active');
		$('.category_menu').removeClass('close');
		$('.category_menu').addClass('open');
	});

	
	$(".close_category_menu").click(function() {
		$('.category_menu').removeClass('is-active');
		$('.category_menu').removeClass('open');
		$('.category_menu').addClass('close');
	});

	

    $('body').on('click', function(e) {
        if(parseInt($('.category_menu').css('margin-left')) >= -10) {
			$('.category_menu').removeClass('is-active');
			$('.category_menu').removeClass('open');
			$('.category_menu').addClass('close');
        }
    });	
	////////////////////////////////////////////////////////////////////////
	/////////////////////////////////
	// category menu

});
