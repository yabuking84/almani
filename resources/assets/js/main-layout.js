$(document).ready(function () {

    // Back to top button
    /////////////////////////////////////////////////////////////////////////
    $('.back-to-top').stop().fadeIn();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.back-to-top').stop().fadeIn('slow');
        } else {
            $('.back-to-top').stop().fadeOut('slow');
        }
    });

    $('.back-to-top').click(function () {
        $('html').stop().animate({
                scrollTop: 0
            },
            1500
        );
        return false;
    });
    /////////////////////////////////////////////////////////////////////////
    // Back to top button








    // Load All Menu
    //////////////////////////////////
    ///////////////////////////////////////////////////////////////////
    $.get($glb_allmenu_urls, function (data) {
        var $content = $(data);
        $.each($glb_menu_urls, function (key, val) {
            $('#' + key + '_here').replaceWith($content.find('#' + key + '_here').html());
            // cnsl('#'+key+'_here');
            // cnsl($content.find('div').html());
        });
        
        if (getDevice() != 'desktop') {
            $.each($glb_mobile_menu_urls, function (key, val) {
                $('#' + key + '_here').html($content.find('#' + key + '_here').html());
            });
        }
        hidePreloader();
    });
    ///////////////////////////////////////////////////////////////////
    //////////////////////////////////
    // Load ALL Menu



    if (getDevice() != 'desktop') {
        $('.messaging_container .whatsapp').attr('href', 'https://api.whatsapp.com/send?phone=+971521043640');
        $('.messaging_container_product .whatsapp').attr('href', 'https://api.whatsapp.com/send?phone=+971521043640')

    }

    // if(getDevice()=='desktop') {   
    //     $('.messaging_btn_product').css('display','none');
    //     $('.messaging_btn').css('display','none');

    // }







    //////////////////
    // Menu //
    /////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////







    if ($(window).width() < 780) {
        //////////////////////
        // For Mobile only //
        //////////////////////////////////////////////////////////////////////////////////////


        // i comment this out for the purpose of mobile

        // $('#nav-menu-container-mobile .btn.open_menu').click(function(){
        //     // $('#nav-menu-container').slideDown();
        // });

        $('#show_category_menu').addClass('menu-has-children');

        $('.nav-menu').on('click', 'li.menu-has-children > a', function (e) {

            e.preventDefault();
            // e.stopPropagation();

            cnsl('nav-menu');

            var $this = $(this).closest('a');

            $this.closest('li').children('ul').toggleClass('show_group');

            var $li = $this.closest('li');
            $li.find('> a .caret_down').toggleClass('hide_group');
            $li.find('> a .caret_right').toggleClass('hide_group');

        });

        // lazyload - disabled because the menu is already
        //////////////////////////////////////////
        $('#header').on('click', '.categories > li > ul > li > a', function () {
            var ul = $(this).closest('li').find('ul');
            ul.find('img').each(function () {
                var src = $(this).attr('data-src');
                $(this).attr('src', src);
            });
        });
        //////////////////////////////////////////
        // lazyload



        //////////////////////////////////////////////////////////////////////////////////////
        // For Mobile only //
        ////////////////////

    } else {


        ////////////////////////////////////////////////
        // For Desktop only If more than 780px width //
        //////////////////////////////////////////////////////////////////////////////////////


        // Issue were when you hover directly to another Main link the tabs wont close
        $('.nav-menu').on('mouseenter', '> li ', function (e) {
            if (!$(this).attr('class') || $(this).attr('class') == 'current_page') {

                // Close products tab
                $('#products_menu').addClass('hide_group');
                $('#show_category').closest('li').removeClass('menu-active');
                // close sub groups in category products
                $('.sub_group').addClass('hide_group');
                $('.product_group li a').removeClass('selected');

                // Close Company tabs
                $('.company_menu').closest('li').removeClass('menu-active');
                $('#company_menu_container > .menu').addClass('hide_group');

                $('.downloads_menu').closest('li').removeClass('menu-active');
                $('#downloads_menu_container > .menu').addClass('hide_group');

            }

        });


        // Show Menus
        //////////////////////////////////////////////////////////////
        $('.show_menu').hoverIntent({
            over: function (e) {
                var target_menu = $(this).attr('data-target_menu_container');
                $(target_menu + ' .menu').removeClass('hide_group');
                $(this).closest('li').addClass('menu-active');

                // Close category products menu
                $('#products_menu').addClass('hide_group');
                $('#show_category').closest('li').removeClass('menu-active');

            },
            out: function (e) {

            },
            timeout: 500
        });



        $('#header').on('mouseleave', '.menu_container', function (e) {
            $(this).find('.menu').addClass('hide_group');
            var parent_menu = $(this).find('.menu').attr('data-parent_menu');
            $(parent_menu).closest('li').removeClass('menu-active');

            // close sub groups in category products
            $('.sub_group').addClass('hide_group');
            $('.product_group li a').removeClass('selected');
        });


        $('#header').on('mouseenter', '.menu_container .menu .group_by > div > a', function (e) {
            var show_menu = $(this).closest('div').attr('data-show_menu');
            var menu = $(this).closest('.menu');

            menu.find('.group_by > div').removeClass('selected');
            $(this).closest('div').addClass('selected');

            menu.find('.menu_group').addClass('hide_group');
            menu.find('.menu_group.' + show_menu).removeClass('hide_group');
        });

        // so that when mouse over top menu will close
        $('#header').on('mouseenter', '#topbar', function () {
            $('.menu').addClass('hide_group');
            $('.show_menu').closest('li').removeClass('menu-active');
            $('#products_menu').addClass('hide_group');
            $('#show_category').closest('li').removeClass('menu-active');
        });
        //////////////////////////////////////////////////////////////
        // Show Menus





        // Show Categories and Products
        //////////////////////////////////////////////////////////////
        $('#show_category').hoverIntent({
            over: function (e) {
                $('#products_menu').removeClass('hide_group');
                $('#show_category').closest('li').addClass('menu-active');

                // Close other Menus
                $('.menu').addClass('hide_group');
                $('.show_menu').closest('li').removeClass('menu-active');
            },
            out: function (e) {

            },
            timeout: 500
        });


        $('#header').on('mouseleave', '#products_menu_container', function (e) {
            $('#products_menu').addClass('hide_group');
            $('#show_category').closest('li').removeClass('menu-active');

            // close sub groups in category products
            $('.sub_group').addClass('hide_group');
            $('.product_group li a').removeClass('selected');
        });


        $('#header').on('mouseenter', '#products_menu_container .group_by > div > span', function (e) {
            var show_menu = $(this).closest('div').attr('data-show_menu');

            $('.group_by > div').removeClass('selected');
            $(this).closest('div').addClass('selected');

            $('.product_group').addClass('hide_group');
            $('.product_group.' + show_menu).removeClass('hide_group');
        });


        // When selecting a product in product menu        
        $('#header').on('click', '.product_group > li > a, .product_group > li > a > span', function (e) {
            e.preventDefault();
            e.stopPropagation();

            var $this = $(this).closest('a');

            var category_subgroup = $this.attr('data-category_subgroup');

            $('.product_group > li > a').removeClass('selected');
            $this.addClass('selected');

            $('.sub_group:not(.' + category_subgroup + ')').addClass('hide_group');

            // Display to left if overflow
            var max_width = $('body').outerWidth() * 0.5;
            var left = $this.offset().left;
            if (max_width < left) {
                $('.sub_group.' + category_subgroup).addClass('left_pos');
            } else {
                $('.sub_group.' + category_subgroup).removeClass('left_pos');
            }

            $('.sub_group.' + category_subgroup).toggleClass('hide_group');
        });


        // So that when li is click it will hide the Category Products box
        $('#header').on('click', 'ul.product_group, .product_group > li', function (e) {

            if ($(e.target).is('.item-list-category') ||
                $(e.target).is('ul.product_group') ||
                $(e.target).is('li')) {

                e.preventDefault();
                e.stopPropagation();
                $('.sub_group').addClass('hide_group');

            }
        });
        //////////////////////////////////////////////////////////////
        // Show Categories and Products



        // Stick the header at top on scroll
        /////////////////////////////////////////////////////////////////////////
        $("#header").sticky({
            topSpacing: 0,
            zIndex: '199'
        });
        /////////////////////////////////////////////////////////////////////////
        // Stick the header at top on scroll









        // lazyload - disabled because the menu is already
        //////////////////////////////////////////
        $("#header").on('mouseenter', '#products_menu_container #products_menu .product_group li', function (e) {
            $(this).find('img').each(function () {
                var src = $(this).attr('data-src');
                $(this).attr('src', src);
            });
        });
        //////////////////////////////////////////
        // lazyload


        //////////////////////////////////////////////////////////////////////////////////////
        // For Desktop only //
        /////////////////////
    }





    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    // Menu //
    ////////////////












    // Detect IE 11 and then do stuff
    /////////////////////////////////////////////////////////////////////////
    var browser_ver = detectBrowser();
    if (browser_ver == 'IE <=10' || browser_ver == 'IE 11') {

        cnsl('Interpolating');

        // Interpolating images for IE
        // $('footer img, #logo img').bicubicImgInterpolation({
        //     crossOrigin: 'anonymous' //otherwise browser security error is triggered
        // });

    }

    // Interpolating images for IE
    // $('footer img, #logo img').bicubicImgInterpolation({
    //     crossOrigin: 'anonymous' //otherwise browser security error is triggered
    // });    
    /////////////////////////////////////////////////////////////////////////
    // Detect IE 11 and then do stuff

































    // stop video playing when modal is hidden
    /////////////////////////////////////////////////////
    $('.youtube_video_modal').on('hidden.bs.modal', function (e) {
        $(this).find('iframe').attr('src', "");
    })
    /////////////////////////////////////////////////////
    // stop video playing when modal is hidden


    $('.floating-icons .icons.open_floating_container').click(function () {

        // $('.floating-icons .icons').removeClass('hover');
        $(this).siblings().removeClass('hover');
        $(this).toggleClass('hover');

        var target = $(this).attr('data-target');

        $('.floating-icons .floating-container:not(.' + target + ')').stop().hide();
        $('.floating-icons .floating-container.' + target + '').stop().fadeToggle();
    });












    $('.open_search').mouseover(function () {
        $('#search_container').show();
        $('#search_container input').focus();
        $("#nav-menu-container").animate({
            scrollTop: $('#nav-menu-container').prop("scrollHeight")
        }, 1000);
    });

    $('.open_search').mouseout(function () {
        if ($('#search_container input').val() === '') {
            $('#search_container').hide();
        } else {
            $('#search_container').show();
        }
    });

    $('.search_container').mouseover(function () {
        $('#search_container').show();
    });

    $('.search_container').mouseout(function () {
        if ($('#search_container input').val() === '') {
            $('#search_container').hide();
        } else {
            $('#search_container').show();
        }
    });

    $('#search_container input').keydown(function (e) {
        if (e.keyCode == 27) {
            $('#search_container').hide();
        }
    });

    // $('.section_title').mouseover(function(){
    //     if($('#search_container input').val() === '') {
    //         $('#search_container').hide();
    //     } else {
    //         $('#search_container').show();
    //     }
    // });

    // $('#intro').mouseover(function(){
    //     if($('#search_container input').val() === '') {
    //         $('#search_container').hide();
    //     } else {
    //         $('#search_container').show();
    //     }
    // });

    $('.open_search').click(function () {
        window.location.href = $superbase + 'search';
    });








    // when body is clicked
    // bodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclick
    // bodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclick

    if ($(window).width() < 780) {
        $('body').click(function (e) {

            if (!$(e.target).closest('#nav-menu-container').length &&
                !$(e.target).closest('#nav-menu-container-mobile').length) {

                // close other menu
                $('.menu-has-children > ul').not($(this).next('ul')).removeClass('show_group');
                $('.menu-has-children > a .caret_down').not($(this).children('.caret_down')).addClass('hide_group');
                $('.menu-has-children > a .caret_right').not($(this).children('.caret_right')).removeClass('hide_group');

                $('#nav-menu-container').hide();

            }

        });
    } else {
        $('body').click(function (e) {

            // // Main Menu
            // if(!$(e.target).closest('.nav-menu').length) {

            //     // Menu
            //     if(!$(e.target).closest('#products_menu_container').length) {
            //         $('.product_group > li > a').removeClass('selected');
            //         $('.sub_group').addClass('hide_group');
            //         $('#products_menu').addClass('hide_group');
            //         $('.nav-menu > li').removeClass('menu-active');

            //         cnsl('menu');
            //     }

            //     // Sub Menu
            //     if(!$(e.target).closest('.product_group').length) {
            //         $('.product_group > li > a').removeClass('selected');
            //         $('.sub_group').addClass('hide_group');
            //     }

            // Search Bar
            if (!$(e.target).closest('.open_search').length &&
                !$(e.target).closest('#search_container').length) {

                $('#search_container').hide();
            }
            //     cnsl(e.target);

            //     cnsl('nav-menu = '+$(e.target).closest('.nav-menu').length);
            //     cnsl('product_group = '+$(e.target).closest('.product_group').length);
            //     cnsl('products_menu_container = '+$(e.target).closest('.products_menu_container').length);
            //     cnsl('open_search = '+$(e.target).closest('.open_search').length);
            //     cnsl('search_container = '+$(e.target).closest('.search_container').length);
            // }

        });
    }
    // bodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclick
    // bodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclick
    // when body is clicked



    // Videos
    ////////////////////////////////////////////////////////////////
    $('.video_intro_img').click(function () {
        var video_url = $(this).attr('data-video_url');
        var target = $(this).attr('data-target');
        $(target + ' iframe').attr('src', video_url);
    });


    $('.change_language').click(function () {
        var video_url = $(this).attr('data-video_url');
        var target = $(this).attr('data-target');
        $(target + ' iframe').attr('src', video_url);
    });
    ////////////////////////////////////////////////////////////////
    // Videos






    // Anchor
    ///////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    if (typeof anchor !== 'undefined' && anchor) {
        anchor = anchor.toLowerCase();
        cnsl('anchor = ' + anchor);

        // $('.document h3').not('.'+term).next('ul').hide();
        // $('.document h3#'+anchor).next('ul').show();
        // $('.document h3#'+anchor).find('.caret_down').addClass('hide_group');
        // $('.document h3#'+anchor).find('.caret_right').removeClass('hide_group');

        var topMargin = 0;
        if ($(window).width() < 780)
            topMargin = -100;
        else
            topMargin = -100;

        if ($('#' + anchor).offset()) {
            $("html, body").delay(500).animate({
                scrollTop: $('#' + anchor).offset().top + topMargin,
            }, 1000);
        }

        if (anchor == "video") {
            $('.video_intro_img').trigger('click');
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////
    // Anchor

    // for hint /////////////////////////////////////////////////////////////////////////////////////

    //determine hour on day 

    function Timer(callback, delay) {
        var timerId, start, remaining = delay;

        this.pause = function() {
            window.clearTimeout(timerId);
            remaining -= new Date() - start;
        };

        this.resume = function() {
            start = new Date();
            window.clearTimeout(timerId);
            timerId = window.setTimeout(callback, remaining);
        };

        this.resume();
    }

    var timer = new Timer(function() {
         setTime();
    }, 8000);


    var catalog_page = getCookie('is_catalog_page');
    var product_page = getCookie('is_product_page');



    if(catalog_page === 'yes' || product_page === 'yes') {

        setTimeout(function () {

            var hr = (new Date()).getHours();

            if(1 < hr && hr < 12) {

                var morningHint = getCookie('morning_hint_cookie');

                if( morningHint === '')
                {
                    setCookie('morning_hint_cookie', 'yes', '1');
                    $(".custom-social-proof").stop().slideDown('slow');
                }
                
            } 

             if(13 < hr && hr < 23) {

                var afternoonHint = getCookie('afternoon_hint_cookie');

                if(afternoonHint === '') {
                    setCookie('afternoon_hint_cookie', 'yes', '1');
                    $(".custom-social-proof").stop().slideDown('slow');
                }
            }
            
        }, 3000);

    }
 

    $(".custom-close").click(function () {
        $(".custom-social-proof").stop().slideUp('slow');
    });

    function setTime() {
           setTimeout(function () {
            $(".custom-social-proof").stop().slideUp('slow');
        }, 3000);
    }

    $(".custom-social-proof").mouseover(function(){
          timer.pause();
    });


    $(".custom-social-proof").mouseleave(function(){
        timer.resume();
    });

    ////////////////////////////////////////////////////////////////////////////////////// end of the hint


    // Cookie agree
    ////////////////////////////////////////////////////////
    $('.cookie-close').click(function () {
        //   $('#cookie-modal').fadeOut();
        setCookie('almani_agree_cookie', 'yes', '60');
        $('#cookie-modal').fadeOut();
        $('#cookie-modal').removeClass('slideInUp').addClass('slideOutDown');
    });


    // Hint Show 

    $('.hint-alert').addClass('slideInRight');

    showCookieModal();

    // $('#btn-cookie-agree').click(function(){
    //    setCookie('almani_agree_cookie','yes');
    //    $('#cookie-modal').fadeOut();
    // });

    function showCookieModal() {
        var cookie = getCookie('almani_agree_cookie');
        if (!cookie || cookie === 'undefined') {

            setTimeout(function () {
                $('#cookie-modal').addClass('slideInUp').show();
            }, 2000);

        }
    }






    ////////////////////////////////////////////////////////
    // Cookie agree







 $('.popup__close').click(function() {
    $('#popup').removeClass('popup_show');
});

setTimeout(function(){

         var isDonewithOffer = getCookie('promotions');

        console.log(isDonewithOffer);

         if(!isDonewithOffer) {
            $('#popup').addClass('popup_show');   
         }else{
            $('body').addClass(isDonewithOffer);

         }

}, 3000);




$('.card-deck').click(function(e){

    var className = $(e.target).closest('.card-size').attr('class').split(' ')[1]

    // console.log(className);

    $('body').addClass(className)
    setCookie('promotions', className, '60')
    $('#popup').removeClass('popup_show');


})















    // // Load Menu
    // //////////////////////////////////
    // ///////////////////////////////////////////////////////////////////
    // $.each($glb_menu_urls, function( key, val ) {
    //     $.get(val, function(data){
    //         content= data;
    //         $('#'+key+'_here').replaceWith(content);
    //     });
    // });
    // ///////////////////////////////////////////////////////////////////
    // //////////////////////////////////
    // // Load Menu


    // // Load Mobile Menu
    // //////////////////////////////////
    // ///////////////////////////////////////////////////////////////////
    // $.each($glb_mobile_menu_urls, function( key, val ) {
    //     $.get(val, function(data){
    //         content= data;
    //         $('#'+key+'_here').html(content);
    //     });
    // });
    // ///////////////////////////////////////////////////////////////////
    // //////////////////////////////////
    // // Load Mobile Menu










        console.log(document.getElementById('main'));

    // For Mobile Menu Only
    var slideout = new Slideout({
        'panel': document.getElementById('main'),
        'menu': document.getElementById('nav-menu-container-for-mobile'),
        'padding': 256,
        'tolerance': 70,
        'touch': false
    });

    $('.js-slideout-toggle').click(function () {
        slideout.open();
    });

    slideout
        .on('beforeopen', function () {
            this.panel.classList.add('panel-open');
        })
        .on('open', function () {
            this.panel.addEventListener('click', close);
        })
        .on('beforeclose', function () {
            this.panel.classList.remove('panel-open');
            this.panel.removeEventListener('click', close);
        });

    $('.open_menu').click(function () {
        slideout.toggle();
        if ($('#nav-menu-container-for-mobile').hasClass('d-none')) {
            $('#nav-menu-container-for-mobile').removeClass('d-none');
            $('.floating-icons').addClass('fadeOut');
            $('.floating-icons').removeClass('fadein');
            $('#body').addClass('padding-bottom-reset');
        } else {
            setTimeout(function () {
                $('#nav-menu-container-for-mobile').addClass('d-none');
                $('.floating-icons').removeClass('fadeOut');
                $('.floating-icons').addClass('fadein');
                $('#body').removeClass('padding-bottom-reset');
            }, 300);
        }
    });

    function close(eve) {
        eve.preventDefault();
        slideout.close();
        setTimeout(function () {
            $('#nav-menu-container-for-mobile').addClass('d-none');
            $('.floating-icons').removeClass('fadeOut');
            $('.floating-icons').addClass('fadein');
            $('#body').removeClass('padding-bottom-reset');
        }, 300);
    }

    $(".menu-mobile").on("click", '.ddd-menu', function () {

        var $this = $(this);
        $(this).next(".menu-list-down").slideToggle().siblings(".menu-list-down").slideUp();
        if ($(this).hasClass('open')) {
            $this.find(".toggles-info").children("i").addClass('fa-caret-right').removeClass('fa-sort-down');
            $this.removeClass('open');
        } else {
            $this.find(".toggles-info").children("i").addClass('fa-sort-down').removeClass('fa-caret-right');
            $this.addClass('open')
        }

        $(this).next('ul').find('img').each(function () {
            var src = $(this).attr('data-src');
            $(this).attr('src', src);
        });

    });


    $(".menu-mobile").on("click", ".dd-menu", function () {
        var $this = $(this);
        $(this).next(".menu-list-down").slideToggle().siblings(".menu-list-down").slideUp();
        if ($(this).hasClass('open')) {
            $(this).find(".toggle-info").children("i").addClass('fa-caret-right').removeClass('fa-sort-down');
            $(this).removeClass('open');
        } else {
            $(this).find(".toggle-info").children("i").addClass('fa-sort-down').removeClass('fa-caret-right');
            $(this).addClass('open')
        }
        console.log('dd-menu')
    });

    // for Mobile Menu Only









});
// docdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdoc
// docdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdoc
// docdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdoc
// Document Ready 




























































function wtyMulti(wty_dflt) {

    var wty_multi = [];

    // 5 years is applicable for almani only.
    if (wty_dflt == 5) {
        wty_multi['5_yrs'] = 1;
    }

    // old formula
    // if(wty_dflt==3) {
    //     wty_multi['5_yrs'] = 1.9;
    //     wty_multi['3_yrs'] = 1;
    //     wty_multi['2_yrs'] = 0.9;
    //     wty_multi['1_yrs'] = 0.9*0.95;
    // }
    // if(wty_dflt==2) {
    //     wty_multi['5_yrs'] = 2.8;
    //     wty_multi['3_yrs'] = 1.8;
    //     wty_multi['2_yrs'] = 1;
    //     wty_multi['1_yrs'] = 0.95;
    // }

    // if(wty_dflt==1) {
    //     wty_multi['5_yrs'] = 2.8/0.95;
    //     wty_multi['3_yrs'] = 1.8/0.95;
    //     wty_multi['2_yrs'] = 1.0/0.95;
    //     wty_multi['1_yrs'] = 1.0;
    // }


    if (wty_dflt == 3) {
        wty_multi['5_yrs'] = 1.45;
        wty_multi['3_yrs'] = 1;
        wty_multi['2_yrs'] = 0.9;
        wty_multi['1_yrs'] = 0.8;
    }

    if (wty_dflt == 2) {
        wty_multi['5_yrs'] = 1.5;
        wty_multi['3_yrs'] = 1.2;
        wty_multi['2_yrs'] = 1;
        wty_multi['1_yrs'] = 0.90;
    }

    // Not sure if there is a 1 year default warranty
    if (wty_dflt == 1) {
        wty_multi['5_yrs'] = 1.5 / 0.90;
        wty_multi['3_yrs'] = 1.2 / 0.90;
        wty_multi['2_yrs'] = 1.0 / 0.90;
        wty_multi['1_yrs'] = 1;
    }

    return wty_multi;
}













































/*** sniff the UA of the client and show hidden div's for that device ***/
function getDevice() {
    var retVal = '';
    var ua = navigator.userAgent;
    var checker = {
        iphone: ua.match(/(iPhone|iPod|iPad)/),
        blackberry: ua.match(/BlackBerry/),
        android: ua.match(/Android/)
    };
    if (checker.android) {
        retVal = 'android';
    } else if (checker.iphone) {
        retVal = 'iphone';
    } else if (checker.blackberry) {
        retVal = 'blackberry';
    } else {
        retVal = 'desktop';
    }

    return retVal;
}



































function setCookie(cname, cvalue, exdays) {

    // if(!exdays)
    if (typeof exdays === 'undefined')
        exdays = 1;

    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}


function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}






function getAndSetCurrency() {

    var currency = getCookie("almani_currency");

    if (currency == "") {
        setCookie('almani_currency', 'AED');
    } else {
        $('#currency option').each(function (i) {
            if ($(this).html() == currency)
                $(this).attr('selected', 'selected');
        });

        $('#currency').trigger('change');
    }

}






function cnsl($console) {

    var url = window.location.href;

    if (!url.indexOf("almani.ae") > -1) {
        // console.log("<< DEVELOPMENT >> ");
        console.log($console);
    } else {
        // console.log("<< LIVE >> ");
        // console.log($console);
    }

}


function detectBrowser() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    var trident = ua.indexOf('Trident/');
    var edge = ua.indexOf('Edge/');
    if (msie > 0) {
        // IE 10 or older 
        //Do some stuff
        return "IE <=10";
    } else if (trident > 0) {
        // IE 11 
        //Do some stuff
        return "IE 11";
    } else if (edge > 0) {
        // Edge 
        //Do some stuff
        return "IE EDGE";
    } else
        // other browser
        return "OTHERS";
};