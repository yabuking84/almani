$(document).ready(function () {

  // cnsl($(document).width());
  // alert($(document).width())

  showWelcomeMessage();

  // for 4 feattured product
  var swiper2 = new Swiper('.swiper-horizontal', {
    slidesPerView: 2,
    slidesPerColumn: 2,
    spaceBetween: 10,
    mousewheel: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },

    breakpoints: {

      // when window width is <= 320px     
      320: {
        slidesPerView: 2,
        slidesPerColumn: 2,
        spaceBetween: 10
      },
      // when window width is <= 480px     
      480: {
        slidesPerView: 2,
        slidesPerColumn: 2,
        spaceBetween: 10
      },

      // when window width is <= 640px     
      640: {
        slidesPerView: 2,
        slidesPerColumn: 2,
        spaceBetween: 10
      }

    }
  });

  // for 4 div price match etc.
  var swiper = new Swiper('.swiper-vertical', {
    // direction: 'vertical',
    slidesPerView: 1,
    autoHeight: true,
    spaceBetween: 30,
    mousewheel: true,
    loop: true,
    centeredSlides: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },

    breakpoints: {

      // when window width is <= 320px     
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      // when window width is <= 480px     
      480: {
        slidesPerView: 1,
        spaceBetween: 20
      },

      // when window width is <= 640px     
      640: {
        slidesPerView: 1,
        spaceBetween: 20
      }
    }
  });

  // Intro background carousel
  $("#intro-carousel").owlCarousel({
    autoplay: true,
    dots: false,
    loop: true,
    rewind: false,
    animateOut: 'fadeOut',
    autoplayTimeout: 6000,
    items: 1
  });

  $('#strap_lines').owlCarousel({
    loop: true,
    rewind: false,
    nav: false,
    items: 1,
    dots: false,
    rewind: false,
    mouseDrag: false,
    autoplay: true,
    autoplayTimeout: 4000,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    margin: 100
  })


  $(window).scroll(function () {
    if ($(window).scrollTop() >= 394) {
      $('#header').hide();
      $('#search').addClass('fix-search');
    } else {
      $('#header').show();
      $('#search').removeClass('fix-search');
    }
  });
  

  $(".section-content").hide();
  $(".open").show();

  $('.section-header.choose_us').click(function(){

    var $youTubeContainer = $('#video-item iframe');
    var isYouTubeOpen = $youTubeContainer.attr('data-open');

      if(isYouTubeOpen===true) {
        $youTubeContainer.attr("src","");
        $youTubeContainer.attr('data-open','close');
      } else {
        $youTubeContainer.attr('data-open','close');
        $youTubeContainer.attr("src","https://www.youtube.com/embed/7-7jbUNA4PQ");
      }
  });

  // for toggling the div
  $(".section-header").click(function () {
    var $this = $(this);

    $(this).next(".section-content").slideToggle().siblings(".section-content").slideUp();

    // icon to the left
    if ($("i").hasClass("fa-sort-down")) {
      $(this).find("i").toggleClass("fa-sort-up");
    }

    // scroll to top when certain div is click
    $('html, body').animate({
      scrollTop: $($this).offset().top - 60
    }, 800);
  });





  $('.change_languaged').click(function () {
    var video = $(this).attr('data-video-url');
    $('#video-item iframe').attr('src', video);
  })

  // welcome almani function
  // change this @ october 13 2018
  // place at the top
  // before it was sticky div at the top

  $('.btn-more').click(function () {
      var $this = $(this);
      var isRead = $this.attr('data-read');

      if(isRead == 'false') {
        $('.content-message .more-welcome-messages').slideDown('fast');
        $this.attr('data-read','true').html('Read Less');
      } else {
        $('.content-message .more-welcome-messages').slideUp('slow');
        $this.attr('data-read','false').html('Read More');
      }

  });


  function showWelcomeMessage() {
    var isAlreadyRead = getCookie('welcome_cookie');
    if (isAlreadyRead == 'yes') {
      $('.welcome-message').addClass('d-none');
    }
  }

  var slideout = new Slideout({
    'panel': document.getElementById('main'),
    'menu': document.getElementById('nav-menu-container-for-mobile'),
    'padding': 256,
    'tolerance': 70,
    'touch':false
  });

  
  $('.js-slideout-toggle').click(function(){
    slideout.open();
  });

  slideout
  .on('beforeopen', function() {
    this.panel.classList.add('panel-open');
  })
  .on('open', function() {
    this.panel.addEventListener('click', close);
  })
  .on('beforeclose', function() {
    this.panel.classList.remove('panel-open');
    this.panel.removeEventListener('click', close);
  });

  function close(eve) {
    eve.preventDefault();
    slideout.close();
    $('#body').removeClass('padding-bottom-reset');
    $('.video_container').removeClass('padding-video-container-reset');
  }

  $('.open_menus').click(function(){
    slideout.toggle();
    $('#body').toggleClass('padding-bottom-reset');
    $('.video_container').toggleClass('padding-video-container-reset');
  });
 
  $(".menu-mobile").on("click",'.ddd-menu',function() {

    var $this = $(this);
    $(this).next(".menu-list-down").slideToggle().siblings(".menu-list-down").slideUp();
    if($(this).hasClass('open')) {
       $this.find(".toggles-info").children("i").addClass('fa-caret-right').removeClass('fa-sort-down');
        $this.removeClass('open');
      } else {
        $this.find(".toggles-info").children("i").addClass('fa-sort-down').removeClass('fa-caret-right');
        $this.addClass('open')
      }

      $(this).next('ul').find('img').each(function(){
        var src = $(this).attr('data-src');
        $(this).attr('src',src);
     });
      
  });
  

    $(".menu-mobile").on("click",".dd-menu",function() {
      var $this = $(this);
      $(this).next(".menu-list-down").slideToggle().siblings(".menu-list-down").slideUp();
        if($(this).hasClass('open')) {
          $(this).find(".toggle-info").children("i").addClass('fa-caret-right').removeClass('fa-sort-down');
          $(this).removeClass('open');
        } else {
          $(this).find(".toggle-info").children("i").addClass('fa-sort-down').removeClass('fa-caret-right');
          $(this).addClass('open')
        }
        console.log('dd-menu')
    });

    $('#approach-read-more').click(function(){

        var $this = $(this);        
        var isRead = $(this).attr('data-read');

        if(isRead == 'false') {
          $('.second-par').slideDown('slow');
          $this.attr('data-read','true').html('Read Less');
          $('.dot-effect').fadeOut('fast');
        } else {
          $('.second-par').slideUp('slow');
          $this.attr('data-read','false').html('Read More');
          $('.dot-effect').fadeIn('fast');
        }
    });

    $('#btn-msg-read').click(function(){
        var $this = $(this);
        var isRead = $this.attr('data-read');
        
        if(isRead == 'false') {
          $('.more-message').slideDown('fast');
          $this.attr('data-read','true').html('Read Less');
          $('.dot-message').fadeOut('fast');
        } else {
          $('.more-message').slideUp('fast');
          $this.attr('data-read','false').html('Read More');
          $('.dot-message').fadeIn('fast');
        }

    });

    $('.section-header.choose-us').click(function(){
        var $YouTubeContainer = $('#video-item iframe');
        var $this = $(this).attr('data-open');
        var $videoUrl = 'https://www.youtube.com/embed/7-7jbUNA4PQ';
        if($this === 'false') {
            $(this).attr('data-open','true');
            $YouTubeContainer.attr('src', $videoUrl);
        } else {
            if($this === 'true') {
              $YouTubeContainer.attr('src', $videoUrl);
            } else {
              $YouTubeContainer.removeAttr('src');
            }
        }
    });



$('#sendlimitedenquiry').click(function() {

     var expiryDate = new Date();
     expiryDate.setMonth(expiryDate.getMonth() + 6);

     setCookie('limitedOffer', 'yes', expiryDate);
     $(this).html('Sending...');
});


   
});