$(document).ready(function(){

	// close hover when X is clicked, shows only on mobile
	$('.close_hover').click(function(){
		var $hovered = $(this).closest('.product').find('.hovered');
	    $hovered.removeClass('hover_to_left');
	    $hovered.removeClass('hover_to_right');
	    $hovered.closest('.product').removeClass('hover');

	    $hovered.closest('.product').trigger('mouseleave');

	});


	// to fix the issue when you click the X, it will not open the details anymore on the same product
    if($(window).width() < 780) {
		$('.product').click(function(){
			// console.log('product click');
	    	$(this).trigger('mouseenter');

		});
	}


});


// rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
// rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
// rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
function addHoverOnItems(items_hover_to_left,product_class,hovered_class){

	if(!items_hover_to_left)
	items_hover_to_left=3;
	if(!product_class)
	product_class='.product';
	if(!hovered_class)
	hovered_class='.hovered';

	// hover on items
	//////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////
	var hover_class = '';	
	var $hovered = {};
	$('body').on({
	    mouseenter: function () {
		    // console.log('hover in');
		    $(this).addClass('hover');
		    $hovered = $(this).closest('.column').find(hovered_class);

		    // Test if it will be off screen right 
		    //////////////////////////////////////////////
		    if(($(hovered_class+':visible').index($hovered)+1) % items_hover_to_left == 0) {
		        hover_class = 'hover_to_left';
		    } else {
		        hover_class = 'hover_to_right';
		    }
		    //////////////////////////////////////////////

		    $hovered.addClass(hover_class);
	    },
	    mouseleave: function () {
		    // console.log('hover out');
		    $hovered = $(this).closest('.column').find(hovered_class);

		    // Test if it will be off screen right 
		    //////////////////////////////////////////////
		    if(($(hovered_class+':visible').index($hovered)+1) % items_hover_to_left == 0) {
		        hover_class = 'hover_to_left';
		        
			    if($(window).width() > 780) {
			        // Fix the issue when hover to hover_to_left on desktop
			        $(this).find('.hovered').css('left','auto');
		    	}

		    } else {
		        hover_class = 'hover_to_right';
		    }
		    //////////////////////////////////////////////

		    $hovered.removeClass(hover_class);

		    $(this).removeClass('hover');

	    }
	}, product_class); //pass the element as an argument to .on

	//////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////
	// hover on items



	if ($(window).width() > 780) {
	    $(product_class+', .product-name2').click(function(e){
	        var a = $(this).closest('.column').find('a.item');
	        window.location = a.attr("href");
	        return false;
	    });
	}


}
// rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
// rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
// rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
