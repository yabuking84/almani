$(document).ready(function(){

    // Back to top button
    /////////////////////////////////////////////////////////////////////////
    // $('.floating-icons').stop().fadeIn('slow');
    // $('.back-to-top').stop().fadeIn('slow');
    $(window).scroll(function() {
      if ($(this).scrollTop() > 400) {
        $('.back-to-top').stop().fadeIn('fast');
        $('.floating-icons').stop().fadeIn('fast');
      } else {
        $('.back-to-top').stop().fadeOut('fast');
        $('.floating-icons').stop().fadeOut('fast');
      }
    });

    $('.back-to-top').click(function(){
        $('html').stop().animate(
            { scrollTop : 0 },
            1500            
        );
      return false;
    });

    /////////////////////////////////////////////////////////////////////////
    // Back to top button




// changing currency in featured 
var count=1;
$('#product-list .items .prodprice').each(function(){
   var total = 0;
   var price = 0;

   var price = parseFloat($(this).attr('data-value')); 
   total = total+price;
   
   //// currency ////////////////////////////////////////////
   total = total*$glbl_currency['EUR'];
   //////////////////////////////////////////////////////////

  total = total.toFixed(2);

  $(this).html(total + ' EUR');
  cnsl(price);
  cnsl(count++);
});


if(getDevice()!='desktop') {
    $('.messaging_container .whatsapp').attr('href','https://api.whatsapp.com/send?phone=+971521043640');
}



function getDevice(){
    var retVal = '';
    var ua = navigator.userAgent;
    var checker = {
      iphone: ua.match(/(iPhone|iPod|iPad)/),
      blackberry: ua.match(/BlackBerry/),
      android: ua.match(/Android/)
    };
    if (checker.android){
        retVal = 'android';
    }
    else if (checker.iphone){
        retVal = 'iphone';
    }
    else if (checker.blackberry){
        retVal = 'blackberry';
    }
    else {
        retVal = 'desktop';
    }

    return retVal;
}










































    //////////////////
    // Menu //
    /////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////



    
    

    
    if($(window).width() < 1024) {
    //////////////////////
    // For Mobile only //
    //////////////////////////////////////////////////////////////////////////////////////

        $('#nav-menu-container-mobile .btn.open_menu').click(function(){
            $('#nav-menu-container').fadeToggle();
        });

        $('#show_category_menu').addClass('menu-has-children');

        $('.nav-menu').on('click', 'li.menu-has-children > a, li.menu-has-children > a >span ',function(e){

            e.preventDefault();
            e.stopPropagation();

            var $this = $(this).closest('a');

            $this.closest('li').children('ul').toggleClass('show_group');
            
            var $li = $this.closest('li');
            $li.find('> a .caret_down').toggleClass('hide_group');
            $li.find('> a .caret_right').toggleClass('hide_group');

        });


    //////////////////////////////////////////////////////////////////////////////////////
    // For Mobile only //
    ////////////////////
        
    } else {


    ////////////////////////////////////////////////
    // For Desktop only If more than 780px width //
    //////////////////////////////////////////////////////////////////////////////////////


        // Issue were when you hover directly to another Main link the tabs wont close
        $('.nav-menu').on('mouseenter', '> li',function(e){
            if(!$(this).attr('class') || $(this).attr('class')=='current_page') {

                // Close products tab
                $('#products_menu').addClass('hide_group');
                $('#show_category').closest('li').removeClass('menu-active');
                // close sub groups in category products
                $('.sub_group').addClass('hide_group');
                $('.product_group li a').removeClass('selected');

                // Close Company tabs
                $('.company_menu').closest('li').removeClass('menu-active');
                $('#company_menu_container > .menu').addClass('hide_group');

            }

        });


        // Show Menus
        //////////////////////////////////////////////////////////////
        $('.show_menu').hoverIntent({
            over: function(e){          
                var target_menu = $(this).attr('data-target_menu_container');
                $(target_menu+' .menu').removeClass('hide_group');
                $(this).closest('li').addClass('menu-active');

                // Close category products menu
                $('#products_menu').addClass('hide_group');
                $('#show_category').closest('li').removeClass('menu-active');

            }, 
            out: function(e){

            },
            timeout: 500
        });



        $('.menu_container').mouseleave(function(e){
            $(this).find('.menu').addClass('hide_group');
            var parent_menu = $(this).find('.menu').attr('data-parent_menu');
            $(parent_menu).closest('li').removeClass('menu-active');

            // close sub groups in category products
            $('.sub_group').addClass('hide_group');
            $('.product_group li a').removeClass('selected');
        });


        $('.menu_container .menu .group_by > div > a').mouseenter(function(e){
            var show_menu = $(this).closest('div').attr('data-show_menu');

            $('.menu_container .menu .group_by > div').removeClass('selected');
            $(this).closest('div').addClass('selected');

            $('.menu_group').addClass('hide_group');
            $('.menu_group.'+show_menu).removeClass('hide_group');
        });

        // so that when mouse over top menu will close
        $('#topbar').mouseenter(function(){
            $('.menu').addClass('hide_group');
            $('.show_menu').closest('li').removeClass('menu-active');
        });
        //////////////////////////////////////////////////////////////
        // Show Menus





        // Show Categories and Products
        //////////////////////////////////////////////////////////////
        $('#show_category').hoverIntent({
            over: function(e){
                $('#products_menu').removeClass('hide_group');
                $('#show_category').closest('li').addClass('menu-active');

                // Close other Menus
                $('.menu').addClass('hide_group');
                $('.show_menu').closest('li').removeClass('menu-active');
            }, 
            out: function(e){

            },
            timeout: 500
        });

        $('#products_menu_container').mouseleave(function(e){
            $('#products_menu').addClass('hide_group');
            $('#show_category').closest('li').removeClass('menu-active');

            // close sub groups in category products
            $('.sub_group').addClass('hide_group');
            $('.product_group li a').removeClass('selected');
        });

        // so that when mouse over top menu will close
        $('#topbar').mouseenter(function(){
            $('#products_menu').addClass('hide_group');
            $('#show_category').closest('li').removeClass('menu-active');
        });


        $('#products_menu_container .group_by > div > span').mouseenter(function(e){
            var show_menu = $(this).closest('div').attr('data-show_menu');

            $('.group_by > div').removeClass('selected');
            $(this).closest('div').addClass('selected');

            $('.product_group').addClass('hide_group');
            $('.product_group.'+show_menu).removeClass('hide_group');
        });


        // When selecting a product in product menu        
        $('.product_group > li > a, .product_group > li > a > span').click(function(e){
            e.preventDefault();
            e.stopPropagation();

            var $this = $(this).closest('a');

            var category_subgroup = $this.attr('data-category_subgroup');

            $('.product_group > li > a').removeClass('selected');
            $this.addClass('selected');

            $('.sub_group:not(.'+category_subgroup+')').addClass('hide_group');

            // Display to left if overflow
            var max_width = $('body').outerWidth() * 0.6;
            var left = $this.offset().left;
            if(max_width < left) {
                $('.sub_group.'+category_subgroup).addClass('left_pos');
            } else {
                $('.sub_group.'+category_subgroup).removeClass('left_pos');
            }

            $('.sub_group.'+category_subgroup).toggleClass('hide_group');
        });


        // So that when li is click it will hide the Category Products box
        $('ul.product_group, .product_group > li').click(function(e){

            cnsl(e.target);
            cnsl($(e.target).attr('itemprop'));

            if($(e.target).attr('itemprop')=='itemListElement' || 
                $(e.target).is('ul.product_group') ||
                $(e.target).is('li')) {

                e.preventDefault();
                e.stopPropagation();
                $('.sub_group').addClass('hide_group');

                cnsl('hideall');
            }
        });
        //////////////////////////////////////////////////////////////
        // Show Categories and Products



        // Stick the header at top on scroll
        /////////////////////////////////////////////////////////////////////////
        $("#header").sticky({topSpacing:0, zIndex: '50'});
        /////////////////////////////////////////////////////////////////////////
        // Stick the header at top on scroll


    //////////////////////////////////////////////////////////////////////////////////////
    // For Desktop only //
    /////////////////////
    }









    // lazyload
    //////////////////////////////////////////
    $("#products_menu_container #products_menu .product_group li").on('mouseenter',function(e){
        $(this).find('img').each(function(){
            var src = $(this).attr('data-src');
            $(this).attr('src',src);
        });
    });

    // for Mobile
    $('.categories > li > ul > li').click(function(){
        $(this).find('img').each(function(){
            var src = $(this).attr('data-src');
            $(this).attr('src',src);
        });        
    });
    //////////////////////////////////////////
    // lazyload


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    // Menu //
    ////////////////



  // Load Mobile Menu
    //////////////////////////////////
    ///////////////////////////////////////////////////////////////////
    $.each($glb_mobile_menu_urls, function( key, val ) {
        $.get(val, function(data){
            content= data;
            $('#'+key+'_here').html(content);
        });
        hidePreloader();
    });
    ///////////////////////////////////////////////////////////////////
    //////////////////////////////////
    // Load Mobile Menu



      







    // Detect IE 11 and then do stuff
    /////////////////////////////////////////////////////////////////////////
    var browser_ver = detectBrowser();    
    if(browser_ver=='IE <=10' || browser_ver=='IE 11') {

        cnsl('Interpolating');

        // Interpolating images for IE
        // $('footer img, #logo img').bicubicImgInterpolation({
        //     crossOrigin: 'anonymous' //otherwise browser security error is triggered
        // });

    }

    // Interpolating images for IE
    // $('footer img, #logo img').bicubicImgInterpolation({
    //     crossOrigin: 'anonymous' //otherwise browser security error is triggered
    // });    
    /////////////////////////////////////////////////////////////////////////
    // Detect IE 11 and then do stuff

































    // stop video playing when modal is hidden
    /////////////////////////////////////////////////////
    $('.youtube_video_modal').on('hidden.bs.modal', function (e) {
        $(this).find('iframe').attr('src',"");      
    })
    /////////////////////////////////////////////////////
    // stop video playing when modal is hidden


    $('.floating-icons .icons.open_floating_container').click(function(){

        // $('.floating-icons .icons').removeClass('hover');
        $(this).siblings().removeClass('hover');
        $(this).toggleClass('hover');

        var target = $(this).attr('data-target');

        $('.floating-icons .floating-container:not(.'+target+')').stop().hide();
        $('.floating-icons .floating-container.'+target+'').stop().fadeToggle();
    });












    $('.open_search').mouseover(function(){
        $('#search_container').show();
        $('#search_container input').focus();
        $("#nav-menu-container").animate({ scrollTop: $('#nav-menu-container').prop("scrollHeight")}, 1000);
    });


    $('.open_search').mouseout(function(){
        if ($('#search_container input').val() === '') {
            $('#search_container').hide();
            $("#nav-menu-container").animate({ scrollTop: $('#nav-menu-container').prop("scrollHeight")}, 1000);
        } else {
            $('#search_container').show();
            $("#nav-menu-container").animate({ scrollTop: $('#nav-menu-container').prop("scrollHeight")}, 1000);
        }
    });

    $('.open_search').click(function(){
        window.location.href = $superbase + 'search';
    });








    // when body is clicked
    // bodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclick
    // bodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclick

    if($(window).width() < 780) {
        $('body').click(function(e){

            if(!$(e.target).closest('#nav-menu-container').length && 
                !$(e.target).closest('#nav-menu-container-mobile').length) {

                // close other menu
                $('.menu-has-children > ul').not($(this).next('ul')).removeClass('show_group');
                $('.menu-has-children > a .caret_down').not($(this).children('.caret_down')).addClass('hide_group');
                $('.menu-has-children > a .caret_right').not($(this).children('.caret_right')).removeClass('hide_group');

                $('#nav-menu-container').hide();

            }

        });
    } else {
        $('body').click(function(e){

            // // Main Menu
            // if(!$(e.target).closest('.nav-menu').length) {

            //     // Menu
            //     if(!$(e.target).closest('#products_menu_container').length) {
            //         $('.product_group > li > a').removeClass('selected');
            //         $('.sub_group').addClass('hide_group');
            //         $('#products_menu').addClass('hide_group');
            //         $('.nav-menu > li').removeClass('menu-active');

            //         cnsl('menu');
            //     }

            //     // Sub Menu
            //     if(!$(e.target).closest('.product_group').length) {
            //         $('.product_group > li > a').removeClass('selected');
            //         $('.sub_group').addClass('hide_group');
            //     }

                // Search Bar
                if(!$(e.target).closest('.open_search').length && 
                    !$(e.target).closest('#search_container').length ) {

                    $('#search_container').hide();
                }
            //     cnsl(e.target);

            //     cnsl('nav-menu = '+$(e.target).closest('.nav-menu').length);
            //     cnsl('product_group = '+$(e.target).closest('.product_group').length);
            //     cnsl('products_menu_container = '+$(e.target).closest('.products_menu_container').length);
            //     cnsl('open_search = '+$(e.target).closest('.open_search').length);
            //     cnsl('search_container = '+$(e.target).closest('.search_container').length);
            // }
        
        });
    }
    // bodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclick
    // bodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclickbodyclick
    // when body is clicked



    // Videos
    ////////////////////////////////////////////////////////////////
    $('.video_intro_img').click(function(){
        var video_url = $(this).attr('data-video_url');
        var target = $(this).attr('data-target');
        $(target+' iframe').attr('src',video_url);
    });


    $('.change_language').click(function(){
        var video_url = $(this).attr('data-video_url');        
        var target = $(this).attr('data-target');
        $(target+' iframe').attr('src',video_url);
    });
    ////////////////////////////////////////////////////////////////
    // Videos






    // Anchor
    ///////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    if(anchor) {
        anchor = anchor.toLowerCase();
        cnsl('anchor = '+anchor);

        // $('.document h3').not('.'+term).next('ul').hide();
        // $('.document h3#'+anchor).next('ul').show();
        // $('.document h3#'+anchor).find('.caret_down').addClass('hide_group');
        // $('.document h3#'+anchor).find('.caret_right').removeClass('hide_group');

        var topMargin = 0;
        if($(window).width() < 780)
        topMargin = -100;
        else    
        topMargin = -100;

        if ($('#'+anchor).offset()) {            
            $("html, body").delay(500).animate({ 
                scrollTop: $('#'+anchor).offset().top+topMargin,
            }, 1000);
        }

        if(anchor=="video") {
            $('.video_intro_img').trigger('click');
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////
    // Anchor

    


    // Cookie agree
    ////////////////////////////////////////////////////////
    $('.cookie-close').click(function(){
     //   $('#cookie-modal').fadeOut();
        setCookie('almani_agree_cookie','yes','60');
        $('#cookie-modal').fadeOut();
        $('#cookie-modal').removeClass('slideInUp').addClass('slideOutDown');
    });    

    showCookieModal();

    // $('#btn-cookie-agree').click(function(){
    //    setCookie('almani_agree_cookie','yes');
    //    $('#cookie-modal').fadeOut();
    // });

    function showCookieModal() {
        var cookie = getCookie('almani_agree_cookie');
        if(!cookie || cookie === 'undefined') {

            setTimeout(function(){
                $('#cookie-modal').addClass('slideInUp').show();
            }, 2000);

        }
    }

    ////////////////////////////////////////////////////////
    // Cookie agree




}); 
// docdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdoc
// docdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdoc
// docdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdocdoc
// Document Ready 







 $('.popup__close').click(function() {
    $('#popup').removeClass('popup_show');
});

setTimeout(function(){

         var isDonewithOffer = getCookie('promotions');

        console.log(isDonewithOffer);

         if(!isDonewithOffer) {
            $('#popup').addClass('popup_show');  
         } else{
            $('body').addClass(isDonewithOffer);

         }

}, 3000);




$('.card-deck').click(function(e){

    var className = $(e.target).closest('.card-size').attr('class').split(' ')[1]

    // console.log(className);

    $('body').addClass(className)
    setCookie('promotions', className, '60')
    $('#popup').removeClass('popup_show');


})


























































function setCookie(cname, cvalue, exdays) {

    // if(!exdays)
    if(typeof exdays === 'undefined')        
    exdays = 1;

    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}


function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}






function getAndSetCurrency() {

    var currency = getCookie("almani_currency");

    if(currency==""){
        setCookie('almani_currency','AED');
    } else {
        $('#currency option').each(function(i){
            if($(this).html()==currency)
            $(this).attr('selected','selected');
        });
        
        $('#currency').trigger('change');
    }

}






function cnsl($console){

    var url = window.location.href;

    if(url.indexOf("localhost")>-1 || 
        url.indexOf("almani.ddns.net")>-1 ) {
        // console.log("<< DEVELOPMENT >> ");
        console.log($console);
    }
    else {
        // console.log("<< LIVE >> ");
        // console.log($console);
    }

}


function detectBrowser() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');
  var trident = ua.indexOf('Trident/');
  var edge = ua.indexOf('Edge/');
  if (msie > 0) {
    // IE 10 or older 
    //Do some stuff
    return "IE <=10";
  }
  else if (trident > 0) {
    // IE 11 
    //Do some stuff
    return "IE 11";
  }
  else if (edge > 0) {
    // Edge 
    //Do some stuff
    return "IE EDGE";
  }
  else
    // other browser
    return "OTHERS";
};

