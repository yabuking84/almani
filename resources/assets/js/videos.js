$(document).ready(function(){

    // Not needed, added in main-layout.js
    
    $('.video_intro_img').click(function(){
        var video_url = $(this).attr('data-video_url');
        var target = $(this).attr('data-target');
        $(target+' iframe').attr('src',video_url);
    });


    $('.change_language').click(function(){
        var video_url = $(this).attr('data-video_url');        
        var target = $(this).attr('data-target');
        $(target+' iframe').attr('src',video_url);
    });

});