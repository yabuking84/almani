$(document).ready(function(){
	// gen-info
	// //////////////////////////////////////////////
	$('.gen-info ul.gen-info-tabs li').click(function(){
		var tab_id = $(this).find('a').attr('data-tab_content');
		var tab_id_opened = $(this).siblings('.is-active').find('a').attr('data-tab_content');
		
		$(this).closest('li').siblings('.is-active').removeClass('is-active');
		$(this).closest('li').addClass('is-active');

		// $('.gen-info #'+tab_id_opened).fadeOut(function(){
		// 	$('.gen-info #'+tab_id).fadeIn();
		// });

		// $('.gen-info #'+tab_id).css({
		// 	display: 'block', 
		// }).animate({
		// 	display: 'block', 
		// }, 500);

		$('.gen-info #'+tab_id_opened).slideUp(function(){
			$('.gen-info #'+tab_id).slideDown();
		});
	});

	// first tab to display
	var init_tab = $('.gen-info ul.gen-info-tabs li.is-active a').attr('data-tab_content');
	$('.gen-info #'+init_tab).slideDown();

	// //////////////////////////////////////////////
	// gen-info


});