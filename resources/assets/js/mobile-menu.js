$(document).ready(function(){


	
	$('#mobile-menu').click(function(){
		$('.navbar-menu').addClass('is-active');
		
		$('.navbar-menu').removeClass('close');
		$('.navbar-menu').addClass('open');

	});
    $("#menu-close-btn").click(function() {
		$('.navbar-menu').removeClass('open');
		$('.navbar-menu').addClass('close');
    });



    $('body').on('click', function(e) {
    	var $target = $(e.target);

        if($('.navbar-menu').hasClass('open') && 
			!$target.closest('.navbar-burger').length &&
			!$target.closest('.navbar-menu').length
        ) {
			$('.navbar-menu').removeClass('open');
			$('.navbar-menu').addClass('close');
        }
    });	

});

