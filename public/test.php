<?php

/**
 * Define MyClass
 */
class MyClass
{
    // Declare a public constructor
    public function __construct() { }

    // Declare a public method
    public function MyPublic() { 
    	echo "MyClass::MyPublic<br>";
    }

    // Declare a protected method
    protected function MyProtected() { 
    	echo "MyClass::MyProtected<br>";
    }

    // Declare a private method
    private function MyPrivate() { 
    	echo "MyClass::MyPrivate<br>";
    }

    // This is public
    function Foo()
    {
        $this->MyPublic();
        $this->MyProtected();
        $this->MyPrivate();
    }
}


/**
 * Define MyClass2
 */
class MyClass2 extends MyClass
{
    // This is public
    function Foo2()
    {
        $this->MyPublic();
        $this->MyProtected();
        // $this->MyPrivate(); // Fatal Error
    }
}



$myclass2 = new MyClass2;
$myclass2->MyPublic(); // Works
$myclass2->MyProtected(); 
$myclass2->Foo2(); // Public and Protected work, not Private






echo "<br><br>xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx<br><br>";




class Bar 
{
    public function test() {
        $this->testPrivate();
        $this->testPublic();
    }

    public function testPublic() {
        echo "Bar::testPublic<br>";
    }
    
    private function testPrivate() {
        echo "Bar::testPrivate<br>";
    }
}

class Foo extends Bar 
{
    public function testPublic() {
        echo "Foo::testPublic<br>";
    }
    
    private function testPrivate() {
        echo "Foo::testPrivate<br>";
    }
}

$myFoo = new Foo();
$myFoo->test(); // Bar::testPrivate 
                // Foo::testPublic	
