<?php

namespace App\Project;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

use App\Util;



class ProjectFile extends Model
{
    
    protected $table = 'b2b_project_files';
    protected $primaryKey = 'bpf_id';

    protected $guarded = [];


    public function user() {
        return $this->belongsTo('App\User', 'uploaded_by', 'bu_id');
    }

    public function project() {
        return $this->belongsTo('App\Project', 'bp_id', 'bp_id');
    }

}
