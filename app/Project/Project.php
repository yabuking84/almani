<?php

namespace App\Project;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Util;



class Project extends Model
{
    use SoftDeletes;
    
    protected $table = 'b2b_projects';
    protected $primaryKey = 'bp_id';

    protected $guarded = [];
    // public $timestamps = false;
    
    protected $fillable = [
        'bpi_model_code',
        'bpi_model_options_dim',
        'bpi_model_options_ips',
        'bpi_model_options_beam_angles',
        'bpi_model_options_warranty',
        'bpi_model_options_wifi',
        'bpi_text',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];



    public function users() {
        return $this->belongsToMany('App\User', 'b2b_projects_users', 'bp_id', 'bu_id');
    }

    public function files($fileType = "misc") {

        if($fileType == "misc")
        return $this->hasMany('App\Project\ProjectFile', 'bp_id', 'bp_id')->where('bpf_upload_type','=','misc');
        else if($fileType == "quotation")
        return $this->hasMany('App\Project\ProjectFile', 'bp_id', 'bp_id')->where('bpf_upload_type','=','quotation');
        else
        return null;
    }

    public function items($itemType = "") {

        if($itemType == "")
        return $this->hasMany('App\Project\ProjectItem\ProjectItem', 'bp_id', 'bp_id')->whereNull('bpi_custom_details');
        else if($itemType == "custom")
        return $this->hasMany('App\Project\ProjectItem\ProjectItem', 'bp_id', 'bp_id')->whereNotNull('bpi_custom_details');
        else if($itemType == "all")
        return $this->hasMany('App\Project\ProjectItem\ProjectItem', 'bp_id', 'bp_id');
    }

    public function comments() {
        return $this->hasMany('App\Project\ProjectComment', 'bp_id', 'bp_id');
    }

    public function getStatus() {

        $arr = [
            "sent" => "Sent",
            "modified" => "Modified",
            "new" => "Pending",
            "others" => "Pending",
        ];

        $prjt_sts = ($this->bp_status)?$this->bp_status:"others";        

        return $arr[$prjt_sts];

    }


}
