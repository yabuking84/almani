<?php

namespace App\Project;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Util;



class ProjectComment extends Model
{
    use SoftDeletes;
    
    protected $table = 'b2b_projects_comments';
    protected $primaryKey = 'bpc_id';

    protected $guarded = [];
    // public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];



    public function user() {
        return $this->belongsTo('App\User', 'bu_id', 'bu_id');
    }

    public function project() {
        return $this->belongsTo('App\Project', 'bp_id', 'bp_id');
    }

}
