<?php

namespace App\Project;

use App\Project\ProjectInterface;
use App\Project\Project;

use Validator;
use Auth;

class ProjectRepository implements ProjectInterface
{
    /**
     * Get all projects.
     *
     * @author Leo
     * @param  int
     * @return array
     */
    public function all($limit = null)
    {

    }

	/**
     * Get projects for home page
     *
     * @author Leo
     * @param  int
     * @return array
     */
    public function featured($limit = 6)
    {
    }

    /**
     * Get project data by project id.
     *
     * @author Leo
     * @param  int
     * @return array
     */
    public function find($id)
    {
       return Project::find($id);
    }

    /**
     * Get projects data by project codes.
     *
     * @author Rene
     * @param  int
     * @return array
     */
    public function findManyByCode($codeArr)
    {
    }

    /**
     * Get project data by project code.
     *
     * @author Leo
     * @param  int
     * @return array
     */
    public function findCode($code)
    {
    }

    /**
     * Creates a new project. Image files
     * are being saved on each company folders
     * respectively.
     *
     * @author Leo
     * @param mixed
     * @return array
     */    
    public function add($data)
    {
        $project = new Project;
        $project->fill($data);

        if ($project->save()) {

            $project->users()->attach(Auth::user());
            return $project;
        } 
        else
        return false;        
    }

    /**
     * Edit existing project.
     *
     * @author Leo
     * @param mixed
     * @return mixed
     */
    public function edit($data)
    {
    }

    /**
     * Delete existing project by hash ID.
     *
     * @author Leo
     * @param string
     * @return mixed
     */
    public function delete($id)
    {
    }



    /**
     * Get Project by status.
     *
     * @author Leo
     * @param string
     * @return mixed
     */
    public function getProjectsWithStatus($status = [],$user_type = "") {

        if($user_type=="")
        $user_type = Auth::user()->bu_user_type;

        $retVal = null;

        if($user_type=="admin") {
            $project = new Project;
            if(count($status)) {
                $retVal =  $project->whereIn("bp_status",$status);
            } else {
                $retVal =  $project;
            }
        } else if($user_type=="client") {            
            if(count($status)) {
                $retVal =  Auth::user()->projects()->whereIn("bp_status",$status);
            } else {
                $retVal =  Auth::user()->projects();
            }
        } 

        return $retVal;
    }


}
