<?php

namespace App\Project\ProjectItem;

use Illuminate\Database\Eloquent\Model;

use App\Util;



class ProjectItemNote extends Model
{
    protected $table = 'b2b_project_item_notes';
    protected $primaryKey = 'bpin_id';

    protected $guarded = [];
    // public $timestamps = false;

    

    public function item() {
        return $this->belongsTo('App\Project\ProjectItem\ProjectItem', 'bpi_id', 'bpi_id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'bu_id', 'bu_id');
    }


}
