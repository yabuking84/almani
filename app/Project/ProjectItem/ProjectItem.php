<?php

namespace App\Project\ProjectItem;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

use App\Util;



class ProjectItem extends Model
{
    protected $table = 'b2b_project_items';
    protected $primaryKey = 'bpi_id';

    protected $guarded = [];
    // public $timestamps = false;

    

    public function project() {
        return $this->belongsTo('App\Project\Project', 'bp_id', 'bp_id');
    }

    public function model() {
        return $this->hasOne('App\Product\ProductModel\ProductModel', 'model', 'bpi_model_code');
    }

    public function notes() {
        return $this->hasMany('App\Project\ProjectItem\ProjectItemNote', 'bpi_id', 'bpi_id');
    }


    public function image($size = 'main'){
        $image_name = $this->bpi_id;

        if($size == 'main')
        $retVal = "images/custom_products/".$image_name;
        else
        $retVal = "images/custom_products/".$image_name;

        if (Util::checkLocalFileExists($retVal))
        $retVal = "assets/".$retVal;
        else
        $retVal = Util::noImg('product');

        return $retVal;        
    }

}
