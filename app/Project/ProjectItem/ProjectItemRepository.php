<?php

namespace App\Project\ProjectItem;

use App\Project\ProjectItem\ProjectItemInterface;
use App\Project\ProjectItem\ProjectItem;

use Validator;
use Auth;

class ProjectItemRepository implements ProjectItemInterface
{
    /**
     * Get all projects.
     *
     * @author Leo
     * @param  int
     * @return array
     */
    public function all($limit = null)
    {

    }

	/**
     * Get projects for home page
     *
     * @author Leo
     * @param  int
     * @return array
     */
    public function featured($limit = 6)
    {
    }

    /**
     * Get project data by project id.
     *
     * @author Leo
     * @param  int
     * @return array
     */
    public function find($id)
    {
    }

    /**
     * Get projects data by project codes.
     *
     * @author Rene
     * @param  int
     * @return array
     */
    public function findManyByCode($codeArr)
    {
    }

    /**
     * Get project data by project code.
     *
     * @author Leo
     * @param  int
     * @return array
     */
    public function findCode($code)
    {
    }

    /**
     * Creates a new project. Image files
     * are being saved on each company folders
     * respectively.
     *
     * @author Leo
     * @param mixed
     * @return array
     */    
    public function add($data)
    {   
    }

    /**
     * Edit existing project.
     *
     * @author Leo
     * @param mixed
     * @return mixed
     */
    public function edit($data)
    {
    }

    /**
     * Delete existing project by hash ID.
     *
     * @author Leo
     * @param string
     * @return mixed
     */
    public function delete($id)
    {
    }
}
