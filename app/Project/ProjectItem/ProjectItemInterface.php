<?php

namespace App\Project\ProjectItem;

interface ProjectItemInterface
{
    public function all($limit);

    public function find($id);

    public function add($data);

    public function edit($data);

    public function delete($id);
}
