<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';
    protected $primaryKey = 'client_id';

    protected $guarded = [];

    public function newwebsitesentemails(){
    	return $this->hasMany('App\NewWebsiteSentEmail','client_id','client_id');
    }

    

}


