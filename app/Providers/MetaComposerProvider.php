<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MetaComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->composeMeta();
    }


    public function composeMeta(){
        view()->composer('sections.meta', 'App\Http\ViewComposers\MetaComposer@compose');
    }
}
