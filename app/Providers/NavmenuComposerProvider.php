<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\View;

use Illuminate\Support\Facades\Cache;

class NavmenuComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        return $this->composeNavmenu();

        // return Cache::rememberForever('composeNavmenu', function() {
        //     return $this->composeNavmenu();
        // });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        

    }


    public function composeNavmenu(){
        // return Cache::remember('view', 2600, function() { 
            view()->composer('sections.navmenu', 'App\Http\ViewComposers\NavmenuComposer@compose');
        // });
    }
}



         
