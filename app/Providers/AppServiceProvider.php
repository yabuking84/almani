<?php

namespace App\Providers;
use Illuminate\Support\Facades\Schema;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->app->view_path_camelled = "camelled";
        // Blade::setEchoFormat('e(utf8_encode(%s))');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Category\CategoryInterface',
            'App\Category\CategoryRepository'
        );
        $this->app->bind(
            'App\Product\ProductInterface',
            'App\Product\ProductRepository'
        );
        $this->app->bind(
            'App\Product\ProductModel\ProductModelInterface',
            'App\Product\ProductModel\ProductModelRepository'
        );
    }
}
