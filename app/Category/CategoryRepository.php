<?php

namespace App\Category;

use App\Category\CategoryInterface;
use App\Category\Category;

use Illuminate\Support\Facades\Cache;

use App\Product\ProductApplicationArea;

use Validator;
use Auth;

class CategoryRepository implements CategoryInterface
{

    /**
     * Get all categories.
     *
     * @author rene
     * @param  int
     * @return collection
     */
    public function all($limit = null) {
        $Category = new Category;
        if ($limit == "orderByName") {
            $categories = $Category
                                // ->has('products')
                                ->where('category_active','=',1)
                                ->where('category_parent_id','<',1)
                                // ->orderBy('category_order')
                                // ->orderBy('category_name')
                                ->orderByRaw('category_name, IF(category_order = 0, 999999999, category_order)')
                                ->get();
        } else if ($limit != null) {
            $categories = $Category
                                // ->has('products')
                                ->where('category_active','=',1)
                                ->where('category_parent_id','<',1)
                                // ->orderBy('category_name')
                                ->orderByRaw('IF(category_order = 0, 999999999, category_order), category_name')                                
                                ->paginate($limit);
            // $categories = $Category->whereHas('products', function ($query) {
            //                                                 $query->where('product_active', '=', '1')->where('camel', '=', '1');
            //                                             })->paginate($limit);
        } else {
            // $categories = $Category->camel()->get();
            $categories = $Category
                                // ->has('products')
                                ->where('category_active','=',1)
                                ->where('category_parent_id','<',1)
                                // ->orderBy('category_name')
                                ->orderByRaw('IF(category_order = 0, 999999999, category_order), category_name')                                
                                ->get();
        }
        return $categories;
    }

    /**
     * Get all Application Areas.
     *
     * @author rene
     * @param  none
     * @return Collection
     */
    public function getAppAreas()
    {
        // return Cache::rememberForever('getAppAreas', function() {    
            return ProductApplicationArea::where('enabled','=',1)->orderBy('app_title')->distinct()->get();
        // });
    }

     /**
     * Get all Application Areas in Random.
     *
     * @author rene
     * @param  none
     * @return Collection
     */
    public function getAppAreasRandom($limit)
    {
        // return Cache::rememberForever('getAppAreas', function() {    
            return ProductApplicationArea::where('enabled','=',1)->inRandomOrder()->limit($limit)->get();
        // });
    }

    /**
     * Get category data by category id.
     *
     * @author rene
     * @param  int
     * @return array
     */
    public function find($id)
    {
        return Category::where('category_id', (int) $id)->first();
    }

    /**
     * Get category data by category alias.
     *
     * @author rene
     * @param  int
     * @return array
     */
    public function findAlias($alias)
    {
        return Category::where('category_alias', $alias)->first();
    }



    

}