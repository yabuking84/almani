<?php

namespace App\Category;

interface CategoryInterface
{
    public function all($limit);

    public function find($id);

    public function findAlias($alias);

    // public function add($data);

    // public function edit($data);

    // public function delete($id);
}
