<?php

namespace App\Category;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

Use App\Util;
use DB;


use App\Product\Product;


class Category extends Model
{
    protected $table = 'categories';
    protected $primaryKey = 'category_id';

    protected $guarded = [];


    public function subCategories() {

        return $this->hasMany('App\Category\Category', 'category_parent_id', 'category_id');
    }


    public function parentCategory() {

        return $this->belongsTo('App\Category\Category', 'category_parent_id', 'category_id');
    }


    // public function camel() {
    //     return $this->whereIn('camel',[0,1]);
    // }

    public function products() {
        // return $this->hasMany('App\Product\Product', 'category_id', 'category_id')->where('product_active','=',1);
        return $this->hasMany('App\Product\Product', 'category_id', 'category_id')
                    // ->where('camel','!=',1)
                    ->where('product_active','=',1);
    }

    public function image() {

        // $image = Util::assetBaseURL()."assets/images/cats/".$this->category_id.".jpg?t=".Util::getTS();
        $image = "images/cats/".$this->category_id.".jpg";
        // $image = Util::assetBaseURL()."assets/images/cats/".$this->category_id.".jpg";


        if(Storage::exists($image))
        $image = Storage::url($image);
        else
        $image = Util::noImg('image-th');

        return $image;

    }

    public function otherImage() { 
        // $img = Util::assetBaseURL()."assets/images/cats/categories/".$this->category_alias.".jpg?t=".Util::getTS();
        $img = Util::assetBaseURL()."assets/images/cats/categories/".$this->category_alias.".jpg";
        return $img.Util::getVT();
    }


    public function getHeadline(){

        return $this->category_other_headline;
    }

    public function getShortDesc($meta=false){

        $retVal = "";

        if($this->category_other_desc && !$meta)
        return $this->category_other_desc;
        else
        if($meta=='meta') {
            $retVal = $this->category_other_desc;
            $retVal = strip_tags($retVal);
            $retVal = str_replace(array("\r\n", "\n"), ' ', $retVal);
            $retVal = preg_replace('/\s+/', ' ',$retVal);            
        } else 
        $retVal = "";

        return $retVal;
    }


    public function getMetaDesc(){

        $retVal = "";
        $retVal = $this->category_meta_desc;

        if ($retVal != '') {
            $retVal = strip_tags($retVal);
            $retVal = str_replace(array("\r\n", "\n"), ' ', $retVal);
            $retVal = preg_replace('/\s+/', ' ',$retVal);
            $retVal = trim($retVal);
        } else {
            $retVal ="";
        }

        return $retVal;
    }



    public function getSubCategories(){
        
        $retVal = [];


        return $this->subCategories;


    }

    public function getProductsAndSubProductsNonClosure(){
        
        // if has subcategories
        if($this->subCategories->count()) { 

            $cat_ids = [];
            $cat_ids[] = $this->category_id;
            $cat_ids = array_merge($cat_ids, $this->subCategories->pluck('category_id')->toArray());
            
            $products = Product::whereIn('category_id', $cat_ids)
                                // ->where('camel','!=',1)
                                ->where('product_active','=',1);
            
        } else { 
            // if normal category
            $products = $this->products();
        }
        

        return $products;

    }



    public function getProductsAndSubProductsNonClosuresorting($sort_type = 'ASC', $sort_key = 'model_price'){
        
        // if has subcategories
        
        if($this->subCategories->count()) { 

            $cat_ids = [];
            $cat_ids[] = $this->category_id;
            $cat_ids = array_merge($cat_ids, $this->subCategories->pluck('category_id')->toArray());
            
            $products = Product::whereIn('category_id', $cat_ids)
                                ->join('product_models','products.product_id','=', 'product_models.product_id')
                                // ->where('camel','!=',1)
                                ->where('product_active','=',1)
                                ->groupBy('products.product_id')
                                ->orderBy('product_models.' . $sort_key, $sort_type);


        } else { 

                // if normal category
                // $products = $this->products();

                 $products =  $this->hasMany('App\Product\Product', 'category_id', 'category_id')
                 ->join('product_models','products.product_id','=', 'product_models.product_id')
                 ->where('camel','!=',1)
                 ->where('product_active','=',1)
                 ->groupBy('products.product_id')
                 ->orderBy('product_models.' . $sort_key, $sort_type);




                 // $pricing = DB::table('product_models')
                 //  ->joinSub($products, 'products', function ($join) {
                 //        $join->on('product_models.product_id', '=', 'products.product_id');
                 //    })->groupBy('products.product_id')->orderBy('product_models.model_price');

                // ->join('product_models','products.product_id','=', 'product_models.product_id')
                // ->select('product_models.product_id', 'product_models.model_price')
                // ->orderBy('product_models.model_price', 'ASC');

                 // dd($products->toSql());

                // $products = Product::join('categories','products.category_id','=', 'categories.category_id')
                //             ->join('product_models','products.product_id','=', 'product_models.product_id')
                //             ->where('products.camel','!=',1)
                //             ->where('products.product_active','=',1)
                //             ->groupBy('products.product_id')
                //             ->orderBy('product_models.model_price', 'asc');

        }
        

        return $products;

    }


  

}