<?php

    namespace App;

    use Illuminate\Notifications\Notifiable;
    use Illuminate\Foundation\Auth\User as Authenticatable;

    class ClientLogin extends Authenticatable
    {
        use Notifiable;

        protected $table = 'clients';

        protected $primaryKey = 'client_id';

        protected $guard = 'client';


        protected $hidden = [
            'client_password'
        ];
        
    }