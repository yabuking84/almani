<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';
    protected $primaryKey = 'p_id';

    protected $guarded = [];


    private static $image_path = "assets/images/ongoing-projects/projects/";

    public function imagePath() {
        return self::$image_path.$this->p_image;
    }


}


