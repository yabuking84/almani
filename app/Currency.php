<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currency_exchange_rate';
    protected $primaryKey = 'cer_id';

    protected $guarded = [];

    public static function getAEDMulti($currency = 'aed')
    {
        $multi =  self::where('cer_currency','=',$currency)->first();

        if($multi)
        $retval = $multi->cer_aed_multiplier;
    	else
        $retval = 0;

    	return $retval;

    }

}


