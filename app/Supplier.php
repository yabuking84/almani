<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'suppliers';
    protected $primaryKey = 'supplier_id';

    protected $guarded = [];

    public function product()
    {
        return $this->hasMany('App\Product\Product', 'supplier_id', 'supplier_id');
    }

    public function newwebsitesentemails(){
    	return $this->hasMany('App\NewWebsiteSentEmail','supplier_id','supplier_id');
    }

}


