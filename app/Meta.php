<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cookie;
use App\Product\ProductImage;

use App\Util;


class Meta extends Model
{
	private $tags = [
				'description' => 'LED Lights in Dubai - Almani Lighting designs, supplies and installs finest LED lighting in Dubai, UAE, within your budgets - 5 years warranty!',
				'keywords' => 'LED, LED light, lighting, downlight, tracklight, dubai, uae, sharjah, ajman, abu dhabi, dxb, ras al khaimah, rak, panel, ceiling, l.e.d., batten, striplight, wall, recess, bulbs, pendant, smarthome, smart, underwater, mirror',
				'title' => 'Shop with the Leading LED lights suppliers in Dubai UAE | Almani Lighting',
				'url' => '',
				'image_url' => '',
			];


	/**
	 * Initialize Interface
	 */
	public function __construct() {
		$this->tags['image_url'] = asset('assets/images/logo/logo-colored-og.png');
	}


	public function tags() {
		return $this->tags;
	}

	public static function webpageTitle($title = " | Almani Lighting") {
		
		// " | Shop for LED lights with best quality & flexible product prices in UAE. | Almani Lighting"

		return $title;
	}

}







// $page_title = "Shop with the Leading LED lights suppliers in Dubai UAE | Almani Lighting";
// $meta_keywords = "Lighting companies in dubai, Lighting company dubai, Lighting companies in uae, Lighting company in Dubai, Led lighting companies in dubai, Lighting store dubai, Lighting shops in dubai";
// $page_desc = "LED Lighting Store Dubai - Almani Lighting is one of the most reputable LED lighting companies in Dubai, UAE. Shop for LED lights with best quality & prices.";

