<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewWebsiteSentEmail extends Model
{
    protected $table = 'new_website_sent_email';
    protected $primaryKey = 'nwse_id';

    protected $guarded = [];
    

}


