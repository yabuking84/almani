<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

use Auth;

use Currency;


class Util extends Model
{


	public static function doDiscount($model, $d){
		//returns discount amount always
		// $letter = substr($model, -1);
		// if($d<11) {
		// 	if(strpos(' ABEG', $letter)){
		// 		return 25;
		// 	}
		// }
		// elseif($d<21) {
		// 	if(strpos(' CDFH', $letter)){
		// 		return 25;
		// 	}
		// }
		// else {
		// 	if(strpos(' ACDHI', $letter)){
		// 		return 25;
		// 	}
		// }
		// return 0;


		// Disabled discount as Joh instructions
		return 0;

	}


	// check time if is it equal or lesser than X days old (X = 8 weeks / 56 days deault)
	public static function checkIfNew($time_unix, $diff_days = 60*60*24*56) {

		$time_check = time() - $diff_days;

		if($time_unix >= $time_check) {
			return	true;
		} else 
			return	false;

	}
	
	public static function number($n,$decimals=0,$thousand=''){
		$n = @number_format($n, $decimals, ".", $thousand);
		return $n ? $n : 0;
	}


	public static function getTS(){
		$date = new \DateTime();
		return $date->getTimestamp();
	}

	public static function getModTS($file_path){

        if(Storage::exists($file_path))
        $image = "?t=".Storage::lastModified($file_path);
        else
        $image = "?exists=false&file_path=".$file_path;

		return  $image;
	}

	public static function d(){
		return \date('d');
	}

	public static function getDefaultCurrency(){
		return "AED";
	}

	public static function currency($n, $comma=false){
		$comma = $comma ? ',' : '';
		return Util::number($n, 2, $comma);
	}



	public static function randomImage($dir, $cnt = 1)
	{
	    
		if($cnt==1) {			
		    $files = glob($dir . '/*.*');
		    $file = array_rand($files);
		    $retVal =  $files[$file];
		    $retVal = asset($retVal);
		} else {
		    $files = glob($dir . '/*.*');
		    shuffle($files);
		    $retVal = [];
		    $x=0;
		    foreach ($files as $filename) {
		    	$x++;
		    	if($x<=$cnt)
			    $retVal[] = $filename;
		    }
		}

	    return $retVal;
	}
	


	public static function getImages($dir,$cnt=99)
	{
	    $files = glob($dir . '/*.*');
	    $arr = [];
	    $x=0;
	    foreach ($files as $key => $value) {
	    	$x++;
		    if($x<=$cnt)
	    	$arr[] = asset($value);
	    }
	    return $arr;
	}
	


	public static function getRate($currency = "")
	{
		
		if ($currency == '') 
		$currency = Util::getDefaultCurrency();

		if ($currency == 'AED') 
		return 1;
		else {
			// $json = file_get_contents('assets/currency/AED_to/'.$currency.'.json');
			// $json = json_decode($json,1);
			// $rate = $json['query']['results']['rate']['Rate'];

			$rate = Currency::getAEDMulti($currency);
		}

		return $rate;
	}

	public static function checkRemoteFileExists($url)
	{

		$retVal = true;

	  //   $ch = curl_init();
	  //   curl_setopt($ch, CURLOPT_URL,$url);
	  //   // don't download content
	  //   curl_setopt($ch, CURLOPT_NOBODY, 1);
	  //   curl_setopt($ch, CURLOPT_FAILONERROR, 1);
	  //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	  //   if(curl_exec($ch)!==FALSE)  {
			// $retVal = true;
	  //   } else {
			// $retVal = false;
	  //   }

	    return $retVal;
	}

	public static function checkLocalFileExists($path)
	{
		$retVal = true;

		if(file_exists($path))
		$retVal = true;			
		else
		$retVal = false;			


	    return $retVal;
	}

	public static function noImg($type = "image") {
        if($type == "product")
        $image = Storage::url("images/not_available/product.jpg");
        else if($type == "image")
        $image = Storage::url("images/not_available/coming_soon.jpg");
        else if($type == "image-sm")
        $image = Storage::url("images/not_available/coming_soon-sm.jpg");
        else if($type == "image-th")
        $image = Storage::url("images/not_available/coming_soon-th.jpg");

    	return $image;
	}


	public static function assetBaseURL() {
		// return asset('');
		// return "https://camelled.almani.ae/";
		return env('ASSET_BASE_URL');
		// return "https://staging.almani.ae:2019/almanilighting/";
	}
    
	public static function manageBaseURL() {
		// return asset('');
		// return "https://camelled.almani.ae/";
		return env('MANAGE_BASE_URL');
		// return "https://staging.almani.ae:2019/almanilighting/";
	}
    
    public static function delFile($file, $path="") { 
		Storage::disk('public')->delete($path.$file);
    }

    
    public static function createFolder($foldername, $path="") { 
		Storage::disk('public')->makeDirectory($path.$foldername);
    }

    
    public static function delFolder($foldername, $path="") { 
		Storage::disk('public')->deleteDirectory($path.$foldername);
    }


    public static function uploadFiles($files, $path) {

    	$file_urls = [];

        // get unique folder name
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        $d = new \DateTime( \date('Y-m-d H:i:s.'.$micro, $t) );
        $unique_folder = $d->format("Ymd_His_v");

        $dest = $path.'/'.$unique_folder;


        // upload files
        if(count($files)) {
        	foreach ($files as $keyFile => $file) {
        		$arr = [];
        		$file_name = $file->getClientOriginalName();
				
				$arr['filename'] = $file_name;
				$arr['url'] = Storage::url($file->storeAs($dest, $file_name));
				$file_urls[] = $arr;
        	}
        }

        return $file_urls;
    }

	
	public static function getDiscount(){
		// return Auth::user()->bu_discount;
		return 0;
	}


	public static function isLocalhost() {
		if(env('APP_ISLOCALHOST'))
		return true;
		else
		return false;
	}


    public static function changeGreaterThanLessThan($value) { 
            preg_match('/([>=]+)([0-9]+)/', $value, $pieces);
            if(count($pieces)>1){
                $symbol = $pieces[1];
                switch($symbol){
                    case '=': $symbol = '>'; break;
                    case '>': $symbol = '>'; break;
                    case '>=': case '=>': $symbol = '&ge;'; break;
                }
                $cri = $pieces[2];
                $value = $symbol." ".$cri;
            }

            return $value;
    }

    public static function changeDash($str) {

    	// $retVal = str_replace('-', ' &#x2014; ', $str);
    	$retVal = str_replace('-', ' &ndash; ', $str);

    	return $retVal;
    }


    public static function getMaxChar($string, $max_chars){

    	$retVal = "";

    	$pos = $max_chars;
    	for ($x=$max_chars; $x >= 0; $x--) { 
    		if(substr($string, $x,1) == " ") {
    			$pos = $x;
    			break;
    		}
    	}

    	$retVal = substr($string, 0,$pos);

    	return $retVal;
    }


    // Asset versioning for cache issue
    /////////////////////////////////////////////////////////////////////////////////
    public static function asset($rel_path,$ver=null) {
    	$ver = ($ver!==null)?$ver:self::getVer();
    	return asset($rel_path.self::getVerText().$ver);
    }
    public static function getVerText() { 
    	return "?v=";
    }
    public static function getVer() { 
    	return env('ASSET_VER','1.0');
    }
    public static function getVT() { 
    	return self::getVerText().self::getVer();
    }
    /////////////////////////////////////////////////////////////////////////////////
    // Asset versioning for cache issue



}
