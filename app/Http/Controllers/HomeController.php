<?php

namespace App\Http\Controllers;

use App\Project;

use App\Product\ProductInterface;
use App\Product\ProductModel\ProductModelInterface;
use App\Category\Category;
use App\Category\CategoryInterface;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;


use Illuminate\Http\Response;

use Jenssegers\Agent\Agent;

Use App\Util;

use Auth;

class HomeController extends Controller
{


	/**
	 * Initialize Interface
	 */
	public function __construct(CategoryInterface $category, ProductInterface $product, ProductModelInterface $model) {

		$this->category = $category;
		$this->product = $product;
		$this->model = $model;

	}

	public function index(Request $rqst) {
		
		$agent = new Agent();

		// echo '<br>Is iphone? = '.$agent->is('iPhone');
		// echo '<br>Is android? = '.$agent->isAndroidOS();
		// echo '<br>Is isMobile? = '.$agent->isMobile();
		// echo '<br>Is isTablet? = '.$agent->isTablet();
		// echo '<br>Is device? = '.$agent->device();

		$isMobile = $agent->isMobile();
		$isTablet = $agent->isTablet();

		if($isMobile && !$isTablet) {
	        return view('mobile.home')
				->with('isOffice',self::isOffice($rqst))
                ->with('featured_prods',$this->product->featured(12))
                ->with('application_areas', $this->category->getAppAreasRandom(12))->render();
		} else {
			return view('home')->with('featured_prods',$this->product->featured())->render();
		}



		// $productRepo = $this->product;
		// return Cache::rememberForever('home', function() use($productRepo) {
 		// return view('home')->with('featured_prods',$productRepo->featured())->render();
		// });
	




	}
	public function isOffice($rqst){

		// disable because the public ip keep changing
		// if($rqst->ip() == "86.98.154.166")
		// return true;
		// else 
		// return false;

		return true;
	}





	


	// With anchors
	////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////

	public function projects($anchor = "") {
		$projects = Project::where('p_status','=',1)->inRandomOrder()->paginate(6);
		$project_link = $projects->links('pagination.projects');

		return view('projects')->with('projects',$projects)
								->with('projects_link',$project_link)
								->with('meta_title','Projects');
	}

	public function qualityAssurance($anchor = "") {
		return view('quality-assurance')->with('anchor',$anchor)
										->with('meta_title','Quality Assurance');
	}

	public function aboutus($anchor = "") {
		return view('aboutus')->with('anchor',$anchor)
										->with('meta_title','About Us');
	}

	public function gallery($anchor = "") {
		return view('gallery')->with('anchor',$anchor)
										->with('meta_title','Gallery'); 
	}


	public function b2bPortal($anchor = "") {
		return view('b2bportal')->with('anchor',$anchor)
										->with('meta_title','B2B Portal'); 
	}

	public function careers($anchor = "") {
		return view('careers')->with('anchor',$anchor)
										->with('meta_title','Careers'); 
	}

	public function ledToOwn($anchor = "") {
		return view('ledtoown')->with('anchor',$anchor)
										->with('meta_title','LED to Own'); 
	}

	public function terms($anchor = "") {
		return view('terms')->with('anchor',$anchor)
										->with('meta_title','Terms'); 
	}

	public function privacypolicy($anchor = "") {
		return view('privacy-policy')->with('anchor',$anchor)
										->with('meta_title','Privacy Policy'); 
	}

	public function servicePromise($anchor = "") {
		return view('service-promise')->with('anchor',$anchor)
										->with('meta_title','Service Promise');
	}

	public function sectorsWeServe($anchor = "") {
		return view('sectorsweserve')->with('anchor',$anchor)
										->with('meta_title','Sectors We Serve');
	}

	public function qualitycontrol($anchor = "") {
		return view('qualitycontrol')->with('anchor',$anchor)
										->with('meta_title','Quality Control'); 
	}

	public function priceMatchPromise($anchor = "") {
		return view('pricematchpromise')->with('anchor',$anchor)
										->with('meta_title','Price Match Promise'); 
	}

	public function whatwedo($anchor = "") {
		return view('whatwedo')->with('anchor',$anchor)
										->with('meta_title','What We Do'); 
	}

	public function whyled($anchor = "") {
		return view('whyled')->with('anchor',$anchor)
										->with('meta_title','Why LED?'); 
	}

	public function roi($anchor = "") {
		return view('roi')->with('anchor',$anchor)
										->with('meta_title','ROI'); 
	}

	public function lightingDesign($anchor = "") {
		return view('lightingdesign')->with('anchor',$anchor)
										->with('meta_title','Lighting Design');
	}

	public function catalogBrands($anchor = "") {
		return view('catalog-brands')->with('anchor',$anchor)
										->with('meta_title','Brands');
	}

	public function catalogDownload($anchor = "") {
		return view('catalog-download')->with('anchor',$anchor)
										->with('meta_title','Downloads');
	}

	public function approvedSupplier($anchor = "") {
		return view('approved-supplier')->with('anchor',$anchor)
										->with('meta_title','Approved Supplier');
	}

	public function customProjects($anchor = "") {
		return view('custom-projects')->with('anchor',$anchor)
										->with('meta_title','Custom Projects');
	}

	public function message($anchor = "") {
		return view('message-from-ceo')->with('anchor',$anchor)
										->with('meta_title','Message from CEO');
	}

	public function welcomeToAlmani($anchor = "") {
		return view('welcome-to-almani')->with('anchor',$anchor)
										->with('meta_title','Welcome');
	}

	public function downloads($anchor = "") {
		return view('downloads-summary')->with('anchor',$anchor)
										->with('meta_title','Downloads');
	}
	public function flyers($anchor = "") {
		return view('flyers-summary')->with('anchor', $anchor)
									 -> with('meta_title', 'Downloads');
	}

	public function homeowners(){
		return view ('homeowners')->with('meta_title', 'Homeowners');
	}

	public function promotions(){
		return view ('promotions')->with('meta_title', 'Promotions');
	}
	///////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////
	// With anchors




 






	public function sitemap() {

        $gallery_images = [];
        for($x=1;$x<90;$x++) {
        	if (Util::checkLocalFileExists("assets/images/gallery/".$x."-original.jpg")) {
                $gallery_images[] = asset('assets/images/gallery/'.$x.'-original.jpg');
            }
        }

        $pages = [
			'customprojects_page',
			'approvedsupplier_page',
			'catalog_landing_page',
			'catalog_brand_page',
			'careers_page',
			'b2bportal_page',
			'roi_page',
			'projects_page',
			'ledtoown_page',
			'lightingdesign_page',
			'whatwedo_page',
			'whyled_page',
			'aboutus_page',
			'pricematchpromise_page',
			'qualityassurance_page',
			'qualitycontrol_page',
			'servicepromise_page',
			'terms_page',
			'privacypolicy_page',
			'sectorsweserve_page',
        ];

        $categories = Category::where('category_active','=',1)->orderBy('category_name')->get();

        $data['categories'] = $categories;
        $data['pages'] = $pages;
        $data['gallery_images'] = $gallery_images;

		return response()
					->view('sitemap',$data,200)
					->header('Content-Type', 'text/xml');

	}










	public function loginLEDDesigners($user) {

		$led_designers = [
			'zain',
			'danish',
			'ammar',
			'shaju',
			'rene',
			'leo',
			'rizvi',
			'larry',
		];

		$sales = [
			'sales'
		];

		if(in_array($user, $led_designers)) {
			session(['login_led_designer' => 1]);
			session(['login_led_designer_user' => $user]);
			echo "<marquee style='width: 80%'><span style='font-size:100px;'>You are now logged in</span></marquee>";
			return redirect('/');
		} else {
			session()->forget(['login_led_designer','login_led_designer_user']);
		}

		if(in_array($user, $sales)) {
			session(['login_sales' => 1]);
			session(['login_sales_user' => $user]);
			echo "<marquee style='width: 80%'><span style='font-size:100px;'>You are now logged in</span></marquee>";
			return redirect('/');
			// return view ('home');
		} else {
			session()->forget(['login_sales','login_sales_user']);
		}

	}





































}
