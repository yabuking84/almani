<?php

namespace App\Http\Controllers;


use App\Category\CategoryInterface;
use App\Product\ProductInterface;
use App\Product\ProductModel\ProductModelInterface;

use App\Product\ProductApplicationArea;

use App\Category\Category;
use App\Product\Product;
use App\Product\ProductModel\ProductModel;

use App\Projects;
use App\Test;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;


use DB;
use App\Util;
use Auth;

class SandboxController extends Controller
{
	/**
	 * Initialize Interface
	 */
	public function __construct(CategoryInterface $category, ProductInterface $product, ProductModelInterface $model)
	{
		$this->category = $category;
		$this->product = $product;
		$this->model = $model;
	}

	public function sandbox() {
		
		// $directories = Storage::allDirectories('/public/emailattachements');
		// dd($directories);

		// $fa['category_id'] = 20;
		// $fa['product_id'] = 20;
  //       $ProductModel = new ProductModel;

  //       // category_id
  //       $ProductModel = 
  //       $ProductModel->whereHas(
  //       	'product.category', 
  //       	function ($query) use ($fa) {
  //       		$query->where('category_id', '=', $fa['category_id']);
  //       	}
  //       )->with('product.category');

  //       // product_id
  //       $ProductModel = 
  //       $ProductModel->whereHas(
  //       	'product', 
  //       	function ($query) use ($fa) {
  //       		$query->where('product_id', '=', $fa['product_id']);
  //       	}
  //       )->with('product.category');


        // // product_id
        // $ProductModel->with(['product' => function ($query) use ($fa) {
        //     $query->with(['category' => function ($query2) use ($fa) {
        //         $query2->where('category_id', '=', $fa['category_id']);
        //     }])->where('camel','=',1);
        // }]);



        // dd($ProductModel->orderBy('model_id', 'desc')->limit(10)->get());

		// $data = $this->model->findModelsWithFilters(['model_code' => 'cl'])->paginate(12);

		// foreach ($data as $key => $value) {
		// 	echo $value->model.'<br>';
		// };

		// echo '<br>';
		// echo '<br>';
		// echo $data->links();
		// // dd($data);
		// echo '<br>';
		// echo '<br>';
		
		// echo "count = ".$data->count().'<br>';
		// echo "firstItem = ".$data->firstItem().'<br>';
		// echo "lastPage = ".$data->lastPage().'<br>';
		// echo "perPage = ".$data->perPage().'<br>';
		// echo "currentPage = ".$data->currentPage().'<br>';
		// echo "hasMorePages = ".$data->hasMorePages().'<br>';
		// echo "total = ".$data->total().'<br>';


  //       $arr = explode('/',"asd");
  //       echo count($arr).'<br>';
  //       $has_more_ips = ($arr[0]!='')?true:false;
  //       echo 'x'.$has_more_ips.'x';


		// $array = 
		// [
		// 	'products' => 
		// 	[
		// 		'desk' => 
		// 		[
		// 			'price' => 1001
		// 		],
		// 		'desk1' => 
		// 		[
		// 			'price' => 10011
		// 		],
		// 	],
		// 	'products2' => 
		// 	[
		// 		'desk2' => 
		// 		[
		// 			'price' => 1002
		// 		],
		// 	],
		// ];

		// $price = array_get($array, 'products');

		// dd($price);

		// $retVal = 
		// Product::select('product_id','product_name','product_code')
		// ->where('camel','=','-1')
		// ->where('product_active','=',0)
		// ->where('product_warranty','<',5)
		// ->whereHas('models',function($query){
  //           $query->where('model_warranty', '=', 5);
		// })
		// ->with([
		// 	'models' => function($q){
		// 		$q->select('model_id','product_id','model','model_warranty')
		// 		// ->where('model_warranty', '<', 5)
		// 		;
		// 	}
		// ])
		// // ->take(3)
		// ->get();

		// dd($retVal->toArray());

		// $retVal = Product::select('product_id','product_name')
		// ->where('product_name','like','%Peter%')
		// // ->where('product_id','=','774')
		// ->get();

		// foreach ($retVal as $key => $product) {
		// 	$name = $product->product_name;
		// 	$name = str_replace("Peter","",$name);

		// 	echo $product->product_name.'   ==   '.$name.'<br>';
			
		// 	$product->product_name = $name;
		// 	$product->save();




		// $text = 'Crailsheim  (CL-CL-0062-3D)';
		// preg_match('#\((.*?)\)#', $text, $match);
		// dd($match);





		// $retVal = ProductModel::where('model','=','CL-WA-0370-1A')->first();

		// dd($retVal->dsOptions());

		// $str = "No product description available. To be updated soon..";
		// if (strpos($str, 'products') == false) {
		// 	echo "Not in string";
		// }
		// else
		// 	echo "in string";


		// $str = 0;
		// if ($str === false) {
		// 	echo "false";
		// }
		// else
		// 	echo "other";


		// dd(
		// 	Product::find(9)
		// 			->models()
		// 			->select(DB::raw('DISTINCT(LEFT(model,length(model)-1)) as distinct_model'))
		// 			->orderByRaw('LENGTH(distinct_model) asc, distinct_model asc')
		// 			->get()
		// 			->pluck('distinct_model')
		// 			->toArray()
		// );


		// beamangles by distinct_model
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		$id = 938;
		dd(Product::find($id)->getDatasheetCCT());




        $table = [];
        foreach (Product::find($id)->getDistinctModelAlpha() as $key => $distinct_model_alpha) {
            $dma = Product::find($id)->models()
                        ->where('model','LIKE','%'.$distinct_model_alpha);
            $dma_arr['distinct_model_alpha'] = $distinct_model_alpha;

            // CCT
            //////////////////////////////////////////////
            $cct = $dma->pluck('model_cct')->toArray();
            
            // Implode and explode so that if value like 1000,2000,3000 
            // can be sorted with the other models and
            // duplicate ccts can be removed
            $cct = implode(',',$cct);
            $cct = explode(',',$cct);
            $cct = array_unique($cct);
            sort($cct);

            $dma_arr['cct'] = implode(',',$cct);
            //////////////////////////////////////////////

            $table[] = $dma_arr;
        }

		echo "<pre>";
		print_r($table);
		echo "</pre><br>";

		exit();

		// dd(Product::find($id)->getDistinctModel());

		foreach(Product::find($id)->getDistinctModel() as $val) {

			echo Product::find($id)->product_name.'<br>';
			echo Product::find($id)->product_code.'<br>';
			echo $val.'<br>';

			$distinct_model = $val;
			$distinct_model_len =  strlen($distinct_model)+1;

			$mds = Product::find($id)
							->models()
							->select('model_id','model','model_size','model_cutout')
							->where('model','LIKE',$distinct_model.'%')
							->whereRaw('LENGTH(model) = '.$distinct_model_len)

							// ->with('beamAngles','product')
							->get()

							// ->pluck('model')

							// ->pluck('beamAngles')
							// ->collapse()
							// ->pluck('model_beam_angle')
							->pluck('model_cutout')

							// ->toArray()
							;

			echo "<pre>";
			print_r(array_unique($mds->toArray()));
			echo "</pre><br>";
		// 	$beam_angles = [];
		// 	foreach ($mds as $md) {
		// 		// echo '= '.$md->model;
		// 		$md_bms = $md->beamAngles->pluck('model_beam_angle');

		// 		if($md_bms->count()) {					
		// 			$md_bms = $md_bms->toArray();
		// 		} else {
		// 			$md_bms = explode('/',Product::find($id)->product_beam_angle);
		// 		}

		// 		// echo "<pre>";
		// 		// print_r($md_bms);
		// 		// echo "</pre><br>";

		// 		$beam_angles = array_merge($beam_angles,$md_bms);
		// 	}

		// 	$beam_angles = array_unique($beam_angles);
		// 	sort($beam_angles);
		// 	echo "beam_angles<pre>";
		// 	print_r($beam_angles);
		// 	echo "</pre><br>";

		// 	echo "<br>";
		}
			

		// 	echo "<pre>";
		// 	print_r($all_beam_angles);
		// 	echo "</pre><br>";
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////
		// beamangles by distinct_model

		// $models = ProductModel::where('model','LIKE','CL-CL-0062%')->limit(300000)->get();

		// // dd($models);

		// // echo '<pre>';
		// // print_r($models->toArray());
		// // echo '</pre>';

		// foreach ($models as $key => $model) {

	 //        $value = $model->where('model_id','=',$model->model_id)->with('invs.blocks.storages')->get();
	 //        $value = $value->pluck('invs')->collapse();
	 //        $value = $value->pluck('blocks')->collapse();
	 //        $value = $value->pluck('storages')->collapse()->sum('inv_storage_quantity');

		// 	echo $model->invOptions().' - '.$value;
		// 	// echo '<pre>';
		// 	// print_r($value->toArray());
		// 	// echo '</pre>';

		// 	// echo ' - '.$model->stockOptions();

		// 	echo "<br>";
			
		// }
		// 	echo "<br>";
		// 	echo "<br>";
		// 	echo "<br>";

		// foreach ($models as $key => $model) {
		// 	echo $model->invOptions().' - '.$model->stockOptions();
		// 	echo "<br>";
        // }            

// CL-PA-0859-1D

        // $search = [
        //     // "category_id"=> "10",
        //     // "category_id"=> "1",
        //     "category_id"=> "",
        //     "cct"=> [
        //         "min"=> "",
        //         "max"=> ""
        //     ],
        //     "dim_options"=> [
        //         "dimm"=> 0,
        //         "triac"=> 0,
        //         "v0_10"=> 0,
        //         "dali"=> 0
        //     ],
        //     "emergency"=> 0,
        //     "ip_rating"=> [
        //         "min"=> "",
        //         "max"=> ""
        //     ],
        //     "lumen"=> [
        //         "min"=> "",
        //         "max"=> ""
        //     ],
        //     "model_code"=> "CL-PA-0859-1D",
        //     "power"=> [
        //         "min"=> "",
        //         "max"=> ""
        //     ],
        //     "price"=> [
        //         "min"=> "",
        //         "max"=> "",
        //         "currency"=> "AED"
        //     ],
        //     "product_id"=> "",
        //     "supplier_code"=> "",
        //     "warranty"=> [
        //         "year_1"=> 0,
        //         "year_2"=> 0,
        //         "year_3"=> 0,
        //         "year_4"=> 0,
        //         "year_5"=> 0
        //     ],
        //     "wifi"=> 0
        // ];

        // $retVal['paginate'] = $this->model->findModelsWithFilters($search)->paginate(12);

        // $retVal['links'] = "".$retVal['paginate']->links('pagination.search')."";

        // $retVal['currency'] = $search['price']['currency'];
        // $retVal['rate'] = Util::getRate($search['price']['currency']);
            

        // foreach ($retVal['paginate']->all() as $key => $value) {
            
        //     echo $value->model."<br>";

        // };

		date_default_timezone_set("Asia/Dubai"); 
        $unix_epoch = 1487403211;
		$time_now = time();
		$time_check = ($time_now-(60*60*24*56));

		echo date('Y m d h:i:s',$unix_epoch); 
		echo "<br><br>"; 
		echo date('Y m d h:i:s',$time_now);
		echo "<br><br>";
		echo ($time_now-$unix_epoch)/60/60/24; 
		echo "<br><br>"; 
		echo $time_now; 
		echo "<br>"; 
		echo $time_check; 
		echo "<br><br>"; 

		echo date('Y m d h:i:s',$time_check); 






        // Product::where()

        // 56 days




    //////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	/////////////////////
	/////////////////////
	// end



	}




	public function sandbox2(){
		// $models = Product::find(9)
		// 		->models()
		// 		->select('model_id','model')
		// 		->with('dimensions')
		// 		->whereHas('dimensions')
		//         ->get()
		//         ->pluck('dimensions','model')
		        // ->collapse()
		       	// ;

		$models = Product::find(9)->getOtherDimension();

		dd($models);


	}


	public function sandbox3(){
		// $models = Product::find(9)
		// 		->models()
		// 		->select('model_id','model')
		// 		->with('dimensions')
		// 		->whereHas('dimensions')
		//         ->get()
		//         ->pluck('dimensions','model')
		        // ->collapse()
		       	// ;

		$collection = Product::where("product_code","=","CL-DO-0761")->first()->getModelsForProductPage();

		dd($collection);


	}

	public function sandbox4(){


	    // $image_path = "assets/images/ongoing-projects/projects/";
	    $image_path = "";

	    $items = [
	        [
	            'image' => $image_path."PRJAE0722635.jpg",
	            'title' => "The Opus",
	            'location' => "Business Bay, Dubai",
	            'description' => "
	            The project involves the construction of the cube-shaped Opus Tower also known as the Floating Tower spread over an area of 23,226 square meters.
	            <br><br>
	            The Opus Tower will be divided into three separate towers each connected by a common podium. The towers will consist of 19 floors, six basement levels, three podium levels and 2,000 parking berths.
	            <br><br>
	            The project will also include the ME by Melia hotel and serviced apartments within the complex, the hotel will consist of 93 rooms and suites, across 19 floors, including the ultra-luxurious Suite ME, as well as 98 serviced apartments and 15 restaurants.
	            <br><br>
	            The uppermost floor, which is located above the 50-meter bridge connection between the two towers includes facilities like a tranquility zone, a beach deck with reflective pool, a shaded roof terrace, media zone and a gymnasium. There will be floors dedicated to office space and four floors dedicated to retail, food and beverage outlets. There will also be a five-story basement. The retail design inspired by natural light and space, reflexive fitting patterns and will be applied in the glass facade to reduce the solar gains inside the building.
	            ",
	            'project_value' => "US $ 680.0 million",
	        ],
	        [
	            'image' => $image_path."mall-of-emirates.jpg",
	            'title' => "Mall of Emirates",
	            'location' => "Interchange 4 on Sheikh Zayed Road, Dubai",
	            'description' => "This project involves the replacements of all traditional lighting within the entire shopping Mall plus parking area into LED fittings.",
	            'project_value' => "US $ 218.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE17133689.jpg",
	            'title' => "Downtown Views II (Tower 3)",
	            'location' => "Sheikh Zayed Road Next to Burj Khalifa, Dubai",
	            'description' => "The project involves construction of three residential towers each comprising a ground floor and 65 additional floors.",
	            'project_value' => "US $ 200.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE16127141.jpg",
	            'title' => "Shams Marina",
	            'location' => "Al Reem Island, Abu Dhabi",
	            'description' => "The project involves construction of multi-faceted four-tower development overlooking the Arabian Sea. The project will include Vida Beach Reem Island with 262 guestrooms and suites, and Vida Residences Beach Reem Island will have 192 serviced residences, a varied food and beverage offering, spa and fitness centre, meeting and conference space, and other recreational facilities. The development also includes 329 marina residences spread across two towers.",
	            'project_value' => "US $ 150.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE16125154.jpg",
	            'title' => "Bloom Heights",
	            'location' => "Jumeirah Village Circle, Dubai",
	            'description' => "
	            Bloom Properties, a Bloom Holding business specialized in the development of integrated and sustainable communities is set to commence the main construction works at Bloom Heights, a mixed-use project in Dubai’s Jumeirah Village Circle
	            <br>
	            <br>
	            Bloom Towers is the Abu Dhabi-based master developer’s second project in Jumeirah Village Circle which is currently under construction.
	            <br>
	            <br>
	            The project's features are as follows:
	            <br>
	            <ul>
	                <li>The project will consist of three towers with a total of 944 units </li>
	                <li>The two towers allocated for sale will comprise 689 contemporary studios and one-bedroom apartments</li>
	                <li>The two high-rise towers will be linked internally by four podium levels</li>
	                <li>Amenities include two swimming pools, a running track, a gym, a multipurpose hall and convenience retail outlets</li>
	                <li>Bloom Towers has attracted high investor uptake due to its location and potential for rental and capital value growth</li>
	                <li>Bloom Heights will feature modern and contemporary architecture and boast an intelligent design that maximizes the value of the living space</li>
	                <li>It is prominently located in proximity to the upcoming Circle Mall, a community center and several parks, retail outlets, schools, play grounds, mosques and adjacent to the iconic Viceroy project at Jumeirah Village Circle</li>
	            <ul>
	            ",
	            'project_value' => "US $ 150.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE16118904.jpg",
	            'title' => "Atlantis Management Staff Accommodation Complex",
	            'location' => "Jumeirah Village Circle, Dubai",
	            'description' => "The project involves the construction of a staff accommodation complex consisting 4 buildings each comprising ground floor and 14 additional floors.",
	            'project_value' => "US $ 114.3 million",
	        ],
	        [
	            'image' => $image_path."PRJAE16125626.jpg?t=1",
	            'title' => "Vincitore Boulevard",
	            'location' => "Dubailand Al Barsha South Third, Dubai",
	            'description' => "The project involves construction of a mixed-use development comprising a basement levels, a ground floor and 2 additional floors offering more than 16,107.25 sqm of retail space, residence and service apartments. The building will comprise of 216 residential apartments including studios and one-bedroom units besides 30 boutique retail outlets. Other leisure facilities will include swimming pools, a gymnasium, steam and sauna baths, kids playing lounge and a tennis court. Vincitore Boulevard offers convenient access to parks, schools, clinics, shopping centres and restaurants.",
	            'project_value' => "US $ 74.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE15109174.jpg",
	            'title' => "Arabian Gates",
	            'location' => "Dubai Silicon Oasis, Dubai",
	            'description' => "The project involves construction of a residential complex consisting of 4 towers each comprising 3 basement levels, a ground floor and 14 additional floors.",
	            'project_value' => "US $ 72.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE14100797.jpg",
	            'title' => "Hamadan Towers",
	            'location' => "Al Zahiya, Abu Dhabi",
	            'description' => "The project involves construction of 2 residential towers each comprising 3 basement levels which will offer car parkings, a ground floor which will offer a showroom, a mezzanine floor, 18 additional floors and a roof.",
	            'project_value' => "US $ 43.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE16116345.jpg",
	            'title' => "Eclipse Towers",
	            'location' => "Al Reem Island, Abu Dhabi",
	            'description' => "The project involves construction of 2 commercial buildings comprising a ground floor and 4 common podium levels. Tower A will have 18 additional floors and a pent house and tower B will have 19 additional floors and a pent house.",
	            'project_value' => "US $ 40.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE16125131.jpg",
	            'title' => "Hotel Apartments",
	            'location' => "Jumeirah Village Circle, Dubai",
	            'description' => "The project involves construction of a hotel apartment building comprising a ground floor, 4 podium levels and 14 additional floors.",
	            'project_value' => "US $ 30.0 million",
	        ],
	        [
	            'image' => $image_path."al-qassimi-palace.jpg",
	            'title' => "HH Sheikh Khalid Al Qassimi Palace",
	            'location' => "Al Noof 4, Sharjah",
	            'description' => "This project involves the installation of all external and internal LED lighting systems.",
	            'project_value' => "US $ 25.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE17130470.jpg",
	            'title' => "3 Star Hotel",
	            'location' => "Al Barsha 1, Dubai",
	            'description' => "The project involves construction a 3 star hotel building comprising 2 basement levels, a ground floor, 8 additional floors and 2 roofs.",
	            'project_value' => "US $ 20.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE17129947.jpg?t=1",
	            'title' => "Commercial/Residential Building",
	            'location' => "Al Jadaf, Dubai",
	            'description' => "The project involves construction of a commercial and residential building comprising a ground floor, 3 parking levels, 8 additional floors and a gym.",
	            'project_value' => "US $ 20.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE14101344.jpg",
	            'title' => "Commercial / Residential Building",
	            'location' => "Oud Metha, Dubai",
	            'description' => "The project involves construction of a commercial and residential building comprising a ground floor, a parking level, 5 additional floors and a health club.",
	            'project_value' => "US $ 18.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE16127017.jpg",
	            'title' => "School Building",
	            'location' => "Al Warqa 4, Dubai",
	            'description' => "The project involves construction of a school building comprising a ground floor and 3 additional floors.",
	            'project_value' => "US $ 15.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE17133400.jpg",
	            'title' => "Commercial / Residential Building",
	            'location' => "Al Jadaf, Dubai",
	            'description' => "The project involves construction of a commercial and residential building comprising a ground floor, 11 additional floors and 2 roofs.",
	            'project_value' => "US $ 12.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE17139100.jpg",
	            'title' => "Commercial/Residential Building",
	            'location' => "Wadi Al Safa 3, Dubai",
	            'description' => "The project involves construction of a commercial and residential building comprising a ground floor, 2 podium levels, 9 additional floors and a gym.",
	            'project_value' => "US $ 12.0 million",
	        ],

	        [
	            'image' => $image_path."PRJAE15112427.jpg",
	            'title' => "Dezire Residences",
	            'location' => "Jumeirah Village Circle, Dubai",
	            'description' => "The project involves construction of a residential tower comprising a ground floor, 4 podium levels and 10 additional floors.",
	            'project_value' => "US $ 10.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE17132046.jpg",
	            'title' => "Arbor School",
	            'location' => "Al Furjan, Dubai",
	            'description' => "The project involves construction of a school building comprising a ground floor and 2 additional floors.",
	            'project_value' => "US $ 10.0 million",
	        ],            
	        [
	            'image' => $image_path."PRJAE17135635.jpg",
	            'title' => "Al Aquila School",
	            'location' => "Wadi Al Safa 5, Dubai",
	            'description' => "The project involves construction of a school building comprising a ground floor and 2 additional floors.",
	            'project_value' => "US $ 8.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE16128876.jpg",
	            'title' => "Commercial Building",
	            'location' => "Al Shahama, Abu Dhabi",
	            'description' => "The project involves construction of a commercial building.",
	            'project_value' => "US $ 8.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE17136830.jpg",
	            'title' => "Commercial / Residential Building",
	            'location' => "Studio City, Dubai",
	            'description' => "The project involves construction of a commercial and residential building comprising a ground floor and 8 additional floors.",
	            'project_value' => "US $ 7.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE17133365.jpg",
	            'title' => "Commercial / Residential Building",
	            'location' => "Al Owaid, Fujairah",
	            'description' => "The project involves construction of a commercial and residential building comprising a ground floor and 9 additional floors.",
	            'project_value' => "US $ 8.0 million",
	        ],            
	        [
	            'image' => $image_path."PRJAE17136987.jpg",
	            'title' => "Latifa Hospital Expansion",
	            'location' => "Al Jaddaf, Dubai",
	            'description' => "The project involves expansion of labour and delivery suite, emergency department, maternity ward, two more operation theatre rooms and CT services. It will also have a new fertility centre.",
	            'project_value' => "US $ 6.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE17135298.jpg",
	            'title' => "Warehouse, Office Building & Workshop",
	            'location' => "Dubai Industrial City, Dubai",
	            'description' => "The project involves construction of a warehouse comprising a ground floor and a mezzanine floor. It will also include an office building, a workshop and a service block.",
	            'project_value' => "US $ 5.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE17139482.jpg",
	            'title' => "Residential Building",
	            'location' => "Saih Shuaib 1 Jebel Ali Hills, Dubai",
	            'description' => "The project involves construction of a residential building comprising a basement level, a ground floor and 4 additional floor.",
	            'project_value' => "US $ 5.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE17137379.jpg",
	            'title' => "3 Commercial / Residential Building",
	            'location' => "Al Jurainah 4, Sharjah",
	            'description' => "The project involves construction of 3 commercial and residential building each comprising a ground floor and 2 additional floors.",
	            'project_value' => "US $ 5.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE17135726.jpg",
	            'title' => "Labour Accommodation",
	            'location' => "Jebel Ali Industrial Area 1, Dubai",
	            'description' => "The project involves construction of a labour accommodation comprising a ground floor and 4 additional floors.",
	            'project_value' => "US $ 5.0 million",
	        ],
	        [
	            'image' => $image_path."PRJAE17136156.jpg",
	            'title' => "Commercial / Residential Building",
	            'location' => "Muwailah Commercial, Sharjah",
	            'description' => "The project involves construction of a commercial and residential building comprising a ground floor, a parking level and 5 additional floors.",
	            'project_value' => "US $ 4.0 million",
	        ],

	    ];
	    


		// foreach ($items as $key => $value) {
		// 	$project = new Projects;
		// 	$project->p_title = $value['title'];
		// 	$project->p_location = $value['location'];
		// 	$project->p_description = $value['description'];
		// 	$project->p_project_value = $value['project_value'];
		// 	$project->p_image = $value['image'];
		// 	$project->save();
		// }

		
	}





















	public function setAppAreasToProducts(){

        $app_areas =  ProductApplicationArea::where('enabled','=',1)->orderBy('app_title')->distinct()->get();

        foreach ($app_areas as $app_area) {
        	
        	echo '<br>xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx<br>';
        	echo $app_area->app_id.' = '.$app_area->app_title.'<br>';
        	// foreach ($app_area->getProducts() as $product) {
        	// 	echo $product->product_id.', ';
        	// }

        	$prod_arr = $app_area->getProducts()->pluck('product_id')->toArray();
        	print_r($prod_arr);

        	$app_area->products()->sync($prod_arr);

        	echo '<br>xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx<br>';

        }

	}



	public function sandbox5(){


		// $prod = Product::where('product_code','=','AL-IL-1219')->first();
		// $prod = Product::where('product_code','=','CL-SL-1017')->first();
		// $prod = Product::where('product_code','=','AL-SL-1017')->first();
		// $product = Product::where('product_code','=','AL-SL-1168')->first();
		$product = Product::where('product_code','=','CL-TI-1273')->first();


        // product_cct
        ////////////////////////////////////////////////
        $value = ''; $min = ''; $max = '';
        $ccts = $product->models()
				        ->select('model_cct')
				        ->orderByRaw('CAST(model_cct AS UNSIGNED)')
				        ->get()
				        ->pluck('model_cct')->toArray();
    	$ccts = array_unique($ccts);

    	// separate numbers and letters
    	$ccts_int = [];
    	$ccts_char = [];
    	$ccts_char_rgb = [];
    	foreach ($ccts as $cct) {
    		if(intval($cct))
    		$ccts_int[] = intval($cct);
    		else if( strpos( $cct, 'RGB' ) !== false ) 
    		$ccts_char_rgb[] = $cct;
    		else
    		$ccts_char[] = $cct;
    	}

    	if(count($ccts_int)) {    		
	    	$ccts_int_min = $ccts_int[0];
	    	$ccts_int_max = $ccts_int[count($ccts_int)-1];
    	} else {
	    	$ccts_int_min = 0;
	    	$ccts_int_max = 0;
    	}

    	natsort($ccts_char);
    	natsort($ccts_char_rgb);
    	natsort($ccts_int);
    	
    	$ccts_char = implode(', ', $ccts_char);
    	$ccts_char_rgb = implode(', ', $ccts_char_rgb);

    	$ccts_other = "";
    	if($ccts_char)
    	$ccts_other.= ", ".$ccts_char;
    	if($ccts_char_rgb)
    	$ccts_other.= ", ".$ccts_char_rgb;

        if($ccts_int_min || $ccts_int_max) {
            if($ccts_int_min==0)
            $value = (float)$ccts_int_max.'K';
            else if($ccts_int_max==0)
            $value = (float)$ccts_int_min.'K';
            else
            $value = number_format((int)$ccts_int_min,0,'.','')."K - ".number_format((int)$ccts_int_max,0,'.','').'K';
        }
        else
        $value = "0";

        $arr = [];
        $arr['label'] = "CCT";
        $arr['value'] = $value.$ccts_other;
        $tds['product_cct'] = $arr;
        ////////////////////////////////////////////////


        dd($arr);

			
	}












}
