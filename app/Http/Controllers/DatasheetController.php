<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Camel\Product\ProductInterface;
use App\Camel\Product\Product;

use App\Camel\Product\ProductModel\ProductModelInterface;
use App\Camel\Product\ProductModel\ProductModel;

use App\Camel\Category\CategoryInterface;
use App\Camel\Category\Category;

use Auth;
use App\Util;
Use App\Camel\Meta;
use View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

use SnappyPDF;
use PDFMerger;


class DatasheetController extends Controller
{
	/**
	 * Initialize Interface
	 */
	public function __construct(CategoryInterface $category, ProductInterface $product, ProductModelInterface $model)
	{
		$this->category = $category;
		$this->product = $product;
		$this->model = $model;
	}




	public function createDatasheet($product_code, Request $rqst) {

		$product = Product::where('product_code','=',$product_code)->get()->first();

		// dd($product_code);

		$data = [];
		$data['product'] = $product;
		$data['project_name'] = (isset($rqst->project_name))?$rqst->project_name:'Client Project';
		$data['project_location'] = (isset($rqst->project_location))?$rqst->project_location:'Dubai';
		$data['project_reference_name'] = (isset($rqst->project_reference_name))?$rqst->project_reference_name:'';
		$data['project_reference_value'] = (isset($rqst->project_reference_value))?$rqst->project_reference_value:'';
		$data['project_location'] = (isset($rqst->project_location))?$rqst->project_location:'Dubai';
		$data['brand'] = $rqst->brand;

		///////////////////////////////////////////
	    $haystack_camel = ["cd","cc","ac",];
	    $haystack_almani = ["ad","aa","ca",];
	    $needle = "";
	    if($product->camel==1)
	    $needle = "c";
	    else
	    $needle = "a";
	    if($rqst->brand=="default")
	    $needle = $needle."d";
	    elseif($rqst->brand=="camel")
	    $needle = $needle."c";
	    elseif($rqst->brand=="almani")
	    $needle = $needle."a";

		$data['needle'] = $needle;
		$data['haystack_almani'] = $haystack_almani;
		$data['haystack_camel'] = $haystack_camel;
		///////////////////////////////////////////

		$pdf_title = $product->product_code;
		// $pdf_folder = 'assets/datasheets/'.$pdf_title;
		$pdf_folder = 'assets/datasheets';

		Util::createFolder($pdf_folder);

		$cover_pdf = SnappyPDF::loadView('pdf.datasheets.covers.template1',$data)
                                ->setPaper('a4')
                                ->setOrientation('portrait')
                                ->setOption('margin-bottom',0)
                                ->setOption('margin-top',0)
                                ->setOption('margin-left',0)
                                ->setOption('margin-right',0)
                                ->setWarnings(false);
        $cover_pdf->save($pdf_folder.'/'.$pdf_title.'_cover.pdf', true);
		
        $header = View::make('pdf.datasheets.header')
						        ->with('product',$product)
						        ->with('needle',$needle)
						        ->with('haystack_camel',$haystack_camel)
						        ->with('haystack_almani',$haystack_almani)
						        ->render();
        $footer = View::make('pdf.datasheets.footer')
						        ->with('product',$product)
						        ->with('needle',$needle)
						        ->with('haystack_camel',$haystack_camel)
						        ->with('haystack_almani',$haystack_almani)
						        ->render();

		$body_pdf = SnappyPDF::loadView('pdf.datasheets.template1',$data)
                                ->setPaper('a4')
                                ->setOrientation('portrait')
                                ->setOption('footer-html',$footer)
                                ->setOption('header-html',$header)
                                ->setOption('margin-bottom',25)
                                ->setOption('margin-top', 15)
                                ->setOption('margin-left', 0)
                                ->setOption('margin-right', 0)
                                ->setWarnings(false);
        $body_pdf->save($pdf_folder.'/'.$pdf_title.'_body.pdf',true);

        $pdf = new PDFMerger;
		$pdf->addPDF($pdf_folder.'/'.$pdf_title.'_cover.pdf', 'all');
		$pdf->addPDF($pdf_folder.'/'.$pdf_title.'_body.pdf', 'all');
		$pdf->merge('file', $pdf_folder.'/'.$pdf_title.'.pdf');
		
		// Uncomment if localhost
		// $retVal_pdf = $pdf->merge('browser', $pdf_title.'.pdf');

        // add watermark on the pdf using `pdftk`
        $pdf_file = $pdf_folder . '/' . $pdf_title;
        shell_exec("pdftk {$pdf_file}.pdf background assets/watermark.pdf output {$pdf_file}-w.pdf");

		// remove excess merged pdf files
		Util::delFile($pdf_folder.'/'.$pdf_title.'_cover.pdf');
		Util::delFile($pdf_folder.'/'.$pdf_title.'_body.pdf');

		// Comment if localhost
		return response()->file($pdf_file."-w.pdf");

	}




}
