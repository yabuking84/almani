<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Product\ProductInterface;

use App\Category\CategoryInterface;
use App\Category\Category;
use App\Supplier;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;

use Auth;
Use App\Meta;
Use App\Util;

use App\Product\ProductModel\ProductModel;

use App\Product\ProductApplicationArea;


class CatalogController extends Controller
{
	/**
	 * Initialize Interface
	 */
	public function __construct(CategoryInterface $category, ProductInterface $product)
	{
		$this->category = $category;
		$this->product = $product;

		$this->meta = new Meta;		
	}




	public function index(Request $rqst)
	{
		$categories = $this->category->all();

    	return view('category.catalog-landing')
							    ->with('isOffice',self::isOffice($rqst))
						    	->with('meta',$this->meta->tags())
						    	->with('categories',$categories);
	}


	



	public function product($category_alias, $product_code, Request $rqst)
	{
		$product = $this->product->findCode($product_code);
		$category = $product->category;


		$product_code = explode('-', $product->product_code);
		
		$code = implode('-', [$product_code[1], $product_code[2]]);

	
		if ($product->camel == 1) {
			$sameProduct = [];
		}
		else {
			$sameProduct = $product->getSameProduct($code);
		}

		



		// dd($product->camel);

		$cat_id = $this->category->findAlias($category_alias);


		

		$desc = $product->getMetaDesc();
		$desc = $product->product_name.', our finest '.$product->category->category_name.', '.$desc;
		$desc = Util::getMaxChar($desc,230);

		// get similar products get random application area then get product base on app area
		// $similarProduct = $product->getSimilarProds(12);
		// Get Suggested Product Model
		// $suggestedProduct = $product->getSuggestedProduct($cat_id->category_id);
		// $suggestedProduct->product_code = '11';
		// get products might be interested
		$productsInterested = $product->getSimilarProducts($cat_id->category_id);
		// $productsInterested = '';

		$meta = $this->meta->tags();
		$meta['description'] = $desc;

		$title = strtolower($product->product_name);
		$meta['title'] = $title;



    	return view('product.product')
						    	->with('meta',$meta)
							    ->with('isOffice',self::isOffice($rqst))
							    ->with('sameProduct', $sameProduct)
								->with('similar_products', $productsInterested)
								->with('category',$category)
								// ->with('product_code',$suggestedProduct->product_code)
						    	->with('product',$product);
		
	}


	public function productModel($category_alias, $product_code, $model_code, Request $rqst)
	{
		$product = $this->product->findCode($product_code);
		// $category = $this->category->findAlias($category_alias);
		$category = $product->category;

		if ($product->product_desc != '') {
			$desc = strip_tags($product->product_desc);
	        $desc = preg_replace('/\s+/', ' ',$desc);
		} else {
			$desc = false;
		}

		$title = strtolower($product->product_name);

		$meta = $this->meta->tags();
		$meta['description'] = ($desc === false)?$meta['description']:$desc;
		$meta['title'] = $title;


    	// return view('product.product', compact('categories','category','product','model_code'));
    	return view('product.product')
						    	->with('meta',$meta)
							    ->with('isOffice',self::isOffice($rqst))
						    	->with('category',$category)
						    	->with('product',$product)
						    	->with('model_code',$model_code);

	}
	




	public function catalogProduct($category_alias, Request $rqst)
	{
		
		$itm_pg = Cookie::get('almani_itm_pg');
		$itm_pg = ($itm_pg)?$itm_pg:"15";

		$category = $this->category->findAlias($category_alias);
    	
    	// this thing generates the item from 
    	$category_products = $category->getProductsAndSubProductsNonClosure()->paginate($itm_pg);
    	

		$products_links = $category_products->links('pagination.products');


		$desc = $category->getMetaDesc('meta');
		$desc = ($desc)?$desc:$category->category_other_desc;
		$desc = 'Our finest '.$category->category_name.', '.$desc;
		$desc = Util::getMaxChar($desc,230);

		$meta = $this->meta->tags();
		$meta['description'] = $desc;

		$meta['title'] = $category->category_meta_title;
		$meta['keywords'] = $category->category_keywords;


    	return view('category.catalog-product')
		    	->with('meta',$meta)
		    	->with('isOffice',self::isOffice($rqst))
		    	->with('category_alias', $category_alias)
		    	->with('category_products',$category_products)
		    	->with('products_links',$products_links)
		    	->with('category',$category);

	}



	// sorting price
	public function productSort(Request $request) {

		// dd($request->all());

		$status = false;

		$itm_pg = Cookie::get('almani_itm_pg');
		
		$itm_pg = ($itm_pg)?$itm_pg:"15";
		
		$category = $this->category->findAlias($request->category_alias);
		
		// $category_products = $category->getProductsAndSubProductsNonClosuresorting($request->sort_type, $request->sort_key)->paginate($itm_pg);

		$category_products = $category->getProductsAndSubProductsNonClosure()->paginate(500);
		
		$products_links = "" . $category_products->links('pagination.product-sort-price') . "";

		$view = view('category.catalog-product-ajax', ['isOffice' => self::isOffice($request), 'category' => $category,'category_products' => $category_products, 'products_links' => $products_links]);
		
		$content = $view->render();

		if (!empty($category_products)) {
			$status = true;
		}		

		return response()->json( ['sort_type' => $request->sort_type ,'links' => $products_links, 'success' => $status, 'product_html' => $content] );
	
	}














	// sorting priceApplication

	public function priceApplication() {
	
		$status = false;


	}


	public function applicationArea($app_slug, Request $rqst) {

		$itm_pg = Cookie::get('almani_itm_pg');
		$itm_pg = ($itm_pg)?$itm_pg:"15";

		$app_area = ProductApplicationArea::where('app_slug','=',$app_slug)->first();

		$products = $app_area->getProductsNonClosure()->paginate($itm_pg);

		// dd($products->toArray());

		$products_links = $products->links('pagination.products');

        $prod_count = count($products);
        $pages = ceil($prod_count/$itm_pg);

		$meta = $this->meta->tags();
		$meta['description'] = $app_area->app_title;
		$meta['title'] = $app_area->app_title;

    	return view('application-area.application-area-product')
				    	->with('meta',$meta)
					    ->with('isOffice',self::isOffice($rqst))
				    	->with('itm_pg',$itm_pg)
				    	->with('prod_count',$prod_count)
				    	->with('products',$products)
				    	->with('products_links',$products_links)
				    	->with('app_area',$app_area);
	}


	// return a ajax result to sort all the application area 
	public function applicationAreaProduct(Request $rqst) {

		$itm_pg = Cookie::get('almani_itm_pg');
		$itm_pg = ($itm_pg)?$itm_pg:"15";

		$app_area = ProductApplicationArea::where('app_slug','=',$app_slug)->first();
		$products = $app_area->getProductsNonClosure()->paginate($itm_pg);
		$products_links = $products->links('pagination.products');
        $prod_count = count($products);
        $pages = ceil($prod_count/$itm_pg);

		$meta = $this->meta->tags();
		$meta['description'] = $app_area->app_title;
		$meta['title'] = $app_area->app_title;

		return response()->json(['']);

	}



	public function applicationAreaLanding(Request $rqst) {
		
		$catIns = $this->category;
		$app_areas = $catIns->getAppAreas();


		return view('application-area.application-area-landing')
				    ->with('isOffice',self::isOffice($rqst))
					->with('meta_title','Application Areas')
					->with('app_areas', $app_areas);

	}


	public function isOffice($rqst){

		// disable because the public ip keep changing
		// if($rqst->ip() == "86.98.154.166")
		// return true;
		// else 
		// return false;

		return true;
	}



}
