<?php

namespace App\Http\Controllers;

use Mail;
use DateTime;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Product\ProductInterface;
use App\Product\ProductModel\ProductModelInterface;
use App\Http\Controllers\Controller;

use App\Util;

use App\Client;
use App\Supplier;
use App\DownloadList;
use App\NewWebsiteSentEmail;

use Auth;


class EnquiryController extends Controller
{

	private static $info_email;

   	public function __construct() {	
		self::$info_email = env('APP_INFO_EMAIL');
   	}

	public function sendEnquiry(Request $rqst) {

		$retVal = "";
		$error = false;
		$message = 'An enquiry has been sent! We will get back to you shortly.';


		$info_name = 'Almani Lighting';
		// $info_email = 'r.delarama@almani.ae';
		$info_email = self::$info_email;

		$title1 = "Thank you ".strtoupper($rqst->full_name)." for choosing Almani Lighting and making an enquiry. We will reply to you as soon as possible.<br><br> A copy of your enquiry is added below for your record.";

		////////////////////
		if(!$rqst->model_code) {
			$href = route('product_page', [
	                      'category_alias' => $rqst->category_alias,
                      	  'product_code' => $rqst->product_code,
	      	]);
		    $name_code = $rqst->product_name.' ('.strtoupper($rqst->product_code).')';
		} else {
			$href = route('productmodel_page', [
                          'category_alias' => $rqst->category_alias,
                          'product_code' => $rqst->product_code,
                          'model_code' => $rqst->model_code,
          			]);
		    $name_code = $rqst->product_name.' ('.strtoupper($rqst->model_code).')';
		}

	    $product_link = '<a href="'.$href.'">'.$name_code.'</a>';
	    $enquiry_msg = str_replace($name_code, $product_link, $rqst->message);
		////////////////////


    	$data = array(

			'name_from' => $rqst->full_name, 
			'email_from' => $rqst->email, 

			'name_to' => $rqst->full_name,
			'email_to' => $rqst->email, 

			'info_name' => $info_name,
			'info_email' => $info_email,

			'subject' => 'Web Enquiry for Almani Lighting', 
			'other_message' => $enquiry_msg, 
			'tel' => $rqst->tel, 
			'title1' => $title1, 
			'title2' => '', 
    	);


		try {

			$mail_status = Mail::send( 'emails.almani.enquiry', $data, function( $message ) use ($data) {
			    // $message->to( $data['email_to'], $data['name_to'] )
			    // 		->from( $data['email_from'], $data['name_from'] )
			    // 		->cc([ $data['info_email'] ])
			    // 		->subject(  $data['subject'] );


			    // $message->to( $data['info_email'] )
			    // 		->from( $data['email_from'], $data['name_from'] )
			    // 		->cc([ $data['email_to'] ])
			    // 		->subject(  $data['subject'] );

				// 
				  $message->to( $data['email_to'], $data['name_to'] )
				       	->from( $data['info_email'], $data['info_name'] )
			    		  // ->cc([ $data['email_from'] ])
			  		    ->cc(['info@almani.ae'])
			  		    ->subject(  $data['subject'] );


			});


		} catch (\Swift_RfcComplianceException $Ste) {
			$retVal = 'sent_failed '.$Ste;	
			$error = true;
			$message = 'Please try again later. There was an error when sending your enquiry.';

	    } catch (\Swift_TransportException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your enquiry.';
	    }

	    // dd($data);

		return back()->with('error',$error)->with('message',$message);
	}

	/* USE VERSION 3*/
	function VerifyGoogleCaptcha() {

	        $captcha_key = env('CAPTCHA_KEY');
	        $captcha_secret_key = env('CAPTCHA_SECRET_KEY');

		    $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
            $recaptcha = json_decode($recaptcha);

            if($recaptcha->success==true) {

            	     if ($recaptcha->score >= 0.5) { 

            	     	// execute form 

            	     } else {

            	     	// not verified show form error

            	     }


            } else {

            	// something wrong here

            }
	}

	public function sendMessage(Request $rqst) {

			// dd($rqst->g-recaptcha-response);
			// $rules = [
			// 			'name_from'
			// ];

			$recaptcha_secret_key = env('CAPTCHA_KEY');
	        $recaptcha_secret = env('CAPTCHA_SECRET_KEY');

        	$recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
	        $recaptcha_response = $rqst->recaptcha_response;

		    $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
            $recaptcha = json_decode($recaptcha);

            if($recaptcha->success==true) {

            	     if ($recaptcha->score >= 0.5) { 
					            	     	
							 $rules = [
					            'email'    => 'required|email|max:255',
					            'full_name' => 'required',
					            'message' => 'required'
					        ];

							$this->validate($rqst, $rules);

							if(!empty($rqst->key)) {
								exit;
							}

							$retVal = "";
							$error = false;
							$message = 'An enquiry has been sent! We will get back to you shortly.';


							$info_name = 'Almani Lighting';
							// $info_email = 'r.delarama@almani.ae';
							$info_email = self::$info_email;

							$title1 = "Thank you ".strtoupper($rqst->full_name)." for choosing Almani Lighting and making an enquiry. We will reply to you as soon as possible.<br><br> A copy of your enquiry is added below for your record.";

							////////////////////
							if(!$rqst->model_code) {
								$href = route('product_page', [
						                      'category_alias' => $rqst->category_alias,
					                      	  'product_code' => $rqst->product_code,
						      	]);
							    $name_code = $rqst->product_name.' ('.strtoupper($rqst->product_code).')';
							} else {
								$href = route('productmodel_page', [
					                          'category_alias' => $rqst->category_alias,
					                          'product_code' => $rqst->product_code,
					                          'model_code' => $rqst->model_code,
					          			]);
							    $name_code = $rqst->product_name.' ('.strtoupper($rqst->model_code).')';
							}

						    $product_link = '<a href="'.$href.'">'.$name_code.'</a>';
						    $enquiry_msg = str_replace($name_code, $product_link, $rqst->message);
							////////////////////


					    	$data = array(
								'name_from' => $rqst->full_name, 
								'email_from' => $rqst->email, 
								'name_to' => $rqst->full_name,
								'email_to' => $rqst->email, 

								'info_name' => $info_name,
								'info_email' => $info_email,

								'subject' => 'Web Enquiry for Almani Lighting', 
								'other_message' => $enquiry_msg, 
								'tel' => $rqst->tel, 
								'title1' => $title1, 
								'title2' => '', 
					    	);


							try {

								$mail_status = Mail::send( 'emails.almani.enquiry', $data, function( $message ) use ($data) {

									    // $message->to( $data['email_to'], $data['name_to'] )
									    // 		->from( $data['email_from'], $data['name_from'] )
									    // 		->cc([ $data['info_email'] ])
									    // 		->subject(  $data['subject'] );

										  $message->to( $data['email_to'], $data['name_to'] )
										    	->from( $data['info_email'], $data['info_name'] )
										    		// ->cc([ $data['email_from'] ])
										  		->cc(['info@almani.ae'])
										  		->subject(  $data['subject'] );
									

								      //   $message->to( $data['info_email'] )
								    		// ->from( $data['email_from'], $data['name_from'] )
								    		// ->cc([ $data['email_to'] ])
								    		// ->subject(  $data['subject'] );
								    		
								});


							} catch (\Swift_RfcComplianceException $Ste) {
								$retVal = 'sent_failed '.$Ste;	
								$error = true;
								$message = 'Please try again later. There was an error when sending your enquiry.';

						    } catch (\Swift_TransportException $Ste) {
								$retVal = 'sent_failed '.$Ste;
								$error = true;
								$message = 'Please try again later. There was an error when sending your enquiry.';
						    }

						    // dd($retVal);

							return back()->with('error',$error)->with('message',$message);

            	     } else {

            	     		// not verified show form error
            	    		$error = true;
            	     		$message = 'Please try again later. We cant verified your data.';
            	     		return back()->with('error',$error)->with('message',$message);

            	     }


            } else {

				$error = true;
            	$message = 'Please try again later. We cant validate you. we feel your like a bot.';
            	return back()->with('error',$error)->with('message',$message);
            }
	}

	// temporary only
	public function sendMessageLimitedOffer(Request $rqst) {

			// dd($rqst->g-recaptcha-response);
			// $rules = [
			// 			'name_from'
			// ];

			$recaptcha_secret_key = env('CAPTCHA_KEY');
	        $recaptcha_secret = env('CAPTCHA_SECRET_KEY');

        	$recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
	        $recaptcha_response = $rqst->recaptcha_response;

		    $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
            $recaptcha = json_decode($recaptcha);

            if($recaptcha->success==true) {

            	     if ($recaptcha->score >= 0.5) { 
					            	     	
							 $rules = [
					            'email'    => 'required|email|max:255',
					            'full_name' => 'required',
					            'message' => 'required'
					        ];

							$this->validate($rqst, $rules);

							if(!empty($rqst->key)) {
								exit;
							}

							$retVal = "";
							$error = false;
							$message = 'An enquiry has been sent! We will get back to you shortly.';


							$info_name = 'Almani Lighting';
							// $info_email = 'r.delarama@almani.ae';
							$info_email = self::$info_email;

							$title1 = "Thank you ".strtoupper($rqst->full_name)." for choosing Almani Lighting and making an enquiry. We will reply to you as soon as possible.<br><br> A copy of your enquiry is added below for your record.";

							////////////////////
							if(!$rqst->model_code) {
								$href = route('product_page', [
						                      'category_alias' => $rqst->category_alias,
					                      	  'product_code' => $rqst->product_code,
						      	]);
							    $name_code = $rqst->product_name.' ('.strtoupper($rqst->product_code).')';
							} else {
								$href = route('productmodel_page', [
					                          'category_alias' => $rqst->category_alias,
					                          'product_code' => $rqst->product_code,
					                          'model_code' => $rqst->model_code,
					          			]);
							    $name_code = $rqst->product_name.' ('.strtoupper($rqst->model_code).')';
							}

						    $product_link = '<a href="'.$href.'">'.$name_code.'</a>';
						    $enquiry_msg = str_replace($name_code, $product_link, $rqst->message);
							////////////////////


					    	$data = array(
								'name_from' => $rqst->full_name, 
								'email_from' => $rqst->email, 
								'name_to' => $rqst->full_name,
								'email_to' => $rqst->email, 

								'info_name' => $info_name,
								'info_email' => $info_email,

								'subject' => 'Full 10-Year Project Warranty Inquiry for Almani Lighting', 
								'other_message' => $enquiry_msg, 
								'tel' => $rqst->tel, 
								'title1' => $title1, 
								'title2' => '', 
					    	);


							try {

								$mail_status = Mail::send( 'emails.almani.enquiry', $data, function( $message ) use ($data) {

									    // $message->to( $data['email_to'], $data['name_to'] )
									    // 		->from( $data['email_from'], $data['name_from'] )
									    // 		->cc([ $data['info_email'] ])
									    // 		->subject(  $data['subject'] );

										  $message->to( $data['email_to'], $data['name_to'] )
										    	->from( $data['info_email'], $data['info_name'] )
										    		// ->cc([ $data['email_from'] ])
										  		->cc(['info@almani.ae'])
										  		->subject(  $data['subject'] );
									

								      //   $message->to( $data['info_email'] )
								    		// ->from( $data['email_from'], $data['name_from'] )
								    		// ->cc([ $data['email_to'] ])
								    		// ->subject(  $data['subject'] );
								    		
								});

								 setcookie('limitedOffer','yes', time() + (86400 * 3000), "/"); // 86400 = 1 day

							} catch (\Swift_RfcComplianceException $Ste) {
								$retVal = 'sent_failed '.$Ste;	
								$error = true;
								$message = 'Please try again later. There was an error when sending your enquiry.';

						    } catch (\Swift_TransportException $Ste) {
								$retVal = 'sent_failed '.$Ste;
								$error = true;
								$message = 'Please try again later. There was an error when sending your enquiry.';
						    }

						    // dd($retVal);

							return back()->with('error',$error)->with('message',$message);

            	     } else {

            	     		// not verified show form error
            	    		$error = true;
            	     		$message = 'Please try again later. We cant verified your data.';
            	     		return back()->with('error',$error)->with('message',$message);

            	     }


            } else {

				$error = true;
            	$message = 'Please try again later. We cant validate you. we feel your like a bot.';
            	return back()->with('error',$error)->with('message',$message);
            }
	}


	public function sendCategoryEnquiry(Request $rqst) {

		$retVal = "";
		$error = false;
		$message = 'An enquiry has been sent! We will get back to you shortly.';


		$info_name = 'Almani Lighting';
		// $info_email = 'r.delarama@almani.ae';
		$info_email = self::$info_email;

		$title1 = "Thank you ".strtoupper($rqst->full_name)." for choosing Almani Lighting and making an enquiry. We will reply to you as soon as possible.<br><br> A copy of your enquiry is added below for your record.";

		////////////////////
		$href = route('catalog_product_page', [
                      'category_alias' => $rqst->category_alias,
      	]);
	    $name_code = strtoupper($rqst->category_name);
	    $product_link = '<a href="'.$href.'">'.$name_code.'</a>';
	    $enquiry_msg = str_replace($name_code, $product_link, $rqst->message);
		////////////////////

    	$data = array(
			'name_from' => $rqst->full_name, 
			'email_from' => $rqst->email, 
			'name_to' => $info_name, 
			'email_to' => $info_email, 
			'subject' => 'Web Enquiry for Almani Lighting', 

			'other_message' => $enquiry_msg, 
			'tel' => $rqst->tel, 
			'title1' => $title1, 
			'title2' => '', 
    	);

		try {

			$mail_status = Mail::send( 'emails.almani.enquiry', $data, function( $message ) use ($data) {
			    $message->to( $data['email_to'], $data['name_to'] )
			    		->from( $data['email_from'], $data['name_from'] )
			    		->cc([ $data['email_from'] ])
			    		->subject(  $data['subject'] );
			});


		} catch (\Swift_RfcComplianceException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your enquiry.';

	    } catch (\Swift_TransportException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your enquiry.';
	    }

	    // dd($data);

		return back()->with('error',$error)->with('message',$message);
	}


	public function sendListEnquiry(Request $rqst) {

		$retVal = "";
		$error = false;
		$message = 'An enquiry has been sent! We will get back to you shortly.';


		$info_name = 'Almani Lighting';
		// $info_email = 'r.delarama@almani.ae';
		$info_email = self::$info_email;

		$title1 = "Thank you ".strtoupper($rqst->full_name)." for choosing Almani Lighting and making an enquiry. We will reply to you as soon as possible.<br><br> A copy of your enquiry is added below for your record.";

		$enquiry_msg = $rqst->message;

		$products_Arr = json_decode($rqst->products_json);

		
		foreach ($products_Arr as $key => $val) {
			$href = route('productmodel_page', [
                      'category_alias' => $val->category_alias,
                      'product_code' => $val->product_code,
                      'model_code' => $val->model_code,
          	]);

		    $name_code = $val->product_name.' ('.$val->model_code.')';
		    $product_link = '<a href="'.$href.'">'.$name_code.'</a>';
		    $enquiry_msg = str_replace($name_code, $product_link, $enquiry_msg);
		}



    	$data = array(
			'name_from' => $rqst->full_name, 
			'email_from' => $rqst->email, 
			'name_to' => $info_name, 
			'email_to' => $info_email, 
			'subject' => 'Web Enquiry for Almani Lighting', 

			'other_message' => $enquiry_msg, 
			'tel' => $rqst->tel, 
			'title1' => $title1, 
			'title2' => '', 
    	);


		try {

			$mail_status = Mail::send( 'emails.almani.enquiry', $data, function( $message ) use ($data) {
			    $message->to( $data['email_to'], $data['name_to'] )
			    		->from( $data['email_from'], $data['name_from'] )
			    		->cc([ $data['email_from'] ])
			    		->subject(  $data['subject'] );
			});


		} catch (\Swift_RfcComplianceException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your enquiry.';

	    } catch (\Swift_TransportException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your enquiry.';
	    }

	    // dd($data);

		return back()->with('error',$error)->with('message',$message);
	}


	public function requestDatasheet(Request $rqst) {

		$retVal = "";
		$error = false;
		$message = 'The request has been sent! We will get back to you shortly.';


		$info_name = 'Almani Lighting';
		// $info_email = 'r.delarama@almani.ae';
		$info_email = self::$info_email;

		$title1 = "Thank you ".strtoupper($rqst->full_name)." for choosing Almani Lighting and requesting for a DATASHEET. We will reply to you as soon as possible.<br><br> A copy of your request is added below for your record.";

		////////////////////
		$href = route('productmodel_page', [
                  'category_alias' => $rqst->category_alias,
                  'product_code' => $rqst->product_code,
                  'model_code' => $rqst->model_code,
      	]);
	    $name_code = $rqst->product_name.' ('.strtoupper($rqst->model_code).')';

	    $product_link = '<a href="'.$href.'">'.$name_code.'</a>';
	    $enquiry_msg = str_replace($name_code, $product_link, $rqst->message);
		////////////////////


    	$data = array(
			'name_from' => $rqst->full_name, 
			'email_from' => $rqst->email, 
			'name_to' => $info_name, 
			'email_to' => $info_email, 
			'subject' => 'Request DATASHEET for '.$rqst->product_name, 

			'other_message' => $enquiry_msg, 
			'tel' => $rqst->tel, 
			'title1' => $title1, 
			'title2' => '<h1 style="text-align: left;margin: 0;margin-bottom: 20px;">Request DATASHEET</h1>', 
    	);


		try {

			$mail_status = Mail::send( 'emails.almani.enquiry', $data, function( $message ) use ($data) {
			    $message->to( $data['email_to'], $data['name_to'] )
			    		->from( $data['email_from'], $data['name_from'] )
			    		->cc([ $data['email_from'] ])
			    		->subject(  $data['subject'] );
			});


		} catch (\Swift_RfcComplianceException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your request.';

	    } catch (\Swift_TransportException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your request.';
	    }

	    // dd($data);

		return back()->with('error',$error)->with('message',$message);
	}



	public function requestTechnicalFiles(Request $rqst) {

		$retVal = "";
		$error = false;
		$message = 'The request has been sent! We will get back to you shortly.';


		$info_name = 'Almani Lighting';
		// $info_email = 'r.delarama@almani.ae';
		$info_email = self::$info_email;

		$title1 = "Thank you ".strtoupper($rqst->full_name)." for choosing Almani Lighting and requesting for a TECHNICAL FILES. We will reply to you as soon as possible.<br><br> A copy of your request is added below for your record.";

		////////////////////
		$href = route('productmodel_page', [
                  'category_alias' => $rqst->category_alias,
                  'product_code' => $rqst->product_code,
                  'model_code' => $rqst->model_code,
      	]);
	    $name_code = $rqst->product_name.' ('.strtoupper($rqst->model_code).')';

	    $product_link = '<a href="'.$href.'">'.$name_code.'</a>';
	    $enquiry_msg = str_replace($name_code, $product_link, $rqst->message);
		////////////////////

    	$data = array(
			'name_from' => $rqst->full_name, 
			'email_from' => $rqst->email, 
			'name_to' => $info_name, 
			'email_to' => $info_email, 
			'subject' => 'Request TECHNICAL FILES for '.$rqst->product_name, 

			'other_message' => $enquiry_msg, 
			'tel' => $rqst->tel, 
			'title1' => $title1, 
			'title2' => '<h1 style="text-align: left;margin: 0;margin-bottom: 20px;">Request TECHNICAL FILES</h1>', 
    	);


		try {

			$mail_status = Mail::send( 'emails.almani.enquiry', $data, function( $message ) use ($data) {
			    $message->to( $data['email_to'], $data['name_to'] )
			    		->from( $data['email_from'], $data['name_from'] )
			    		->cc([ $data['email_from'] ])
			    		->subject(  $data['subject'] );
			});


		} catch (\Swift_RfcComplianceException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your request.';

	    } catch (\Swift_TransportException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your request.';
	    }

	    // dd($data);

		return back()->with('error',$error)->with('message',$message);
	}


	public function shareViaEmail(Request $rqst) {

		$retVal = "";
		$error = false;
		$message = 'Thank you for sharing our product!';


		$info_name = 'Almani Lighting';
		// $info_email = 'r.delarama@almani.ae';
		$info_email = self::$info_email;

		$title1 = strtoupper($rqst->full_name)." shared a product ".$rqst->model_name." to you via Email.";

		////////////////////
		$href = route('productmodel_page', [
                  'category_alias' => $rqst->category_alias,
                  'product_code' => $rqst->product_code,
                  'model_code' => $rqst->model_code,
      	]);

	    $name_code = $rqst->product_name.' ('.strtoupper($rqst->model_code).')';

	    $product_link = '<a href="'.$href.'">'.$name_code.'</a>';
	    $enquiry_msg = str_replace($name_code, $product_link, $rqst->message);
		////////////////////

    	$data = array(
			'name_from' => $info_name, 
			'email_from' => $info_email, 
			'email_cc' => $rqst->email, 
			'email_to' => $rqst->email_to_share, 
			'subject' => strtoupper($rqst->full_name)." shared a product ".$rqst->model_name." to you", 

			'other_message' => $enquiry_msg, 
			'tel' => $rqst->tel, 
			'title1' => $title1, 
			'title2' => '', 
    	);


		try {

			$mail_status = Mail::send( 'emails.almani.shareviaemail', $data, function( $message ) use ($data) {
			    $message->to( $data['email_to'] )
			    		->from( $data['email_from'], $data['name_from'] )
			    		->cc([ $data['email_cc'], $data['email_from'] ])
			    		->subject(  $data['subject'] );
			});


		} catch (\Swift_RfcComplianceException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your request.';

	    } catch (\Swift_TransportException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your request.';
	    }

	    // dd($data);

		return back()->with('error',$error)->with('message',$message);
	}


	public function requestDownloads(Request $rqst) {


			$recaptcha_secret_key = env('CAPTCHA_KEY');
	        $recaptcha_secret = env('CAPTCHA_SECRET_KEY');

        	$recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
	        $recaptcha_response = $rqst->recaptcha_response;

		    $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
            $recaptcha = json_decode($recaptcha);

            if($recaptcha->success==true) {

            	     if ($recaptcha->score >= 0.5) { 

							$retVal = "";
							$url = '';
							$catalogName = '';
							$error = false;
							$message = 'The request has been sent! We will get back to you shortly.';

							switch($rqst->catalogType) {
								case 'almani-catalog':
									$url = asset('assets/pdfs/kf73489hdjs992hh-almani.pdf');
									$catalogName = 'Almani Catalog';

									$dl = new DownloadList;
									$dl->dl_name = $rqst->full_name;
									$dl->dl_email = $rqst->email;
									$dl->dl_brand = 'almani';
									$dl->save();

								break;
								case 'camel-catalog':
									$url = asset('assets/pdfs/bcj8syh7fi3of328j-camelled.pdf');
									$catalogName = 'CamelLED Catalog';

									$dl = new DownloadList;
									$dl->dl_name = $rqst->full_name;
									$dl->dl_email = $rqst->email;
									$dl->dl_brand = 'camel';
									$dl->save();

								break;
								case 'company-download': 
									$url = asset('assets/pdfs/ki8suiois8gugnjdk-company-profile.pdf');
									$catalogName = 'Company Profile';

									$dl = new DownloadList;
									$dl->dl_name = $rqst->full_name;
									$dl->dl_email = $rqst->email;
									$dl->dl_brand = 'company-profile';
									$dl->save();

								break;
								case 'pq':
									$url = asset('assets/pdfs/kpr8opqvg67ijug-pre-qualifications.pdf');
									
									$catalogName = 'Pre-Qualification';

									$dl = new DownloadList;
									$dl->dl_name = $rqst->full_name;
									$dl->dl_email = $rqst->email;
									$dl->dl_brand = 'pre-qualification';
									$dl->save();

								break;


								case 'b2b':
								$url = asset('assets/pdfs/3zo1ffcsg7-b2b.pdf');
								
								$catalogName = 'Business to Business (B2B)';
								
								$dl = new DownloadList;
								$dl->dl_name = $rqst->full_name;
								$dl->dl_email = $rqst->email;
								$dl->dl_brand = 'business-to-business';
								$dl->save();

								break;

								case 'ledtowon';
								
								$url = asset('assets/pdfs/5b30vmfde2-led-to-own.pdf');
								$catalogName = 'Led to Own';

								$dl = new DownloadList;
								$dl->dl_name = $rqst->full_name;
								$dl->dl_email = $rqst->email;
								$dl->dl_brand = 'led-to-own';
								$dl->save();

								break;

								case 'approved-supplier';

								$url = asset('assets/pdfs/74zcj7hzal-approved-supplier.pdf');
								$catalogName = 'Approved Supplier';

								$dl = new DownloadList;
								$dl->dl_name = $rqst->full_name;
								$dl->dl_email = $rqst->email;
								$dl->dl_brand = 'approved-supplier';
								break;

								case 'price-match-promise';

								$url = asset('assets/pdfs/ueficla8fl-price-match-promise.pdf');
								$catalogName = 'Price Match Promise';

								$dl = new DownloadList;
								$dl->dl_name = $rqst->full_name;
								$dl->dl_email = $rqst->email;
								$dl->dl_brand = 'price-match-promise';

								break;
							}


							$info_name = 'Almani Lighting';
							$info_email = self::$info_email; 
							// $info_email = 'l.parba@almani.ae';

							$title1 = "Thank you ".strtoupper($rqst->full_name)." for choosing Almani Lighting. Please click the link below for your downloadables";

					    	$data = array(
								'name_from' => $info_name, 
								'email_from' => $info_email, 
								'name_to' => $rqst->full_name, 
								'email_to' => $rqst->email, 
								'subject' => 'Downloadble Link of ' . $catalogName, 
								'catalog_name' => $catalogName,
								'url' => $url,
								'phone' => $rqst->phone,
								'other_message' => '', 
								'tel' => '', 
								'title1' => $title1, 
								'title2' => '<h1 style="text-align: left;margin: 0;margin-bottom: 20px;"></h1>', 
					    	);

					    	// dd($data);

							try {

							$res = $mail_status = Mail::send( 'emails.almani.downloads', $data, function( $message ) use ($data) {
								    $message->to( $data['email_to'], $data['name_to'] )
								    		->from( $data['email_from'], $data['name_from'] )
										  		->cc(['info@almani.ae'])
								    		->subject(  $data['subject'] );
								});

								// ->bcc([ 'r.delarama@almani.ae','info@almani.ae' ])

							} catch (\Swift_RfcComplianceException $Ste) {
								$retVal = 'sent_failed '.$Ste;
								$error = true;
								$message = 'Please try again later. There was an error when sending your request.';

						    } catch (\Swift_TransportException $Ste) {
								$retVal = 'sent_failed '.$Ste;
								$error = true;
								$message = 'Please try again later. There was an error when sending your request.';
						    }

							// dd($retVal);

							return back()->with('error',$error)->with('message',$message)->with('is_dl', true);

            	     } else {

						$error = true;
        	     		$message = 'Please try again later. We cant verified your data.';
        	     		return back()->with('error',$error)->with('message',$message);

            	     }

           } else {

				$error = true;
            	$message = 'Please try again later. We cant validate you. we feel your like a bot.';
            	return back()->with('error',$error)->with('message',$message);


           }
	}

































	public function viewEmailCLientsAndSuppliers() {

	////////////////////////////////////////////


		// clients and suppliers here
		///////////////////////////////////////////
		$full_name = "Supplier 1";
		$email = self::$info_email;
		///////////////////////////////////////////
		// clients and suppliers here


		$info_name = 'Almani Lighting';
		$info_email = self::$info_email;

    	$data = array(
			'name_from' => $info_name, 
			'email_from' => $info_email, 
			'name_to' => $full_name, 
			'email_to' => $email, 
			'subject' => 'Almani Lighting Website Relaunch',  

    	);

    	return view('emails.almani.newwebsiteemail')->with('data',$data);


	////////////////////////////////////////////
	}

	public function emailCLientsAndSuppliers() {

		// $val = [
		// 	"name" => "Supplier Name",
		// 	"email" => "yabuking84@gmail.com",
		// 	"cc" => [],
		// ];

		// self::sendNewWebsiteMessage($val);


		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$suppliers = Supplier::select('supplier_id as id','supplier_contact as name','supplier_email as email','supplier_cc as cc')
							->where('supplier_enable','=','1')
							->has('newwebsitesentemails','<',1)
							->limit(3)
							->get()
							->toArray();
		foreach ($suppliers as &$val) {
			$val['type'] = "supplier";
			$json = [];
			$json = json_decode($val['cc'],true);
			$val['cc'] = collect($json)->pluck('email')->toArray();
		}
		unset($val);


		foreach ($suppliers as $val) {
			$status = self::sendNewWebsiteMessage($val);

			$nwse = new NewWebsiteSentEmail;
			$nwse->nwse_status = $status;
			$nwse->supplier_id = $val['id'];
			$nwse->sent_at = date('Y-m-d H:i:s');
			$nwse->save();

		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$clients = Client::select('client_id as id','client_name as name','client_email as email','client_cc as cc')
							->where('client_active','=','1')
							->has('newwebsitesentemails','<',1)
							->limit(3)
							->get()
							->toArray();

		foreach ($clients as &$val) {
			$val['type'] = "client";
			
			if($val['cc'])
			$val['cc'] = explode(',',$val['cc']);
			else
			$val['cc'] = [];

		}
		unset($val);

		foreach ($clients as $val) {
			$status = self::sendNewWebsiteMessage($val);

			$nwse = new NewWebsiteSentEmail;
			$nwse->nwse_status = $status;
			$nwse->client_id = $val['id'];
			$nwse->sent_at = date('Y-m-d H:i:s');
			$nwse->save();

		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		

		echo "<pre>";
		print_r($clients);
		echo "</pre>";
		echo "<pre>";
		print_r($suppliers);
		echo "</pre>";



		echo '<br><br>fin<br>';

	}




	public function sendNewWebsiteMessage($data) {


		$retVal = "";
		$error = false;
		$message = "";


		// clients and suppliers here
		///////////////////////////////////////////
		$full_name = $data['name'];
		$email = $data['email'];
		$email_cc = $data['cc'];

		// $full_name = 'Client or Supplier Name';
		// $email = 'r.delarama@almani.ae';
		// $email_cc = [ 'r.delarama84@gmail.com'];
		///////////////////////////////////////////
		// clients and suppliers here


		$info_name = 'Almani Lighting';
		$info_email = self::$info_email;
		$bcc = [ 'r.delarama@almani.ae'];









		$data = array(
			'name_from' => $info_name, 
			'email_from' => $info_email, 
			'name_to' => $full_name, 
			'email_to' => $email, 
			'email_cc' => $email_cc, 
			'bcc' => $bcc, 
			'subject' => 'Almani Lighting Website Relaunch',  
    	);


		try {

			$mail_status = Mail::send( 'emails.almani.newwebsiteemail', $data, function( $message ) use ($data) {
			    $message->to( $data['email_to'], $data['name_to'] )
			    		->cc($data['email_cc'])
			    		->from( $data['email_from'], $data['name_from'] )
			    		->bcc($data['bcc'])
			    		->subject( $data['subject'] );
			});


		} catch (\Swift_RfcComplianceException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your enquiry.';

	    } catch (\Swift_TransportException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your enquiry.';
	    }

		if($error) {			
			$retVal = 'error';
		} else 
		$retVal = '';

		return $retVal;

	}





}
