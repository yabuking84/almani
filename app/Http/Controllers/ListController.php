<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App\Camel\Product\ProductInterface;
use App\Camel\Product\ProductModel\ProductModelInterface;
use App\Camel\Category\CategoryInterface;

use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;

Use App\Camel\Meta;
use Auth;

class ListController extends Controller
{
	/**
	 * Initialize Interface
	 */
	public function __construct(CategoryInterface $category, ProductInterface $product, ProductModelInterface $model)
	{
		$this->category = $category;
		$this->product = $product;
		$this->model = $model;
	}

	public function index()
	{
		$camelled_list = Cookie::get('camelled_list');
		if($camelled_list) {
			$camelled_list = json_decode($camelled_list,true);
			$model_ids = array_keys($camelled_list);
			$models = $this->model->findManyByCode($model_ids);
			$cookie_items = $camelled_list;
		} else {
			$models = [];		
			$cookie_items = [];		
		}

		$meta = new Meta;


    	return view('list', compact('models','cookie_items','meta'));
	}

}
