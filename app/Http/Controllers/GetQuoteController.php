<?php

namespace App\Http\Controllers;

use Mail;
use DateTime;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Product\ProductInterface;
use App\Product\ProductModel\ProductModelInterface;
use App\Http\Controllers\Controller;

use App\Util;
Use App\Meta;

use Auth;

class GetQuoteController extends Controller
{
	private static $info_email;

   	public function __construct() {	
		self::$info_email = env('APP_INFO_EMAIL');
   	}


	public function index() {
		return view('getaquote');
	}

	public function viewEmail() {

		return view('emails.getaquote')
		->with('other_message','asd')
		->with('name_from','asd')
		->with('email_from','asd')
		->with('name_to','asd')
		->with('email_to','asd')
		->with('subject','asd')
		->with('files',[])
		->with('tel','asd')
		->with('project_type','asd')
		->with('project_type_others','asd')
		->with('construction_status','asd')
		->with('construction_status_others','asd')
		->with('project_budget_currency','asd')
		->with('project_budget','asd')
		->with('location','asd');
	}

	public function sendQuote(Request $rqst) {



			$recaptcha_secret_key = env('CAPTCHA_KEY');
	        $recaptcha_secret = env('CAPTCHA_SECRET_KEY');

        	$recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
	        $recaptcha_response = $rqst->recaptcha_response;

		    $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
            $recaptcha = json_decode($recaptcha);

            // dd($recaptcha);

            if($recaptcha->success==true) {

            	  if ($recaptcha->score >= 0.5) { 

							$retVal = "";
							$error = false;
							$message = 'Quote request has been sent! We will get back to you shortly.';

							$files = $rqst->file('file_quote');

							$filesArr = Util::uploadFiles($files, env('EMAIL_ATTACHMENTS'));

							$info_name = 'Almani Lighting';
							$info_email = self::$info_email;
							// $info_email = 'info@almani.ae';

					    	$data = array(
								'other_message' => $rqst->project_definition, 
								'name_from' => $rqst->full_name, 
								'email_from' => $rqst->email, 
								'name_to' => $rqst->full_name, 
								'email_to' => $rqst->email, 
								'subject' => 'Quote Request for Almani Lighting', 

								'info_name' => $info_name,
								'info_email' => $info_email,


								'tel' => $rqst->tel, 
								'project_type' => $rqst->project_type, 
								'project_type_others' => $rqst->project_type_others, 
								'construction_status' => $rqst->construction_status, 
								'construction_status_others' => $rqst->construction_status_others, 
								'project_budget_currency' => $rqst->project_budget_currency, 
								'project_budget' => $rqst->project_budget, 
								'location' => $rqst->location, 
								'files' => $filesArr, 
					    	);

							try {

								$mail_status = Mail::send( 'emails.almani.getaquote', $data, function( $message ) use ($data) {
								     $message->to( $data['email_to'], $data['name_to'] )
										    	->from( $data['info_email'], $data['info_name'] )
										    		// ->cc([ $data['email_from'] ])
										  		->cc(['info@almani.ae'])
										  		->subject(  $data['subject'] );
								});


							} catch (\Swift_RfcComplianceException $Ste) {
								$retVal = 'sent_failed '.$Ste;
								$error = true;
								$message = 'Please try again later. There was an error when sending your qoute.';

						    } catch (\Swift_TransportException $Ste) {
								$retVal = 'sent_failed '.$Ste;
								$error = true;
								$message = 'Please try again later. There was an error when sending your qoute.';
						    }

						    // dd($data);
							return redirect()
										->route('getaquote_page')
										->with('error',$error)
										->with('message',$message);
            	  
            	  } else {

            	  		$error = true;
        	     		$message = 'Please try again later. We cant verified your data.';
        	     		return back()->with('error',$error)->with('message',$message);

            	  }

            } else {

            	$error = true;
            	$message = 'Please try again later. We cant validate you. we feel your like a bot.';
            	return back()->with('error',$error)->with('message',$message);

            }

		
	}






}
