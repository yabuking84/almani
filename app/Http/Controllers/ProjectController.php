<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Mail;
use DateTime;


use App\Project\ProjectItem\ProjectItem;

use App\Project\ProjectItem\ProjectItemNote;

use App\Project\ProjectComment;

use App\Project\Project;
use App\Project\ProjectInterface;

use App\Project\ProjectFile;

use Illuminate\Http\Request;
// use Illuminate\Support\Str;


use Illuminate\Support\Facades\Storage;


use Auth;

class ProjectController extends Controller
{
	private static $info_email = 'info@almani.ae';	
	private static $info_name = 'Almani Lighting LLC';		
	private static $custom_product_path = 'images/custom_products';
	private static $project_files_path = 'project_uploads';

	/**
	 * Initialize Interface
	 */
	public function __construct(ProjectInterface $project)
	{
		$this->project = $project;
	}

	public function projects()
	{

		if(Auth::user()->bu_user_type=="admin") {			
			$user_type = "admin";
		}
    	else {
			$user_type = "client";
    	}

		$projects_pending = $this->project->getProjectsWithStatus(['new','modified'],$user_type)->get();
		$projects_sent = $this->project->getProjectsWithStatus(['sent'],$user_type)->get();			

    	return view('projects')
				    	->with('projects_pending',$projects_pending)
				    	->with('projects_sent',$projects_sent);
	}

	public function viewProject($bp_id)
	{
		$project = $this->project->find($bp_id);

		$ifUserHasProject = Auth::user()->projects()->get()->pluck('bp_id')->toArray();
		$ifUserHasProject = in_array($bp_id, $ifUserHasProject);

		if(Auth::user()->bu_user_type == "admin" || ($project && $ifUserHasProject))
    	return view('project')->with('project',$this->project->find($bp_id));
    	else
		return redirect()->route('projects_page');
	}

	public function viewProjectUrl($url_key)
	{
		$project = Project::where('bp_url_key','=',$url_key)->first();
    	// return view('project-view')->with('project',$project);

		if($project && Auth::user()->bu_user_type=="admin")
    	return view('project-view')->with('project',$project);
    	else
		return redirect()->route('projects_page');
	}


	public function addProject(Request $rqst) {

		// $data = [];
		// $data['bp_name'] = $rqst->project_name;
		// $data['bp_location'] = $rqst->project_location;
		// $data['bp_desc'] = $rqst->project_desc;
		// $data['bp_status'] = $rqst->project_status;

		$project = new Project;
		$project->bp_name = $rqst->project_name;
		$project->bp_location = $rqst->project_location;
		$project->bp_desc = $rqst->project_desc;
		$project->bp_bldg_status = $rqst->project_bldg_status;
		$project->bp_url_key = strtolower(sha1('dxb_'.time()));
		$project->save();

		Auth::user()->projects()->attach($project->bp_id);

		return redirect()->route('project_view_page',['bp_id'=>$project->bp_id]);
	}


	public function addCommentProject(Request $rqst) {

		$p_comment = new ProjectComment();
		$p_comment->bpc_comment = $rqst->project_comment."";
		$p_comment->save();

		Auth::user()->comments()->save($p_comment);
		Project::find($rqst->project_id)->comments()->save($p_comment);

		$strtotime = strtotime($p_comment->created_at);

		$created_at = [
			"year"=> date("Y", $strtotime)+0,
			"month"=> date("n", $strtotime)-1,
			"day"=> date("j", $strtotime)+0,
			"hour"=> date("G", $strtotime)+0,
			"min"=> date("i", $strtotime)+0,
			"timezone"=> "UTC",
		];

		$retVal = [
			"name" => Auth::user()->bu_fname.' '.Auth::user()->bu_lname,
			"comment" => $p_comment->bpc_comment,
			"date" => $created_at,
		];

		return $retVal;
	}

	public function editProject(Request $rqst) {

		$project = Project::find($rqst->project_id);
		$project->bp_name = $rqst->project_name;
		$project->bp_location = $rqst->project_location;
		$project->bp_desc = $rqst->project_desc;
		$project->bp_bldg_status = $rqst->project_bldg_status;
		$retVal = $project->save();

		return redirect()->route('projects_page');
	}


	public function deleteProject(Request $rqst) {

		Auth::user()->projects()->detach($rqst->project_id);
		Project::find($rqst->project_id)->delete();

		
		return redirect()->route('projects_page');
	}


	public function setDefaultProject(Request $rqst) {

		Auth::user()->bu_default_project = $rqst->project_id;
		Auth::user()->save();

		return "1";

	}

	public function addItemToProject(Request $rqst) {


		// $def_project = Auth::user()->defaultProject;
		$def_project = Project::find($rqst->project_id);

		$items = json_decode($rqst->items,true);

		$def_project->items()->createMany([
		    [
		        'bpi_model_code' => $items['model_code'],
		        'bpi_model_options_dim' => $items['model_options_dim'],
		        'bpi_model_options_ips' => $items['model_options_ips'],
		        'bpi_model_options_beam_angles' => $items['model_options_beam_angles'],
		        'bpi_model_options_warranty' => $items['model_options_warranty'],
		        'bpi_model_options_wifi' => $items['model_options_wifi'],
		        'bpi_text' => $items['model_text'],
		    ],
		]);

		return '1';
	}

	public function addCustomProduct(Request $rqst) {

		// $def_project = Auth::user()->defaultProject;
		$def_project = Project::find($rqst->project_id);

		$product_custom_name = trim($rqst->product_custom_name);
		$product_custom_details = trim($rqst->product_custom_details);
		$product_custom_price = trim($rqst->product_custom_price);

		// $item = $def_project->items()->createMany([
		//     [
		//         'bpi_custom_name' => $product_custom_name,
		//         'bpi_custom_details' => $product_custom_details,
		//         'bpi_custom_price' => $product_custom_price,
		//     ],
		// ]);

		$item = new ProjectItem;
		$item->bpi_custom_name = $product_custom_name;
		$item->bpi_custom_details = $product_custom_details;
		$item->bpi_custom_price = $product_custom_price;
		$item->bp_id = $rqst->project_id;
		$item->save();

		$image = $rqst->file('product_custom_image');

		if($image)
		$image->storeAs(self::$custom_product_path,$item->bpi_id);

		// $filesArr = Util::uploadFiles($files, env('EMAIL_ATTACHMENTS'));


		// return '1';
		return back();

	}


	
	public function uploadProjectFile(Request $rqst) {

		$def_project = Project::find($rqst->project_id);
		$project_file_note = trim($rqst->project_file_note);
		$upload_type = $rqst->upload_type;

		$file = $rqst->file('project_file');
		if($upload_type && $file) {
			$filename = $file->getClientOriginalName();

			$pFile = new ProjectFile;
			$pFile->bp_id = $rqst->project_id;
			$pFile->uploaded_by = Auth::user()->bu_id;
			$pFile->bpf_note = $project_file_note;
			$pFile->bpf_filename = $filename;
			$pFile->bpf_upload_type = $upload_type;
			$pFile->save();
			
			$sha1 = strtolower(sha1('dxb_'.time()));
			$url_key = substr($sha1, 0, 3).$pFile->bpf_id.substr($sha1, 3, 10);
			$pFile->bpf_url_key = $url_key;
			$pFile->save();

			$file->storeAs(self::$project_files_path,$url_key);
			$retVal = "file uploaded";
			
		} else {
			$retVal = "file upload error";			
		}

		return back();
	}


	public function downloadProjectFile($url_key) {

		$file = ProjectFile::where('bpf_url_key','=',$url_key)->get()->first();

		return Storage::download(self::$project_files_path.'/'.$url_key,$file->bpf_filename);

	}


	public function deleteProjectFile(Request $rqst) {

		$url_key = $rqst->url_key;
		$file = ProjectFile::where('bpf_url_key','=',$url_key)->get()->first();

		$file->delete();

		Storage::delete(self::$project_files_path.'/'.$url_key);

		return back();
	}




	public function addNoteToItem(Request $rqst) {

		$note = new ProjectItemNote();
		$note->bu_id = Auth::user()->bu_id;
		$note->bpi_id = $rqst->item_id;
		$note->bpin_text = $rqst->text."";
		$note->save();

		$strtotime = strtotime($note->created_at);
		$created_at = [
			"year"=> date("Y", $strtotime)+0,
			"month"=> date("n", $strtotime)-1,
			"day"=> date("j", $strtotime)+0,
			"hour"=> date("G", $strtotime)+0,
			"min"=> date("i", $strtotime)+0,
			"timezone"=> "UTC",
		];

		$retVal = [
			"name" => Auth::user()->bu_fname.' '.Auth::user()->bu_lname,
			"user_type" => Auth::user()->bu_user_type,
			"note" => $note->bpin_text,
			"date" => $created_at,
		];

		return $retVal;		
	}

	public function saveItemsProject(Request $rqst) {

		$project = Project::find($rqst->project_id);
		$project->bp_status = "modified";
		$project->save();

		$items = collect(json_decode($rqst->items,true));
		$bpi_ids = $items->pluck('item_id')->toArray();


		
		// delete custom images
		///////////////////////////////////////////
		$cstm_itm_del = $project->items('all')->whereNotIn('bpi_id',$bpi_ids)->whereNotNull('bpi_custom_name')->get();
		foreach ($cstm_itm_del as $itm) {
			$id = $itm->bpi_id;
			if($id)
			Storage::delete(self::$custom_product_path.'/'.$id);
		}
		$project->items('all')->whereNotIn('bpi_id',$bpi_ids)->delete();
		///////////////////////////////////////////
		// delete custom images


		$model_items = $project->items;

		foreach ($items as $item) {

			$model_item = ProjectItem::find($item['item_id']);
			$model_item->bpi_model_code = $item['model_code'];
			$model_item->bpi_model_options_dim = $item['model_options_dim'];
			$model_item->bpi_model_options_ips = $item['model_options_ips'];
			$model_item->bpi_model_options_beam_angles = $item['model_options_beam_angles'];
			$model_item->bpi_model_options_warranty = $item['model_options_warranty'];
			$model_item->bpi_model_options_wifi = $item['model_options_wifi'];
			$model_item->bpi_quantity = $item['quantity'];
			// $model_item->bpi_text = $item['text'];
			$model_item->save();

		}

		// echo "<pre>";
		// print_r($bpi_ids);
		// echo "</pre>";

		return '1';
	}



	public function sendProject(Request $rqst) {

		$project = Project::find($rqst->project_id);

		$project->bp_status = "sent";
		$project->sent_at = date('Y-m-d H:i:s');
		$project->save();


		$retVal = "";
		$error = false;
		$message = 'An enquiry has been sent! We will get back to you shortly.';

		$info_name = self::$info_name;
		// $info_email = 'r.delarama@almani.ae';
		$info_email = self::$info_email;

		$route = route('view_project_url',['url_key'=>$project->bp_url_key]);





		$title1 = "Thank you ".strtoupper(Auth::user()->bu_fname.' '.Auth::user()->bu_lname)." for choosing B2B Portal of Almani Lighting LLC and sending a project. We will reply to you as soon as possible.<br><br>";

		$enquiry_msg = "<br>Please click this link to view project details: &nbsp;&nbsp;&nbsp; <a href='".$route."'><strong>".$project->bp_name."</strong></a>";


    	$data = array(
			'name_to' => Auth::user()->bu_fname.' '.Auth::user()->bu_lname,
			'email_to' => Auth::user()->bu_email, 
			'name_from' => $info_name, 
			'email_from' => $info_email, 
			'subject' => 'Project Enquiry for Almani Lighting from B2B Portal', 

			'other_message' => $enquiry_msg, 
			'tel' => Auth::user()->bu_contact."", 
			'title1' => $title1, 
			'title2' => '', 
    	);


		try {

			$mail_status = Mail::send( 'emails.almani.project', $data, function( $email ) use ($data) {
			    $email->to( $data['email_to'], $data['name_to'] )
			    		->from( $data['email_from'], $data['name_from'] )
			    		->cc([ 
			    			$data['email_from'], 
			    			"z.abdin@almani.ae",
			    		])
			    		->bcc([ "r.delarama@almani.ae" ])
			    		->subject(  $data['subject'] );
			});


		} catch (\Swift_RfcComplianceException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your project.';

	    } catch (\Swift_TransportException $Ste) {
			$retVal = 'sent_failed '.$Ste;
			$error = true;
			$message = 'Please try again later. There was an error when sending your project.';
	    }

	    // dd($data);

	    return $retVal;
		
	}

}


