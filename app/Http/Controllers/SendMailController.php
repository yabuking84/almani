<?php

namespace App\Http\Controllers;

use Mail;
use DateTime;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;


class SendMailController extends Controller
{

    public function sendEmail() {


    	$data = array(
			'first_name' => 'Info Camel LED', 
			'email' => 'info@almani.ae', 
			'email_from' => 'info@almani.ae', 
			'subject' => 'Get a Quote' 
    	);

		try {

			$mail_status = Mail::send( 'emails.getaquote', $data, function( $message ) use ($data) {
			    $message->to( $data['email'] )
			    		->from( $data['email_from'], $data['first_name'] )
			    		->subject(  $data['subject'] );
			});


		} catch (\Swift_RfcComplianceException $Ste) {
			$retVal = 'sent_failed '.$Ste;
	    } catch (\Swift_TransportException $Ste) {
			$retVal = 'sent_failed '.$Ste;
	    }

    }


    public function emailCampaign(){
    	return view('emails.campaign');
    }

    public function viewEmail($template_name){
	    return view('emails.'.$template_name)
	    			->with('url_email',"sadsad");
    }


}
