<?php

namespace App\Http\Controllers;

use Mail;
use DateTime;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Http\Controllers\Controller;

use App\Util;
Use App\Meta;

use Auth;

class ContactUsController extends Controller
{
	private static $info_email;

   	public function __construct() {	
		self::$info_email = env('APP_INFO_EMAIL');
   	}

	public function index() {
		return view('contactus'); 
	}

	public function viewEmail() {

		return view('emails.contactus')
		->with('other_message','asd')
		->with('name_from','asd')
		->with('email_from','asd')
		->with('name_to','asd')
		->with('email_to','asd')
		->with('subject','asd')
		->with('tel','asd')
		->with('project_type','asd')
		->with('project_type_others','asd')
		->with('construction_status','asd')
		->with('construction_status_others','asd')
		->with('project_budget_currency','asd')
		->with('project_budget','asd')
		->with('location','asd');
		
	}


	public function sendContactDetails(Request $rqst) {

		// dd(\Route::current()->getName());
		$recaptcha_secret_key = env('CAPTCHA_KEY');
        $recaptcha_secret = env('CAPTCHA_SECRET_KEY');

    	$recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
        $recaptcha_response = $rqst->recaptcha_response;

	    $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
        $recaptcha = json_decode($recaptcha);

        // dd($recaptcha);

        if($recaptcha->success==true) {

        	     if ($recaptcha->score >= 0.5) { 


						 $rules = [
				            'email'    => 'required|email|max:255',
				            'full_name' => 'required',
				            'message' => 'required'
				        ];

						$rqst->validate($rules);

						if(!empty($rqst->key)) {
							exit;
						}

						$retVal = "";
						$error = false;
						$message = 'An enquiry has been sent! We will get back to you shortly.';


						$info_name = 'Almani Lighting';
						$info_email = self::$info_email;
						// $info_email = 'info@almani.ae';

				    	$data = array(
							'name_from' => $rqst->full_name, 
							'email_from' => $rqst->email, 

							'name_to' => $rqst->full_name,
							'email_to' => $rqst->email, 

							'subject' => 'Web Enquiry for Almani Lighting', 

							'info_name' => $info_name,
							'info_email' => $info_email,

							'other_message' => $rqst->message, 
							'tel' => $rqst->tel, 
				    	);

						try {

							$mail_status = Mail::send( 'emails.almani.contactus', $data, function( $message ) use ($data) {
							  $message->to( $data['email_to'], $data['name_to'] )
										    	->from( $data['info_email'], $data['info_name'] )
										    		// ->cc([ $data['email_from'] ])
										  		->cc(['info@almani.ae'])
										  		->subject(  $data['subject'] );
							});

						} catch (\Swift_RfcComplianceException $Ste) {
							$retVal = 'sent_failed '.$Ste;
							$error = true;
							$message = 'Please try again later. There was an error when sending your enquiry.';

					    } catch (\Swift_TransportException $Ste) {
							$retVal = 'sent_failed '.$Ste;
							$error = true;
							$message = 'Please try again later. There was an error when sending your enquiry.';
					    }

					    // dd($data);

						// return redirect()
						// 			->route('contactus_page')
						// 			->with('error',$error)
						// 			->with('message',$message);


					    // $previousUrl = app('url')->previous();


						return redirect()
									->back()
									->with('error',$error)
									->with('message',$message);

        	     } else {

        	     		// not verified show form error
            	    	$error = true;
            	     	$message = 'Please try again later. We cant verified your data.';
            	     	return back()->with('error',$error)->with('message',$message);

        	     }



       	} else {

				$error = true;
            	$message = 'Please try again later. We cant validate you. we feel your like a bot.';
            	return back()->with('error',$error)->with('message',$message);


       	}
	}



	public function sendHomeDetails(Request $rqst) {

		// dd(\Route::current()->getName());
		

		$recaptcha_secret_key = env('CAPTCHA_KEY');
        $recaptcha_secret = env('CAPTCHA_SECRET_KEY');

    	$recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
        $recaptcha_response = $rqst->recaptcha_response;

	    $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
        $recaptcha = json_decode($recaptcha);

        // dd($recaptcha);

        if($recaptcha->success==true) {

        	     if ($recaptcha->score >= 0.5) { 


						 $rules = [
				            'email'    => 'required|email|max:255',
				            'full_name' => 'required',
				            'message' => 'required'
				        ];

						$rqst->validate($rules);

						if(!empty($rqst->key)) {
							exit;
						}

						$retVal = "";
						$error = false;
						$message = 'An enquiry has been sent! We will get back to you shortly.';


						$info_name = 'Almani Lighting';
						$info_email = self::$info_email;
						// $info_email = 'info@almani.ae';

				    	$data = array(
							'name_from' => $rqst->full_name, 
							'email_from' => $rqst->email, 

							'name_to' => $rqst->full_name,
							'email_to' => $rqst->email, 

							'subject' => 'Almani promotions page Enquiry', 

							'info_name' => $info_name,
							'info_email' => $info_email,

							'other_message' => $rqst->message, 
							'tel' => $rqst->tel, 
				    	);

						try {

							$mail_status = Mail::send( 'emails.almani.contactus', $data, function( $message ) use ($data) {
							  $message->to( $data['email_to'], $data['name_to'] )
										    	->from( $data['info_email'], $data['info_name'] )
										    		// ->cc([ $data['email_from'] ])
										  		->cc(['info@almani.ae'])
										  		->subject(  $data['subject'] );
							});

						} catch (\Swift_RfcComplianceException $Ste) {
							$retVal = 'sent_failed '.$Ste;
							$error = true;
							$message = 'Please try again later. There was an error when sending your enquiry.';

					    } catch (\Swift_TransportException $Ste) {
							$retVal = 'sent_failed '.$Ste;
							$error = true;
							$message = 'Please try again later. There was an error when sending your enquiry.';
					    }

					    // dd($data);

						// return redirect()
						// 			->route('contactus_page')
						// 			->with('error',$error)
						// 			->with('message',$message);


					    $previousUrl = app('url')->previous();
					    

						return redirect()
									->to($previousUrl.'&thank-you')
									->with('error',$error)
									->with('message',$message);

        	     } else {
        	     		// not verified show form error
            	    	$error = true;
            	     	$message = 'Please try again later. We cant verified your data.';
            	     	return back()->with('error',$error)->with('message',$message);

        	     }



       	} else {

				$error = true;
            	$message = 'Please try again later. We cant validate you. we feel your like a bot.';
            	return back()->with('error',$error)->with('message',$message);


       	}
	}
}