<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Client;




class ClientLoginController extends Controller
{


    public function clientAction(Request $rqst) {

    	$res = Client::where('client_email', $rqst->email)->where('client_password', $rqst->password)->first();

    	if($res) {

				session(['login_sales' => 1]);
				session(['login_sales_user' => $res->client_name]);
				return redirect('/');

    	} else {
    		
    			return redirect('/');

    	}

		// dd($res);

    }


    public function client() {
    	return view('client-login'); 
    }


}
