<?php

namespace App\Http\Controllers;

use App\Category\CategoryInterface;
use App\Http\Controllers\Controller;
use App\Meta;
use App\Product\Product;
use App\Product\ProductInterface;
use App\Product\ProductModel\ProductModelInterface;
use App\Util;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * Initialize Interface
     */
    public function __construct(CategoryInterface $category, ProductInterface $product, ProductModelInterface $model)
    {
        $this->category = $category;
        $this->product = $product;
        $this->model = $model;
    }

    public function index()
    {
        $meta = new Meta;

        $categories = $this->category->all();
        $products = $this->product->all();
        $application_area = $this->category->getAppAreas();

        return view('search')
            ->with('categories', $categories)
            ->with('products', $products)
            ->with('app_areas', $application_area)
            ->with('meta', $meta);
    }

    public function searchNav(Request $rqst)
    {

        $categories = $this->category->all();
        $products = $this->product->all();
        $application_area = $this->category->getAppAreas();

        $data = array(
            'camel' => $rqst->camel,
            'almani' => $rqst->almani,
            'category_name' => $rqst->category_name,
            'product_name' => $rqst->product_name,
            'model_code' => $rqst->model_code,
            'dimm' => $rqst->dimm,
            'triac' => $rqst->triac,
            'dali' => $rqst->dali,
            'v0_10' => $rqst->v0_10,
            'emergency' => $rqst->emergency,
            'wifi' => $rqst->wifi,
            'cct_min' => $rqst->cct_min,
            'cct_max' => $rqst->cct_max,
            'lumen_min' => $rqst->lumen_min,
            'lumen_max' => $rqst->lumen_max,
            'power_min' => $rqst->power_min,
            'power_max' => $rqst->power_max,
            'ip_rating_min' => $rqst->ip_rating_min,
            'ip_rating_max' => $rqst->ip_rating_max,
            'price_min' => $rqst->price_min,
            'price_max' => $rqst->price_max,
            'currency' => $rqst->currency,
            'frcatalog' => $rqst->frcatalog,
        );

        return view('search')
            ->with('search_fld', $rqst->search_fld)
            ->with('frcatalog', $rqst->frcatalog)
            ->with('categories', $categories)
            ->with('products', $products)
            ->with('app_areas', $application_area)
            ->with('datacatalog', $data);
    }

    public function search(Request $rqst)
    {
        $search = json_decode($rqst->search, true);

        // $paginate_data = $this->model->findModelsWithFilters($search)->paginate(12);
        $paginate_data = $this->product->findProductsWithFilters($search)->paginate(12);

        $retVal['paginate'] = $paginate_data;
        $retVal['links'] = "" . $paginate_data->links('pagination.search') . "";

        $retVal['currency'] = $search['price']['currency'];
        $retVal['rate'] = Util::getRate($search['price']['currency']);

        return json_encode($retVal, JSON_PARTIAL_OUTPUT_ON_ERROR);

        // return $search;

    }

}
