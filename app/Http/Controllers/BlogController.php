<?php

namespace App\Http\Controllers;

use App\Project;

use App\Product\ProductInterface;
use App\Product\ProductModel\ProductModelInterface;
use App\Category\Category;
use App\Category\CategoryInterface;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Response;

Use App\Util;

Use App\Blog;

use Auth;

class BlogController extends Controller
{



	public function blogMainPage() {

		$posts = Blog::where('b_status','=','1')->orderBy('created_at','DESC')->get();
		return view('blog.blog-main')->with('posts',$posts)
									->with('meta_title','Blog'); 
	}



	public function blogPostPage($slug) {

		$post = Blog::where('b_slug','=',$slug)->first();
		
		
		// Disabled for security reasons
		// if(!$post) //slug maybe an id
		// $post = Blog::find($slug);

		if(!$post) 
		return redirect()->route('blog_page');
		else		
		return view('blog.blog-post')->with('post',$post)
									->with('meta_title','Blog'); 
	}





	public function sitemap() {

		$posts = Blog::where('b_status','=','1')->where('b_slug','!=','')->orderBy('created_at','DESC')->get();

        $data['posts'] = $posts;

		return response()
					->view('blog.sitemap',$data,200)
					->header('Content-Type', 'text/xml');

	}







}