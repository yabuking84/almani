<?php

namespace App\Http\Controllers;

use App\Product\ProductInterface;
use App\Product\ProductModel\ProductModelInterface;

// 
use App\Category\CategoryInterface;
use App\Category\Category;



use Illuminate\Http\Request;

class MobileController extends Controller
{
    public function __construct(ProductInterface $product, CategoryInterface $category) {
        $this->product = $product;
        $this->category = $category;
    }
    
    public function index() {

        // dd($this->category->getAppAreasRandom(12));

        return view('mobile.home')
                ->with('featured_prods',$this->product->featured(12))
                    ->with('application_areas', $this->category->getAppAreasRandom(12))->render();
    }
}
