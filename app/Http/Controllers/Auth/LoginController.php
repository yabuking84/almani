<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */



    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:client')->except('logout');
    }


    public function clientLogin(Request $rqst) {

    	$this->validate($rqst, [

            'email'   => 'required|email',
            'password' => 'required|min:6'
            
        ]);

        if (Auth::guard('client')->attempt(['client_email' => $rqst->email, 'client_password' => $rqst->password], $rqst->get('remember'))) {
            // return redirect()->intended('/admin');
            echo 'login';

        } else {
        	
        	echo 'not login';
        }

        // return back()->withInput($rqst->only('email', 'remember'));

    }
}
