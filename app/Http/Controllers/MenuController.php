<?php

namespace App\Http\Controllers;

use App\Project;

use App\Product\ProductInterface;
use App\Product\ProductModel\ProductModelInterface;
use App\Category\Category;
use App\Category\CategoryInterface;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Response;

use Jenssegers\Agent\Agent;

Use App\Util;

use Auth;

class MenuController extends Controller
{


	/**
	 * Initialize Interface
	 */
	public function __construct(CategoryInterface $category, ProductInterface $product, ProductModelInterface $model) {

		$this->category = $category;
		$this->product = $product;
		$this->model = $model;

	}











	// Create blade files
	public function createHTMLMenus(){

		self::createHTMLMenusforDesktop();
		self::createHTMLMenusforMobile();

	}






	// Create blade files
	public function createHTMLMenusforDesktop(){


		$menus = [];
		$menus[] = 'categories-links';
		$menus[] = 'application_areas-links';

		foreach ($menus as $menu) {
			Storage::disk('internal')->put('resources/views/sections/static/'.$menu.'.blade.php', $this->showHTMLMenu($menu));
		}

	}
	public function createHTMLMenusforMobile(){

		$menus = [];
		$menus[] = 'categories-links-mobile';
		$menus[] = 'application_areas-links-mobile';

		foreach ($menus as $menu) {
			Storage::disk('internal')->put('resources/views/sections/static/mobile/'.$menu.'.blade.php', $this->showHTMLMenuforMobile($menu));
		}

	}





    public function showHTMLSubMenu($submenu="blank") {
          
        return view('sections.submenus.'.$submenu);

	}
	
	public function showMobileHTMLSubMenu($submenu="blank") {
		return view('sections.submenus.mobile.'.$submenu);
	}


	public function showHTMLMenu($menu="application_areas-links"){

    	$catIns = $this->category;

    	$categories = $catIns->all("orderByName");
    	$app_areas = $catIns->getAppAreas();


		if($menu=="application_areas-links") {
			$retVal = "";
			$retVal.= '<ul class="product_group by_application_areas show_hide_group hide_group" >';

			// echo '<pre>';
			// var_dump($app_areas);
			// echo '</pre>';
			
			// die();

	        foreach($app_areas as $app_area) {
	            // $app_products = $app_area->products;
	            $app_products = $app_area->getProducts();
	            
	            if($app_products->count()) {
	            $retVal.= '<li>';
	                $retVal.= '<a data-category_subgroup="app_'.$app_area->app_id.'">';
	                    $retVal.= ucwords(strtolower($app_area->app_title));
	                $retVal.= '</a>';

	                $retVal.= '<div style="display: block;position: relative;">';
	                $retVal.= '<div class="sub_group app_'.$app_area->app_id.' hide_group">';
	                    $retVal.= '<div class="product_list">';
	                        $retVal.= '<div style="">';
	                            $retVal.= '<a href="'.route('application_area_page',['app_slug'=>$app_area->app_slug]).'">';
	                                $retVal.= ucwords(strtolower($app_area->app_title));
	                            $retVal.= '</a>';
 								$retVal.= '<button class="btn-almani btn-sm"><a class="showall" href="'.route('application_area_page',['app_slug'=>strtolower($app_area->app_slug)]).'">Show All</a></button>';
	                        $retVal.= '</div>';                        
	                        $retVal.= '<ul class="">';
	                            foreach($app_products as $product) {
			                        if($product->product_active && $product->camel != '1') {		                            	
			                            $retVal.= '<li>';
			                                $retVal.= '<a href="'.route('product_page',[
			                                    'category_alias'=>$product->category->category_alias,
			                                    'product_code'=>$product->product_code,
			                                ]).'">';
			                                    $retVal.= '<img data-src="'.Util::asset($product->image_th_eager).'" alt="'.$product->product_name.' '.$product->product_code.'">';
			                                    $retVal.= ucwords(strtolower(str_ireplace(' and ', ' & ', $product->product_name)));
			                                $retVal.= '</a>';
			                            $retVal.= '</li>';
		                            }
	                            }
	                        $retVal.= '</ul>';
	                    $retVal.= '</div>';
	                $retVal.= '</div>';
	                $retVal.= '</div>';

	            $retVal.= '</li>';
	            }
	        }
	        $retVal.= '</ul>';
		}



		else if($menu=="categories-links") {
			$retVal = "";
	        $retVal.= '<ul class="product_group by_category show_hide_group" itemscope itemtype="http://schema.org/BreadcrumbList" >';
	        foreach($categories as $category) {
                /////////////////
	            if($category->products->count() > 0) {
	            /////////////////////////////////////////

	            $retVal.= '<li class="item-list-category">';
	                $retVal.= '<a data-category_subgroup="cat_'.$category->category_alias.'">';
	                    // $retVal.= '<img itemprop="image" src="" alt="'.substr(ucwords(strtolower($category->category_name)), 3).'" style="display:none;"/>';
	                    $retVal.= '<span>LED '.substr(ucwords(strtolower(str_ireplace(' and ', ' & ', $category->category_name))), 3).'</span>';	                    
	                $retVal.= '</a>';


	                $retVal.= '<div style="display: block;position: relative;">';
	                $retVal.= '<div class="sub_group cat_'.$category->category_alias.' hide_group">';

	                	// category
		                ////////////////////////////////////////////////////////
	                    $retVal.= '<div class="product_list">';
	                        $retVal.= '<div style="">';
	                            $retVal.= '<a href="'.route('catalog_product_page',['category_alias'=>strtolower($category->category_alias)]).'">';
	                                $retVal.= 'LED '.substr(ucwords(strtolower(str_ireplace(' and ', ' & ', $category->category_name))), 3);
	                            $retVal.= '</a>';
 								$retVal.= '<button class="btn-almani btn-sm"><a class="showall" href="'.route('catalog_product_page',['category_alias'=>strtolower($category->category_alias)]).'">Show All</a></button>';
	                        $retVal.= '</div>';                        
	                        $retVal.= '<ul class="">';
	                            foreach($category->getProductsAndSubProductsNonClosure()->get() as $product) {
		                            $retVal.= '<li>';
		                                $retVal.= '<a href="'.route('product_page',['category_alias'=>$category->category_alias,'product_code'=>$product->product_code,]).'">';
		                                    $retVal.= '<img data-src="'.Util::asset($product->image('th')).'" alt="'.$product->product_name.' '.$product->product_code.'">';
		                                    $retVal.= ucwords(strtolower(str_ireplace(' and ', ' & ', $product->product_name)));
		                                $retVal.= '</a>';
		                            $retVal.= '</li>';
	                            }
	                        $retVal.= '</ul>';

		                	// sub-category
			                ////////////////////////////////////////////////////////	                        
		                	foreach($category->subCategories as $sub_category) {
	                        $retVal.= '<div style="margin-top:20px;">';
	                            $retVal.= '<a href="'.route('catalog_product_page',['category_alias'=>strtolower($sub_category->category_alias)]).'">';
	                                $retVal.= 'LED '.ucwords(strtolower(str_ireplace(' and ', ' & ', $sub_category->category_name)));
	                            $retVal.= '</a>';
									$retVal.= '<button class="btn-almani btn-sm"><a class="showall" href="'.route('catalog_product_page',['category_alias'=>strtolower($sub_category->category_alias)]).'">Show All</a></button>';
	                        $retVal.= '</div>';                        
	                        $retVal.= '<ul class="">';
	                            foreach($sub_category->getProductsAndSubProductsNonClosure()->get() as $product) {
		                            $retVal.= '<li>';
		                                $retVal.= '<a href="'.route('product_page',['category_alias'=>$sub_category->category_alias,'product_code'=>$product->product_code,]).'">';
		                                    $retVal.= '<img data-src="'.Util::asset($product->image('th')).'" alt="'.$product->product_name.' '.$product->product_code.'">';
		                                    // $retVal.= ucwords(strtolower($product->product_name));
		                                    $retVal.= ucwords(strtolower(str_ireplace(' and ', ' & ', $product->product_name)));
		                                $retVal.= '</a>';
		                            $retVal.= '</li>';
	                            }
	                        $retVal.= '</ul>';                    
		                	}
			                ////////////////////////////////////////////////////////
		                	// sub-category

	                    $retVal.= '</div>';
		                ////////////////////////////////////////////////////////
	                	// category



	                $retVal.= '</div>';
	                $retVal.= '</div>';
	            $retVal.= '</li>';


	            /////////////////////////////////////////
	            }
	            /////////////////
	        }
	        $retVal.= '</ul>';

		} 



        // echo "<textarea style='height: 600px;width: 1300px;'>".$retVal."</textarea>";

        return $retVal;

	}





































	// for main mobile

	public function showHTMLMenuforMobile($menu="application_areas-links-mobile"){

		$retVal = "";

    	$catIns = $this->category;

    	$categories = $catIns->all("orderByName");
    	$app_areas = $catIns->getAppAreas();



	 if($menu=="application_areas-links-mobile") {
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			$retVal = "";
			$retVal.= '<ul class="menu-list-down down hide">';
			$retVal.= '<li class="showall_mobile"><a class="btn btn-sm btn-dark btn-border" href="'.route('application_area_landing_page').'">View All</a></li>';
				foreach ($app_areas as $app_area) {
					
	                // $app_products = $app_area->products; 
		            $app_products = $app_area->getProducts();
	                
					if ($app_products->count()) {
						$retVal .= '<a href="javascript:void(0)" class="ddd-menu btn-atr">';
						$retVal .= '<li >';
						$retVal .= ''.$app_area->app_title.' <span class="toggles-info"><i class="fas fa-caret-right"></i></span>';	
						$retVal .= '</li>';
						$retVal .= '</a>'; 		
					

						$retVal .= '<ul class="menu-list-down mm-list hide">';
						// if($app_products->product_active && $app_products->camel != 1) {
							$retVal.= '<li class="showall_mobile"><a class="btn btn-sm btn-dark btn-border" href="'.route('application_area_page',['app_slug'=>strtolower($app_area->app_slug)]).'">Show All</a></li>';
							foreach ($app_products as $product) {
								if($product->product_active && $product->camel != 1) {
									$retVal .= '<a href="'.route('product_page',['category_alias'=>$product->category->category_alias,'product_code'=>$product->product_code,]).'">'; 
									$retVal .= '<li class="app-margin-right"> ';
									$retVal .= '<img data-src="'.Util::asset($product->image('th')).'" src="" alt="">';
									$prod_names = '';
									$prod_name = '';
									if(strlen($product->product_name) >= 14 ) {
										$prod_names = substr($product->product_name, 0, 14);
										$prod_name = $prod_names.'..';
									} else {
										$prod_name = $product->product_name;
									}
									// $retVal .= ucwords(strtolower($prod_name));
									$retVal .= ucwords(strtolower(str_ireplace(' and ', ' & ', $prod_name)));
									$retVal .= '</li>';
									$retVal .= '</a>';
								}
							}
							$retVal .= '</ul>';	
						// }
					}
				}
			$retVal .= '</ul>'; 



		} else if($menu=="categories-links-mobile") {

			

				$retVal = "";
				$retVal .= '<ul class="menu-list-down down hide">';
				$retVal.= '<li class="showall_mobile"><a class="btn btn-sm btn-dark btn-border" href="'.route('catalog_landing_page').'">View All</a></li>';
					foreach ($categories as $category) {
						
						if ($category->products->count() > 0) {
							$retVal .= '<a href="javascript:void(0)" class="ddd-menu btn-atr">';
							$retVal .= '<li >';
							$retVal .= 'LED '.substr(ucwords(strtolower(str_ireplace(' and ', ' & ', $category->category_name))), 3).'<span class="toggles-info"><i class="fas fa-caret-right"></i></span>';	
							$retVal .= '</li>';
							$retVal .= '</a>'; 		
						

							$retVal .= '<ul class="menu-list-down mm-list down down-menu hide">';
								$retVal.= '<li class="showall_mobile"><a class="btn btn-sm btn-dark btn-border" href="'.route('catalog_product_page',['category_alias'=>strtolower($category->category_alias)]).'">Show All</a></li>';
								foreach ($category->products as $product) {

										$retVal .= '<a href="'.route('product_page',['category_alias'=>$category->category_alias,'product_code'=>$product->product_code,]).
										'">'; 
										$retVal .= '<li> ';
										$retVal .= '<img data-src="'.Util::asset($product->image('th')).'" src="" alt="">';
										if(strlen($product->product_name) >= 16 ) {
											$prod_names = substr($product->product_name, 0, 14);
											$prod_name = $prod_names.'..';
										} else {
											$prod_name = $product->product_name;
										}
										// $retVal .= ucwords(strtolower($prod_name));
										$retVal .= ucwords(strtolower(str_ireplace(' and ', ' & ', $prod_name)));
										$retVal .= '</li>';
										$retVal .= '</a>';
								}
							$retVal .= '</ul>';	
						}
					}
				$retVal .= '</ul>'; 

			 //////////////////////////////////////////////////////////////////////////////////////////////
 
			 

		}

        // echo "<textarea style='height: 600px;width: 1300px;'>".$retVal."</textarea>";

		return $retVal;

	}







}
