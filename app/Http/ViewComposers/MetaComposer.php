<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
Use App\Meta;


class MetaComposer {

	public function compose(View $view) {

		$meta = new Meta;
        $view->with('meta',$meta->tags());
	}
	
}



					    	