<?php

namespace App\Http\ViewComposers;

use App\Product\ProductInterface;
use App\Product\ProductModel\ProductModelInterface;
use App\Category\CategoryInterface;


use Illuminate\View\View;

use Illuminate\Support\Facades\Cache;


class NavmenuComposer {


	/**
	 * Initialize Interface
	 */
	public function __construct(CategoryInterface $category, ProductInterface $product, ProductModelInterface $model)
	{
		$this->category = $category;
		$this->product = $product;
		$this->model = $model;
	}


    public function compose(View $view)
    {
    	$catIns = $this->category;

		$categories = $catIns->all("orderByName");
		$app_areas = $catIns->getAppAreas();

		$view->with('categories',$categories)
		        ->with('app_areas',$app_areas);


		// $view->with('categories',Cache::rememberForever('categories', function() {
	 //            return $this->category->all("orderByName");
	 //        }))
		//     ->with('app_areas',Cache::rememberForever('app_areas', function() {
	 //            // return $this->category->all("orderByName");
	 //            return $this->category->getAppAreas();
	 //        }));

		

    }




}



