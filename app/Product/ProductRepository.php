<?php

namespace App\Product;

use App\Product\ProductInterface;
use App\Product\Product;

use App\Product\ProductModel\ProductModel;

use Validator;
use Auth;

class ProductRepository implements ProductInterface
{
    /**
     * Get all products.
     *
     * @author Leo
     * @param  int
     * @return array
     */
    public function all($limit = null)
    {
        $Product = new Product;
        if ($limit != null) {
            $products = $Product->getProducts()->paginate($limit);
        } else {
            $products = $Product->getProducts()->get();
        }
        return $products;
    }

	/**
     * Get products for home page
     *
     * @author Leo
     * @param  int
     * @return array
     */
    public function featured($limit = 6)
    {
        $Product = new Product;
        $products = $Product->featured($limit)->get();
        return $products;
    }

    /**
     * Get product data by product id.
     *
     * @author Leo
     * @param  int
     * @return array
     */
    public function find($id)
    {
        return Product::where('product_id', (int) $id)->first();
    }

    /**
     * Get products data by product codes.
     *
     * @author Rene
     * @param  int
     * @return array
     */
    public function findManyByCode($codeArr)
    {
        return Product::whereIn('product_code', $codeArr)->get();
    }

    /**
     * Get product data by product code.
     *
     * @author Leo
     * @param  int
     * @return array
     */
    public function findCode($code)
    {
        return Product::where('product_code','=',$code)->first();
    }

    /**
     * Creates a new product. Image files
     * are being saved on each company folders
     * respectively.
     *
     * @author Leo
     * @param mixed
     * @return array
     */    
    public function add($data)
    {
        $product = new Product;
        $product->fill($data);

        if ($product->save()) {
            return $product;
        }
        return false;
    }

    /**
     * Edit existing product.
     *
     * @author Leo
     * @param mixed
     * @return mixed
     */
    public function edit($data)
    {
        $product = Product::where('product_id', $data['id'])->first();

        $product->fill($data);

        if ($product->save()) {
            return $product;
        }
        return false;
    }

    /**
     * Delete existing product by hash ID.
     *
     * @author Leo
     * @param string
     * @return mixed
     */
    public function delete($id)
    {
        $product = Product::where('product_id', $id)->get();

        if (count($product) > 0) {
            $product = $product->first();

            return $product->delete();
        }
        return false;
    }



    /**
     * Get model data by filters.
     *
     * @author Leonardo De Caprio
     * @param  ?
     * @return ?
     */
    public function findProductsWithFilters($filters_arr)
    {
        $fa = $filters_arr;   
        $Product = new Product;

        ////////////////////////////////////
        if(isset($fa['category_name']) && $fa['category_name']!="") {
            $Product = $Product->getProducts('active');
            $Product->whereHas(
                'category', 
                function ($query) use ($fa) {
                    $query->where('category_name', 'like', '%'.$fa['category_name'].'%');
                }
            );
        }
        else if(isset($fa['product_name']) && $fa['product_name']!="") {
            $Product = $Product->getProducts('active');       
            $Product->where('product_name','like','%'.$fa['product_name'].'%');
        }
        else if(isset($fa['model_code']) && $fa['model_code']!="") {
            // This is so that unavailable products will be shown when searching by model code.
            $Product = $Product->getProducts('all');
        } else {
            $Product = $Product->getProducts('active');
        }
       
        // echo($Product->toSql());
        // exit;


        // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //
        // old version, seems to be slower
        // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //
        // $Product = $Product->whereHas(
        //     'models', 
        //     function($query) use($fa) {
        //         $query->filterModel($fa);
        //     }
        // );
        // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //
        // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //


        // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //
        // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //

        // dd( (new ProductModel)->filterModel($fa)->toSql() );

        $models = (new ProductModel)->filterModel($fa)->get();
        $product_ids = array_unique($models->pluck('product_id')->toArray());
        $Product = $Product->whereIn('product_id',$product_ids);
        // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //
        // xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx //



        // change this! this is for debuging    
        return $Product->with([
            'category',
            // 'models'=>function($query) use($fa) {
            //     $query->filterModel($fa);
            // },
        ]);
    }

}
