<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'product_images';
    protected $primaryKey = 'image_id';

    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo('App\Product\Product', 'product_id', 'product_id');
    }

}