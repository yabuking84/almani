<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;

use App\Product\ProductImage;

use App\Product\ProductModel\ProductModel;

use App\Product\ProductApplicationArea;

use App\Util;



class Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'product_id';
    protected $appends = [
                'image_th_eager',
                'image_eager',
                'lifespan_eager',
                'power_eager',
                'kelvin_eager',
                'lumens_eager',
                'minPrice_eager',
                'minPriceText_eager'
            ];

    protected $guarded = [];
    public $timestamps = false;


    public function isActive() {
        return $this->product_active;
    }

    public function getProducts($get='active') {

        // if($get == 'active') {
        //     return $this->where('product_active','=',1);
        // } else if($get == 'all') {
        //     return $this;
        // } else if($get == 'inactive') {
        //     return $this->where('product_active','!=',1);
        // }

        return $this->CheckStatus($get);

    }


// test hot fix



    /**
     * Scope a query to filter product-models of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCheckStatus($queryScope, $get)
    {
        if($get == 'active') {
            return $queryScope->where('product_active','=',1);
        } else if($get == 'all') {
            return $queryScope;
        } else if($get == 'inactive') {
            return $queryScope->where('product_active','!=',1);
        }
    }





    public function applicationAreas() {
        return $this->belongsToMany('App\Product\ProductApplicationArea', 'applicationareas_products', 'product_id', 'app_id');
    }

    public function featured($limit) {        
        return $this->getProducts()
                    ->where('product_homepage','=','1')
                    ->where('camel','!=','1')
                    ->inRandomOrder()->limit($limit);
    }

    public function models()
    {
        return $this->hasMany('App\Product\ProductModel\ProductModel', 'product_id', 'product_id')
                    ->where('model_active','=',1);
    }

    public function images()
    {

        return $this->hasMany('App\Product\ProductImage', 'product_id', 'product_id');
    }

    public function defaultImage()
    {
        return $this->hasOne('App\Product\ProductImage', 'image_id', 'product_default_image');
    }

    public function defaultSketch()
    {
        return $this->hasOne('App\Product\ProductImage', 'image_id', 'product_default_sketch');
    }

    public function category() {
        return $this->belongsTo('App\Category\Category', 'category_id', 'category_id');
    }

    public function supplier() {
        return $this->belongsTo('App\Supplier', 'supplier_id', 'supplier_id');
    }

    public function lifespan() { 
        $lifespan = number_format(floatval($this->product_lifespan),0,".",",");
        return $lifespan;
    }
    public function getLifespanEagerAttribute()
    {
        return $this->lifespan();
    }


    public function lumens() {
        // $lumen_range = $this->product_lumen;
        // return $lumen_range;
        $lumen_rangeArr = explode('-',$this->lumen_range);
        foreach ($lumen_rangeArr as &$value) {
            $value = number_format(floatval(str_replace(",", "", $value)),0," ","");
        }
        $lumen_range = implode(' – ', $lumen_rangeArr);
        return $lumen_range;
    }
    public function getLumensEagerAttribute()
    {
        return $this->lumens();
    }

    public function kelvin() {
        $product_cctArr = explode('-',$this->product_cct);
        foreach ($product_cctArr as &$value) {
            $value = number_format(floatval(str_replace(",", "", $value)),0," ","");
        }
        $product_cct = implode(' – ', $product_cctArr);
        return $product_cct;
    }
    public function getKelvinEagerAttribute()
    {
        return $this->kelvin();
    }

    public function power() { 
        $product_power = trim(str_replace('-', ' – ', $this->product_power));
        $product_power = ($product_power)?$product_power:'-';

        return $product_power;
    }
    public function getPowerEagerAttribute()
    {
        return $this->power();
    }


    public function minPriceText() {
        $model = $this->models()->orderBy('model_price','asc')->first();

        $model_price = ($model)?$model->model_price:"0";
        $model_code = ($model)?$model->model:"0";

        $currency = Util::getDefaultCurrency();
        $rate = Util::getRate();

        // Price
        ////////////////////////////////////////////////////////////////////////////////////
        $discount = Util::getDiscount();
        $min_price = ($model_price - ($discount / 100 * $model_price)) * $rate;
        if ($min_price > 0) {
            // Apply user discount
            // $min_price = Util::applyUserDiscount($min_price);
            $price_text = Util::currency($min_price).' '.$currency;
        } else {
            $price_text = 'AVAILABLE UPON REQUEST';
        }
        ////////////////////////////////////////////////////////////////////////////////////


        return $price_text;
    }
    public function getMinPriceTextEagerAttribute()
    {
        return $this->minPriceText();
        // return "1111.0";
    }

    public function minPrice() {
        $model = $this->models()->orderBy('model_price','asc')->first();

        $model_price = ($model)?$model->model_price:"0";
        $model_code = ($model)?$model->model:"0";

        $currency = Util::getDefaultCurrency();

        // dd($currency);
        $rate = Util::getRate();

        // Price
        ////////////////////////////////////////////////////////////////////////////////////
        $discount = Util::getDiscount();
        $min_price = (($model_price - ($discount / 100 * $model_price)) * $rate) / 2;
        if ($min_price > 0) {
            // $min_price = Util::applyUserDiscount($min_price);
            $price_text = Util::currency($min_price);
        } else {
            $price_text = '0.0';
        }
        ////////////////////////////////////////////////////////////////////////////////////


        return $price_text;
    }
    public function getMinPriceEagerAttribute()
    {
        return $this->minPrice();
    }


    public function getLogos() {

        $logos = [];

        // for logos
        /////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////
        $gl = [];
        $gl['product_ce'] = [ 'alt' => 'CE', 'value' => $this->product_ce ];
        $gl['product_rohs'] = [ 'alt' => 'ROHS', 'value' => $this->product_rohs ];
        $gl['product_saa'] = [ 'alt' => 'SAA', 'value' => $this->product_saa ];
        $gl['product_tuv_sud'] = [ 'alt' => 'TUVSUD', 'value' => $this->product_tuv_sud ];
        $gl['product_tuv_rheinland'] = [ 'alt' => 'TUVRHEINLAND', 'value' => $this->product_tuv_rheinland ];
        $gl['product_vde'] = [ 'alt' => 'VDE', 'value' => $this->product_vde ];
        $gl['product_dim'] = [ 'alt' => 'DIM', 'value' => $this->product_dim ];
        $gl['product_wifi'] = [ 'alt' => 'WIFI', 'value' => $this->product_wifi ];
        $gl['product_lifespan'] = [ 'alt' => 'Lifespan', 'value' => $this->product_lifespan ];
        $gl['product_cri'] = [ 'alt' => 'CRI', 'value' => $this->product_cri ];

        foreach ($gl as $glKey => $glVal) {
            $value = $glVal['value'];
            if($value) {
                if($glKey == "product_lifespan")
                $val2 = number_format((int)$value,0,'.',',');
                else if($glKey == "product_cri") {
                    $val2 = str_replace('>=','&ge;',$value);
                    $val2 = str_replace('=>','&ge;',$val2);
                }
                else
                $val2 = "";

                $arr = [];            
                $arr['alt'] = $glVal['alt'];
                $arr['data'] = $val2;
                $logos[$glKey] = $arr;
            }
        }
        /////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////
        // for logos


        return $logos;

    }

    public function getTechnicalDataSummary() {


        // for standard data
        /////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////
        $ik_rating = str_pad($this->product_ik,2,"0",STR_PAD_LEFT);
        $ik_rating = ($ik_rating==0)?'':$ik_rating;
        

        $tds = [];
        $tds['product_voltage'] = [ 'label' => 'Voltage (V)', 'value' => $this->product_voltage ];
        $tds['product_chip'] = [ 'label' => 'LED Chip', 'value' => $this->product_chip ];
        $tds['product_led_driver'] = [ 'label' => 'LED Driver', 'value' => ucfirst($this->product_led_driver) ];
        $tds['product_beam_angle'] = [ 'label' => 'Beam Angle (&deg;)', 'value' => $this->product_beam_angle ];
        $tds['product_ik'] = [ 'label' => 'IK Rating', 'value' => $ik_rating];
        $tds['product_lmw_efficiency'] = [ 'label' => 'Efficiency (Lm/W)', 'value' => $this->product_lmw_efficiency ];
        $tds['product_op_efficiency'] = [ 'label' => 'Ambient Temperature (ta)', 'value' => $this->product_op_efficiency ];
        $tds['product_material'] = [ 'label' => 'Material', 'value' => ucfirst($this->product_material) ];
        $tds['product_color'] = [ 'label' => 'Color', 'value' => ucfirst($this->product_color) ];
        $tds['product_light_distro'] = [ 'label' => 'Light Distribution', 'value' => $this->product_light_distro ];
        $tds['product_warranty'] = [ 'label' => 'Warranty', 'value' => ($this->product_warranty>1)?$this->product_warranty.' years':$this->product_warranty.' year' ];
        $tds['product_application_areas'] = [ 'label' => 'Application Areas', 'value' => str_replace(',', ', ', $this->product_application_areas) ];

        /////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////
        // for standard data


        // product_ip
        ////////////////////////////////////////////////
        $value = $this->product_ip;
        if(!$value)
        $value = "NA";
        
        $arr = [];
        $arr['label'] = "IP";
        $arr['value'] = $value;
        $tds['product_ip'] = $arr;
        ////////////////////////////////////////////////


        // product_ip
        ////////////////////////////////////////////////
        $value = $this->product_ip;
        if(!$value)
        $value = "NA";
        
        $arr = [];
        $arr['label'] = "IP";
        $arr['value'] = $value;
        $tds['product_ip'] = $arr;
        ////////////////////////////////////////////////

        // Dim options
        ////////////////////////////////////////////////
        $value = '';
        
        $value.= ($this->product_dim_triac)?'Triac, ':'';
        $value.= ($this->product_dim_010v)?'0-10v, ':'';
        $value.= ($this->product_dim_dali)?'DALI, ':'';

        $value = substr(trim($value), 0, -1);


        $arr = [];
        $arr['label'] = "Dim Options";
        $arr['value'] = $value;
        $tds['product_dim_options'] = $arr;
        ////////////////////////////////////////////////

        // power
        ////////////////////////////////////////////////
        $value = ''; $min = ''; $max = '';
        if($this->models()->orderBy('model_power')->first())
        $min = $this->models()->orderBy('model_power')->first()->model_power;
        else
        $min = '0';

        if($this->models()->orderBy('model_power','desc')->first())
        $max = $this->models()->orderBy('model_power','desc')->first()->model_power;
        else
        $max = '0';

        if($max == $min){

            // $value = (is_int($min))?number_format((int)$min,0):number_format((float)$min,1);
            
            // $valArr = explode('.',$min,2);
            // $value = $valArr[0].'.'.$valArr[1];

            
            // $value = rtrim((float)$min, '0');
            $value = (float)$min;
        }
        else if($max)
        $value = (float)$min."W - ".(float)$max.'W';
        else
        $value = "0";

        $arr = [];
        $arr['label'] = "Power (W)";
        $arr['value'] = $value;
        $tds['product_power'] = $arr;
        ////////////////////////////////////////////////

        // product_weight
        ////////////////////////////////////////////////
        $value = ''; $min = ''; $max = '';
        if($this->models()->orderBy('model_weight')->first())
        $min = $this->models()->orderBy('model_weight')->first()->model_weight;
        else
        $min = '0';

        if($this->models()->orderBy('model_weight','desc')->first())
        $max = $this->models()->orderBy('model_weight','desc')->first()->model_weight;
        else
        $max = '0';

        if($max == $min) {
            if((int)$min)
            $value = (float)$min.'kg';
            // $value = number_format((int)$min,0).'kg';
        } else if($min=='0' || $max=='0') {
            if($min==0)
            $value = (float)$max.'kg';
            // $value = number_format((int)$max,0).'kg';
            else 
            $value = (float)$min.'kg';
            // $value = number_format((int)$min,0).'kg';
        }
        else if($max) {
            $value = (float)$min."kg - ".(float)$max.'kg';
            // $value = number_format((int)$min,0,'.','')."kg - ".number_format((int)$max,0,'.','').'kg';
        }

        $arr = [];
        $arr['label'] = "Weight (kg)";
        $arr['value'] = $value;
        $tds['product_weight'] = $arr;
        ////////////////////////////////////////////////

        // product_lifespan
        ////////////////////////////////////////////////
        $value = $this->product_lifespan;
        if($value) {
            $arr = [];
            $arr['label'] = "Lifespan (h)";
            $arr['value'] = number_format((int)$value, '0', '.', ',');
            $tds['product_lifespan'] = $arr;
        }
        ////////////////////////////////////////////////

        // lumen
        ////////////////////////////////////////////////
        $value = ''; $min = ''; $max = '';
        if($this->models()->orderByRaw('CAST(model_lumen AS UNSIGNED)')->first())
        $min = $this->models()->orderByRaw('CAST(model_lumen AS UNSIGNED)')->first()->model_lumen;
        else
        $min = '0';

        if($this->models()->orderByRaw('CAST(model_lumen AS UNSIGNED) desc')->first())
        $max = $this->models()->orderByRaw('CAST(model_lumen AS UNSIGNED) desc')->first()->model_lumen;
        else 
        $max = '0';

        if($max || $min) {
            if($min==0)
            $value = (float)$max.'lm';
            else if($max==0)
            $value = (float)$min.'lm';
            else
            $value = number_format((int)$min,0,'.','')."lm - ".number_format((int)$max,0,'.','').'lm';
        }
        else
        $value = "0";

        $arr = [];
        $arr['label'] = "Delivered Lumen (lm)";
        $arr['value'] = $value;
        $tds['product_lumen'] = $arr;
        ////////////////////////////////////////////////

        // dim_lumen
        ////////////////////////////////////////////////
        $value = ''; $min = ''; $max = '';
        if($this->models()->orderByRaw('CAST(model_lumen AS UNSIGNED)')->first())
        $min = $this->models()->orderByRaw('CAST(model_lumen AS UNSIGNED)')->first()->model_dim_lumen;
        else
        $min = '0';

        if($this->models()->orderByRaw('CAST(model_lumen AS UNSIGNED) desc')->first())
        $max = $this->models()->orderByRaw('CAST(model_lumen AS UNSIGNED) desc')->first()->model_dim_lumen;
        else
        $max = '0';


        if($max || $min) {
            if($min==0)
            $value = (float)$max.'lm';
            else if($max==0)
            $value = (float)$min.'lm';
            else
            $value = number_format((int)$min,0,'.','')."lm - ".number_format((int)$max,0,'.','').'lm';
        }
        else
        $value = "0";

        $arr = [];
        $arr['label'] = "Dim Lumen (lm)";
        $arr['value'] = $value;
        $tds['product_dim_lumen'] = $arr;
        ////////////////////////////////////////////////

        // product_cri
        ////////////////////////////////////////////////
        $value = ''; $min = ''; $max = '';
        if($this->models()->orderBy('model_cri')->first())
        $min = $this->models()->orderBy('model_cri')->first()->model_cri;
        else
        $min = '0';

        if($this->models()->orderBy('model_cri','desc')->first())
        $max = $this->models()->orderBy('model_cri','desc')->first()->model_cri;
        else
        $max = '0';

        if($max)
        $value = number_format((int)$min,0)." - ".number_format((int)$max,0);
        else {
            $value = $this->product_cri;
            preg_match('/([>=]+)([0-9]+)/', $value, $pieces);
            if(count($pieces)>1){
                $symbol = $pieces[1];
                switch($symbol){
                    case '=': $symbol = '>'; break;
                    case '>': $symbol = '>'; break;
                    case '>=': case '=>': $symbol = '&ge;'; break;
                }
                $cri = $pieces[2];
                $value = $symbol."".$cri;
            }
        }

        $arr = [];
        $arr['label'] = "CRI";
        $arr['value'] = $value;
        $tds['product_cri'] = $arr;
        ////////////////////////////////////////////////

        // lamp_holders
        ////////////////////////////////////////////////
        $lamp_holders = $this->models()
                                ->select('model_lamp_holder')
                                ->where('model_lamp_holder','!=','N/A')
                                ->distinct()
                                ->get()
                                ->pluck('model_lamp_holder')
                                ->toArray();
        $value = implode(', ', $lamp_holders);

        $arr = [];
        $arr['label'] = "Lamp Holders";
        $arr['value'] = $value;
        $tds['lamp_holders'] = $arr;
        ////////////////////////////////////////////////



        // product_cct
        ////////////////////////////////////////////////

        ////////////////////////////
        $ccts_arr = $this->getProductCCTs();        
        $ccts_char = $ccts_arr['ccts_char'];
        $ccts_char_rgb = $ccts_arr['ccts_char_rgb'];
        $ccts_int = $ccts_arr['ccts_int'];
        ///////////////////////////

        if(count($ccts_int)) {          
            $ccts_int_min = $ccts_int[0];
            $ccts_int_max = $ccts_int[count($ccts_int)-1];
        } else {
            $ccts_int_min = 0;
            $ccts_int_max = 0;
        }


        $ccts_char = implode(', ', $ccts_char);
        $ccts_char_rgb = implode(', ', $ccts_char_rgb);

        $ccts_other = "";
        if($ccts_char)
        $ccts_other.= ", ".$ccts_char;
        if($ccts_char_rgb)
        $ccts_other.= ", ".$ccts_char_rgb;

        if($ccts_int_min || $ccts_int_max) {
            if($ccts_int_min==0)
            $value = (float)$ccts_int_max.'K';
            else if($ccts_int_max==0)
            $value = (float)$ccts_int_min.'K';
            else
            $value = number_format((int)$ccts_int_min,0,'.','')."K - ".number_format((int)$ccts_int_max,0,'.','').'K';
        }
        else
        $value = "";

        $arr = [];
        $arr['label'] = "CCT";
        $arr['value'] = trim($value.$ccts_other,',');
        $tds['product_cct'] = $arr;
        ////////////////////////////////////////////////


        // Horizontal & Vertical
        ////////////////////////////////////////////////
        $value = ($this->product_rotate_hor)?$this->product_rotate_hor:'0';
        $value = str_replace("+-", "&plusmn;", $value);        
        $arr = [];
        $arr['label'] = "Horizontal Rotation (&deg;)";
        $arr['value'] = $value;
        $tds['product_rotate_hor'] = $arr;

        $value = ($this->product_rotate_ver)?$this->product_rotate_ver:'0';
        $value = str_replace("+-", "&plusmn;", $value);        
        $arr = [];
        $arr['label'] = "Tilt (&deg;)";
        $arr['value'] = $value;
        $tds['product_rotate_ver'] = $arr;
        ////////////////////////////////////////////////




        // replace - to long dash
        foreach ($tds as $key => &$val) {
            $val = Util::changeDash($val);
        }


        // Sort Technical Data Summary
        //////////////////////////////////////////////////////////////
        $buff = [];

        $buff['product_voltage'] = $tds['product_voltage'];
        $buff['product_power'] = $tds['product_power'];
        $buff['product_lumen'] = $tds['product_lumen'];
        $buff['product_dim_lumen'] = $tds['product_dim_lumen'];
        $buff['product_lifespan'] = (isset($tds['product_lifespan']))?$tds['product_lifespan']:'';
        $buff['product_lmw_efficiency'] = $tds['product_lmw_efficiency'];
        $buff['product_beam_angle'] = $tds['product_beam_angle'];
        $buff['product_warranty'] = $tds['product_warranty'];
        $buff['product_dim_options'] = $tds['product_dim_options'];
        // $buff['product_application_areas'] = $tds['product_application_areas'];
        $buff['product_material'] = $tds['product_material'];
        $buff['product_color'] = $tds['product_color'];
        $buff['product_chip'] = $tds['product_chip'];
        $buff['product_led_driver'] = $tds['product_led_driver'];
        $buff['lamp_holders'] = $tds['lamp_holders']; 
        $buff['product_ip'] = $tds['product_ip'];
        $buff['product_cri'] = $tds['product_cri'];
        $buff['product_cct'] = $tds['product_cct'];
        $buff['product_ik'] = $tds['product_ik'];
        $buff['product_op_efficiency'] = $tds['product_op_efficiency'];
        $buff['product_weight'] = $tds['product_weight'];
        $buff['product_rotate_hor'] = $tds['product_rotate_hor'];
        $buff['product_rotate_ver'] = $tds['product_rotate_ver'];

        $tds = $buff;
        //////////////////////////////////////////////////////////////
        // Sort Technical Data Summary

        // remove empty values
        $buff = [];
        foreach ($tds as $tdsKey => $tdsVal) {
            $value = (isset($tdsVal['value']))?$tdsVal['value']:'';
            if($value) {
            // if(true) {
                $arr = [];
                $arr['label'] = $tdsVal['label'];
                $arr['value'] = $value;
                $buff[$tdsKey] = $arr;
            }
        }        

        return $buff;
    }



    public function imageFast(){
        $defaultImage = $this->defaultImage;
        $image_name = ($defaultImage)?$defaultImage->image_name:"0.jpg";
        return $image_name.Util::getVT();
    }

    public function image($size = 'main'){

        $defaultImage = $this->defaultImage;
        $image_name = ($defaultImage)?$defaultImage->image_name:"0.jpg";
        $retVal = "";

        // if($size == 'main')
        // $retVal = "assets/images/products/".$image_name."?t=".(Util::getTS());
        // else if($size == 'th')
        // $retVal = "assets/images/products/th/".$image_name."?t=".(Util::getTS());
        // else if($size == 'm')
        // $retVal = "assets/images/products/m/".$image_name."?t=".(Util::getTS());

        if($size == 'main')
        $retVal = "images/products/".$image_name;
        else if($size == 'th')
        $retVal = "images/products/th/".$image_name;
        else if($size == 'm')
        $retVal = "images/products/m/".$image_name;
        else if($size == 'l')
        $retVal = "images/products/l/".$image_name;
        else if($size == 'seo')
        $retVal = "images/seo/products/almani-lighting-".$image_name;

        // if(!file_exists(Util::assetBaseURL().$retVal))
        // $retVal = Util::assetBaseURL()."assets/images/products/".$image_name;

        // if(true)
        // if(Storage::exists($retVal))
        // if (1)
        if (Util::checkLocalFileExists($retVal) || Util::isLocalhost())
        $retVal = Storage::url($retVal);
        else
        $retVal = Util::noImg('product');
        // $retVal = Storage::url("images/produts/".$image_name);
        // $retVal = env('APP_URL')."/assets/images/products/l/".$image_name;

        return $retVal;
    }
    public function getImageEagerAttribute()
    {
        return $this->image();
    }
    public function getImageThEagerAttribute()
    {
        return $this->image('th');
    }



    public function mainImage() { 
        $image_name = ($this->defaultImage)?$this->defaultImage->image_name:"0.jpg";        
        $img_src = "assets/images/products";
        $image = $img_src."/".$image_name;
        return Util::assetBaseURL().$image.Util::getVT();
    }


    public function galleryImages($param="") { 
        $img_src = "assets/images/products";
        $img_th_src = "assets/images/products/th";
        
        $images = [];
        foreach ($this->images()->orderBy('image_sketch_order')->get() as $key => $val) {
            // do not include sketch image
            if($val->image_id != $this->product_default_sketch) {
                $arr = [];
                $arr['gallery_img']  = Util::assetBaseURL().$img_src."/".$val->image_name.Util::getVT();
                $arr['gallery_th_img']  = Util::assetBaseURL().$img_th_src."/".$val->image_name.Util::getVT();
                $images[] = $arr;
            }
        // dd($this->images()->orderBy('image_thumbnail')->get() );
        }


        if($this->defaultSketch) {
            $arr = [];
            $arr['gallery_img']  = Util::assetBaseURL().$img_src."/".$this->defaultSketch->image_name.Util::getVT();
            $arr['gallery_th_img']  = Util::assetBaseURL().$img_th_src."/".$this->defaultSketch->image_name.Util::getVT();
            $images[] = $arr;
        } else if($param!="no_sketches" && $param!="has_sketches") {
            $arr = [];
            $arr['gallery_img']  = Util::assetBaseURL().'assets/images/coming_soon_sketch.jpg'.Util::getVT();
            $arr['gallery_th_img']  = Util::assetBaseURL().'assets/images/coming_soon_sketch.jpg'.Util::getVT();
            $images[] = $arr;
        }

        return $images;

    }

    public function thumb($model) { 
        // $models = $this->models();
        $img_src = "assets/images/products";
        // $img_th_src = "assets/images/products/th";
        $thumb = "assets/images/products/models/" . trim($model) . ".jpg";

        $thumbnail = [];
        $thumbnail['gallery_img'] = '';

        $thumbn = $this->images()->where('image_thumbnail','LIKE','%'. $model. '%')->orderBy('image_thumbnail')->first();      
        // do not include sketch image
        	// dd($thumbn);

           if ($thumbn) {
             $thumbnail['gallery_img']  = Util::assetBaseURL().$img_src."/".$thumbn->image_name.Util::getVT();
           } else {
             $thumbnail['gallery_img']  = Util::assetBaseURL().$thumb.Util::getVT();
           }
           
        // dd($thumbnail);

        return $thumbnail;

    }


    // Edit product option
    public function epOptions() {
        $admin_baseurl = Util::manageBaseURL();

        $options = 
        '<a class="btn btn-sm btn-almani" 
            href="'.$admin_baseurl.'/manage/product-add.html?edit='.$this->product_id.'" 
            target="_blank" 
            style=" margin: 5px;" 
            title="Edit Product">          
          Edit
        </a>
        ';

        return $options;
    }

    public function getApplicationAreasIcons() {

        $areas = explode(',', $this->product_application_areas);

        $retVal = [];
        foreach ($areas as $value) {

            if($value) {
                $temp = [];
                $temp['title'] = "";
                $temp['icon'] = "";

                $value = trim($value);
                $temp['title'] = $value;

                $icon = ProductApplicationArea::where('app_title','=',$value)->first();
                if($icon) {
                    $temp['icon'] = $icon->app_image;
                    $temp['extension'] = $icon->app_extension;
                } else {
                    $temp['icon'] = $value;
                    $temp['extension'] = "";
                }


                $retVal[] = $temp;
            }

        }


        return $retVal;

    }

    public function getApplicationAreaforMobile($limit) {
        return ProductApplicationAreas::where('enabled','=','1')->inRandomOrder()->limit($limit);
    }




    public function slOptions() {

        $category = $this->category;
        $product = $this;

        // Share link
        ////////////////////////////////////////////////////////////////////
        $share_link = asset('catalog/'.$category->category_alias.'/'.$product->product_code);
        ////////////////////////////////////////////////////////////////////
        // Share link

        return $share_link;
    }











    public function getMetaDesc(){

        if ($this->product_desc != '') {
            $desc = strip_tags($this->product_desc);
            $desc = str_replace(array("\r\n", "\n"), ' ', $desc);
            $desc = preg_replace('/\s+/', ' ',$desc);
            $desc = trim($desc);
        } else {
            $desc = "";
        }

        return trim($desc);
    }



    public function getProductCCTs(){
        $ccts = $this->models()
                        ->select('model_cct')
                        ->orderByRaw('CAST(model_cct AS UNSIGNED)')
                        ->get()
                        ->pluck('model_cct')->toArray();
        $ccts = array_unique($ccts);

        // separate numbers and letters
        $ccts_int = [];
        $ccts_char = [];
        $ccts_char_rgb = [];
        foreach ($ccts as $cct) {
            if(intval($cct))
            $ccts_int[] = intval($cct);
            else if( strpos( $cct, 'RGB' ) !== false ) 
            $ccts_char_rgb[] = $cct;
            else
            $ccts_char[] = $cct;
        }

        natsort($ccts_int);
        natsort($ccts_char);
        natsort($ccts_char_rgb);

        return [
            'ccts_int' => $ccts_int,
            'ccts_char' => $ccts_char,
            'ccts_char_rgb' => $ccts_char_rgb,
        ];
    }











































    public function getDistinctModelAlpha(){
        $retVal = [];

        $distinct_models = $this->models()
                                ->where('model_active','=',1)  
                                ->orderByRaw('LENGTH(model) asc, model asc')
                                ->get()
                                ->pluck('model')
                                ->toArray();

        foreach ($distinct_models as $key => $distinct_model) {
            $expld = explode('-', $distinct_model);
            $expld_ln = count($expld);

            if($expld_ln) {
                $str = preg_replace('/\d/', '', $expld[$expld_ln-1]);
                if($str)
                $retVal[$str] = [];
            }
        }

        ksort($retVal);

        return array_keys($retVal);
    }







    public function getDistinctModel(){
        $retVal = [];

        $distinct_models = $this->models()
                                ->where('model_active','=',1)  
                                ->orderByRaw('LENGTH(model) asc, model asc')
                                ->get()
                                ->pluck('model')
                                ->toArray();

        foreach ($distinct_models as $key => $distinct_model) {
            $expld = explode('-', $distinct_model);
            $expld_ln = count($expld);

            if($expld_ln) {
                $str = preg_replace('/\D/', '', $expld[$expld_ln-1]);
                if($str != '' && $str != null)
                $retVal[$this->product_code.'-'.$str] = [];
            }
        }
                                

        return array_keys($retVal);
    }











    public function getModelsForProductPage() {

        // return $this->models()->orderBy('model_heading')->orderBy('model_order')->orderBy('model')->get();

        $models = [];

        foreach ($this->getDistinctModel() as $value) {

            $modelsOf = ProductModel::whereRaw("model REGEXP '".$value."[^0-9]'")
                                    ->orWhere('model','=',$value)
                                    ->get();

               $model = $modelsOf->first();


            if($model) {

                $model->model_group_code = $value;

                $model->model_codes = implode(' ', $modelsOf->pluck('model')->toArray());


                // get MAX effieciency 
                $model->model_lm_efficiency = null;

                $efficiency = ProductModel::select('model_lm_efficiency')
                                ->whereRaw("model REGEXP '".$value."[^0-9]'")
                                ->get();
               $model->model_lm_efficiency = $efficiency->max('model_lm_efficiency');
                // get MAX efficiency

               
               // revise for getting max lumen ..
               // ------------------------------------------------------------

                $model->lumen = null;
                
                $maxlumen = ProductModel::whereRaw("model REGEXP '".$value."[^0-9]'")
                                ->max('model_lumen');

                $model->lumen = ($maxlumen == null ? 'N/A' : $maxlumen . 'lm');       


               // ------------------------------------------------------------



                // get CCT Options
                ////////////////////////////////////////////////////////////////////////
                $model->cct_options = null;

                $ccts_arr = ProductModel::select('model_cct as cct')
                                            ->selectRaw('model_price / 2 as price')
                                            ->whereRaw("model REGEXP '".$value."[^0-9]'")
                                            ->orWhere('model','=',$value)
                                            ->get()
                                            ->toArray();                

                // dd($ccts_arr);
                $ret_ccts = [];
                $ccts_int = [];
                $ccts_char = [];
                $ccts_char_rgb = [];

                foreach ($ccts_arr as $cct) {
                    if(intval(str_replace('/', '', $cct['cct']))) 
                    $ccts_int[] = $cct;                    
                    else if( strpos( $cct['cct'], 'RGB' ) !== false ) 
                    $ccts_char_rgb[] = $cct;
                    else
                    $ccts_char[] = $cct;
                }
                
                array_multisort(array_column($ccts_int, 'cct'), SORT_NATURAL, $ccts_int);
                array_multisort(array_column($ccts_char, 'cct'), SORT_NATURAL, $ccts_char);
                array_multisort(array_column($ccts_char_rgb, 'cct'), SORT_NATURAL, $ccts_char_rgb);

                $ccts = [];
                $ccts = array_merge($ccts,$ccts_int);
                $ccts = array_merge($ccts,$ccts_char);
                $ccts = array_merge($ccts,$ccts_char_rgb);

                $model->cct_options = $ccts;

                ////////////////////////////////////////////////////////////////////////
                // get CCT Options

                $models[$value] = $model;
            }
        }

        return $models;



    }

        // get random application area then get 1 product base on application area
        public function getSimilarProds($limit = 12) {
            $products = [];
            $application_area = ProductApplicationArea::where('enabled','1')->inRandomOrder()->limit($limit)->get();
            foreach ($application_area as $value) {
                $app_text = $value->app_title;
                $product =  Product::WhereRaw('FIND_IN_SET( ?, product_application_areas)', $app_text)->where('camel',"!=",1)->where('product_active','=',1)->inRandomOrder()->first();
                if($product) {
                    array_push($products,$product);
                }
            }
            return $products;
        }

        // get random product based on the category
        public function getSuggestedProduct($cat_id) {
            $suggestedProd = Product::where('camel','1')->where('category_id', $cat_id)->inRandomOrder()->first();
            return $suggestedProd;
        }
      
        // get similar products base on category except product already show
        // for product page "might be interested"
        public function getSimilarProducts($category_id) {
            $products = Product::where('camel','-1')->where('category_id',$category_id)->where('product_active',1)->limit(6)->inRandomOrder()->get();
            return $products;
        }
    


        public function getSameProduct($product_id) {
            $sameProduct = Product::where('camel' , '1')->where('product_code', 'LIKE' , '%'.$product_id.'%')->get();
            return $sameProduct;
        }





}
