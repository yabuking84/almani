<?php

namespace App\Product\ProductModel;

use App\Util;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ProductModel extends Model
{
    protected $table = 'product_models';
    protected $primaryKey = 'model_id';

    protected $guarded = [];

    public function isActive()
    {
        return $this->model_active;
    }

    //////////////////////////////////////////////////////////
    public function getProductModels($get = 'active')
    {

        return $this->CheckStatus($get);
    }
    public function camel($get = 'active')
    {
        // if($get == 'active') {
        //     return $this->whereHas('product', function ($query) {
        //                         $query->where('product_active','=',1);
        //                     })->where('model_active','=',1);
        // } else if($get == 'all') {
        //     return $this->whereHas('product');
        // } else if($get == 'inactive') {
        //     return $this->whereHas('product', function ($query) {
        //                         $query->where('product_active','!=',1);
        //                     })->where('model_active','!=',1);

        // }

        return $this->CheckStatus($get);
    }
    //////////////////////////////////////////////////////////

    public function product()
    {
        return $this->belongsTo('App\Product\Product', 'product_id', 'product_id');
    }

    public function ips()
    {
        return $this->hasMany('App\Product\ProductModel\ProductModelIp', 'model_id', 'model_id');
    }

    public function invs()
    {
        return $this->hasMany('App\Product\ProductModel\ProductModelInv', 'model_id', 'model_id');
    }

    public function warranties()
    {
        return $this->hasMany('App\Product\ProductModel\ProductModelWarranty', 'model_id', 'model_id');
    }

    public function thumbnail()
    {

        $retVal = [];
        $retVal['href'] = '';
        $retVal['src'] = '';
        $retVal['lightbox'] = '';

        // thumbnails
        ///////////////////////////////////////////////////////////////////
        // $thumb = 'assets/images/products/models/th-'.trim($this->model).'.jpg';
        // $thumb_main = 'assets/images/products/models/'.trim($this->model).'.jpg';
        $thumb = 'images/products/models/th-' . trim($this->model) . '.jpg';
        $thumb_main = 'images/products/models/' . trim($this->model) . '.jpg';
        $lightbox = $this->model . '_' . rand();

        $retVal['test'] = $thumb_main;

        if (Util::checkLocalFileExists($thumb_main) || Util::isLocalhost()) {
            // $thumb = $thumb."?t=".Util::getTS();
            // $thumb_main = $thumb_main."?t=".Util::getTS();
            $thumb = $thumb;
            $thumb_main = $thumb_main;
        } else {
            $thumb = 'images/products/th/0.jpg';
            $thumb_main = 'images/products/0.jpg';
        }

        $retVal['href'] = Storage::url($thumb_main) . Util::getVT();
        $retVal['src'] = Storage::url($thumb) . Util::getVT();
        $retVal['crop'] = 'assets/'.($thumb_main); 

        $retVal['lightbox'] = $lightbox;
        ///////////////////////////////////////////////////////////////////
        // thumbnails

        // $this->thumbnail =  $retVal;
        return $retVal;
    }

    public function cct()
    {
        $retVal = $this->model_cct;

        return $retVal;
    }

    public function cri()
    {
        $value = $this->product->product_cri;
        preg_match('/([>=]+)([0-9]+)/', $value, $pieces);
        if (count($pieces) > 1) {
            $symbol = $pieces[1];
            switch ($symbol) {
                case '=':$symbol = '>';
                    break;
                case '>':$symbol = '>';
                    break;
                case '>=':case '=>':$symbol = '&ge;';
                    break;
            }
            $cri = $pieces[2];
            $value = "<span style=''>" . $symbol . "</span>" . $cri;
        }
        return $value;
    }

    public function lumen()
    {

        return $this->model_lumen . 'lm';
    }

    public function moq()
    {
        $moq = (($this->model_min_qty) ? $this->model_min_qty . " pcs" : 'NA');
        return $moq;
    }

    public function dimOptions()
    {

        // Dimmable
        ////////////////////////////////////////////////////////////////////
        $dim_options = [];
        $has_dim_triac = !$this->model_dim_triac ? false : true;
        $has_dim_010v = !$this->model_dim_010v ? false : true;
        $has_dim_dali = !$this->model_dim_dali ? false : true;
        $has_lumen = !$this->model_lumen ? false : true;

        // echo $row['model']." xxxx ";
        // echo $row['model_lumen']."-";
        // echo $row['model_dim_lumen']." = ";
        // echo $row['model_dim_triac']." . ";
        // echo $row['model_dim_010v']." . ";
        // echo $has_dim_option."";
        // echo "<br>";

        if ($has_dim_triac || $has_dim_010v || $has_dim_dali) {

            $has_3_dim_options = false;

            if ($this->model_dim_triac) {
                $arr = [];
                // $arr['enabled'] = $this->model_dim_triac;
                $arr['enabled'] = 1; // we set this to enable the button @ 8-10 -18
                $arr['price'] = $this->model_dim_triac_price;
                $arr['price_title'] = ($arr['price'] == 0) ? 'Price available upon request.' : "+" . $arr['price'] . " AED";
                $arr['title'] = "Triac Dim";
                $dim_options['dim_triac'] = $arr;
                $has_3_dim_options = true;
            }
            if ($this->model_dim_010v) {
                $arr = [];
                // $arr['enabled'] = $this->model_dim_010v;
                $arr['enabled'] = 1; // we set this to enable the button @ 8-10 -18
                $arr['price'] = $this->model_dim_010v_price;
                $arr['price_title'] = ($arr['price'] == 0) ? 'Price available upon request.' : "+" . $arr['price'] . " AED";
                $arr['title'] = "0-10v Dim";
                $dim_options['dim_010v'] = $arr;
                $has_3_dim_options = true;
            }
            if ($this->model_dim_dali) {
                $arr = [];
                // $arr['enabled'] = $this->model_dim_dali;
                $arr['enabled'] = 1; // we set this to enable the button @ 8-10 -18
                $arr['price'] = $this->model_dim_dali_price;
                $arr['price_title'] = ($arr['price'] == 0) ? 'Price available upon request.' : "+" . $arr['price'] . " AED";
                $arr['title'] = "DALI Dim";
                $dim_options['dim_dali'] = $arr;
                $has_3_dim_options = true;
            }

            // if there is no 3 dim options choose default model dim price
            if (!$has_3_dim_options) {
                $arr = [];
                $arr['enabled'] = 1;
                $arr['price'] = $this->model_dim_price;
                $arr['price_title'] = ($arr['price'] == 0) ? 'Price available upon request.' : "+" . $arr['price'] . " AED";
                $arr['title'] = '<i class="fa fa-lightbulb-o" style="margin-right: 3px;"></i> Dim.';
                $dim_options['dim_default'] = $arr;
            }

        }
        ////////////////////////////////////////////////////////////////////
        // Dimmable
        // dd($dim_options);
        return [
            'dim_options' => $dim_options,
            'has_dim_triac' => $has_dim_triac,
            'has_dim_010v' => $has_dim_010v,
            'has_dim_dali' => $has_dim_dali,
        ];

    }

    public function ipOptions()
    {

        // IP
        ///////////////////////////////////////////////////////////////////
        $ip_ratingArr = [];

        // Get Product IPs first and set them to 0
        $arr = explode('/', $this->product->product_ip);
        foreach ($arr as $val) {
            if ($val) {
                $arr = [];
                $arr['ip'] = $val;
                $arr['price'] = 0;
                $ip_ratingArr['IP' . $val] = $arr;
            }
        }

        // refer for price from product_model_ips.
        foreach ($this->ips as $key => $val) {
            if (isset($ip_ratingArr['IP' . $val->model_ip])) {
                $ip_ratingArr['IP' . $val->model_ip]['price'] = $val->model_ip_price;
            }
        }

        // if Model'S IP is not 0, remove other IPs
        if ($this->model_ip) {
            $temp = [];

            // Check if IP is in Product IPs
            // $ip = array_get($ip_ratingArr, 'IP'.$this->model_ip,false);
            // if($ip)
            // $temp['IP'.$this->model_ip] = $ip;

            // IP doesnt have to be in Product IPs
            $temp['IP' . $this->model_ip]["ip"] = $this->model_ip;
            $temp['IP' . $this->model_ip]["price"] = "0";

            // and add if has product_model_ips
            if ($this->ips->count()) {
                foreach ($this->ips as $key => $val) {
                    // Check if IP is in Product IPs
                    // $ip = array_get($ip_ratingArr, 'IP'.$val->model_ip,false);
                    // if($ip)
                    // $temp['IP'.$val->model_ip] = $ip;

                    // No checking if IP is in product_ip
                    $temp['IP' . $val->model_ip]['ip'] = $val->model_ip;
                    $temp['IP' . $val->model_ip]['price'] = $val->model_ip_price;
                }
            }

            // If IPs is not in Products IPs then leave it blank
            $ip_ratingArr = $temp;
        }

        $has_more_ips = (count($ip_ratingArr)) ? true : false;

        ///////////////////////////////////////////////////////////////////
        // IP

        return [
            'has_more_ips' => $has_more_ips,
            'ip_ratingArr' => $ip_ratingArr,
        ];

    }

    public function beamAngleOptions()
    {

        // Beam Angle
        ///////////////////////////////////////////////////////////////////
        $beamAngleArr = [];

        if ($this->product->product_beam_angle) {
            if (strpos($this->product->product_beam_angle, '/') !== false) {
                foreach (explode("/", $this->product->product_beam_angle) as $val) {
                    $arr = [];
                    $arr['beam_angle'] = $val;
                    $beamAngleArr[] = $arr;
                }
            } else {
                foreach (explode(",", $this->product->product_beam_angle) as $val) {
                    $arr = [];
                    $arr['beam_angle'] = $val;
                    $beamAngleArr[] = $arr;
                }
            }
        }
        ///////////////////////////////////////////////////////////////////
        // Beam Angle

        return $beamAngleArr;

    }

    public function wifiOptions()
    {
        // WIFI
        ////////////////////////////////////////////////////////////////////
        $wifi_tip = "";
        $wifi_price = 0;
        if ($this->model_wifi_price == 0 && !$this->model_wifi) {
            // $wifi_icon = 'na';
            // $wifi_tip = 'No Wifi Option';
            // $wifi_class = '';
            $has_wifi_option = false;
        } else {
            $has_wifi_option = true;
            if ($this->model_wifi_price > 0) {
                $wifi_tip = '+' . $this->model_wifi_price . ' AED';
                $wifi_price = $this->model_wifi_price;
            } else {
                $wifi_tip = 'Wifi Enabled - Price available on request';
            }
        }
        ////////////////////////////////////////////////////////////////////
        // WIFI

        return [
            'wifi_tip' => $wifi_tip,
            'has_wifi_option' => $has_wifi_option,
            'wifi_price' => $wifi_price,
        ];
    }

    public function priceOptions()
    {

        // Price
        ///////////////////////////////////////////////////////////////////
        $date_day = Util::d();
        $discount = Util::doDiscount($this->model, $date_day);
        $def_price = $this->model_price;
        $final_price = 0;

        $final_price = Util::currency($this->model_price - ($discount / 100 * $this->model_price));
        $price_to_display =
        '<span class="price_value" data-uae_value="' . $final_price . '" data-value="' . $final_price . '">' .
        number_format($final_price, 2, '.', '') .
            '</span> ' .
            '<span class="currency">AED</span>';

        ///////////////////////////////////////////////////////////////////
        // Price

        return [
            'price_to_display' => $price_to_display,
            'discount' => $discount,
            'final_price' => $final_price,
            'def_price' => $def_price,
        ];

    }

    public function invOptions()
    {
        return $this->invs()->count();
    }

    public function stockOptions()
    {

        // Collpase every level so that it will not be an array inside an array
        // $value = $this->with('invs.blocks.storages')->get();
        $value = $this->where('model_id', '=', $this->model_id)->with('invs.blocks.storages')->get();
        $value = $value->pluck('invs')->collapse();
        $value = $value->pluck('blocks')->collapse();
        $value = $value->pluck('storages')->collapse()->sum('inv_storage_quantity');

        return $value;

    }

    public function warrantyOptions()
    {

        $warranties = [];

        // Default Warranty
        ///////////////////////////////////////////////////////////////////
        $dfltWrntyArr = [];
        $dfltWrntyArr['warranty'] = $this->model_warranty;
        $dfltWrntyArr['title'] = number_format($this->model_price, 2, '.', '') . ' AED';
        $dfltWrntyArr['price'] = $this->model_price;
        $dfltWrntyArr['default'] = true;
        $warranties[$dfltWrntyArr['warranty']] = $dfltWrntyArr;
        ///////////////////////////////////////////////////////////////////
        // Default Warranty

        if ($dfltWrntyArr['warranty'] == 3) {

            // 2 year warranty
            //////////////////////////////////////
            $price = $dfltWrntyArr['price'] * 0.9;
            $arr = [];
            $arr['warranty'] = 2;
            $arr['title'] = number_format($price, 2, '.', '') . ' AED';
            $arr['price'] = $price;
            $arr['default'] = false;
            $warranties[$arr['warranty']] = $arr;
            //////////////////////////////////////

            // 1 year warranty
            //////////////////////////////////////
            $price = $dfltWrntyArr['price'] * 0.9 * 0.95;
            $arr = [];
            $arr['warranty'] = 1;
            $arr['title'] = number_format($price, 2, '.', '') . ' AED';
            $arr['price'] = $price;
            $arr['default'] = false;
            $warranties[$arr['warranty']] = $arr;
            //////////////////////////////////////
        }

        if ($dfltWrntyArr['warranty'] == 2) {

            // 3 year warranty
            //////////////////////////////////////
            $price = $dfltWrntyArr['price'] * 1.8;
            $arr = [];
            $arr['warranty'] = 3;
            $arr['title'] = number_format($price, 2, '.', '') . ' AED';
            $arr['price'] = $price;
            $arr['default'] = false;
            $warranties[$arr['warranty']] = $arr;
            //////////////////////////////////////

            // 1 year warranty
            //////////////////////////////////////
            $price = $dfltWrntyArr['price'] * 0.95;
            $arr = [];
            $arr['warranty'] = 1;
            $arr['title'] = number_format($price, 2, '.', '') . ' AED';
            $arr['price'] = $price;
            $arr['default'] = false;
            $warranties[$arr['warranty']] = $arr;
            //////////////////////////////////////
        }

        krsort($warranties);

        return [
            'warranties' => $warranties,
        ];
    }

    public function dsOptions()
    {
        // Datasheets
        ////////////////////////////////////////////////////////////////////
        $scan_model = substr($this->model, 0, -1);
        $scan_ds = 'assets/uploads/models/datasheets/' . $scan_model . '.pdf';

        if (Util::checkLocalFileExists($scan_ds)) {
            return [
                'file_link' => Util::assetBaseURL() . $scan_ds,
                'filename' => $scan_model . '.pdf',
            ];
        }
        ////////////////////////////////////////////////////////////////////
        // Datasheets
    }

    public function tfOptions()
    {
        // Technical Files
        ////////////////////////////////////////////////////////////////////
        $scan_model = $this->model;
        $scan_tf = 'assets/uploads/models/technical_files/' . $scan_model;

        $ext = "";
        if (Util::checkLocalFileExists($scan_tf . '.zip')) {
            $ext = "zip";
        } else
        if (Util::checkLocalFileExists($scan_tf . '.ZIP')) {
            $ext = "ZIP";
        } else
        if (Util::checkLocalFileExists($scan_tf . '.ies')) {
            $ext = "ies";
        } else
        if (Util::checkLocalFileExists($scan_tf . '.IES')) {
            $ext = "IES";
        }

        if ($ext != "") {
            return [
                'file_link' => Util::assetBaseURL() . $scan_tf . "." . $ext,
                'filename' => $scan_model . "." . $ext,
            ];

        }
        ////////////////////////////////////////////////////////////////////
        // Technical Files

    }

    public function slOptions()
    {

        $category = $this->product->category;
        $product = $this->product;

        // Share link
        ////////////////////////////////////////////////////////////////////
        $share_link = asset('catalog/' . $category->category_alias . '/' . $product->product_code . '/model/' . $this->model);
        ////////////////////////////////////////////////////////////////////
        // Share link

        return $share_link;
    }

    public function emOptions()
    {

        // edit model
        ////////////////////////////////////////////////////////////////////
        // $admin_baseurl = 'https://staging.almani.ae:2019/almanilighting/';
        $admin_baseurl = Util::assetBaseURL();

        $edit_model =
        '<a href="' . $admin_baseurl . 'manage/model-add.html?edit=' . $this->model_id . '" target="_blank" style="font-size:20px;color:red;display:inline-block;">' .
            '<span class="fa fa-edit"></span>' .
            '</a>&nbsp;';

        // $edit_model = '';
        // if($admin_id and $admindata['priv_img']){
        // }
        ////////////////////////////////////////////////////////////////////
        // edit model

        return $edit_model;

    }

    public function modelSize()
    {
        $size = $this->model_size;

        $cnt = count(explode("x", $size));

        if ($cnt == 2) {
              if( $this->product->product_size_unit == ''){
                $retVal =  "DXH:" . $size;
            } else{
                $retVal =  $this->product->product_size_unit.":" . $size;
            }
        } else if ($cnt == 3) {
            $retVal = "LxWxH: " . $size;
        } else {
            $retVal = "Size " . $size;
        }

        return $retVal;
    }

    /**
     * Scope a query to filter product-models of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCheckStatus($queryScope, $get)
    {

        if ($get == 'active') {
            return $queryScope->whereHas('product', function ($query) {
                $query->where('product_active', '=', 1);
            })->where('model_active', '=', 1);
        } else if ($get == 'all') {
            return $queryScope->whereHas('product');
        } else if ($get == 'inactive') {
            return $queryScope->whereHas('product', function ($query) {
                $query->where('product_active', '!=', 1);
            })->where('model_active', '!=', 1);

        }
    }

    /**
     * Scope a query to filter product-models of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilterModel($query, $fa)
    {

        // Create model query builder
        // QBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQB
        // QBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQB

        // if(isset($fa['category_id']) && $fa['category_id']!="") {
        //     $query->camel('active');
        //     $query->whereHas(
        //         'product.category',
        //         function ($query) use ($fa) {
        //             $query->where('category_id', '=', $fa['category_id']);
        //         }
        //     );
        // }
        // else if(isset($fa['product_id']) && $fa['product_id']!="") {
        //     $query->camel('all');
        //     $query->whereHas(
        //         'product',
        //         function ($query) use ($fa) {
        //             $query->where('product_id', '=', $fa['product_id']);
        //         }
        //     );
        // }
        // else if(isset($fa['model_code']) && $fa['model_code']!="") {
        //     $query->camel('all');
        //     $query->where('model','like','%'.$fa['model_code'].'%');
        // } else {
        //     $query->camel('active');
        // }



      

        // dd($fa['product']);

        if(isset($fa['product']['product_id']) && !empty($fa['product']['product_id'])) {
            $query->whereHas(
                'product',
                function ($query) use ($fa) {
                    $query->where('product_name', '=', (string) $fa['product']['product_id']);
                }
            );
            // dd();
            // dd($query->toSql());
        }

        if(isset($fa['product']['category_id']) && !empty($fa['product']['category_id'])) {
            $query->whereHas(
                'product',
                function ($query) use ($fa) {
                    $query->whereIn('category_id', $fa['product']['category_id']);
                }
            );
        }

        if(isset($fa['product']['application_area']) && !empty($fa['product']['application_area'])) {
            $query->whereHas(
                'product',
                function ($query) use ($fa) {
                    foreach($fa['product']['application_area'] as $app_text) {
                        // $query->whereRaw(FIND_IN_SET 'category_id', $fa['product']['category_id']);
                        // $query->whereRaw('FIND_IN_SET( ?, product_application_areas)', $fa['product']['category_id']);
                        $query->WhereRaw('FIND_IN_SET( ?, product_application_areas)', strtolower($app_text));
                        // $query->whereRaw("FIND_IN_SET( $app_text  ,product_application_areas)");
                    }
                }
            );

            // dd($query->toSql());
        }

        // dd($query->toSql());

        if (isset($fa['model_code']) && $fa['model_code'] != "") {
            // $query->CheckStatus('active');
            $query->where('model_active', '=', '1');
            $query->where('model', 'like', '%' . $fa['model_code'] . '%');
        } else {
            // $query->CheckStatus('active');
            $query->where('model_active', '=', '1');
        }

        // supplier_code
        ////////////////////////////////////
        if (isset($fa['supplier_code']) && $fa['supplier_code'] != "") {
            $query->where('model_supplier_code', 'like', '%' . $fa['supplier_code'] . '%');
        }
        ////////////////////////////////////

        // CCT / Kelvin
        ////////////////////////////////////
        $fld = 'cct';
        if (isset($fa[$fld]) && ($fa[$fld]['min'] || $fa[$fld]['max'])) {
            if ($fa[$fld]['min']) {
                $query->where('model_' . $fld . '_min', '>=', $fa[$fld]['min']);
            }

            if ($fa[$fld]['max']) {
                $query->where('model_' . $fld . '_max', '<=', $fa[$fld]['max']);
            }

        }
        ////////////////////////////////////

        // Lumen
        ////////////////////////////////////
        $fld = 'lumen';
        if (isset($fa[$fld]) && ($fa[$fld]['min'] || $fa[$fld]['max'])) {
            if ($fa[$fld]['min']) {
                $query->where('model_' . $fld . '_min', '>=', $fa[$fld]['min']);
            }

            if ($fa[$fld]['max']) {
                $query->where('model_' . $fld . '_max', '<=', $fa[$fld]['max']);
            }

        }
        ////////////////////////////////////

        // Power
        ////////////////////////////////////
        $fld = 'power';
        if (isset($fa[$fld]) && ($fa[$fld]['min'] || $fa[$fld]['max'])) {
            if ($fa[$fld]['min']) {
                $query->where('model_' . $fld, '>=', $fa[$fld]['min']);
            }

            if ($fa[$fld]['max']) {
                $query->where('model_' . $fld, '<=', $fa[$fld]['max']);
            }

        }
        ////////////////////////////////////

        // Brand
        ////////////////////////////////////
            // when both are active
            if ($fa['brand']['almani'] == '1' && $fa['brand']['camel'] == '1') {
                $query->whereHas(
                    'product',
                    function ($query) use ($fa) {
                        $query->where('camel', '<>', 0)->orderByRaw('RAND()');
                    }
                );
            // when camel is active 
            } else if ($fa['brand']['camel'] == '1') {
                $query->whereHas(
                    'product',
                    function ($query) use ($fa) {
                        $query->where('camel', '=', 1);
                    }
                );
            // when almani is active
            } else if ($fa['brand']['almani'] == '1') {
                $query->whereHas(
                    'product',
                    function ($query) use ($fa) {
                        $query->where('camel', '!=', 1);
                    }
                );
            // when all is active     
            } else if ($fa['brand']['all'] == '1') {
                $query->whereHas(
                    'product',
                    function ($query) use ($fa) {
                        $query->where('camel', '<>', 0)->orderByRaw('RAND()');
                    }
                );
            }
        // dd($query->toSql());


        // Price
        ////////////////////////////////////
        if (isset($fa['price']) && ($fa['price']['min'] || $fa['price']['max'])) {

            $price_min = 0;
            $price_max = 0;

            if ($fa['price']['currency']) {
                $price_min = (float) $fa['price']['min'] / Util::getRate($fa['price']['currency']);
                $price_max = (float) $fa['price']['max'] / Util::getRate($fa['price']['currency']);
            }

            if ($price_min) {
                $query->where('model_price', '>=', intval($price_min));
            }

            if ($price_max) {
                $query->where('model_price', '<=', intval($price_max));
            }

        }
        ////////////////////////////////////

        // Dim options
        ////////////////////////////////////
        if (isset($fa['dim_options'])) {
            if ($fa['dim_options']['dimm'] == '1') {

                $query->where((function ($query) {
                    $query->Where('model_dim_triac', '=', 1)
                        ->orWhere('model_dim_010v', '=', 1)
                        ->orWhere('model_dim_dali', '=', 1);
                }));
            } else {

                if ($fa['dim_options']['triac'] == '1') {
                    $query->where('model_dim_triac', '=', 1);
                }

                if ($fa['dim_options']['v0_10'] == '1') {
                    $query->where('model_dim_010v', '=', 1);
                }

                if ($fa['dim_options']['dali'] == '1') {
                    $query->where('model_dim_dali', '=', 1);
                }
            }
        }
        ////////////////////////////////////

        // Warranty
        ////////////////////////////////////
        // because warranty is not used here
        
        if (isset($fa['warranty']) && false) {
            $years = [];
            if ($fa['warranty']['year_1'] == '1') {
                $years[] = '1';
            }
            if ($fa['warranty']['year_2'] == '1') {
                $years[] = '2';
            }
            if ($fa['warranty']['year_3'] == '1') {
                $years[] = '3';
            }
            if ($fa['warranty']['year_4'] == '1') {
                $years[] = '4';
            }
            if ($fa['warranty']['year_5'] == '1') {
                $years[] = '5';
            }

            // print_r($years);

            // $query->whereIn('model_warranty',$years);
            // $query->WhereHas('warranties', function ($query) use ($years){
            //                                     $query->whereIn('model_warranty',$years);
            //                                 });
            if (count($years)) {
                $query->where(function ($query) use ($years) {
                    $query
                        ->where('model_active', '=', 1)
                        ->whereIn('model_warranty', $years)
                        ->orWhereHas('warranties', function ($query2) use ($years) {
                            $query2->whereIn('model_warranty', $years);
                        });
                });
            }
        }
        ////////////////////////////////////

        // Emergency
        ////////////////////////////////////
        if (isset($fa['emergency']) && $fa['emergency'] == '1') {
            $query->where('model_emergency', '=', 1);
        }
        ////////////////////////////////////

        // Wifi
        ////////////////////////////////////
        if (isset($fa['wifi']) && $fa['wifi'] == '1') {
            $query->where('model_wifi', '=', 1);
        }
        ////////////////////////////////////

        // IP Rating
        ////////////////////////////////////
        $fld = 'ip_rating';
        if (isset($fa[$fld]) && ($fa[$fld]['min'] || $fa[$fld]['max'])) {
            if ($fa[$fld]['min']) {
                $query = $query->where('model_ip', '>=', $fa[$fld]['min']);
            }

            if ($fa[$fld]['max']) {
                $query = $query->where('model_ip', '<=', $fa[$fld]['max']);
            }

            $query = $query->where('model_ip', '>', "0");

        }
        ////////////////////////////////////

        // Must be active
        $query = $query->where('model_active', '=', "1");

        // QBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQB
        // QBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQBQB
        // Create model query builder

        // dd($query->toSql());
        // dd($query);
        return $query;
    }

}
