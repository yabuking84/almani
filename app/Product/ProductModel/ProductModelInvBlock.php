<?php

namespace App\Product\ProductModel;

use Illuminate\Database\Eloquent\Model;

class ProductModelInvBlock extends Model
{
    protected $table = 'inv_blocks';
    protected $primaryKey = 'inv_block_id';

    protected $guarded = [];


    public function inv()
    {
        return $this->belongsTo('App\Product\ProductModel\ProductModelInv', 'inv_id', 'inv_id');
    }


    public function storages()
    {
        return $this->hasMany('App\Product\ProductModel\ProductModelInvStorage', 'inv_block_id', 'inv_block_id');
    }

}
