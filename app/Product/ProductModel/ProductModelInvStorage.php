<?php

namespace App\Product\ProductModel;

use Illuminate\Database\Eloquent\Model;

class ProductModelInvStorage extends Model
{
    protected $table = 'inv_storages';
    protected $primaryKey = 'inv_storage_id';

    protected $guarded = [];


    public function block()
    {
        return $this->belongsTo('App\Product\ProductModel\ProductModelInvBlock', 'inv_block_id', 'inv_block_id');
    }



}
