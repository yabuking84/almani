<?php

namespace App\Camel\Product\ProductModel;

use Illuminate\Database\Eloquent\Model;

class ProductModelDimension extends Model
{
    protected $table = 'product_model_dimensions';
    protected $primaryKey = 'model_dimension_id';

    protected $guarded = [];


    public function model()
    {
        return $this->belongsTo('App\Camel\Product\ProductModel\ProductModel', 'model_id', 'model_id');
    }


}
