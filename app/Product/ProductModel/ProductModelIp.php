<?php

namespace App\Product\ProductModel;

use Illuminate\Database\Eloquent\Model;

class ProductModelIp extends Model
{
    protected $table = 'product_model_ips';
    protected $primaryKey = 'model_ip_id';

    protected $guarded = [];


    public function model()
    {
        return $this->belongsTo('App\Product\ProductModel\ProductModel', 'model_id', 'model_id');
    }


}
