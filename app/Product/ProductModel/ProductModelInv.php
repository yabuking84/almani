<?php

namespace App\Product\ProductModel;

use Illuminate\Database\Eloquent\Model;

class ProductModelInv extends Model
{
    protected $table = 'inv';
    protected $primaryKey = 'inv_id';

    protected $guarded = [];


    public function model()
    {
        return $this->belongsTo('App\Product\ProductModel\ProductModel', 'model_id', 'model_id');
    }


    public function blocks()
    {
        return $this->hasMany('App\Product\ProductModel\ProductModelInvBlock', 'inv_id', 'inv_id');
    }

}
