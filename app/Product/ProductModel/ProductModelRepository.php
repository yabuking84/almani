<?php

namespace App\Product\ProductModel;

use App\Product\ProductModel\ProductModelInterface;
use App\Product\ProductModel\ProductModel;

use App\Util;

use Validator;
use Auth;

class ProductModelRepository implements ProductModelInterface
{
	/**
     * Get all products.
     *
     * @author Leo
     * @param  int
     * @return array
     */
    public function all($limit = null)
    {
        $ProductModel = new ProductModel;
        if ($limit != null) {
            $product_models = $ProductModel->camel()->with('product')->paginate($limit);
        } else {
            $product_models = $ProductModel->camel()->with('product')->get();
        }
        return $product_models;
    }

    /**
     * Get model data by model id.
     *
     * @author Leo
     * @param  int
     * @return array
     */
    public function find($id)
    {
        return ProductModel::where('model_id', (int) $id)->first();
    }

    /**
     * Get models data by model codes.
     *
     * @author Rene
     * @param  int
     * @return array
     */
    public function findManyByCode($codeArr)
    {
        return ProductModel::whereIn('model', $codeArr)->where('model_active',1)->get();
    }

    /**
     * Get a model data by model code.
     *
     * @author Rene
     * @param  int
     * @return array
     */
    public function findByCode($code)
    {
        return ProductModel::where('model', '=', $code)->where('model_active',1)->get()->first();
    }


    /**
     * Creates a new product. Image files
     * are being saved on each company folders
     * respectively.
     *
     * @author Leo
     * @param mixed
     * @return array
     */
    public function add($data)
    {
        $product_model = new ProductModel;
        $product_model->fill($data);

        if ($product_model->save()) {
            return $product_model;
        }
        return false;
    }

    /**
     * Edit existing product.
     *
     * @author Leo
     * @param mixed
     * @return mixed
     */
    public function edit($data)
    {
        $product_model = ProductModel::where('model_id', $data['id'])->first();

        $product_model->fill($data);

        if ($product_model->save()) {
            return $product_model;
        }
        return false;
    }

    /**
     * Delete existing product by hash ID.
     *
     * @author Leo
     * @param string
     * @return mixed
     */
    public function delete($id)
    {
        $product_model = ProductModel::where('model_id', $id)->get();

        if (count($product_model) > 0) {
            $product_model = $product_model->first();

            return $product_model->delete();
        }
        return false;
    }



    /**
     * Get model data by filters. commented because this feature is now in ProductModel
     *
     * @author Leonardo De Caprio
     * @param  ?
     * @return ?
     */
    public function findModelsWithFilters($filters_arr)
    {
        // $fa = $filters_arr;   
        // $ProductModel = new ProductModel;

        // ////////////////////////////////////

        // if(isset($fa['category_id']) && $fa['category_id']!="") {
        //     $ProductModel = $ProductModel->camel('active');        
        //     $ProductModel = 
        //     $ProductModel->whereHas(
        //         'product.category', 
        //         function ($query) use ($fa) {
        //             $query->where('category_id', '=', $fa['category_id']);
        //         }
        //     );
        // }
        // else if(isset($fa['product_id']) && $fa['product_id']!="") {
        //     $ProductModel = $ProductModel->camel('all');
        //     $ProductModel = 
        //     $ProductModel->whereHas(
        //         'product', 
        //         function ($query) use ($fa) {
        //             $query->where('product_id', '=', $fa['product_id']);
        //         }
        //     );
        // }
        // else if(isset($fa['model_code']) && $fa['model_code']!="") {
        //     $ProductModel = $ProductModel->camel('all');
        //     $ProductModel = 
        //     $ProductModel->where('model','like','%'.$fa['model_code'].'%');
        // } else {
        //     $ProductModel = $ProductModel->camel('active');            
        // }


        // // supplier_code
        // ////////////////////////////////////
        // if(isset($fa['supplier_code']) && $fa['supplier_code']!="") {
        //     $ProductModel = $ProductModel->where('model_supplier_code','like','%'.$fa['supplier_code'].'%');
        // }
        // ////////////////////////////////////

        // // CCT / Kelvin
        // ////////////////////////////////////
        // $fld = 'cct';
        // if(isset($fa[$fld]) && ($fa[$fld]['min'] || $fa[$fld]['max'])) {
        //     if($fa[$fld]['min']) 
        //     $ProductModel = $ProductModel->where('model_'.$fld.'_min','>=',$fa[$fld]['min']);
        //     if($fa[$fld]['max']) 
        //     $ProductModel = $ProductModel->where('model_'.$fld.'_max','<=',$fa[$fld]['max']);
        // }
        // ////////////////////////////////////


        // // Lumen
        // ////////////////////////////////////
        // $fld = 'lumen';
        // if(isset($fa[$fld]) && ($fa[$fld]['min'] || $fa[$fld]['max'])) {
        //     if($fa[$fld]['min']) $ProductModel = $ProductModel->where('model_'.$fld.'_min','>=',$fa[$fld]['min']);
        //     if($fa[$fld]['max']) $ProductModel = $ProductModel->where('model_'.$fld.'_max','<=',$fa[$fld]['max']);
        // }
        // ////////////////////////////////////

        // // Power
        // ////////////////////////////////////
        // $fld = 'power';
        // if(isset($fa[$fld]) && ($fa[$fld]['min'] || $fa[$fld]['max'])) {
        //     if($fa[$fld]['min']) $ProductModel = $ProductModel->where('model_'.$fld,'>=',$fa[$fld]['min']);
        //     if($fa[$fld]['max']) $ProductModel = $ProductModel->where('model_'.$fld,'<=',$fa[$fld]['max']);
        // }
        // ////////////////////////////////////


        // // Price
        // ////////////////////////////////////
        // if(isset($fa['price']) && ($fa['price']['min'] || $fa['price']['max'])) {
        //     $price_min = 0;
        //     $price_max = 0;
        //     if($fa['price']['currency']) {
        //         $price_min = $fa['price']['min']/Util::getRate($fa['price']['currency']);
        //         $price_max = $fa['price']['max']/Util::getRate($fa['price']['currency']);
        //     }

        //     if($price_min) $ProductModel = $ProductModel->where('model_price','>=',$price_min);
        //     if($price_max) $ProductModel = $ProductModel->where('model_price','<=',$price_max);
        // }
        // ////////////////////////////////////
        
        // // Dim options
        // ////////////////////////////////////
        // if(isset($fa['dim_options'])) {
        //     if ($fa['dim_options']['dimm'] == '1') {
        //         $ProductModel = 
        //         $ProductModel->where((function ($query) {
        //             $query->Where('model_dim_triac','=',1)
        //                 ->orWhere('model_dim_010v','=',1)
        //                 ->orWhere('model_dim_dali','=',1);
        //         }));
        //     } else {

        //         if ($fa['dim_options']['triac'] == '1') {
        //             $ProductModel = $ProductModel->where('model_dim_triac','=',1);
        //         }

        //         if ($fa['dim_options']['v0_10'] == '1') {
        //             $ProductModel = $ProductModel->where('model_dim_010v','=',1);
        //         }

        //         if ($fa['dim_options']['dali'] == '1') {
        //             $ProductModel = $ProductModel->where('model_dim_dali','=',1);
        //         }
        //     }
        // }
        // ////////////////////////////////////
       
        // // Warranty
        // ////////////////////////////////////
        // if(isset($fa['warranty'])) {
        //     $years = [];
        //     if ($fa['warranty']['year_1'] == '1') {
        //         $years[] = '1';
        //     }
        //     if ($fa['warranty']['year_2'] == '1') {
        //         $years[] = '2';
        //     }
        //     if ($fa['warranty']['year_3'] == '1') {
        //         $years[] = '3';
        //     }
        //     if ($fa['warranty']['year_4'] == '1') {
        //         $years[] = '4';
        //     }
        //     if ($fa['warranty']['year_5'] == '1') {
        //         $years[] = '5';
        //     }

        //     // print_r($years);

        //     // $ProductModel = $ProductModel->whereIn('model_warranty',$years);
        //     // $ProductModel = $ProductModel->WhereHas('warranties', function ($query) use ($years){
        //     //                                     $query->whereIn('model_warranty',$years);
        //     //                                 });
        //     if(count($years)) {                
        //         $ProductModel = $ProductModel->where(function($query)  use ($years) {
        //             $query
        //             ->where('model_active','=',1)
        //             ->whereIn('model_warranty',$years)
        //             ->orWhereHas('warranties', function ($query2) use ($years){
        //                 $query2->whereIn('model_warranty',$years);
        //             });
        //         });
        //     }
        // }
        // ////////////////////////////////////


        // // Emergency
        // ////////////////////////////////////
        // if (isset($fa['emergency']) && $fa['emergency'] == '1') {
        //     $ProductModel = $ProductModel->where('model_emergency','=',1);
        // }
        // ////////////////////////////////////

        // // Wifi
        // ////////////////////////////////////
        // if (isset($fa['wifi']) && $fa['wifi'] == '1') {
        //     $ProductModel = $ProductModel->where('model_wifi','=',1);
        // }
        // ////////////////////////////////////


        // // IP Rating
        // ////////////////////////////////////
        // $fld = 'ip_rating';
        // if(isset($fa[$fld]) && ($fa[$fld]['min'] || $fa[$fld]['max'])) {
        //     if($fa[$fld]['min']) $ProductModel = $ProductModel->where('model_ip','>=',$fa[$fld]['min']);
        //     if($fa[$fld]['max']) $ProductModel = $ProductModel->where('model_ip','<=',$fa[$fld]['max']);
        // }
        // ////////////////////////////////////





        // return $ProductModel->with('product.category');
    }    
}
