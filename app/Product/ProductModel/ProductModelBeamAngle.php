<?php

namespace App\Camel\Product\ProductModel;

use Illuminate\Database\Eloquent\Model;

class ProductModelBeamAngle extends Model
{
    protected $table = 'product_model_beam_angles';
    protected $primaryKey = 'model_beam_angle_id';

    protected $guarded = [];


    public function model()
    {
        return $this->belongsTo('App\Camel\Product\ProductModel\ProductModel', 'model_id', 'model_id');
    }


}
