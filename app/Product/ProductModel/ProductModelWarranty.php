<?php

namespace App\Product\ProductModel;

use Illuminate\Database\Eloquent\Model;

class ProductModelWarranty extends Model
{
    protected $table = 'product_model_warranties';
    protected $primaryKey = 'model_warranty_id';

    protected $guarded = [];


    public function model()
    {
        return $this->belongsTo('App\Product\ProductModel\ProductModel', 'model_id', 'model_id');
    }

}
