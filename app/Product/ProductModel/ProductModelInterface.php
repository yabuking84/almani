<?php

namespace App\Product\ProductModel;

interface ProductModelInterface
{
    public function all($limit);

    public function find($id);
    
    public function findModelsWithFilters($filters_arr);

    public function findManyByCode($codeArr);

    public function add($data);

    public function edit($data);

    public function delete($id);
}
