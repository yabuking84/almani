<?php

namespace App\Camel\Product;

use Illuminate\Database\Eloquent\Model;

class ProductFeature extends Model
{
    protected $table = 'product_features';
    protected $primaryKey = 'pf_id';

    protected $guarded = [];

    public function product()   
    {
        return $this->belongsTo('App\Camel\Product\Product', 'product_id', 'product_id');
    }

}