<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

use App\Product\Product;

class ProductApplicationArea extends Model {

    protected $table = 'product_applicationareas';
    protected $primaryKey = 'app_id';

    protected $guarded = [];

    public function getProducts($param = "almani") {

    	if($param=="almani") {
        	// return Product::where('product_application_areas', 'LIKE', '%'.$this->app_title.'%')        	
        	// 			->where('camel','<',1)
        	// 			->orderBy('product_name')->get();

			return Product::where('product_application_areas', 'LIKE', '%'.$this->app_title.'%')
                            ->where('product_active','=',1)
                            ->where('camel','<',1)
                            ->orderBy('product_name')
                            ->get();
        }
        else if($param=="camel") {
            return Product::where('product_application_areas', 'LIKE', '%'.$this->app_title.'%')
                            ->where('product_active','=',1)
                            ->where('camel','=',1)
                            ->orderBy('product_name')->get();
        }
        else if($param=="all") {
            return Product::where('product_application_areas', 'LIKE', '%'.$this->app_title.'%')
                            ->where('product_active','=',1)
            				->orderBy('product_name')->get();
    	}
    }

    public function getProductsNonClosure($param = "almani",$limit=16) {

    	if($param=="almani") {

        	// return Product::where('product_application_areas', 'LIKE', '%'.$this->app_title.'%')        	
        	// 			->where('camel','<',1)
        	// 			->orderBy('product_name')->get();

			return Product::where('product_application_areas', 'LIKE', '%'.$this->app_title.'%')
                            ->where('product_active','=',1)
            				->where('camel','<',1)
            				->orderBy('product_name');
    	}
    	else if($param=="camel") {
        	return Product::where('product_application_areas', 'LIKE', '%'.$this->app_title.'%')
                            ->where('product_active','=',1)
                            ->where('camel','=',1)
            				->orderBy('product_name');
    	}
    	else if($param=="all") {
        	return Product::where('product_application_areas', 'LIKE', '%'.$this->app_title.'%')
            				->orderBy('product_name');
    	}
    }


     public function getProductsNonClosuresortprice($param = "almani",$sort_type = 'ASC',$limit=16) {

        if($param=="almani") {
            
            // return Product::where('product_application_areas', 'LIKE', '%'.$this->app_title.'%')         
            //          ->where('camel','<',1)
            //          ->orderBy('product_name')->get();

            $test = Product::where('product_application_areas', 'LIKE', '%'.$this->app_title.'%')
                        ->join('product_models','products.product_id','=', 'product_models.product_id')
                        ->where('product_active','=',1)
                        ->where('camel','<',1)
                        ->groupBy('products.product_id')
                        ->orderBy('product_models.model_price', $sort_type);

                        return $test->toSql();

                // return $test;
        }
        else if($param=="camel") {
            $test = Product::where('product_application_areas', 'LIKE', '%'.$this->app_title.'%')
                            ->join('product_models','products.product_id','=', 'product_models.product_id')
                            ->where('product_active','=',1)
                            ->where('camel','=',1)
                            ->groupBy('products.product_id')
                             ->orderBy('product_models.model_price', $sort_type);


                            // ->orderBy('product_name');
        }

        else if($param=="all") {
            $test = Product::where('product_application_areas', 'LIKE', '%'.$this->app_title.'%')
                            // ->orderBy('product_name');
                    ->join('product_models','products.product_id','=', 'product_models.product_id')
                    ->orderBy('product_models.model_price', $sort_type)
                    ->groupBy('products.product_id');
                                            return $test->toSql();

        }

    }

    public function products() {    
        return $this->belongsToMany('App\Product\Product', 'applicationareas_products', 'app_id', 'product_id');
    }

}